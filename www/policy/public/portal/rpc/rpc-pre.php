<?php
    require_once('include/share/ssl/ssl.php');

    // MOVE

    function getRegisztraltIskolaAdat($OMKod) {
	$q = "SELECT nev,naploUrl,publicKey,OMKod FROM regisztracio WHERE OMKod='%s'";
	$v = array($OMKod);
	$r = db_query($q, array('modul'=>'portal','result'=>'record','values'=>$v));
	return $r;
    }


    /* Class: Interconnect AES */

    /* remote procedure call remote controller */
    try 
    {
	/* prototype:
		$RPC = new Interconnect();
		$RPC->setRemoteOMKod($initOMKod);
		$REQUEST = $RPC->processRequest();
		$RPC->processRequest();
	*/
	$REQUEST = $RPC->getRequest();
	$func = $REQUEST->func;
    }
    catch (Exception $e)
    {
	$func='';
	$DATA = array('error'=>$e->getMessage());	
    }
    // processing
    $DATA = array();
    if (isset($func) && $func!='') {
	switch ($func) {
	    case 'getVersion':
	    case 'ping':
		$DATA = $RPC->prepareReply(
		    array('func'=>'getVersion','response_revision'=>_MAYORREV,'pong')
		);
		$RPC->setResponse($DATA);
		break;
	    case 'checkRegistration':
		$otherPublicKey = getSslPublicKeyByOMKod($REQUEST->OMKOD);
		if ($otherPublicKey===false) $valid=0;
		elseif (str_replace("\n","",$REQUEST->publicKey)==str_replace("\n","",$otherPublicKey)) {
		    $valid=1;
		} else $valid=2;
		$DATA = $RPC->prepareReply(
		    array('func'=>$func,'valid'=>$valid)
		);
		$RPC->setResponse($DATA);
		break;
	    case 'getIskola':
		$iskolaAdat = getRegisztraltIskolaAdat($REQUEST->otherOMKOD);
		//$iskolaAdat = array('ez'=>$REQUEST->otherOMKOD); // demo
		$DATA = $RPC->prepareReply(
		    array('func'=>$func,'iskolaAdat'=>$iskolaAdat)
		);
		$RPC->setResponse($DATA);
		break;
	    case 'getRegistrationData':
//		$REGDATA = getRegistrationData($REQUEST->OMKOD);
		break;
	    case 'refreshRegistration':
		break;
	    default:		
		break;
	}
	
    }
?>
