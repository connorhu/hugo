<?php

    $_SESSION['alert'][] = 'page:insufficient_access';
    exit;    

    if (_RIGHTS_OK !== true) die();
    if (!__NAPLOADMIN) {
	$_SESSION['alert'][] = 'page:insufficient_access';
    } else {

	// ---- MySQL ---- //
	$lr = db_connect('naplo_intezmeny');

	// ---- LDAP ---- //
        // Kapcsolódás a szerverhez
        $ds = ldap_connect($AUTH[_POLICY]['ldap hostname']);
        if (!$ds) {
            $_SESSION['alert'][] = 'alert:ldap_connect_failure';
            return false;
        }

        // Csatlakozás a szerverhez
        $r  = ldap_bind($ds, _USERDN, _USERPASSWORD);
        if (!$r) {
            $_SESSION['alert'][] = 'message:ldap_bind_failure';
            return false;
        }

        // Van-e adott azonosítójú felhasználó?
        $filter = "(objectClass=posixAccount)";
        $justthese = array("studyid","cn");
        $sr = ldap_search($ds, "ou=tanar,".$AUTH[_POLICY]['ldap base dn'], $filter, $justthese);
        if (!$sr) {
	    echo ldap_error($ds);
	    ldap_close($ds);
	    die();
	}

	$info = ldap_get_entries($ds, $sr);
	for ($i = 0; $i < $info['count']; $i++) {
	    $ldapTanarDns[] = $info[$i]['dn'];
	    $tanarAdatByDn[$info[$i]['dn']] = $info[$i];
	    $tanarAdatByCn[kisbetus($info[$i]['cn'][0])] = $info[$i];
	}
//echo '<pre>'; var_dump($ldapTanarDns); echo '</pre>';
	// tanári adatok lekérdezése
	$q = "SELECT viseltNevElotag, viseltCsaladiNev, viseltUtonev, oId,
		REPLACE(REPLACE(dn,'illyes','mayor'),'vmg','mayor') AS dn FROM tanar";
        $ret = db_query($q, array('fv' => 'getOsztalyAzonositok', 'modul' => 'naplo_intezmeny', 'result' => 'indexed'), $lr);

	for ($i = 0; $i < count($ret); $i++) {

	    if ($ret[$i]['viseltNevElotag'] == '') 
		$ret[$i]['sn'] = $ret[$i]['viseltCsaladiNev'];
	    else
		$ret[$i]['sn'] = $ret[$i]['viseltNevElotag'].' '.$ret[$i]['viseltCsaladiNev'];
	    $ret[$i]['cn'] = $cn = $ret[$i]['sn'].' '.$ret[$i]['viseltUtonev'];
	    if (in_array($ret[$i]['dn'], $ldapTanarDns)) $ret[$i]['ldap'] = $tanarAdatByDn[$ret[$i]['dn']];
	    if ($ret[$i]['ldap'] == '') $ret[$i]['ldap'] == $tanarAdatByCn[kisbetus($cn)];

	}

	if ($action == 'addOId') {

	    for ($i = 0; $i < count($ret); $i++) {
		if (
		    $ret[$i]['dn'] != ''
		    && $ret[$i]['dn'] == $ret[$i]['ldap']['dn']
		    && $ret[$i]['oId'] != ''
		    && $ret[$i]['oId'] != $ret[$i]['ldap']['studyid'][0]
		) {
echo 'IGEN: '.$ret[$i]['cn'];
//		    var_dump($ret[$i]['ldap']['studyid']);
		    // add oId
		    $mod = array(
			'studyid' => array($ret[$i]['oId']),
			'sn' => array($ret[$i]['sn']),
			'givenname' => array($ret[$i]['viseltUtonev'])
		    );
		    $r = ldap_mod_replace($ds, $ret[$i]['dn'], $mod);
//		    var_dump($mod);
		    echo '<hr>';
		} else {
//		    echo 'NEM: ';
//		    var_dump($ret[$i]['oId']);
//		    var_dump($ret[$i]['dn']);
//		    var_dump($ret[$i]['ldap']['dn']);
//		    echo '<hr>';
		}
	    }

	}

	ldap_close($ds);
	db_close($lr);

/*	require_once('include/modules/naplo/share/osztaly.php');
	require_once('include/backend/ldap/session/createGroup.php');
	require_once('include/backend/ldap/session/createAccount.php');

	if (isset($_POST['osztalyId']) && $_POST['osztalyId'] != '') $osztalyId = $_POST['osztalyId'];

	if (isset($osztalyId)) {

	    $osztalyAdat = getOsztalyAdat($osztalyId, __TANEV, $lr);
	    $osztalyOu = $osztalyAdat['kezdoEvfolyam'].$osztalyAdat['jel'].$osztalyAdat['kezdoTanev'];
	    $osztalyTagok = getOsztalyAzonositok($osztalyId);
	    $ldapTagok = getLdapTagok($osztalyOu);
	    for ($i = 0; $i < count($osztalyTagok); $i++) {
		$osztalyTagok[$i]['accountOk'] = in_array($osztalyTagok[$i]['oId'], $ldapTagok['oIds']);
		$osztalyTagok[$i]['dn'] = $ldapTagok['oid2dn'][$osztalyTagok[$i]['oId']];
	    }
	}
	if ($action == 'createAzonosito') {

	    for ($i = 0; $i < count($osztalyTagok); $i++) {
		$Tagok[$osztalyTagok[$i]['oId']] = $osztalyTagok[$i];
	    }

	    if ($_POST['createOu']) createOsztalyOu($osztalyOu);
	    $oIds = $_POST['oId'];
	    $Account = $_POST['Account'];
	    $userPassword = $_POST['userPassword'];
	    if ($userPassword == '') $userPassword = '1212';
	    $category = "$osztalyOu,ou=diak";

	    for ($i = 0; $i < count($Account); $i++) {
		if ($Account[$i] != '') {
		    $oId = $oIds[$i];
		    $userCn = $Tagok[$oId]['viseltCsaladiNev'].' '.$Tagok[$oId]['viseltUtonev'];
		    $userAccount = $Account[$i];
		    ldapCreateAccount($userCn, $userAccount, $userPassword, $category, array('studyid' => $oId));
		}
	    }
	    // újraolvasás
	    $osztalyTagok = getOsztalyAzonositok($osztalyId);
	    $ldapTagok = getLdapTagok($osztalyOu);
	    for ($i = 0; $i < count($osztalyTagok); $i++) {
		$osztalyTagok[$i]['accountOk'] = in_array($osztalyTagok[$i]['oId'], $ldapTagok['oIds']);
	    }

	}
	$TOOL['osztalySelect'] = array('tipus'=>'cella','paramName' => 'osztalyId', 'post'=>array('igDt'));
	getToolParameters();
*/

    }
