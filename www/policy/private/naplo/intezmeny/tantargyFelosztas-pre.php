<?php

    if (_RIGHTS_OK !== true) die();
    if (__NAPLOADMIN) {
	require_once('include/modules/naplo/share/tanar.php');
	require_once('include/modules/naplo/share/munkakozosseg.php');
	require_once('include/modules/naplo/share/tankor.php');
	require_once('include/modules/naplo/share/osztaly.php');
	require_once('include/modules/naplo/share/targy.php');
        require_once('include/modules/naplo/share/szemeszter.php');
	require_once('include/modules/naplo/share/tankorModifier.php');
	require_once('include/modules/naplo/share/tankorBlokk.php');

	/* Input Variables */
	$mkId = readVariable($_POST['mkId'],'numeric unsigned');
	$tanarId = readVariable($_POST['tanarId'],'numeric unsigned');
	$osztalyId = readVariable($_POST['osztalyId'],'numeric unsigned');
	$tanev = readVariable($_POST['tanev'], 'numeric unsigned', __TANEV);

	/* Setting default data */
	if ($tanev != __TANEV) $TA = getTanevAdat($tanev);
	else $TA = $_TANEV;

	$ADAT['tanev'] = $tanev;

        $refDt = $TA['kezdesDt'];
	$igDt = $TA['zarasDt'];
        if (strtotime($igDt) < strtotime($refDt)) $ADAT['igDt'] = $igDt = $refDt;

	/* Action */
	if ($skin=='ajax') {
	  switch ($action) {
	    case 'tankorTanarFelvesz':
		$_tankorId = readVariable($_POST['tankorId'],'id');
		$_tanarId = readVariable($_POST['tanarId'],'id');
		$_JSON['action'] = $action;
		$_JSON['result'] = tankorTanarModosit($_tankorId, $_tanarId, array('tanev'=>$tanev));
		break;
	    case 'tankorTanarTorol':
		$_tankorId = readVariable($_POST['tankorId'],'id');
		$_tanarId = readVariable($_POST['tanarId'],'id');
		tankorTanarTorol($_tankorId,$_tanarId,array('tanev'=>$tanev));
		break;
	  }
	  $_JSON['html'] = array($_tankorId,$refDt,$igDt);
	  return;
	}

        // tankörök lekérdezése
	if (isset($mkId)) {
            $TANKOROK = getTankorByMkId($mkId, $tanev);
        } elseif (isset($osztalyId)) {
            $TANKOROK = getTankorByOsztalyId($osztalyId, $tanev, array('tanarral' => true));
        } elseif (isset($tanarId)) {
            $TANKOROK = getTankorByTanarId($tanarId, $tanev,
		array('csakId' => false, 'tolDt' => $refDt, 'igDt' => $igDt, 'tanarral' => true)
            );
	}

	// kiegészítő: tankorSzemeszter tábla és szemeszterek lekérdezése
	if (is_array($TANKOROK)) {

	    $tankorIds = array();
	    for ($i = 0; $i < count($TANKOROK); $i++) $tankorIds[] = $TANKOROK[$i]['tankorId'];
/*	    $tankorSzemeszterek = getTankorSzemeszterek($tankorIds);
	    $tankorSzemeszter = array();
	    foreach ($tankorSzemeszterek as $tankorId => $tankorAdat) {
		for ($i = 0; $i < count($tankorAdat); $i++) {
		    $tankorSzemeszter[$tankorId][$tankorAdat[$i]['tanev']][$tankorAdat[$i]['szemeszter']] = $tankorAdat[$i];
		}
	    }
	    $ADAT['tankorSzemeszter'] = $tankorSzemeszter;
	    $ADAT['szemeszterek'] = $Szemeszterek = getSzemeszterek_spec($tanev-1);
*/
	    $ADAT['tankorok'] = $TANKOROK;
    	    if (is_array($ADAT['tankorok'])) {
        	for($i=0; $i<count($ADAT['tankorok']); $i++) {
            	    $_tankorId=$ADAT['tankorok'][$i]['tankorId'];
            	    $ADAT['tankorTanarok'][$_tankorId] = getTankorTanarai($_tankorId);
        	}
    	    }
	    $ADAT['targyak'] = getTargyak(array('arraymap'=>array('targyId')));
	    if ($mkId!='') $ADAT['tanarok'] = getTanarok(array('mkId'=>$mkId));
	    else $ADAT['tanarok'] = getTanarok();

	    // ha meg van jelölve olyan tanár is, aki nincs ebben benne, akkor vajon mi lesz? ugye...

	} // van Tankorok


	// ToolBar
	$TOOL['tanevSelect'] = array('tipus'=>'cella','paramName' => 'tanev', 
	    'tervezett'=>true,
	    'post'=>array('mkId','targyId','tankorId')
	);
	$TOOL['munkakozossegSelect'] = array('tipus'=>'cella','paramName' => 'mkId', 'post'=>array('tanev'));
	$TOOL['tanarSelect'] = array('tipus'=>'sor','paramName'=>'tanarId', 'post'=>array('tanev'));
	$TOOL['osztalySelect'] = array('tipus'=>'sor','paramName'=>'osztalyId', 'post'=>array('tanev'));
	getToolParameters();

    } // NAPLOADMIN
?>
