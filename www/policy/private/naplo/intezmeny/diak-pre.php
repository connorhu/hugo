<?php

if (_RIGHTS_OK !== true) die();

if (!__NAPLOADMIN && !__VEZETOSEG && !__TITKARSAG && !__TANAR && !__DIAK) {
    $_SESSION['alert'][] = 'message:insufficient_access';
} else {

    require_once('include/share/net/upload.php');
    require_once('include/modules/naplo/share/file.php');
    require_once('include/modules/naplo/share/diak.php');
    require_once('include/modules/naplo/share/szulo.php');
    require_once('include/modules/naplo/share/intezmenyek.php');
    require_once('include/modules/naplo/share/zaradek.php');
    require_once('include/modules/naplo/share/tankor.php');
    require_once('include/modules/naplo/share/jegy.php');
    require_once('include/modules/naplo/share/osztaly.php');
    require_once('include/modules/naplo/share/kepzes.php');
    require_once('include/modules/naplo/share/szemeszter.php');
    require_once('include/modules/naplo/share/ora.php');
    require_once('include/modules/naplo/share/diakModifier.php');
    require_once('include/modules/naplo/share/tankorDiakModifier.php');
    require_once('include/modules/naplo/share/hianyzas.php');
    require_once('include/modules/naplo/share/hianyzasModifier.php');
    require_once('include/modules/naplo/share/jegyModifier.php');
    require_once('include/modules/naplo/share/osztalyModifier.php');

    $GLOBALS['ADAT']['tanev'] = $GLOBALS['tanev'] = readVariable($_POST['tanev'], 'numeric unsigned', defined('__TANEV')?__TANEV:null );
    if (__DIAK) {
        $GLOBALS['ADAT']['diakId'] = $GLOBALS['diakId'] = __USERDIAKID;
    }
    else {
        $GLOBALS['ADAT']['diakId'] = $GLOBALS['diakId'] = readVariable($_POST['diakId'],'numeric unsigned');
    }
    $GLOBALS['ADAT']['osztalyId'] = $GLOBALS['osztalyId'] = readVariable($_POST['osztalyId'],'numeric unsigned');

    $GLOBALS['ADAT']['szocialisHelyzet'] = getSetField('naplo_intezmeny', 'diak', 'szocialisHelyzet');
    $GLOBALS['ADAT']['penzugyiStatusz'] = getEnumField('naplo_intezmeny', 'diak', 'penzugyiStatusz');
    $GLOBALS['ADAT']['lakohelyiJellemzo'] = getEnumField('naplo_intezmeny', 'diak', 'lakohelyiJellemzo');
    $GLOBALS['ADAT']['torvenyesKepviselo'] = getSetField('naplo_intezmeny', 'diak', 'torvenyesKepviselo');
    $GLOBALS['ADAT']['fogyatekossag'] = getSetField('naplo_intezmeny', 'diak', 'fogyatekossag');
    $GLOBALS['ADAT']['kozteruletJelleg'] = getEnumField('naplo_intezmeny', 'diak', 'lakhelyKozteruletJelleg');
    $GLOBALS['ADAT']['statusz'] = getEnumField('naplo_intezmeny', 'diak', 'statusz');
    // EZ MI????
    $GLOBALS['ADAT']['zaradek'] = $ZaradekIndex['jogviszony megnyitás'];
    $GLOBALS['ADAT']['iktatoszam'] = readVariable($_POST['iktatoszam'], 'string' );
    //$ZaradekIndex['jogviszony']['megnyitas'|'változás'|'lezárás']

    $GLOBALS['ADAT']['bekerulesModja'] = array_keys($GLOBALS['ADAT']['zaradek']);
    array_push($GLOBALS['ADAT']['bekerulesModja'],'beiratkozásra vár','vendégtanuló');
    define('_MODOSITHAT',(__NAPLOADMIN || __TITKARSAG));
    //define('_KERELMEZHET',(__DIAK===true));
    define('_KERELMEZHET',false);
    if (isset($GLOBALS['diakId'])) {
        // A SET típusú attribútumok string reprezentációja
        if ($action == 'diakSzocialisAdatModositas') {
            $tk = $szH = $fgy = array(); // ilyenkor kell definiálni - legalább üresként - így lehet törölni
            if (isset($_POST['szocialisHelyzet'])) {
                $szH = readVariable($_POST['szocialisHelyzet'], 'enum', null, $GLOBALS['ADAT']['szocialisHelyzet']);
            }
            if (isset($_POST['fogyatekossag'])) {
                $fgy = readVariable($_POST['fogyatekossag'], 'enum', null, $GLOBALS['ADAT']['fogyatekossag']);
            }
            if (isset($_POST['torvenyesKepviselo'])) {
                $tk = readVariable($_POST['torvenyesKepviselo'], 'enum', null, $GLOBALS['ADAT']['torvenyesKepviselo']);
            }
            // A törlés miatt mindenképp kell legyen beállítva valami
            $_POST['szocialisHelyzet'] = is_array($szH) ? implode(',', $szH) : null;
            $_POST['fogyatekossag'] = is_array($fgy) ? implode(',', $fgy) : null;
            $_POST['torvenyesKepviselo'] = is_array($tk) ? implode(',', $tk) : null;
        }
        // diák adatainak lekérdezése
        $GLOBALS['ADAT']['diakAdat'] = getDiakAdatById($GLOBALS['diakId']); // csak a státusz miatt kell...
        // action
        if (_MODOSITHAT) {
            $LZI = array_values($ZaradekIndex['jogviszony lezárás']);
            $GLOBALS['ADAT']['jogviszonyLezarasZaradek'] = getZaradekokByIndexes($LZI);
            if ($action == 'jogviszonyValtas') {
                $GLOBALS['ADAT']['jogviszonyValtasDt'] = readVariable($_POST['jogviszonyValtasDt'], 'datetime');
                $GLOBALS['ADAT']['ujStatusz'] = readVariable($_POST['statusz'], 'enum', null, $GLOBALS['ADAT']['statusz']);
                if ($GLOBALS['ADAT']['ujStatusz'] == 'jogviszonya felfüggesztve') {
                    $GLOBALS['ADAT']['felfuggesztesOk'] = readVariable($_POST['felfuggesztesOk'], 'string');
                    $GLOBALS['ADAT']['felfuggesztesIgDt'] = readVariable($_POST['felfuggesztesIgDt'], 'string');
                } elseif ($GLOBALS['ADAT']['ujStatusz'] == 'jogviszonya lezárva') {
                    /* A lezárás záradékolása sokféle lehet, userinterakció */
                    $GLOBALS['ADAT']['lezarasZaradekIndex'] = readVariable($_POST['lezarasZaradekIndex'], 'numeric unsigned', null, $LZI);
                    $GLOBALS['ADAT']['lezarasIgazolatlanOrakSzama'] = readVariable($_POST['lezarasIgazolatlanOrakSzama'], 'numeric unsigned');
                    $GLOBALS['ADAT']['lezarasIskola'] = readVariable($_POST['lezarasIskola'], 'string');
                }
                diakJogviszonyValtas($GLOBALS['ADAT']);
            } elseif ( $action == 'diakAlapadatModositas'
                || $action == 'diakSzuletesiAdatModositas'
                    || $action == 'diakCimModositas'
                        || $action == 'diakElerhetoseg'
                            || $action == 'diakElerhetosegModositas'
                                || $action == 'diakTanulmanyiAdatModositas'
                                    || $action == 'diakSzocialisAdatModositas'
            ) {
                $GLOBALS['_JSON']['result'] = diakAdatModositas($_POST);
            } elseif ($action== 'diakHozottHianyzas') {
                diakHozottHianyzas($_POST);
            } elseif ($action== 'diakTorol' && $GLOBALS['ADAT']['diakAdat']['statusz'] == 'felvételt nyert' ) { // csak a felvételt nyert
                if (diakTorol($GLOBALS['ADAT']['diakAdat'])) { // csak a felvételt nyert
                    unset($GLOBALS['ADAT']['diakAdat']);
                    unset($GLOBALS['diakId']);
                }
            } elseif ($action == 'diakKepUpload') {
                // --TODO könyvtár létrehozás?
                mayorFileUpload(array('subdir'=>_DOWNLOADDIR.'/private/naplo/face/'.__TANEV,'filename'=>$GLOBALS['diakId'].'.jpg'));
            }
        }
    } else {
        //$GLOBALS['ADAT']['zaradek'] = array('felvétel' => 1,'átvétel' => 2, 'áthelyezés' => 3, 'beiratkozásra vár' => null);
        if (isset($GLOBALS['osztalyId'])) {
            $GLOBALS['ADAT']['osztaly'] = getOsztalyAdat($GLOBALS['osztalyId'], $GLOBALS['tanev']);
            $GLOBALS['ADAT']['zaradek']['felvétel'] = $ZaradekIndex['jogviszony']['megnyitás']['felvétel osztályba']; // 1 helyett --> 67  ???
        }
    } // van diakId / nincs diakId


    if (_MODOSITHAT===true && $action == 'ujDiak') {
        $kotelezoParamOk = (isset($_POST['viseltCsaladinev']) && $_POST['viseltCsaladinev'] != '');
        $kotelezoParamOk &= (isset($_POST['kezdoTanev']) && $_POST['kezdoTanev'] != '');
        $kotelezoParamOk &= (isset($_POST['kezdoSzemeszter']) && $_POST['kezdoSzemeszter'] != '');
        $kotelezoParamOk &= ($_POST['felvetelTipus']=='beiratkozásra vár' || (isset($_POST['jogviszonyKezdete']) && $_POST['jogviszonyKezdete'] != ''));
        if ($kotelezoParamOk) {
            $_POST['zaradek'] = $GLOBALS['ADAT']['zaradek']; // felülírjuk a post-ot... remek
            $_POST['osztaly'] = $GLOBALS['ADAT']['osztaly'];
            $GLOBALS['diakId'] = ujDiak($_POST);
        } else {
            $_SESSION['alert'][] = 'message:empty_field:(viseltCsaladinev,kezdoTanev,kezdoSzemeszter,jogviszonyKezdete)';
        }
    }

    if (isset($GLOBALS['diakId'])) {
        // diák adatainak lekérdezése
        $GLOBALS['Szulok'] = getSzulok();
        $GLOBALS['ADAT']['diakAdat'] = getDiakAdatById($GLOBALS['diakId']);
        switch ($GLOBALS['ADAT']['diakAdat']['statusz']) {
            case 'felvételt nyert': 
            $GLOBALS['ADAT']['valthatoStatusz'] = array('jogviszonyban van');
            break;
            case 'jogviszonya lezárva':
            $GLOBALS['ADAT']['valthatoStatusz'] = array('jogviszonyban van', 'vendégtanuló');
            break;
            case 'vendégtanuló':
            $GLOBALS['ADAT']['valthatoStatusz'] = array('jogviszonya lezárva');
            break;
            default:
            $GLOBALS['ADAT']['valthatoStatusz'] = array_diff($GLOBALS['ADAT']['statusz'],array($GLOBALS['ADAT']['diakAdat']['statusz'],'felvételt nyert'));
            break;
        }
        $GLOBALS['ADAT']['diakAdat']['anyaNev'] = $GLOBALS['Szulok'][ $GLOBALS['ADAT']['diakAdat']['anyaId'] ]['szuleteskoriCsaladinev']?
        trim(implode(' ', array(
            $GLOBALS['Szulok'][ $GLOBALS['ADAT']['diakAdat']['anyaId'] ]['szuleteskoriNevElotag'],  
            $GLOBALS['Szulok'][ $GLOBALS['ADAT']['diakAdat']['anyaId'] ]['szuleteskoriCsaladinev'], 
            $GLOBALS['Szulok'][ $GLOBALS['ADAT']['diakAdat']['anyaId'] ]['szuleteskoriUtonev']
        ))):$GLOBALS['Szulok'][ $GLOBALS['ADAT']['diakAdat']['anyaId'] ]['szuloNev'];
        $GLOBALS['ADAT']['diakAdat']['apaNev'] = 
        trim(implode(' ', array(
            $GLOBALS['Szulok'][ $GLOBALS['ADAT']['diakAdat']['apaId'] ]['szuleteskoriNevElotag'],  
            $GLOBALS['Szulok'][ $GLOBALS['ADAT']['diakAdat']['apaId'] ]['szuleteskoriCsaladinev'], 
            $GLOBALS['Szulok'][ $GLOBALS['ADAT']['diakAdat']['apaId'] ]['szuleteskoriUtonev']
        ))) . ' - ' . $GLOBALS['Szulok'][ $GLOBALS['ADAT']['diakAdat']['apaId'] ]['szuloNev'];
        $GLOBALS['ADAT']['diakAdat']['gondviseloNev'] = $GLOBALS['Szulok'][ $GLOBALS['ADAT']['diakAdat']['gondviseloId'] ]['szuleteskoriCsaladinev']?
        trim(implode(' ', array(
            $GLOBALS['Szulok'][ $GLOBALS['ADAT']['diakAdat']['gondviseloId'] ]['szuleteskoriNevElotag'],  
            $GLOBALS['Szulok'][ $GLOBALS['ADAT']['diakAdat']['gondviseloId'] ]['szuleteskoriCsaladinev'], 
            $GLOBALS['Szulok'][ $GLOBALS['ADAT']['diakAdat']['gondviseloId'] ]['szuleteskoriUtonev']
        ))):$GLOBALS['Szulok'][ $GLOBALS['ADAT']['diakAdat']['gondviseloId'] ]['szuloNev'];

        $GLOBALS['ADAT']['diakAdat']['osztaly'] = getDiakOsztalya($GLOBALS['diakId'],array('tanev'=>$GLOBALS['tanev']));
        $GLOBALS['ADAT']['diakAdat']['mindenOsztaly'] = getDiakMindenOsztaly($GLOBALS['diakId']);
        $GLOBALS['ADAT']['diakJogviszony'] = getDiakJogviszony($GLOBALS['diakId']);
        $GLOBALS['ADAT']['hozottHianyzas'] = getDiakHozottHianyzas($GLOBALS['diakId'],array('tanev'=>$GLOBALS['tanev']));
        $GLOBALS['ADAT']['diakKepzes'] = getKepzesByDiakId($GLOBALS['diakId']);

    }


    $GLOBALS['ADAT']['osztalyok'] = getOsztalyok($GLOBALS['tanev'],array('result'=>'assoc', 'minden'=>true));

    // ToolBar
    $GLOBALS['TOOL']['tanevSelect'] = array('tipus' => 'cella', 'action' => 'tanevValasztas', 'post' => array('tanev','diakId'));
    if (!__DIAK) {
        $GLOBALS['TOOL']['osztalySelect'] = array('tipus' => 'cella', 'tanev' => $GLOBALS['tanev'], 'post' => array('tanev'));
        $GLOBALS['TOOL']['diakSelect'] = array('tipus'=>'cella', 'tanev'=>$GLOBALS['tanev'], 'osztalyId' => $GLOBALS['osztalyId'],
        'statusz' => $GLOBALS['ADAT']['statusz'],
        'post' => array('tanev','osztalyId')
    );
}
    if (isset($GLOBALS['osztalyId']) || isset($GLOBALS['diakId'])) {
        $GLOBALS['TOOL']['nyomtatasGomb'] = array('titleConst' => '_NYOMTATAS','tipus'=>'cella', 'url'=>'index.php?page=naplo&sub=nyomtatas&f=diakAdatlap','post' => array('osztalyId','diakId','tanev'));
        if (!__DIAK) {
            $GLOBALS['TOOL']['diakLapozo'] = array('tipus'=>'sor', 'tanev'=>$GLOBALS['tanev'], 'osztalyId' => $GLOBALS['osztalyId'],
                'statusz' => $GLOBALS['ADAT']['statusz'],
                'post' => array('tanev','osztalyId')
            );
        }
    }

    if (__NAPLOADMIN === true) {
        $GLOBALS['TOOL']['oldalFlipper'] = array('tipus' => 'cella',
            'url' => array('index.php?page=naplo&sub=intezmeny&f=diakStatusz'),
            'titleConst' => array('_DIAKSTATUSZ'),
            'post' => array('diakId'),
        );
    }
 
    getToolParameters();
} // naploadmin / vezetőség / titkárság / tanár
