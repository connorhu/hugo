<?php

    if (__INTEZMENY!='vmg') $_SESSION['alert'][] = 'page:under_construction';

    if (_RIGHTS_OK !== true) die();
    if (!__NAPLOADMIN) {
	$_SESSION['alert'][] = 'page:insufficient_access';
    } else {

	require_once('include/modules/naplo/share/kepzes.php');
	require_once('include/modules/naplo/share/osztaly.php');
	require_once('include/modules/naplo/share/targy.php');
	require_once('include/modules/naplo/share/tankor.php');
	require_once('include/modules/naplo/share/file.php');

	$tanev = __TANEV;
	$ADAT['kepzesId'] = $kepzesId = readVariable($_POST['kepzesId'], 'id', null);
	$ADAT['osztalyId'] = $osztalyId = readVariable($_POST['osztalyId'], 'id', null);

	$ADAT['kepzesOraterv.kovetelmeny'] = getEnumField('naplo_intezmeny','kepzesOraterv','kovetelmeny');
	$KOT = $ADAT['kepzesOraterv.tipus'] = getEnumField('naplo_intezmeny','kepzesOraterv','tipus');
	$ADAT['tankorTipusok'] = getTankorTipusok();


	function _escape($val,$A=null) {
	    global $KOT;
	    if (is_null($A) || !is_array($A)) $A = $KOT;
	    for ($i=0; $i<count($A); $i++) {
		if ($A[$i]==$val) { return 'E'.$i; }
	    }
	}
	function _unescape($val,$A=null) {
	    global $KOT;
	    if (!is_array($A)) $A = $KOT;
	    for ($i=0; $i<count($A); $i++) {
		if ($i==intval(substr($val,1))) { return $A[$i]; }
	    }
	}

/* 
	Ezt átadjuk a tankör oldalnak inkább

	if ($action == 'tankorFelvesz') {
	    $UJTANKOR['osztalyok'] = array($osztalyId);
	    $UJTANKOR['kepzesTipusId'] = readVariable($_POST['kepzesTipusId'],'id');
	    $UJTANKOR['targyId'] = readVariable($_POST['targyId'],'id');
	    $UJTANKOR['tipus'] = readVariable($_POST['tipus'],'string');
	    $UJTANKOR['kovetelmeny'] = readVariable($_POST['kovetelmeny'],'string');
	    $UJTANKOR['oraszam'] = readVariable($_POST['kovetelmeny'],'string');
	}
*/

	if (isset($kepzesId)) {
	    $ADAT['kepzesAdat'] = getKepzesAdatById($kepzesId);
//	    $ADAT['oraszam'] = getKepzesOraszam($kepzesId);
	    $ADAT['oraterv'] = getKepzesOraterv($kepzesId);
	    $ADAT['targyak'] = getTargyak(array('arraymap' => array('targyId')));
	}
	$ADAT['osztalyok'] = getOsztalyok($tanev, array('result' => 'assoc','minden'=>true));
	$ADAT['osztalyEvfolyam'] = getEvfolyam($osztalyId,$tanev);

	if (isset($osztalyId)) {
	    $ADAT['osztalyTankor'] = reindex(getTankorByOsztalyId($osztalyId,__TANEV,array('tanarral'=>true)),array('targyId'));
	}

        if (isset($kepzesId) && !is_numeric($ADAT['kepzesAdat']['kezdoEvfolyam']) && !is_numeric($ADAT['kepzesAdat']['zaroEvfolyam']))
            $_SESSION['alert'][] = '::nincs kezdő/záró évfolyam beállítva a képzéshez';

	for ($i=0; $i<count($ADAT['kepzesAdat']['osztalyIds']); $i++) {
	    $_osztalyId = $ADAT['kepzesAdat']['osztalyIds'][$i];
	    $TOOL_OSZTALYSELECT[] = $ADAT['osztalyok'][$_osztalyId];
	}


	$TOOL['kepzesSelect'] = array('tipus'=>'cella', 'post' => array());
	$TOOL['osztalySelect'] = array('tipus'=>'cella', 'post' => array('kepzesId'), 'osztalyok'=> $TOOL_OSZTALYSELECT);
        $TOOL['oldalFlipper'] = array('tipus' => 'cella', 'url' => array('index.php?page=naplo&sub=intezmeny&f=osztaly','index.php?page=naplo&sub=intezmeny&f=kepzes'),   
                                              'titleConst' => array('_OSZTALYHOZ','_KEPZESHEZ'), 'post' => array('kepzesId'),                                                   
                                                                                      'paramName'=>'kepzesId'); // paramName ?                
	getToolParameters();

    }
?>
