<?php

if (_RIGHTS_OK !== true) die();

if (is_array($GLOBALS['ADAT']['diakAdat']) && $skin!='ajax') {
    putBizonyitvanyTorzslap($GLOBALS['ADAT']);
    putDiakAdatForm($GLOBALS['ADAT'], $GLOBALS['Szulok'], $GLOBALS['tanev']);
    if (_MODOSITHAT===true) {
        putDiakJogviszonyValtozas($GLOBALS['ADAT']); //['diakAdat']
        if ($GLOBALS['ADAT']['diakAdat']['statusz']=='felvételt nyert') putDiakTorol($GLOBALS['ADAT']);
    }
    if (__NAPLOADMIN && __ALLOW_SULIX_REST===true) {
        putSuliXRESTForm($GLOBALS['ADAT']);
    }

} elseif (_MODOSITHAT) {
    // Új diák felvétele
    putUjDiak($GLOBALS['ADAT']);
}
if (_MODOSITHAT===true || _VEZETOSEG===true) {
    putDiakExportForm($GLOBALS['ADAT']);
}

