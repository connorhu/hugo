<?php

    if (_RIGHTS_OK !== true) die();

    $tanev = __TANEV;

        require_once('include/modules/naplo/share/tanar.php');
	require_once('include/modules/naplo/share/diak.php');
	require_once('include/modules/naplo/share/targy.php');
	require_once('include/modules/naplo/share/munkakozosseg.php');
	require_once('include/modules/naplo/share/tankor.php');
	require_once('include/modules/naplo/share/osztaly.php');
	require_once('include/modules/naplo/share/kepzes.php');
	require_once('include/modules/naplo/share/orarend.php');
	require_once('include/share/date/names.php');
        require_once('include/modules/naplo/share/terem.php');
        require_once('include/modules/naplo/share/ora.php');
        require_once('include/modules/naplo/share/intezmenyek.php');
	     
    $targyId = readVariable($_POST['targyId'], 'id');
    $tankorId = readVariable($_POST['tankorId'], 'id');
    $osztalyId = readVariable($_POST['osztalyId'], 'id');

    $tanarId = readVariable($_POST['tanarId'], 'id');
    $diakId = readVariable($_POST['diakId'], 'id');
    $teremId = readVariable($_POST['teremId'], 'id');
    $mkId = readVariable($_POST['mkId'], 'id');
    $het = readVariable($_POST['het'], 'numeric unsigned');

    $ADAT['telephelyek'] = getTelephelyek();
    $telephelyIds = array();
    foreach ($ADAT['telephelyek'] as $tAdat) $telephelyIds[] = $tAdat['telephelyId'];
    $telephelyId = readVariable($_POST['telephelyId'], 'id', (count($ADAT['telephelyek'])>1?null:1), $telephelyIds);
    /* A telephelyet ki tudnánk találni a lekérdezett órák termeiből is... */

    $tolDt = readVariable($_POST['tolDt'], 'date', getTanitasihetHetfo(array('napszam'=>0)));
    $dt = readVariable($_POST['dt'], 'date'); // mutatni
	if ($mkId=='' && $tanarId=='' && $diakId=='' && $osztalyId=='' && $tankorId=='' && $teremId=='') { // ez itt mind isnotset
	    if (__DIAK && defined('__USERDIAKID')) $diakId=__USERDIAKID;
	    if (__TANAR && defined('__USERTANARID')) $tanarId=__USERTANARID;
	}
    /* ----------------------------------------- */
    if (_POLICY=='private' && $action == 'setPublic' && is_numeric($diakId) && ((__NAGYKORU===true && $diakId==__USERDIAKID) || (__NAGYKORU === false && $diakId==__SZULODIAKID))) {
	require_once('include/modules/naplo/share/diakModifier.php');	
	diakAdatkezelesModositas(array('diakId'=>$diakId,'kulcs'=>'publikusOrarend','ertek'=>'1'));
    }
    /* ----------------------------------------- */

    /* Az órarendihét kiválasztása */
    if (isset($dt)) $tolDt = date('Y-m-d', strtotime('last Monday', strtotime('+1 days', strtotime($dt) )));
        if (!isset($tolDt))
	    // A következő nap előtti hétfő
	    $tolDt = date('Y-m-d', strtotime('last Monday', strtotime('+1 days', time())));

/*
	if (strtotime($tolDt) > strtotime($_TANEV['zarasDt'])) $_tolDt = $_TANEV['zarasDt'];
	elseif (strtotime($tolDt) < strtotime($_TANEV['kezdesDt'])) $_tolDt = $_TANEV['kezdesDt'];
	// és akkor korrigáljunk még egyszer
        if (isset($_tolDt)) // A következő nap előtti hétfő
	    $tolDt = date('Y-m-d', strtotime('last Monday', strtotime('+1 days', time())));
*/
	if ($tolDt != '') $het = getOrarendiHetByDt($tolDt);		
	if ($het == '') $het = getLastOrarend();
	$igDt = date('Y-m-d', mktime(0,0,0,date('m',strtotime($tolDt)), date('d',strtotime($tolDt))+6, date('Y',strtotime($tolDt))));

    $ADAT['termek'] = getTermek(array('result'=>'assoc','telephelyId'=>$telephelyId));
    $ADAT['tanarok'] = getTanarok(array('result'=>'assoc','telephelyId'=>$telephelyId));                                 //--TODO telephely
// =====================
    if ($tankorId!='') {
	$ADAT['orarend'] = getOrarendByTankorId($tankorId, array('tolDt'=>$tolDt,'igDt'=>$igDt));
	//$ADAT['toPrint'] = getTankorNev($tankorId); // vagy getTankornev külön?
	$TANKOROK['haladasi'] = array($tankorId);
    } elseif($tanarId!='') {
//	if (_POLICY == 'public') $_SESSION['alert'][] = 'info:adatkezeles:letilva';
//	else {
    	    $ADAT['orarend'] = getOrarendByTanarId($tanarId,array('tolDt'=>$tolDt,'igDt'=>$igDt,'telephelyId'=>$telephelyId,'orarendiOraTankor'=>true));
	    $ADAT['toPrint'] = $ADAT['tanarok'][$tanarId]['tanarNev'];
//	}
    } elseif($diakId!='') {
	$ADAT['orarendTipus'] = 'diakOrarend';
	/* ide kerülhet, hogy a diák (__NAGYKORU)/szülő engedélyezte-e a saját/gyermeke órarendjének mutatását */
	$ADAT['adatKezeles'] = getDiakAdatkezeles($diakId,array('publikusOrarend'=>1));
	$ADAT['publikusOrarend'] = ($ADAT['adatKezeles']['publikusOrarend']['ertek'] == 1) ? true : false;
	/* Ha belül vagyunk, akkor állíthassa be egy gombnyomással, hogy ő bizony engedélyezi */
	define(__ALLOWSET, ((__NAGYKORU===true && $diakId==__USERDIAKID) || (__NAGYKORU === false && $diakId==__SZULODIAKID))); 
	if (_POLICY == 'public' && $ADAT['publikusOrarend'] === false) { 
	    $_SESSION['alert'][] = 'info:adatkezeles:'.'A keresési feltétel a felhasználó által belépés után a napló modulban engedélyezhető!';
	} else {
	    $ADAT['orarend'] = getOrarendByDiakId($diakId,array('tolDt'=>$tolDt,'igDt'=>$igDt,'osztalyId'=>$osztalyId)); // itt az osztalyId-t nem lenne kötelező megadni, de a felületen úgysem lehet máshogy idejutni
	    $TANKOROK['haladasi'] = getTankorByDiakId($diakId, __TANEV, array('csakId' => true, 'tolDt' => $tolDt, 'igDt' => $igDt, 'result'=>'idonly'));
	    $ADAT['diakFelmentes'] = getTankorDiakFelmentes($diakId, __TANEV, array('tolDt'=>$tolDt,'igDt'=>$igDt));
	}
	$ADAT['diakEvfolyamAdat'] = getEvfolyamAdatByDiakId($diakId,$tolDt,__TANEV);
	$ADAT['kepzes'] = getKepzesByDiakId($diakId, array('result' => '', 'dt' => $tolDt, 'arraymap' => null));
	if (count($ADAT['kepzes'])==1) {
	    $ADAT['kepzesOraterv'] = getOraszamByKepzes($ADAT['kepzes'][0]['kepzesId'], array('szemeszter'=>1,'evfolyam'=>$ADAT['diakEvfolyamAdat']['evfolyam']));
	} else {//nincs képzése, vagy több van
	}
    } elseif ($osztalyId!='') {
	$ADAT['orarendTipus'] = 'osztalyOrarend';
	$ADAT['orarend'] = getOrarendByOsztalyId($osztalyId,array('tolDt'=>$tolDt,'igDt'=>$igDt));
	$OADAT = getOsztalyAdat($osztalyId);
	$ADAT['toPrint'] = $OADAT['osztalyJel'];
	$TANKOROK['haladasi'] = getTankorByOsztalyId($osztalyId, __TANEV, array('csakId' => true, 'tanarral' => false, 'result' => 'idonly'));
    } elseif ($mkId!='') {
	$ADAT['orarend'] = getOrarendByMkId($mkId,array('tolDt'=>$tolDt,'igDt'=>$igDt,'telephelyId'=>$telephelyId));
	$TANKOROK['haladasi'] = getTankorByMkId($mkId, __TANEV, array('csakId' => true,'filter' => array()) );
    } elseif ($teremId!='') {
	$ADAT['orarend'] = getOrarendByTeremId($teremId,'',array('tolDt'=>$tolDt,'igDt'=>$igDt,'telephelyId'=>$telephelyId));
	$teremAdat = getTeremAdatById($teremId);
	$ADAT['toPrint'] = $teremAdat['leiras'];
    }
        else $ADAT = array();
// -----------
	$TANKOROK['erintett'] = $ADAT['orarend']['tankorok']; // --FIXME (2013.05.03)
	$TANKOROK['mindenByDt'] = $ADAT['orarend']['mindenTankorByDt']; // --FIXME (2013.05.03)

    	$ADAT['NAPOK'] = $_NAPOK = _genNapok($tolDt,$igDt);

	if (isset($tanarId)) {
	    $ADAT['haladasi'] = getTanarOrak($tanarId,array('tolDt'=>$tolDt,'igDt'=>$igDt,'result'=>'likeOrarend'));
	} elseif ((is_array($TANKOROK['mindenByDt']) && count($TANKOROK['mindenByDt']>0)) || $teremId!='') {
	    // akkor egészítsük ki a haladási naplós órákkal
	    /* FS#100 */
	    $ADAT['haladasi'] = array('tankorok'=>array());
    	    // dátumfüggő FS#100
    	    for ($i=0; $i<count($_NAPOK); $i++) {
        	$_dt = $_NAPOK[$i];
		$_TK = $TANKOROK['mindenByDt'][$_dt];
		if ($teremId!='') 
		    $_D = getOrakByTeremId($teremId,array('tolDt'=>$_dt,'igDt'=>$_dt,'result'=>'likeOrarend'));
		else 
		    $_D = getOrak($TANKOROK['haladasi'],array('tolDt'=>$_dt,'igDt'=>$_dt,'result'=>'likeOrarend'));
		$ADAT['haladasi']['orak'][$_dt] = $_D['orak'][$_dt];
		if (is_array($_D['tankorok'])) $ADAT['haladasi']['tankorok'] = array_unique(array_merge($_D['tankorok'],$ADAT['haladasi']['tankorok']));
	    }
	}

	/* FIX */
	// a haladási naplóban olyan tankörök is szerepelnek, amik eddig nem...
	// először gyűjtsük ki a szükséges tanköröket, és utána kérdezzük le az adataikat egyben
	// ...

	if (!is_array($TANKOROK['haladasi'])) $TANKOROK['haladasi'] = array();
	$TANKOROK = (is_array($ADAT['orarend']['tankorok']) && is_array($ADAT['haladasi']['tankorok'])) ?
	    array_unique(array_merge($ADAT['orarend']['tankorok'],$ADAT['haladasi']['tankorok'],$TANKOROK['haladasi']))
	    :
	    $ADAT['orarend']['tankorok'];
	$ADAT['tankorok'] = getTankorAdatByIds($TANKOROK);
	/* tankörlétszámok */
	if (is_array($ADAT['tankorok'])) foreach ($ADAT['tankorok'] as $_tankorId =>$_T) {
	    $ADAT['tankorLetszamok'][$_tankorId] = getTankorLetszam($_tankorId,array('refDt'=>$tolDt));
	}
	//--//
	$ADAT['dt'] = $dt; // show this... ha a skin ajax

	$ADAT['csengetesiRend'] = getCsengetesiRend();
	$ADAT['telephelyId'] = $telephelyId;
	$ADAT['napiMinOra'] = getMinOra();
	$ADAT['napiMaxOra'] = getMaxOra();
	$ADAT['hetiMaxNap'] = getMaxNap(array('haladasi'=>true,'tolDt'=>$tolDt,'igDt'=>$igDt));
	$ADAT['tankorTipus'] = getTankorTipusok();

	if ($skin=='ajax' && $_REQUEST['httpResponse']=='json') $_JSON['orarend']=$ADAT;

//=====================================

	/* TOOL ME :) */
        $TOOL['datumSelect'] = array(
            'tipus'=>'cella', 'post'=>array('tanarId','osztalyId','tankorId','mkId','diakId','telephelyId'),
	    'paramName' => 'tolDt', 'hanyNaponta' => 7,
	    'override'=>true, // használathoz még át kell írni pár függvényt!!!
//	    'tolDt' => date('Y-m-d', strtotime('Monday', strtotime($_TANEV['kezdesDt']))),
	    'tolDt' => date('Y-m-d', strtotime('last Monday', strtotime($_TANEV['kezdesDt']))),
	    'igDt' => $_TANEV['zarasDt'],
	);
	$TOOL['telephelySelect'] = array('tipus'=>'cella', 'paramName'=>'telephelyId', 'post'=>array('tolDt','mkId','tanarId'));

	//$TOOL['orarendiHetSelect'] = array('tipus'=>'cella' , 'paramName' => 'het', 'post'=>array('targyId','tankorId','osztalyId','tanarId'), 'disabled'=>true);
	if (($osztalyId!='' || $diakId!='') && _POLICY=='private') {
	    $TOOL['diakSelect'] = array('tipus'=>'sor','paramName'=>'diakId', 'post'=>array('tolDt','osztalyId','telephelyId'));
	} else 
	    $TOOL['munkakozossegSelect'] = array('tipus'=>'sor', 'paramName'=>'mkId', 'post'=>array('tolDt','telephelyId'));
	$TOOL['tanarSelect'] = array('tipus'=>'cella', 'paramName'=>'tanarId', 'post'=>array('tolDt','telephelyId'));
	$TOOL['osztalySelect']= array('tipus'=>'cella', 'paramName'=>'osztalyId', 'post'=>array('tolDt','telephelyId'));
	$TOOL['teremSelect'] = array('tipus'=>'cella', 'paramName'=>'teremId', 'telephelyId'=>$telephelyId, 'post'=>array('tolDt','telephelyId'));
        if ($osztalyId!='' || $tanarId!='' || $diakId!='' || $mkId!='') $TOOL['tankorSelect'] = array('tipus'=>'sor','paramName'=>'tankorId', 'tolDt'=>$tolDt, 'igDt'=>$igDt, 'post'=>array('tolDt','osztalyId','targyId','tanarId','diakId','telephelyId'));
	getToolParameters();

?>
