<?php

if (_RIGHTS_OK !== true) die();
if (__NAPLOADMIN !== true) {

    $_SESSION['alert'][] = 'page:insufficient_access';

} else {

    require_once('include/modules/naplo/share/osztaly.php');
    require_once('include/modules/naplo/share/nap.php');
    require_once('include/modules/naplo/share/osztalyModifier.php');
    require_once('include/modules/naplo/share/diak.php');
    require_once('include/modules/naplo/share/diakModifier.php');
    require_once('include/modules/naplo/share/tankorDiakModifier.php');
    require_once('include/modules/naplo/share/hianyzasModifier.php');
    require_once('include/modules/naplo/share/jegyModifier.php');
    require_once('include/modules/naplo/share/tankor.php');
    require_once('include/modules/naplo/share/zaradek.php');
    require_once('include/modules/naplo/share/szemeszter.php');
    require_once('include/modules/naplo/share/intezmenyek.php');
    require_once('include/modules/naplo/share/hianyzas.php');
    require_once('include/modules/naplo/share/mysql.php');

    $Tanevek = getTanevek($tervezett = true);
    $Intezmenyek = getIntezmenyek();
    $IntezmenyRoividNevek = array();
    for ($i = 0; $i < count($Intezmenyek); $i++) $IntezmenyRovidNevek[] = $Intezmenyek[$i]['rovidNev'];
    $intezmeny = readVariable($_POST['intezmeny'], 'strictstring', defined('__INTEZMENY') ? __INTEZMENY : null, $IntezmenyRovidNevek);

    $GLOBALS['tanev'] = readVariable($_POST['tanev'], 'numeric unsigned', null, $Tanevek);
    if (!isset($GLOBALS['tanev']) && defined('__TANEV')) $GLOBALS['tanev'] = __TANEV;
    if (isset($GLOBALS['tanev'])) {
        $GLOBALS['ADAT']['tanev'] = $GLOBALS['tanev'];
        $GLOBALS['ADAT']['tanevAdat'] = getTanevAdat($GLOBALS['tanev']);
        if ($GLOBALS['ADAT']['tanevAdat']['statusz'] == 'aktív') {
            $Osztalyok = getOsztalyok($GLOBALS['tanev']);
            $GLOBALS['ADAT']['vegzoOsztalyok'] = array();
            for ($i = 0; $i < count($Osztalyok); $i++) {
                if ($Osztalyok[$i]['vegzoTanev'] == $GLOBALS['tanev']) $GLOBALS['ADAT']['vegzoOsztalyok'][] = $Osztalyok[$i];
            }
            $GLOBALS['ADAT']['dt'] = readVariable($_POST['dt'], 'datetime', date('Y-m-d', strtotime('+7 days', strtotime($GLOBALS['ADAT']['tanevAdat']['zarasDt']))));
        }
    }
    $rootUser = readVariable($_POST['rootUser'], 'strictstring', 'root');
    $rootPassword = readVariable($_POST['rootPassword'], 'emptystringnull', null); // lehet benne bármilyen karakter

    if ( $action == 'ujTanev' ) {

        $GLOBALS['tanev'] = readVariable($_POST['ujTanev'], 'numeric unsigned', null);
        if ( isset($GLOBALS['tanev']) ) {

            $DATA = array(); $j = 0;
            for ($i = 0; $i < count($_POST['kezdesDt']); $i++) {
                $kezdesDt = readVariable($_POST['kezdesDt'][$i], 'datetime', null);
                $zarasDt  = readVariable($_POST['zarasDt' ][$i], 'datetime', null);
                if (isset($kezdesDt) && isset($zarasDt)) {
                    $DATA[$j++] = array(
                        'tanev' => $GLOBALS['tanev'],
                        'szemeszter' => readVariable($_POST['szemeszter'][$i], 'numeric unsigned'),
                        'kezdesDt' => $kezdesDt,
                        'zarasDt' => $zarasDt,
                        'statusz' => 'tervezett'
                    );
                }
            }
            for ($i = 0; $i < count($DATA); $i++) szemeszterBejegyzes($DATA[$i]);
            $Tanevek = getTanevek($tervezett = true);

        }

    } elseif ($action == 'intezmenyValasztas') {

        if (isset($intezmeny) && $intezmeny !== __INTEZMENY) {
            if (updateSessionIntezmeny($intezmeny)) {
                header('Location: '.location('index.php?page=naplo&sub=admin&f=tanevek'));
            }
        }

    } elseif ($action == 'tanevAktival') {
        $TA = getTanevAdat($GLOBALS['tanev']);
        $dbNev = tanevDbNev(__INTEZMENY, $GLOBALS['tanev']);
        if ($TA['statusz'] == 'tervezett') {
            // hozzuk létre az adatbázist és adjunk megfelelő jogokat hozzá!
            if (createDatabase($dbNev, __TANEV_DB_FILE, $rootUser, $rootPassword, array("%DB%" => intezmenyDbNev(__INTEZMENY)) )
                !== false)
            {
                // frissítsük az osztalyNaplo táblát
                refreshOsztalyNaplo($dbNev,$GLOBALS['tanev']);
                activateTanev($GLOBALS['tanev']);
            }
        } else {
            grantWriteAccessToDb($dbNev, $rootUser, $rootPassword);
            activateTanev($GLOBALS['tanev']);
        }
    } elseif ($action == 'tanevLezar' && $GLOBALS['ADAT']['tanevAdat']['statusz'] == 'aktív' && is_array($_POST['step'])) {
    
        $GLOBALS['ADAT']['step'] = $_POST['step'];
        $GLOBALS['ADAT']['vjlOsztaly'] = $_POST['vjlOsztaly'];
        $GLOBALS['ADAT']['vatOsztaly'] = $_POST['vatOsztaly'];
        if (closeTanev($GLOBALS['ADAT']) && in_array('tanevLezaras', $GLOBALS['ADAT']['step']))
            revokeWriteAccessFromDb(tanevDbNev(__INTEZMENY, $GLOBALS['tanev']), $rootUser, $rootPassword);
    
    } elseif ($action == 'tanevLezar' && $GLOBALS['ADAT']['tanevAdat']['statusz'] == 'aktív' && !is_array($_POST['step'])) {
    
        $_SESSION['alert'][] = 'message:nothing_to_do:'.$action;
    
    } elseif ($action == 'szemeszterTorles') {

        // Szemeszterek kezdes és zaras dátumainak változtatása
        $GLOBALS['Szemeszterek'] = getTanevSzemeszterek($GLOBALS['tanev']);
        if (is_array($_POST['kezdesDt']) && is_array($_POST['zarasDt']) && is_array($GLOBALS['Szemeszterek']) && count($GLOBALS['Szemeszterek']) == count($_POST['kezdesDt'])) {
            $GLOBALS['ADAT']['modSzemeszter'] = array();
            $elozoDt = ''; $rendezett = true;
            for ($i = 0; $i < count($GLOBALS['Szemeszterek']); $i++) {
                $kezdesDt = readVariable($_POST['kezdesDt'][$i],'datetime','');
                $zarasDt = readVariable($_POST['zarasDt'][$i],'datetime','');
                if ($elozoDt >= $kezdesDt || $kezdesDt >= $zarasDt) {
                    $rendezett = false;
                    $_SESSION['alert'][] = 'message:wrong_data:szemeszter dátum módosítás:'.$kezdesDt.'-'.$zarasDt;
                    break;
                } elseif ($GLOBALS['Szemeszterek'][$i]['kezdesDt'] != $kezdesDt || $GLOBALS['Szemeszterek'][$i]['zarasDt'] != $zarasDt) {
                    $GLOBALS['Szemeszterek'][$i]['kezdesDt'] = $kezdesDt; $GLOBALS['Szemeszterek'][$i]['zarasDt'] = $zarasDt;
                    $GLOBALS['ADAT']['modSzemeszter'][] = $GLOBALS['Szemeszterek'][$i];
                }
                $elozoDt = $zarasDt;
            }
            if ($rendezett && count($GLOBALS['ADAT']['modSzemeszter']) > 0) szemeszterModositas($GLOBALS['ADAT']['modSzemeszter']);
        }
        if (is_array($_POST['szemeszterId'])) szemeszterTorles($_POST['szemeszterId']);

    } // action
    updateNaploSession($sessionID,__INTEZMENY,$GLOBALS['tanev']);

    if (isset($GLOBALS['tanev'])) {
        $GLOBALS['Szemeszterek'] = getTanevSzemeszterek($GLOBALS['tanev']);
    }
    
    $i = 0; 
    while (($i < count($GLOBALS['Szemeszterek'])) && ($GLOBALS['Szemeszterek'][$i]['statusz'] != 'aktív')) {
        $i++;
    }
    $GLOBALS['aktivTanev'] = ($i < count($GLOBALS['Szemeszterek']));

    $GLOBALS['TOOL']['intezmenySelect'] = array('tipus' => 'cella', 'action' => 'intezmenyValasztas', 'intezmenyek' => $Intezmenyek, 'post' => array());
    $GLOBALS['TOOL']['tanevSelect'] = array('tipus' => 'cella', 'tanevek' => $Tanevek, 'action' => 'tanevValasztas', 'tervezett' => true, 'post' => array(), 'paramName'=>'tanev');
    getToolParameters();
}
