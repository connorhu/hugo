<?php

if (_RIGHTS_OK !== true) die();

if (defined('__INTEZMENY') && __INTEZMENY != '') {
    putIntezmenyModositasForm($GLOBALS['ADAT']);
    putIntezmenyTorlesForm($GLOBALS['ADAT']);
}
putUjIntezmenyForm();
