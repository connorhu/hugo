<?php

if (_RIGHTS_OK !== true) die();

if (!__NAPLOADMIN && !__VEZETOSEG) {
    $_SESSION['alert'][] = "page:insufficient_access";
} else {

    /*
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    BUG - zárás és írás időszak kezdődjön együtt!!!!!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    */

    require_once('include/modules/naplo/share/szemeszter.php');
    require_once('include/modules/naplo/share/file.php');

    define('_TIME', time());
    $szemeszterId = readVariable($_POST['szemeszterId'], 'id', readVariable($_GET['szemeszterId'], 'id'));

    if (!isset($szemeszterId)) {
        for ($i = 1; $i <= count($GLOBALS['_TANEV']['szemeszter']); $i++) {
            if (
            strtotime($GLOBALS['_TANEV']['szemeszter'][$i]['kezdesDt']) <= _TIME
                && strtotime($GLOBALS['_TANEV']['szemeszter'][$i]['zarasDt']) >= _TIME
            ) {
                $szemeszterId = $_POST['szemeszterId'] = $GLOBALS['_TANEV']['szemeszter'][$i]['szemeszterId'];
                $tanev = $GLOBALS['_TANEV']['szemeszter'][$i]['tanev'];
                $szemeszter = $GLOBALS['_TANEV']['szemeszter'][$i]['szemeszter'];
                break;
            }
        }
    } else {
        // szándékosan nincs szemeszter beállítva
    }
    if (isset($szemeszterId)) $GLOBALS['ADAT']['szemeszterAdat'] = getSzemeszterAdatById($szemeszterId);
    $GLOBALS['ADAT']['idoszakTipusok'] = getIdoszakTipusok();

    // -------- action --------- //
    if ($action != '') {
        if ($action == 'idoszakModositas') {
            for ($i = 0; $i < count($_POST['idoszakId']); $i++) 
                $Mod[ $_POST['idoszakId'][$i] ] = array('tolDt' => $_POST['tolDt'][$i], 'igDt' => $_POST['igDt'][$i]);
            for ($i = 0; $i < count($_POST['torlendo']); $i++) $Mod[ $_POST['torlendo'][$i] ]['torlendo'] = true;
            foreach ($GLOBALS['ADAT']['szemeszterAdat']['idoszak'] as $i => $iAdat) {
                $iId = $iAdat['idoszakId'];
                if ($Mod[$iId]['torlendo'] == true) {
                    //		    	echo 'Torol: '.$iId.'<hr>';
                    idoszakTorles($iId);
                } elseif (
                $iAdat['tolDt'] != $Mod[$iId]['tolDt']
                    || $iAdat['igDt'] != $Mod[$iId]['igDt']
                ) {
                    //		    	echo '<br>'.$iId.' : '.$iAdat['tolDt'].' -- '.$_POST['tolDt'][$i].'<hr>';
                    idoszakModositas($iId, $Mod[$iId]['tolDt'], $Mod[$iId]['igDt']);
                }

            }
        } elseif ($action == 'ujIdoszak') {
            ujIdoszak($_POST['tolDt'], $_POST['igDt'], $_POST['tipus'], $GLOBALS['ADAT']['szemeszterAdat']['tanev'], $GLOBALS['ADAT']['szemeszterAdat']['szemeszter'], $GLOBALS['ADAT']['idoszakTipusok']);
        }
        $GLOBALS['ADAT']['szemeszterAdat']['idoszak'] = getIdoszakByTanev(array('tanev' => $GLOBALS['ADAT']['szemeszterAdat']['tanev'], 'szemeszter' => $GLOBALS['ADAT']['szemeszterAdat']['szemeszter']));
    }

    $GLOBALS['TOOL']['szemeszterSelect'] = array('tipus' => 'cella', 'action' => 'szemeszterValasztas', 'post' => array(), 'paramName'=>'szemeszterId');
    getToolParameters();
}
