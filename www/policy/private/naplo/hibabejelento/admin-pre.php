<?php

    if (_RIGHTS_OK !== true) die();

    require_once('include/modules/naplo/share/intezmenyek.php');        
    require_once('include/modules/naplo/share/kerelem.php');        

    $_telephelyIdDefault = (isset($_POST['telephelyId'])?null:__TELEPHELYID);
    $telephelyId = readVariable($_POST['telephelyId'],'id', $_telephelyIdDefault);      

    if (__VEZETOSEG || __NAPLOADMIN) {
	if ($action == 'hibaAdmin') {
	    $_ADAT = $_POST;
	    if (isset($_ADAT['jovahagy'])) $_ADAT['jovahagyasAccount'] = _USERACCOUNT;
	    hibaAdmin($_ADAT);
	}
	// Összes lezáratlan kérelem lekérdezése
	$Kerelmek = getKerelmek($telephelyId);
    } else {
	// Saját kérelmek lekérdezése
	$Kerelmek = getSajatKerelmek($telephelyId);
    }


    $TELEPHELY = getTelephelyek();
  
    $TOOL['kerelemStat'] = array('tipus'=>'cella', 'paramName'=>'telephelyId', 'post'=>array());
    $TOOL['telephelySelect'] = array('tipus'=>'cella', 'paramName'=>'telephelyId', 'post'=>array());                                                                             
    getToolParameters();

?>
