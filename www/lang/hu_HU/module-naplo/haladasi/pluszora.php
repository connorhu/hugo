<?php
/*
    module:	naplo
    file:	pluszora.php
    nyelv:	hu_HU (magyar)
*/

    define('__PAGETITLE','Plusz óra felvétele');
    define('_EREDET','Eredet');
    define('_PLUSZ','plusz óra');
    define('_ORAREND','órarendi óra');
    define('_ORA_FELVETELE','Óra felvétele');
    define('_ORAI','órái');
    define('_OSZTALY','osztály');
    define('_ORA', 'óra');

?>
