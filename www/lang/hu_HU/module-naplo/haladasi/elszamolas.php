<?php

    define('__PAGETITLE','Haladási naplóba rögzített órák');

    define('_HETI_MUNKAORA','Heti munkaóra');
    define('_ELOIRT_HETI_MUNKAORA','Előírt heti munkaóra');
    define('_HETI_ORASZAM','Heti óraszám<br/>órarend alapján');
    define('_MEGTARTOTT_ORAK','Megtartott órák');
    define('_NAPRA_VETITVE','napra vetítve');
    define('_TIK','Tanítási időkeret');
    define('_TIK_TITLE','(tanítási + speciális tanítási napok száma)*(heti óraszám / 5)');
    define('_TM','Teljesítménymutató');
    define('_TM_TITLE','Összes megtartott órák száma - tik');
    define('_MUNKATERV','munkaterv');
    define('_KOTOTT_MUNKAIDO','kötött munkaidő');

?>
