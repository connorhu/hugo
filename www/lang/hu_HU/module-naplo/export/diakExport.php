<?php

    define('__PAGETITLE','Diákok adatainak exportálása');

    define('_EXPORT','Adatok exportálása');
    define('_SZULO_ADATOKKAL','Szülő adatokkal');
    define('_OSZTALY_ADATOKKAL','Osztály adatokkal');
    define('_FORMATUM','Formátum');
    define('_CSV','.csv (OpenOffice.org)');
    define('_XLS','.xls (Microsoft Excel XML)');
    define('_XML','.xml (Microsoft Excel XML)');
    define('_HTML','Weblapként (html)');
    define('_BEALLITASOK', 'Beállítások');
    define('_MEGJELENITENDO_MEZOK','Megjelenítendő mezők kiválasztása');

/*
    define('_PDF','.pdf (Adobe Acrobat Reader)');
    define('_LEZARAS','Lezárás');
    define('_JOGVISZONY_VEGE','Jogviszony vége');
    define('_JOGVISZONY_LEZARASA','Jogviszony lezárása');
    define('_BIZTOS_LEZARJA','Biztos lezárja a tanuló jogviszonyát?');

    define('_JOGVISZONY_MEGNYITASA','Jogviszony megnyitása');
    define('_JOGVISZONY_KEZDETE','Jogviszony kezdete');
    define('_VEGZO_TANEV','Végzés (várható) tanéve');
    define('_VEGZO_SZEMESZTER','Végző szemeszter');
    define('_KEZDO_TANEV','Beiratkozás tanéve');
    define('_KEZDO_SZEMESZTER','Beiratkozás szemesztere');
    define('_MEGNYITAS','Megnyitás');
    define('_BIZTOS_MEGNYITJA','Biztos újra megnyitja a tanulói jogviszonyt?');
    define('_DIAK_TANULMANYI_ADATOK','Tanulmányi adatok');

    define('_MAGANTANULOI_STATUS','Magántanulóvá nyilvánítás');
    define('_MAGANTANULOI_STATUS_ELETBELEPESE','Magántanulói stárusz életbelépése');
    define('_BIZTOS_MAGANTANULO_LESZ','Biztos magántanulóvá kívánja tenni?');

    define('_DIAK_OSZTALYA','Osztálya');
    define('_DIAK_ADATAI','Diák adatai');
    define('_DIAK_ALAPADATAI','Diák adatai (alap)');
    define('_DIAK_SZULETESI_ADATAI','Születési adatok');
    define('_DIAK_CIM_LAKHELY','Lakhely');
    define('_DIAK_CIM_TART','Tartózkodási hely / Értesítési cím');
    define('_DIAK_ELERHETOSEG','Elérhetőség');
    define('_DIAK_SZULO','Szülők');
    define('_NEV','Név');
    define('_OID','Oktatási&nbsp;azonosító');
    define('_NEM','Neme');
    define('_DIAKIGAZOLVANYSZAM','Diákigazolványszám');
    define('_ALLAMPOLGARSAG','Állampolgárság');
    define('_TAJSZAM','Tajszám');

    define('_SZULETESKORINEV','Születéskori név');
    define('_SZULETESIHELY','Születési hely');
    define('_SZULETESIIDO','Születési idő');
    define('_SZULETES','');

    define('_ORSZAG','Ország');
    define('_IRSZ','Irányítószám');
    define('_HELYSEG','Helység');
    define('_KOZTERULETNEV','Közterület');
    define('_HAZSZAM','Házszám');
    define('_EMELET','Emelet');
    define('_AJTO','Ajtó');

    define('_EMAIL','e-mail cím');
    define('_TELEFON','telefon');
    define('_MOBIL','mobiltelefon');

    define('_ANYA','anya');
    define('_APA','apa');
    define('_GONDVISELO','gondviselo');

    define('_UJ_DIAK','Új diák felvétele');
    define('_TERVEZETT_KEZDO_TANEV','Beiratkozás (várható) tanéve'); 
*/    
?>
