<?php
/* 

    Module:	base
    File:	base.php
    Nyelv:	hu_HU (magyar)

*/

define('_MAIN_TITLE','MaYoR - 4.8');
define('_MAIN_FOOTER','<a href="http://www.mayor.hu./">MaYoR</a> - 2002-2015 &copy; <a href="LICENSE.txt">GPL</a>');

define('_TANAR','tanár');
define('_DIAK','diák');
define('_TITKARSAG','titkárság');
define('_GAZDASAGI','gazdasági');
define('_EGYEB','egyéb');

define('_ERROR','Hiba');
define('_INFORMATION','Információ');

define('_CONTROL_FLAG_REQUIRED','A bejelentkezés kötelező.');
define('_CONTROL_FLAG_OPTIONAL','A bejelentkezés nem kötelező.');

define('_PRIVATE_PASSWORD','Jelszó');
define('_PARENT_PASSWORD','Szülői jelszó');
define('_PUBLIC_PASSWORD','Külső jelszó');
define('_MAYOR_DESC','MaYoR elektronikus napló program');

define('_HELP','Súgó');
define('_LOGOUT','Kilépés');
define('_PASSWORD','Jelszó');
define('_PARENT_LOGIN','Szülői bejelentkezés');
define('_LOGIN','Bejelentkezés');
define('_SEARCH','Keresés');
?>
