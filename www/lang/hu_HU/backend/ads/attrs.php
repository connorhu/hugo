<?php
/*
    Module:	base/ads
*/

define('_ADSDN', 'ADS Dn');
define('_ADSUID', 'ADS azonosító');
define('_ADSUIDNUMBER', 'ADS felhasználó azonosító szám');
define('_ADSGIDNUMBER', 'ADS csoport azonosító szám');
define('_ADSCN', 'ADS teljes név');
define('_ADSNAME', 'ADS név');
define('_ADSSN', 'ADS vezetéknév');
define('_ADSGIVENNAME', 'ADS keresztnév');
define('_ADSUNIXHOMEDIRECTORY', 'ADS Unix home könyvtár');
define('_ADSDESCRIPTION', 'ADS leírás');
define('_ADSSERIALNUMBER', 'ADS oktatási azonosító');

define('_ADSBADPWDCOUNT','ADS hibás jelszó szám');
define('_ADSBADPASSWORDTIME','ADS hibás jelszó időbélyeg');
define('_ADSLASTLOGON','ADS utolsó belépés időbélyeg');
define('_ADSPWDLASTSET','ADS jelszó módosítás időbélyeg');
define('_ADSACCOUNTEXPIRES','ADS account lejár');
define('_ADSSAMACCOUNTNAME','ADS sAM azonosító');
define('_USERACCOUNTCONTROL','ADS account control');
define('_ADSUSERPRINCIPALNAME','ADS principal név');
define('_ADSOBJECTCATEGORY','ADS objektum kategória');
define('_ADSLOGINSHELL','ADS bejelentkezési shell');

define('_ADSSHADOWLASTCHANGE','ADS shadow utolsó módosítás');
define('_ADSSHADOWEXPIRE','ADS shadow lejár');
define('_ADSSHADOWWARNING','ADS shadow figyelmeztetés');
define('_ADSSHADOWMIN','ADS shadow minimális idő');
define('_ADSSHADOWMAX','ADS shadow maximáis idő');
define('_ADSSHADOWINACTICE','ADS shadow inaktív');

define('_ADSMEMBER', 'ADS tag');
define('_ADSMEMBERUID', 'ADS tag azonosító');

// A kakukktojások
/*
define('_ADS_USERACCOUNT','Azonosító');
define('_ADS_USERCN','Név');
define('_YEAR','év');
define('_MONTH','hó');
define('_DAY','nap');
define('_FIU','fiú');
define('_LANY','lány');
// És a rendes atttribútumok
define('_ADSGECOS', 'ADS Gecos');
define('_ADSSTUDYID', 'ADS oktatási azonosító');
define('_ADSMAIL', 'ADS email');
define('_ADSHOMEPAGE', 'ADS honlap');
define('_ADSURL', 'ADS url');
define('_ADSTELEPHONENUMBER', 'ADS telefonszám');
define('_ADSMOBILE', 'ADS mobil szám');
define('_ADSYEAR', 'ADS évfolyam');
define('_ADSCLASS', 'ADS osztály');
define('_ADSL', 'ADS város');
define('_ADSSTREET', 'ADS utca');
define('_ADSPOSTALADDRESS', 'ADS posta cím');
define('_ADSPOSTALCODE', 'ADS irányítószám');
define('_ADSROOMNUMBER', 'ADS teremszám');
define('_ADSREGISTERTIMESTAMP', 'ADS beiratkozás dátuma');
define('_ADSPRIMARYSCHOOLOMCODE', 'ADS OM kód');
define('_ADSCLASSTIMESTAMP', 'ADS osztályba kerülés dátuma');
define('_ADSSTUDENTCARDNUMBER', 'ADS diákigazolvány szám');
define('_ADSSTUDENTCARDTIMESTAMP', 'ADS diákigazolvány dátuma');
define('_ADSTAXID', 'ADS adószám');
define('_ADSBIRTHTIMESTAMP', 'ADS születési dátum');
define('_ADSBIRTHLOCALITY', 'ADS születési hely');
define('_ADSDIARYNUMBER', 'ADS napló sorszám');
define('_ADSSEX', 'ADS nem');
define('_ADSGUARDIANCN', 'ADS gondviselő neve');
define('_ADSMOTHERCN', 'ADS anyja neve');
define('_ADSLOCALITYTIMESTAMP', 'ADS lakcím dátuma');
define('_ADSTAJNUMBER', 'ADS TAJ szám');
define('_ADSSTUDENTMEMBER', 'ADS tanuló tag');
define('_ADSEXEMPTMEMBER', 'ADS felmentett tag');
define('_ADSEXAMERMEMBER', 'ADS vizsgázó tag');

*/
?>
