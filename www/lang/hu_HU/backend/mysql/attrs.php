<?php

        define('_MYSQLUID', 'MySQL felhasználói azonosító szám');
        define('_MYSQLPOLICY', 'MySQL hozzáférési szint');
        define('_MYSQLCN', 'MySQL a felhasználó neve (cn)');
        define('_MYSQLSTUDYID', 'MySQL oktatási azonosító');
        define('_MYSQLUIDNUMBER', 'MySQL uid');
        define('_MYSQLMAIL', 'MySQL e-mail cím');
        define('_MYSQLTELEPHONENUMBER', 'MySQL telefonszám');
	define('_MYSQLUSERPASSWORD', 'MySQL jelszó');
        define('_MYSQLSHADOWLASTCHANGE', 'MySQL shadow utolsó módosítás');
        define('_MYSQLSHADOWEXPIRE', 'MySQL shadow lejár');
        define('_MYSQLSHADOWWARNING', 'MySQL shadow figyelmeztetés');
        define('_MYSQLSHADOWMIN', 'MySQL shadow minimális idő');
        define('_MYSQLSHADOWMAX', 'MySQL shadow maximális idő');
        define('_MYSQLSHADOWINACTICE', 'MySQL shadow inaktív');

	define('_MYSQLGID', 'MySQL csoport azonosító szám');
	define('_MYSQLGROUPDESC', 'MySQL csoportnév');
	define('_MYSQLGROUPCN', 'MySQL csoport azonosító');
	define('_MYSQLMEMBER', 'MySQL csoporttag');
?>
