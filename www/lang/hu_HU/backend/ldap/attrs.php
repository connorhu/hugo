<?php
/*
    Module:	base/ldap
*/

// A kakukktojások
define('_LDAP_USERACCOUNT','Azonosító');
define('_LDAP_USERCN','Név');
define('_YEAR','év');
define('_MONTH','hó');
define('_DAY','nap');
define('_FIU','fiú');
define('_LANY','lány');
// És a rendes atttribútumok
define('_LDAPDN', 'LDAP Dn');
define('_LDAPUID', 'LDAP azonosító');
define('_LDAPUIDNUMBER', 'LDAP felhasználó azonosító szám');
define('_LDAPGIDNUMBER', 'LDAP csoport azonosító szám');
define('_LDAPGECOS', 'LDAP Gecos');
define('_LDAPCN', 'LDAP teljes név');
define('_LDAPSTUDYID', 'LDAP oktatási azonosító');
define('_LDAPSN', 'LDAP vezetéknév');
define('_LDAPGIVENNAME', 'LDAP keresztnév');
define('_LDAPMAIL', 'LDAP email');
define('_LDAPHOMEPAGE', 'LDAP honlap');
define('_LDAPURL', 'LDAP url');
define('_LDAPTELEPHONENUMBER', 'LDAP telefonszám');
define('_LDAPMOBILE', 'LDAP mobil szám');
define('_LDAPYEAR', 'LDAP évfolyam');
define('_LDAPCLASS', 'LDAP osztály');
define('_LDAPL', 'LDAP város');
define('_LDAPSTREET', 'LDAP utca');
define('_LDAPPOSTALADDRESS', 'LDAP posta cím');
define('_LDAPPOSTALCODE', 'LDAP irányítószám');
define('_LDAPHOMEDIRECTORY', 'LDAP home könyvtár');
define('_LDAPDESCRIPTION', 'LDAP leírás');
define('_LDAPROOMNUMBER', 'LDAP teremszám');
define('_LDAPREGISTERTIMESTAMP', 'LDAP beiratkozás dátuma');
define('_LDAPPRIMARYSCHOOLOMCODE', 'LDAP OM kód');
define('_LDAPCLASSTIMESTAMP', 'LDAP osztályba kerülés dátuma');
define('_LDAPSTUDENTCARDNUMBER', 'LDAP diákigazolvány szám');
define('_LDAPSTUDENTCARDTIMESTAMP', 'LDAP diákigazolvány dátuma');
define('_LDAPTAXID', 'LDAP adószám');
define('_LDAPBIRTHTIMESTAMP', 'LDAP születési dátum');
define('_LDAPBIRTHLOCALITY', 'LDAP születési hely');
define('_LDAPREGISTERNUMBER', 'LDAP regisztrációs szám');
define('_LDAPDIARYNUMBER', 'LDAP napló sorszám');
define('_LDAPSEX', 'LDAP nem');
define('_LDAPGUARDIANCN', 'LDAP gondviselő neve');
define('_LDAPMOTHERCN', 'LDAP anyja neve');
define('_LDAPLOCALITYTIMESTAMP', 'LDAP lakcím dátuma');
define('_LDAPTAJNUMBER', 'LDAP TAJ szám');
define('_LDAPMEMBER', 'LDAP tag');
define('_LDAPSTUDENTMEMBER', 'LDAP tanuló tag');
define('_LDAPEXEMPTMEMBER', 'LDAP felmentett tag');
define('_LDAPEXAMERMEMBER', 'LDAP vizsgázó tag');
define('_LDAPMEMBERUID', 'LDAP tag azonosító');

define('_LDAPSHADOWLASTCHANGE','LDAP shadow utolsó módosítás');
define('_LDAPSHADOWEXPIRE','LDAP shadow lejár');
define('_LDAPSHADOWWARNING','LDAP shadow figyelmeztetés');
define('_LDAPSHADOWMIN','LDAP shadow minimális idő');
define('_LDAPSHADOWMAX','LDAP shadow maximáis idő');
define('_LDAPSHADOWINACTICE','LDAP shadow inaktív');

?>
