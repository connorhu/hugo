<?php

if (defined('_SESSIONID') and _SESSIONID != '') {
    $MENU['logout'] = array(
        array(
            'txt' => 'Kilépés',
            'url' => 'index.php?policy=public&page=session&f=logout',
            'get' => array('sessionID','skin','lang')
        )
    );
    $MENU['session'] = array(
        array(
            'txt' => 'Felhasználók',
            'url' => 'index.php?page=session'
                //		'url' => 'index.php?page=session&sub=search&f=searchAccount'
        )
    );
}

if (memberOf(_USERACCOUNT, $AUTH[_POLICY]['adminGroup'])) {
    $MENU['modules']['session'] = array(
        'createAccount' => array(array('txt' => 'Új felhasználó')),
        'createGroup' => array(array('txt' => 'Új csoport')),
        'searchAccount' => array(array('txt' => 'Felhasználó keresése','url' => 'index.php?page=session&sub=search&f=searchAccount')),
        'searchGroup' => array(array('txt' => 'Csoport keresése','url' => 'index.php?page=session&sub=search&f=searchGroup')),
        'changeMyPassword' => array(array('txt' => 'Jelszóváltoztatás','url' => 'index.php?page=password&sub=&f=changeMyPassword&userAccount='._USERACCOUNT.'&policy=public&toPolicy=private','get' => array('skin','lang','sessionID'))),
        'sessionAdmin' => array(array('txt' => 'Munkamenetek')),
    );
} else {
    $MENU['modules']['session'] = array(
        'searchAccount' => array(array('txt' => 'Felhasználó keresése','url' => 'index.php?page=session&sub=search&f=searchAccount')),
        'searchGroup' => array(array('txt' => 'Csoport keresése','url' => 'index.php?page=session&sub=search&f=searchGroup')),
        'changeMyPassword' => array(array('txt' => 'Jelszóváltoztatás','url' => 'index.php?page=password&sub=&f=changeMyPassword&userAccount='._USERACCOUNT.'&policy=public&toPolicy=private','get' => array('skin','lang','sessionID'))),
    );
}

global $NAV;

if ($page=='session') {
    if (is_array($MENU['modules']['session'])) {
        foreach ($MENU['modules']['session'] as $_sub => $M) {
            $NAV['2'][] = array('page' => 'session', 'f' => $_sub);
        }
    }
} else {
    if (memberOf(_USERACCOUNT, $AUTH[_POLICY]['adminGroup'])) {
        $NAV[1][] = array('page'=>'session');
    } else {
        $NAV[1][] = array('page'=>'session','f'=>'changeMyPassword');
    }
}
