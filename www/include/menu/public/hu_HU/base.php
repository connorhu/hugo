<?php

    $MENU = array(
	'home'=>array(),
	'session'=>array(),
	'naplo'=>array(),
	'portal'=>array(),
	'felveteli'=>array(),
	'forum'=>array(),
	'auth'=>array(),

    );

    if (defined('_SESSIONID') and _SESSIONID != '') {
	$MENU['session'] = array(
	    array('txt' => 'Kilépés', 'url' => 'index.php?page=session&f=logout')
	);
    }
    $MENU['home'] = array(
	array('txt' => 'Kezdőlap', 'url' => 'index.php')
    );
    $MENU['auth'] = array(
	array('txt' => 'Bejelentkezés', 'url' => 'index.php?page=auth&f=login&toPolicy=private'),
	array('txt' => 'Szülői bejelentkezés', 'url' => 'index.php?page=auth&f=login&toPolicy=parent'),
    );
    $MENU['modules']['auth']['login'] = array(
	array('txt' => 'Fórum bejelentkezés', 'url' => 'index.php?page=auth&f=login&toPolicy=public&toPSF=forum::forum'),
	array('txt' => 'Szülői bejelentkezés', 'url' => 'index.php?page=auth&f=login&toPolicy=parent'),
	array('txt' => 'Védett oldalak','url' => 'index.php?page=auth&f=login&toPolicy=private'),
//	array('txt' => 'Regisztráció','url' => 'index.php?page=session&f=createAccount&toPolicy=parent')
    );
//    $MENU['modules']['session']['createAccount'] = array(
//	array('txt' => 'Szülői regisztráció', 'url' => 'index.php?page=session&f=createAccount&toPolicy=parent'),
//	array('txt' => 'Fórum regisztráció', 'url' => 'index.php?page=session&f=createAccount&toPolicy=public'),
//    );
    if ($f == 'changeMyPassword') {
	$MENU['back'] = array(array('txt'=>'Vissza','url'=>'index.php?policy='.$toPolicy,'get'=>array('sessionID','skin','lang')));
	$MENU['password'] = array(array('txt'=>'Jelszóváltoztatás','url'=>'index.php?page=password&sub=&f=changeMyPassword'));
	$NAV[1][] = array('page'=>'back');
    }

?>
