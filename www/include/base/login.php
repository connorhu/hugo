<?php

if ($action == 'mayorGlobalLogin') {
    $toPolicy = readVariable($_REQUEST['toPolicy'], 'enum', 'private', $POLICIES);
    $toSkin = readVariable($_POST['toSkin'], 'enum', readVariable($_GET['toSkin'], 'enum', null, $SKINSSHOW), $SKINSSHOW);
    @list($toPage,$toSub,$toF) = readVariable(explode(':',$_REQUEST['toPSF']), 'strictstring');
    $toPSF = "$toPage:$toSub:$toF";
    // Autentikáció - alapok
    if (file_exists('include/share/auth/base.php')) {
        require_once('include/share/auth/base.php');
    }
    // Autentikáció - becsatoljuk minkét auth szintet is (a public-ot nem)
    /*	if (file_exists('include/backend/'.$AUTH['private']['backend'].'/auth/login.php')) {
    require_once('include/backend/'.$AUTH['private']['backend'].'/auth/login.php');
    }
    if (file_exists('include/backend/'.$AUTH['parent']['backend'].'/auth/login.php')) {
    require_once('include/backend/'.$AUTH['parent']['backend'].'/auth/login.php');
    }
    */
    require_once('include/modules/auth/base/login.php');

    // lejart session-ok torlese
    require_once('include/share/session/close.php');
    closeOldAndIdleSessions();

    /* ez csak egy másolat...*/
    if ($action=='autologin' && defined('_ALLOW_SULIX_SSO') && _ALLOW_SULIX_SSO===true) {
        $userPassword = readVariable($_SESSION['portalLoggedPassword'], 'string');
        $userAccount  = readVariable($_SESSION['portalLoggedUsername'], 'regexp', null, array("^([a-z]|[A-Z]|[0-9]| |\.|,|_|[űáéúőóüöíŰÁÉÚŐÓÜÖÍäÄ]|-)*$"));
    } else {
        $userPassword = readVariable($_POST['userPassword'], 'string');
        $userAccount = readVariable($_POST['userAccount'], 'regexp', null, array("^([a-z]|[A-Z]|[0-9]| |\.|,|_|[űáéúőóüöíŰÁÉÚŐÓÜÖÍäÄ]|-)*$"));
    }

    if (defined('_BOLONDOS') && _BOLONDOS===true) $userAccount = visszafele($userAccount);

    if ($sessionID != '') $accountInformation['sessionID'] = $sessionID;

    if ($userAccount != '' and $userPassword != '') {
        for ($i=0; $i<=2;$i++) {
            if ($i==1 && $toPolicy=='private') $toPolicy='parent';
            elseif ($i==2 && $toPolicy=='parent') $toPolicy='public';
            if (!in_array($AUTH[$toPolicy]['authentication'],array('required','try')))
                continue;;
            $accountInformation = array('account' => $userAccount, 'password' => $userPassword, 'policy' => $toPolicy, 'skin'=>$toSkin);
            $result = userAuthentication($userAccount, $userPassword, $accountInformation, $toPolicy); // ??? toPolicy benne van az AccountInformation-ben!!! Ldap backend only?
            logLogin($toPolicy, $userAccount, $result);
            define('_MAYORAUTHRESULT',$result);
            if ($result === _AUTH_SUCCESS) {
                $sessionID = newSession($accountInformation, $toPolicy);
                if ($toSkin == '') $toSkin = $skin;
                header('Location: '.location("index.php?page=$toPage&sub=$toSub&f=$toF&sessionID=$sessionID&policy=$toPolicy&lang=$lang&skin=$toSkin", array('alertOLD')));
                break;
            } elseif ($result === _AUTH_EXPIRED) {
                $_SESSION['alert'][] = 'message:force_pw_update';
                header('Location: '.location("index.php?policy=public&page=password&f=changeMyPassword&userAccount=".$userAccount."&toPolicy=$toPolicy&skin=$toSkin", array('alertOLD')));
                break;
            } elseif ($result === _AUTH_FAILURE_1) {
                // nincs ilyen user, megpróbáljuk beauthentikálni parent-tel is.
            } elseif ($result >= _AUTH_FAILURE) {
                // sikertelen azonosítás - a hibaüzenetet a függvény generálja
                // megpróbáljuk beauthentikálni parent-tel is.
                break;
            } else {
                // Ilyen csak hibás függvényműködés esetén lehet:
                $_SESSION['alert'][] = "message:default:hibás visszatérési érték:userAuthentication:(".serialize($result).")";
                break;
            }
        }
    } else {
        $_SESSION['alert'][] = 'message:empty_field';
    }
} 
