<?php
    /*
	Vigyázat!! A létszám adatok csak az épp aktuális státuszokkal dolgoznak. Visstamenőleg, korábbi évekre nézni őket nincs értelme.
     */

    function getDiakLetszamByStatusz() {

	$q = "select statusz, count(*) as letszam from diak group by statusz";
	$ret = db_query($q, array('fv'=>'getDiakLetszamByStatusz','modul'=>'naplo_intezmeny','result'=>'keyvaluepair'));
	$q = "select statusz, count(*) as letszam from diak where nem='fiú' group by statusz";
	$ret['fiú'] = db_query($q, array('fv'=>'getDiakLetszamByStatusz/fiú','modul'=>'naplo_intezmeny','result'=>'keyvaluepair'));
	$q = "select statusz, count(*) as letszam from diak where nem='lány' group by statusz";
	$ret['lány'] = db_query($q, array('fv'=>'getDiakLetszamByStatusz/lány','modul'=>'naplo_intezmeny','result'=>'keyvaluepair'));

	return $ret;

    }

    function getDiakLetszamByOsztalyId($osztalyIds) {
	if (is_array($osztalyIds) && count($osztalyIds)>0) {
	    $q = "select osztalyId, count(*) as letszam from diak left join osztalyDiak using (diakId) 
		where statusz in ('jogviszonyban van','magántanuló') and osztalyId in (".implode(',', array_fill(0, count($osztalyIds), '%u')).") group by osztalyId";
	    $ret = db_query($q, array('fv'=>'getDiakLetszamByStatusz','modul'=>'naplo_intezmeny','result'=>'keyvaluepair','values'=>$osztalyIds));
	    $q = "select osztalyId, count(*) as letszam from diak left join osztalyDiak using (diakId) 
		where statusz in ('jogviszonyban van','magántanuló') and osztalyId in (".implode(',', array_fill(0, count($osztalyIds), '%u')).") and nem='fiú' group by osztalyId";
	    $ret['fiú'] = db_query($q, array('fv'=>'getDiakLetszamByStatusz/fiú','modul'=>'naplo_intezmeny','result'=>'keyvaluepair','values'=>$osztalyIds));
	    $q = "select osztalyId, count(*) as letszam from diak left join osztalyDiak using (diakId) 
		where statusz in ('jogviszonyban van','magántanuló') and osztalyId in (".implode(',', array_fill(0, count($osztalyIds), '%u')).") and nem='lány' group by osztalyId";
	    $ret['lány'] = db_query($q, array('fv'=>'getDiakLetszamByStatusz/lány','modul'=>'naplo_intezmeny','result'=>'keyvaluepair','values'=>$osztalyIds));

	    return $ret;

	} else {
	    return false;
	}
    }

    function getTanarLetszamByBesorolas() {
	$q = "select besorolas, count(*) as letszam from tanar where statusz<>'jogviszonya lezárva' and statusz<>'külső óraadó' group by besorolas";
	return db_query($q, array('fv'=>'getTanarLetszamByBesorolas','modul'=>'naplo_intezmeny','result'=>'keyvaluepair'));
    }

    function getTanarLetszamByStatusz() {
	$q = "select statusz, count(*) as letszam from tanar group by statusz";
	return db_query($q, array('fv'=>'getTanarLetszamByBesorolas','modul'=>'naplo_intezmeny','result'=>'keyvaluepair'));
    }

    function getTankorOraszamOsszesites($tankorTipusIds) {

	$q = "select sum(oraszam/2) from tankorSzemeszter where tanev=".__TANEV;
	$ret['összes'] = db_query($q, array('fv'=>'getTankorOraszamOsszesites','modul'=>'naplo_intezmeny','result'=>'value'));
	$q = "select sum(oraszam/2) from tankorSzemeszter left join tankor using (tankorId) 
	    where tankorTipusId in (".implode(',', array_fill(0, count($tankorTipusIds['óratervi']), '%u')).") and tanev=".__TANEV;
	$ret['óratervi'] = db_query($q, array('fv'=>'getTankorOraszamOsszesites/óratervi','modul'=>'naplo_intezmeny','result'=>'value','values'=>$tankorTipusIds['óratervi']));
	$q = "select sum(oraszam/2) from tankorSzemeszter left join tankor using (tankorId) 
	    where tankorTipusId in (".implode(',', array_fill(0, count($tankorTipusIds['tanórán kívüli']), '%u')).") and tanev=".__TANEV;
	$ret['tanórán kívüli'] = db_query($q, array('fv'=>'getTankorOraszamOsszesites/tanórán kívüli','modul'=>'naplo_intezmeny','result'=>'value','values'=>$tankorTipusIds['tanórán kívüli']));

	return $ret;
    }

    function getTargyOraszamok($tankorTipusIds) {

	$q = "select targyId, sum(oraszam/2) from tankorSzemeszter left join tankor using (tankorId) 
	    where tanev=".__TANEV." group by targyId";
	$ret['összes'] = db_query($q, array('fv'=>'getTargyOraszamok','modul'=>'naplo_intezmeny','result'=>'keyvaluepair'));
	$q = "select targyId, sum(oraszam/2) from tankorSzemeszter left join tankor using (tankorId) 
	    where tankorTipusId in (".implode(',', array_fill(0, count($tankorTipusIds['óratervi']), '%u')).") and tanev=".__TANEV." group by targyId";
	$ret['óratervi'] = db_query($q, array('fv'=>'getTargyOraszamok/óratervi','modul'=>'naplo_intezmeny','result'=>'keyvaluepair','values'=>$tankorTipusIds['óratervi']));
	$q = "select targyId, sum(oraszam/2) from tankorSzemeszter left join tankor using (tankorId) 
	    where tankorTipusId in (".implode(',', array_fill(0, count($tankorTipusIds['tanórán kívüli']), '%u')).") and tanev=".__TANEV." group by targyId";
	$ret['tanórán kívüli'] = db_query($q, array('fv'=>'getTargyOraszamok/tanórán kívüli','modul'=>'naplo_intezmeny','result'=>'keyvaluepair','values'=>$tankorTipusIds['tanórán kívüli']));

	return $ret;
    }

    function getOsztalyOraszamok($osztalyIds, $tankorTipusIds) {

	$q = "select osztalyId, sum(oraszam/2) from tankorSzemeszter left join tankorOsztaly using (tankorId) 
	    where osztalyId in (".implode(',', array_fill(0, count($osztalyIds), '%u')).") 
	    and tanev=".__TANEV." group by osztalyId";
	$ret['összes'] = db_query($q, array('fv'=>'getOsztalyOraszamok','modul'=>'naplo_intezmeny','result'=>'keyvaluepair','values'=>$osztalyIds));
	$q = "select osztalyId, sum(oraszam/2) from tankorSzemeszter left join tankor using (tankorId) left join tankorOsztaly using (tankorId)
	    where tankorTipusId in (".implode(',', array_fill(0, count($tankorTipusIds['óratervi']), '%u')).") 
	    and osztalyId in (".implode(',', array_fill(0, count($osztalyIds), '%u')).") 
	    and tanev=".__TANEV." group by osztalyId";
	$v = array_merge($tankorTipusIds['óratervi'], $osztalyIds);
	$ret['óratervi'] = db_query($q, array('fv'=>'getOsztalyOraszamok/óratervi','modul'=>'naplo_intezmeny','result'=>'keyvaluepair','values'=>$v));
	$q = "select osztalyId, sum(oraszam/2) from tankorSzemeszter left join tankor using (tankorId) left join tankorOsztaly using (tankorId)
	    where tankorTipusId in (".implode(',', array_fill(0, count($tankorTipusIds['tanórán kívüli']), '%u')).") 
	    and osztalyId in (".implode(',', array_fill(0, count($osztalyIds), '%u')).") 
	    and tanev=".__TANEV." group by osztalyId";
	$v = array_merge($tankorTipusIds['tanórán kívüli'], $osztalyIds);
	$ret['tanórán kívüli'] = db_query($q, array('fv'=>'getOsztalyOraszamok/tanórán kívüli','modul'=>'naplo_intezmeny','result'=>'keyvaluepair','values'=>$v));

	return $ret;
    }

    function getTankorLetszamStat() {
	global $_TANEV;
	$r = getTankorByTanev(__TANEV);
	foreach ($r as $idx => $tAdat) {
	    $return[ $tAdat['targyId'] ]['tankorIds'][] = $tAdat['tankorId'];
	    $letszam = getTankorLetszam($tAdat['tankorId'], array('refDt'=>$_TANEV['kezdesDt']));
	    $return[ $tAdat['targyId'] ]['sum'] += $letszam;
	    $return[ $tAdat['targyId'] ]['db']++;
	    if ($return[ $tAdat['targyId'] ]['max'] < $letszam) $return[ $tAdat['targyId'] ]['max'] = $letszam;
	    if (!isset($return[ $tAdat['targyId'] ]['min']) || $return[ $tAdat['targyId'] ]['min'] > $letszam) $return[ $tAdat['targyId'] ]['min'] = $letszam;
	}
	return $return;
    }

?>