<?php

    function getKerelmek($telephelyId='') {

	$W = (isset($telephelyId) && $telephelyId!='') ? ' AND (telephelyId=%u OR telephelyId IS NULL)' : '';
	$q = "SELECT * FROM kerelem WHERE lezarasDt IS NULL $W ORDER BY rogzitesDt DESC";
	return db_query($q, array('fv' => 'getKerelmek', 'modul' => 'naplo_base', 'result' => 'indexed', 'values'=>array($telephelyId)));

    }

    function getSajatKerelmek($telephelyId='') {

	$W = (isset($telephelyId) && $telephelyId!='') ? ' AND (telephelyId=%u OR telephelyId IS NULL)' : '';
	$q = "SELECT * FROM kerelem WHERE userAccount='"._USERACCOUNT."' AND (lezarasDt IS NULL OR (lezarasDt > (curdate() - interval 5 day))) $W ORDER BY rogzitesDt DESC";
	return db_query($q, array('fv' => 'getSajatKerelmek', 'modul' => 'naplo_base', 'result' => 'indexed', 'values'=>array($telephelyId)));

    }

    function hibaAdmin($Adat) {

        $kerelemId = $Adat['kerelemId'];
        $valasz = $Adat['valasz'];
        $kategoria = $Adat['kategoria'];
	$userAccount = $Adat['jovahagyasAccount'];
	$telephelyId = ($Adat['kerelemTelephelyId']!='') ? $Adat['kerelemTelephelyId'] : 'NULL';
	$q = "UPDATE kerelem SET kategoria='%s',telephelyId='%s' WHERE kerelemId=%u";
	$v = array($kategoria,$telephelyId,$kerelemId);
        db_query($q, array('fv' => 'hibaAdmin', 'modul' => 'naplo_base', 'values' => $v));
        if (isset($Adat['jovahagy'])) {
            $q = "UPDATE kerelem SET valasz='%s', jovahagyasAccount='%s',jovahagyasDt=NOW() WHERE kerelemId=%u";
	    $v = array($valasz, $userAccount, $kerelemId);
        } elseif ($Adat['nemHagyJova']) {
	    $q = "SELECT jovahagyasDt FROM kerelem WHERE kerelemId=%u";
	    $jdt = db_query($q, array('fv' => 'hibaAdmin', 'modul' => 'naplo_base', 'result' => 'value', 'values' => array($kerelemId)));
            $q = "UPDATE kerelem SET valasz='%s [exJovahagyva: %s]', jovahagyasDt=NULL WHERE kerelemId=%u";
	    $v = array($valasz, $jdt, $kerelemId);
        } elseif (isset($Adat['lezar'])) {
            $q = "UPDATE kerelem SET valasz='%s', lezarasDt=NOW() WHERE kerelemId=%u";
	    $v = array($valasz, $kerelemId);
        } else {
            $q = "UPDATE kerelem SET valasz='%s' WHERE kerelemId=%u";
	    $v = array($valasz, $kerelemId);
        }

        return db_query($q, array('fv' => 'hibaAdmin', 'modul' => 'naplo_base', 'values' => $v));
    
    }

?>
