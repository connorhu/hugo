<?php

if ( !isset($GLOBALS['TOOL']) || !is_array($GLOBALS['TOOL']))
$GLOBALS['TOOL'] = array();

function getToolParameters() {

	$TOOLVARS = array(
	    'diakId'=> array('type'=>'id'),
	    'tanarId'=> array('type'=>'id'),
	    'szuloId'=> array('type'=>'id'),
	    'teremId'=> array('type'=>'id'),
	    'osztalyId'=> array('type'=>'id'),
	    'targyId'=> array('type'=>'id'),
	    'mkId'=> array('type'=>'id'),
	    'tankorId'=> array('type'=>'id'),
	    'telephelyId' => array('type'=>'id'),

	    'tolDt' => array('type'=>'datetime'),
	    'igDt' => array('type'=>'datetime'),
	    'refDt' => array('type'=>'datetime'),
	    'dt' => array('type'=>'datetime'),

	    'tanev' => array('type'=>'numeric unsigned'),
	    'het' => array('type'=>'numeric unsigned'),

	    'fileName' => array('type'=>'strictstring'),
	    'conv' => array('type'=>'strictstring'),
	    'sorrendNev' => array('type'=>'enum','allowOnly' => array('napló','anyakönyv','ellenőrző','bizonyítvány','egyedi')),
	    'targySorrend' => array('type'=>'strictstring'),

	    // ellenőrizendő még:	    
	    'ho' => array('type'=>'strictstring'),
	    'ora' => array('type'=>'strictstring'),
	    'tipus' => array('type'=>'strictstring'),
//	    'telephely' => array('type'=>'strictstring'),
	    // ...
	);


	foreach ( $GLOBALS['TOOL'] as $name => $params ) if (is_array($params)) {

	    for ($i = 0; $i < count($params['post']); $i++) {
		$_var = $params['post'][$i];
		if ( $_POST[$_var]!='' && ($TOOLVARS[$_var]['type']!='') ) { // ellenőrizzük a fenti tömb szerinti változókat.
		    // itt típuskonverzió is történik
		    $_POST[$_var] = readVariable($_POST[$_var], $TOOLVARS[$_var]['type'],null, $TOOLVARS[$_var]['allowOnly']);
		}
	    }

	    if ( function_exists( $func = "get$name" ) ) {
			$func();
	    }

	}

}


/* AUDIT */

/*
getAuditInfo();
function getAuditInfo() {

global $page,$sub,$f;

$_SESSION['alert'][] = '::'.$page.$sub.$f;

$WORK = $_POST;

for ($i
reset($WORK);
ksort($WORK);
$X = unserialize(serialize($WORK));

var_dump($X);

}
  */


/* ------------------------- */


function getSzamSelect() {

	

	if (!is_array($GLOBALS['TOOL']['szamSelect']['szamok'])) {
	    $minValue = (isset($GLOBALS['TOOL']['szamSelect']['minValue']))?$GLOBALS['TOOL']['szamSelect']['minValue']:1;
	    $maxValue = (isset($GLOBALS['TOOL']['szamSelect']['maxValue']))?$GLOBALS['TOOL']['szamSelect']['maxValue']:100;
	    $GLOBALS['TOOL']['szamSelect']['szamok'] = range($minValue, $maxValue);
	}

	if ( !isset($GLOBALS['TOOL']['szamSelect']['paramName']) || $GLOBALS['TOOL']['szamSelect']['paramName']=='' )
	$GLOBALS['TOOL']['szamSelect']['paramName'] = 'szam';

}

function getIntezmenySelect() {

	if (!is_array($GLOBALS['TOOL']['intezmenySelect']['intezmenyek'])) {
	    require_once('include/modules/naplo/share/intezmenyek.php');
	    $GLOBALS['TOOL']['intezmenySelect']['intezmenyek'] = getIntezmenyek();
	}
	if ( !isset($GLOBALS['TOOL']['intezmenySelect']['paramName']) || $GLOBALS['TOOL']['intezmenySelect']['paramName'] == '' )
	    $GLOBALS['TOOL']['intezmenySelect']['paramName'] = 'intezmeny';

}

function getTelephelySelect() {

	

	if (!is_array($GLOBALS['TOOL']['telephelySelect']['telephelyek'])) {
	    require_once('include/modules/naplo/share/intezmenyek.php');
	    $GLOBALS['TOOL']['telephelySelect']['telephelyek'] = getTelephelyek();
	}
	if ( !isset($GLOBALS['TOOL']['telephelySelect']['paramName']) || $GLOBALS['TOOL']['telephelySelect']['paramName'] == '' )
	    $GLOBALS['TOOL']['telephelySelect']['paramName'] = 'telephelyId';
	if (count($GLOBALS['TOOL']['telephelySelect']['telephelyek']) < 2) unset($GLOBALS['TOOL']['telephelySelect']);

}

function getTanevSelect() {
	require_once('include/modules/naplo/share/intezmenyek.php');
	if (!isset($GLOBALS['TOOL']['tanevSelect']['tanevek']) || !is_array($GLOBALS['TOOL']['tanevSelect']['tanevek'])) {
	    $GLOBALS['TOOL']['tanevSelect']['tanevek'] = getTanevek($GLOBALS['TOOL']['tanevSelect']['tervezett']);
	}
    
	if ( !isset($GLOBALS['TOOL']['tanevSelect']['paramName']) || $GLOBALS['TOOL']['tanevSelect']['paramName']=='' )
	$GLOBALS['TOOL']['tanevSelect']['paramName'] = 'tanev';
}

function getSzemeszterSelect() {

	

	require_once('include/modules/naplo/share/szemeszter.php');
	$GLOBALS['TOOL']['szemeszterSelect']['szemeszterek'] = getSzemeszterek($GLOBALS['TOOL']['szemeszterSelect']);

	if ( !isset($GLOBALS['TOOL']['szemeszterSelect']['paramName']) || $GLOBALS['TOOL']['szemeszterSelect']['paramName'] == '' )
	$GLOBALS['TOOL']['szemeszterSelect']['paramName'] = 'szemeszterId';

}

function getTargySorrendSelect() {

	

	require_once('include/modules/naplo/share/targy.php');
	if (!isset($GLOBALS['TOOL']['targySorrendSelect']['tanev'])) $GLOBALS['TOOL']['targySorrendSelect']['tanev'] = __TANEV;
	$GLOBALS['TOOL']['targySorrendSelect']['sorrendNevek'] = getTargySorrendNevek($GLOBALS['TOOL']['targySorrendSelect']['tanev']);

	if ( !isset($GLOBALS['TOOL']['targySorrendSelect']['paramName']) || $GLOBALS['TOOL']['targySorrendSelect']['paramName'] == '' )
	$GLOBALS['TOOL']['targySorrendSelect']['paramName'] = 'sorrendNev';

}

function getMunkakozossegSelect() {

	

	if (!is_array($GLOBALS['TOOL']['munkakozossegSelect']['munkakozossegek'])) 
	    $GLOBALS['TOOL']['munkakozossegSelect']['munkakozossegek'] = getMunkakozossegek();
	if ( !isset($GLOBALS['TOOL']['munkakozossegSelect']['paramName']) || $GLOBALS['TOOL']['munkakozossegSelect']['paramName']=='' )
	$GLOBALS['TOOL']['munkakozossegSelect']['paramName'] = 'mkId';

}

function getTargySelect() {

	

	if (!is_array($GLOBALS['TOOL']['targySelect']['targyak']))
	    $GLOBALS['TOOL']['targySelect']['targyak'] = getTargyak(array('mkId' => $GLOBALS['TOOL']['targySelect']['mkId']));
	if ( !isset($GLOBALS['TOOL']['targySelect']['paramName']) || $GLOBALS['TOOL']['targySelect']['paramName']=='' )
	$GLOBALS['TOOL']['targySelect']['paramName'] = 'targyId';

}

function getMunkatervSelect() {

	

	if (!is_array($GLOBALS['TOOL']['munkatervSelect']['munkatervek']))
	    $GLOBALS['TOOL']['munkatervSelect']['munkatervek'] = getMunkatervek();
	if ( !isset($GLOBALS['TOOL']['munkatervSelect']['paramName']) || $GLOBALS['TOOL']['munkatervSelect']['paramName']=='' )
	$GLOBALS['TOOL']['munkatervSelect']['paramName'] = 'munkatervId';

}

function getTanarSelect() {

    

    if (!is_array($GLOBALS['TOOL']['tanarSelect']['tanarok'])) {
	if (!isset($GLOBALS['TOOL']['tanarSelect']['tanev']) && defined('__TANEV')) $GLOBALS['TOOL']['tanarSelect']['tanev'] = __TANEV;
	if (is_array($GLOBALS['TOOL']['tanarSelect']['Param'])) $Param = $GLOBALS['TOOL']['tanarSelect']['Param'];
	else $Param = array(
	    'mkId' => $GLOBALS['TOOL']['tanarSelect']['mkId'],
	    'tanev' => $GLOBALS['TOOL']['tanarSelect']['tanev'],
	    'beDt' => $GLOBALS['TOOL']['tanarSelect']['beDt'],
	    'kiDt' => $GLOBALS['TOOL']['tanarSelect']['kiDt'],
	    'összes' => $GLOBALS['TOOL']['tanarSelect']['összes'],
	    'override' => $GLOBALS['TOOL']['tanarSelect']['override'],
	);
	$GLOBALS['TOOL']['tanarSelect']['tanarok'] = getTanarok($Param);
    }
    if (!isset($GLOBALS['TOOL']['tanarSelect']['paramName']) || $GLOBALS['TOOL']['tanarSelect']['paramName']=='' )
	$GLOBALS['TOOL']['tanarSelect']['paramName'] = 'tanarId';

}

function getDiakSelect() {

	

	if (!isset($GLOBALS['TOOL']['diakSelect']['osztalyId']) && isset($osztalyId))
		$GLOBALS['TOOL']['diakSelect']['osztalyId'] = $osztalyId;
	if (!is_array($GLOBALS['TOOL']['diakSelect']['diakok']))
	    $GLOBALS['TOOL']['diakSelect']['diakok'] = getDiakok(array(
		'osztalyId' => $GLOBALS['TOOL']['diakSelect']['osztalyId'], 
		'tanev' => $GLOBALS['TOOL']['diakSelect']['tanev'], 
		'statusz' => $GLOBALS['TOOL']['diakSelect']['statusz'],
		'tolDt' => $GLOBALS['TOOL']['diakSelect']['tolDt'],
		'igDt' => $GLOBALS['TOOL']['diakSelect']['igDt'],
	    ));
	if (!is_array($GLOBALS['TOOL']['diakSelect']['statusz'])) 
	    $GLOBALS['TOOL']['diakSelect']['statusz'] = array('jogviszonyban van','magántanuló','vendégtanuló','jogviszonya felfüggesztve','jogviszonya lezárva','felvételt nyert');
	if ( !isset($GLOBALS['TOOL']['diakSelect']['paramName']) || $GLOBALS['TOOL']['diakSelect']['paramName']=='' )
	$GLOBALS['TOOL']['diakSelect']['paramName'] = 'diakId';
}

function getDiakLapozo() {

	

	if (!isset($GLOBALS['TOOL']['diakLapozo']['osztalyId']) && isset($osztalyId))
		$GLOBALS['TOOL']['diakLapozo']['osztalyId'] = $osztalyId;
	if (!is_array($GLOBALS['TOOL']['diakLapozo']['diakok']))
	    $GLOBALS['TOOL']['diakLapozo']['diakok'] = getDiakok(array(
		'osztalyId' => $GLOBALS['TOOL']['diakLapozo']['osztalyId'], 
		'tanev' => $GLOBALS['TOOL']['diakLapozo']['tanev'], 
		'statusz' => $GLOBALS['TOOL']['diakLapozo']['statusz'],
		'tolDt' => $GLOBALS['TOOL']['diakLapozo']['tolDt'],
		'igDt' => $GLOBALS['TOOL']['diakLapozo']['igDt'],
	    ));
	if (!is_array($GLOBALS['TOOL']['diakLapozo']['statusz'])) 
	    $GLOBALS['TOOL']['diakLapozo']['statusz'] = array('jogviszonyban van','magántanuló','vendégtanuló','jogviszonya felfüggesztve','jogviszonya lezárva','felvételt nyert');
	if ( !isset($GLOBALS['TOOL']['diakLapozo']['paramName']) || $GLOBALS['TOOL']['diakLapozo']['paramName']=='' )
	$GLOBALS['TOOL']['diakLapozo']['paramName'] = 'diakId';

}


function getTableSelect () {

	

	$GLOBALS['TOOL']['tableSelect']['naplo'] =$GLOBALS['TOOL']['tableSelect']['naplo_intezmeny'] = array();
	if (defined('__INTEZMENY')) {
		$GLOBALS['TOOL']['tableSelect']['naplo_intezmeny'] = db_query('SHOW TABLES', array('fv' => 'getTableSelect', 'modul' => 'naplo_intezmeny', 'result' => 'idonly'));
	}
	if (defined('__TANEV')) {
		$GLOBALS['TOOL']['tableSelect']['naplo'] = 
		db_query('SHOW TABLES', array('fv' => 'getTableSelect', 'modul' => 'naplo', 'result' => 'idonly'));
	}
	if ( !isset($GLOBALS['TOOL']['tableSelect']['paramName']) || $GLOBALS['TOOL']['tableSelect']['paramName']=='' )
	$GLOBALS['TOOL']['tableSelect']['paramName'] = 'dbtable';
	 
}

function getOsztalySelect() {
    
	if (!isset($GLOBALS['TOOL']['osztalySelect']['tanev'])) {
	    if (isset($GLOBALS['tanev'])) $GLOBALS['TOOL']['osztalySelect']['tanev'] = $GLOBALS['tanev'];
	    elseif (defined('__TANEV')) $GLOBALS['TOOL']['osztalySelect']['tanev'] = __TANEV;
	}

	if (!isset($GLOBALS['TOOL']['osztalySelect']['osztalyok']))
	    if (isset($GLOBALS['TOOL']['osztalySelect']['tanev']))
		$GLOBALS['TOOL']['osztalySelect']['osztalyok'] = getOsztalyok($GLOBALS['TOOL']['osztalySelect']['tanev'],array('mindenOsztalyfonok'=>true, 'result'=>'indexed','telephelyId' => $GLOBALS['telephelyId']));
	    else 
		$GLOBALS['TOOL']['osztalySelect']['osztalyok'] = array();

	if ( !isset($GLOBALS['TOOL']['osztalySelect']['paramName']) || $GLOBALS['TOOL']['osztalySelect']['paramName']=='' )
	$GLOBALS['TOOL']['osztalySelect']['paramName'] = 'osztalyId';

}

function getTanmenetSelect() {

	

	if (!isset($GLOBALS['TOOL']['tanmenetSelect']['tanev'])) {
	    if (isset($GLOBALS['tanev'])) $GLOBALS['TOOL']['tanmenetSelect']['tanev'] = $GLOBALS['tanev'];
	    elseif (defined('__TANEV')) $GLOBALS['TOOL']['tanmenetSelect']['tanev'] = __TANEV;
	}

	if (!isset($GLOBALS['TOOL']['tanmenetSelect']['tanmenetek']))
	    if (isset($GLOBALS['TOOL']['tanmenetSelect']['tanev'])) {
		if (isset($tanarId)) $GLOBALS['TOOL']['tanmenetSelect']['tanmenetek'] = getTanmenetByTanarId($tanarId, array('tanev' => $GLOBALS['TOOL']['tanmenetSelect']['tanev']));
		elseif (isset($targyId)) $GLOBALS['TOOL']['tanmenetSelect']['tanmenetek'] = getTanmenetByTargyId($targyId, array('tanev' => $GLOBALS['TOOL']['tanmenetSelect']['tanev']));
//	    } else {
//		$GLOBALS['TOOL']['tanmenetSelect']['tanmenetek'] = array();
	    }

	if ( !isset($GLOBALS['TOOL']['tanmenetSelect']['paramName']) || $GLOBALS['TOOL']['tanmenetSelect']['paramName']=='' )
	$GLOBALS['TOOL']['tanmenetSelect']['paramName'] = 'tanmenetId';

}

function getTankorSelect() {

	

	// Tanév beállítás: paraméter, globális változó, konstans
	if (!isset($GLOBALS['TOOL']['tankorSelect']['tanev'])) {
	    if (isset($GLOBALS['tanev'])) $GLOBALS['TOOL']['tankorSelect']['tanev'] = $GLOBALS['tanev'];
	    elseif (defined('__TANEV')) $GLOBALS['TOOL']['tankorSelect']['tanev'] = __TANEV;
	}
	$tolDt=$GLOBALS['TOOL']['tankorSelect']['tolDt'];
	$igDt=$GLOBALS['TOOL']['tankorSelect']['igDt'];

	// Paraméter neve
	if ( !isset($GLOBALS['TOOL']['tankorSelect']['paramName']) || $GLOBALS['TOOL']['tankorSelect']['paramName']=='' )
	$GLOBALS['TOOL']['tankorSelect']['paramName'] = 'tankorId';

	// tankörök lekérdezése - ha még nem történt meg
	if (!is_array($GLOBALS['TOOL']['tankorSelect']['tankorok'])) {
	    if (isset($diakId) && $diakId!='') { // diák tankörei
		$GLOBALS['TOOL']['tankorSelect']['tankorok'] = getTankorByDiakId($diakId, $GLOBALS['TOOL']['tankorSelect']['tanev'], array('tolDt'=>$tolDt, 'igDt'=>$igDt));
	    } elseif (isset($osztalyId) && $osztalyId!='') { // osztály tankörei
		$GLOBALS['TOOL']['tankorSelect']['tankorok'] = getTankorByOsztalyId($osztalyId, $GLOBALS['TOOL']['tankorSelect']['tanev'], array('tolDt'=>$tolDt, 'igDt'=>$igDt));
	    } elseif (isset($tanarId) && $tanarId!='') { // tanár tankörei
		$GLOBALS['TOOL']['tankorSelect']['tankorok'] = getTankorByTanarId($tanarId, $GLOBALS['TOOL']['tankorSelect']['tanev'], array('tolDt'=>$tolDt, 'igDt'=>$igDt));
	    } else { // általános tankörlekérdző
		$WHERE = array();
		if (isset($targyId) && $targyId != '') { // leszűkítés adott tárgyra
		    $WHERE[] = 'targyId='.$targyId;
		} elseif (isset($mkId) && $mkId != '') { // leszűkítés adott munkaközösségre
		    $TARGYAK = getTargyakByMkId($mkId);
		    for ($i = 0; $i < count($TARGYAK); $i++) $T[] = $TARGYAK[$i]['targyId'];
		    if (count($T) > 0) $WHERE[] = 'targyId IN ('.implode(',', $T).')';
		}

		if (isset($GLOBALS['TOOL']['tankorSelect']['tanev'])) // szűkítés adott tanévre
		    $WHERE[] = 'tankorSzemeszter.tanev='.$GLOBALS['TOOL']['tankorSelect']['tanev'];

		$GLOBALS['TOOL']['tankorSelect']['tankorok'] = getTankorok($WHERE);
	    }
	} else {
	    // A megadott tankörök csoportosításához
	    if (!is_array($GLOBALS['TOOL']['tankorSelect']['tankorIds'])) {
		if (isset($diakId) && $diakId != '') { // diák tankörei
		    $GLOBALS['TOOL']['tankorSelect']['tankorIds'] = getTankorByDiakId($diakId, $GLOBALS['TOOL']['tankorSelect']['tanev'],array('csakId' => true, 'tolDt'=>$tolDt, 'igDt'=>$igDt ));
		} elseif (isset($osztalyId) && $osztalyId != '') { // osztály tankörei
		    $GLOBALS['TOOL']['tankorSelect']['tankorIds'] = getTankorByOsztalyId($osztalyId, $GLOBALS['TOOL']['tankorSelect']['tanev'], array('csakId' => true, 'tolDt'=>$tolDt, 'igDt'=>$igDt));
		} elseif (isset($tanarId) && $tanarId != '') { // tanár tankörei
		    $GLOBALS['TOOL']['tankorSelect']['tankorIds'] = getTankorByTanarId($tanarId, $GLOBALS['TOOL']['tankorSelect']['tanev'], array('csakId' => true,'tolDt'=>$tolDt, 'igDt'=>$igDt));
		}
	    }
	}
	if ($tolDt!='' || $igDt!='')
	    $GLOBALS['TOOL']['tankorSelect']['tankorIdsDt'] = $tolDt.'-'.$igDt;

}

function getDatumSelect() {

	

	if (isset($GLOBALS['tanev'])) $GLOBALS['TOOL']['datumSelect']['tanev'] = $GLOBALS['tanev'];
	elseif (defined('__TANEV')) $GLOBALS['TOOL']['datumSelect']['tanev'] = __TANEV;
	
	if (
	    (is_array($GLOBALS['TOOL']['datumSelect']['napTipusok']) || isset($GLOBALS['TOOL']['datumSelect']['napokSzama']))
	    && !is_array($GLOBALS['TOOL']['datumSelect']['napok'])
	) {
	    $GLOBALS['TOOL']['datumSelect']['napok'] = getNapok(
		array(
		    'tanev' => $GLOBALS['TOOL']['datumSelect']['tanev'],
		    'tolDt' => $GLOBALS['TOOL']['datumSelect']['tolDt'],
		    'igDt' => $GLOBALS['TOOL']['datumSelect']['igDt'],
		    'tipus' => $GLOBALS['TOOL']['datumSelect']['napTipusok'],
		    'napokSzama' => $GLOBALS['TOOL']['datumSelect']['napokSzama'],
		)
	    );
	} else {

	    $tolDt = $GLOBALS['TOOL']['datumSelect']['tolDt']; $igDt = $GLOBALS['TOOL']['datumSelect']['igDt'];
	    initTolIgDt($GLOBALS['TOOL']['datumSelect']['tanev'], $tolDt, $igDt, $GLOBALS['TOOL']['datumSelect']['override']);
	    $GLOBALS['TOOL']['datumSelect']['tolDt'] = $tolDt; $GLOBALS['TOOL']['datumSelect']['igDt'] = $igDt;

	    if (!isset($GLOBALS['TOOL']['datumSelect']['hanyNaponta']) || $GLOBALS['TOOL']['datumSelect']['hanyNaponta']=='' )
	    $GLOBALS['TOOL']['datumSelect']['hanyNaponta'] = 1;

	};

	if (!isset($GLOBALS['TOOL']['datumSelect']['paramName']) || $GLOBALS['TOOL']['datumSelect']['paramName']=='' )
	$GLOBALS['TOOL']['datumSelect']['paramName'] = 'dt';
}

function getDatumTolIgSelect() {

	if (isset($GLOBALS['tanev'])) $GLOBALS['TOOL']['datumTolIgSelect']['tanev'] = $GLOBALS['tanev'];
	elseif (defined('__TANEV')) $GLOBALS['TOOL']['datumTolIgSelect']['tanev'] = __TANEV;
	
	if (
	    (is_array($GLOBALS['TOOL']['datumTolIgSelect']['napTipusok']) || isset($GLOBALS['TOOL']['datumTolIgSelect']['napokSzama']))
	    && !is_array($GLOBALS['TOOL']['datumTolIgSelect']['napok'])
	) {
	    $GLOBALS['TOOL']['datumTolIgSelect']['napok'] = getNapok(
		array(
		    'tanev' => $GLOBALS['TOOL']['datumTolIgSelect']['tanev'],
		    'tolDt' => $GLOBALS['TOOL']['datumTolIgSelect']['tolDt'],
		    'igDt' => $GLOBALS['TOOL']['datumTolIgSelect']['igDt'],
		    'tipus' => $GLOBALS['TOOL']['datumTolIgSelect']['napTipusok'],
		    'napokSzama' => $GLOBALS['TOOL']['datumTolIgSelect']['napokSzama'],
		)
	    );
	} else {
	    $tolDt = $GLOBALS['TOOL']['datumTolIgSelect']['tolDt']; $igDt = $GLOBALS['TOOL']['datumTolIgSelect']['igDt'];
	    initTolIgDt($GLOBALS['TOOL']['datumTolIgSelect']['tanev'], $tolDt, $igDt, $GLOBALS['TOOL']['datumTolIgSelect']['override']);
	    $GLOBALS['TOOL']['datumTolIgSelect']['tolDt'] = $tolDt; $GLOBALS['TOOL']['datumTolIgSelect']['igDt'] = $igDt;

	    if (!isset($GLOBALS['TOOL']['datumTolIgSelect']['hanyNaponta']) || $GLOBALS['TOOL']['datumTolIgSelect']['hanyNaponta']=='' )
	    $GLOBALS['TOOL']['datumTolIgSelect']['hanyNaponta'] = 1;

	};

	if (!isset($GLOBALS['TOOL']['datumTolIgSelect']['tolParamName']) || $GLOBALS['TOOL']['datumTolIgSelect']['tolParamName']=='' )
	$GLOBALS['TOOL']['datumTolIgSelect']['tolParamName'] = 'tolDt';
	if (!isset($GLOBALS['TOOL']['datumTolIgSelect']['igParamName']) || $GLOBALS['TOOL']['datumTolIgSelect']['igParamName']=='' )
	$GLOBALS['TOOL']['datumTolIgSelect']['igParamName'] = 'igDt';

}

function getOraSelect() {

	

	if (!isset($GLOBALS['TOOL']['oraSelect']['tol']) || $GLOBALS['TOOL']['oraSelect']['tol'] == '' )
	$GLOBALS['TOOL']['oraSelect']['tol'] = getMinOra();
	if (!isset($GLOBALS['TOOL']['oraSelect']['ig']) || $GLOBALS['TOOL']['oraSelect']['ig'] == '' )
	$GLOBALS['TOOL']['oraSelect']['ig'] = getMaxOra();

	if (!isset($GLOBALS['TOOL']['oraSelect']['paramName']) || $GLOBALS['TOOL']['oraSelect']['paramName']=='' )
	$GLOBALS['TOOL']['oraSelect']['paramName'] = 'ora';

}

function getTeremSelect() {

	

	$GLOBALS['telephelyId'] = $GLOBALS['TOOL']['teremSelect']['telephelyId'];
	if (!is_array($GLOBALS['TOOL']['teremSelect']['termek']))	$GLOBALS['TOOL']['teremSelect']['termek'] = getTermek(array('telephelyId' => $GLOBALS['telephelyId']));
	if (!isset($GLOBALS['TOOL']['teremSelect']['paramName']) || $GLOBALS['TOOL']['teremSelect']['paramName']=='' )
	    $GLOBALS['TOOL']['teremSelect']['paramName'] = 'teremId';

}

function getKepzesSelect() {

    

    $GLOBALS['TOOL']['kepzesSelect']['kepzes'] = getKepzesek();
    if (!is_array($GLOBALS['TOOL']['kepzesSelect']['kepzes']) || count($GLOBALS['TOOL']['kepzesSelect']['kepzes']) == 0) {
	unset($GLOBALS['TOOL']['kepzesSelect']);
    } else {
	if ( !isset($GLOBALS['TOOL']['kepzesSelect']['paramName']) || $GLOBALS['TOOL']['kepzesSelect']['paramName'] == '' )
	    $GLOBALS['TOOL']['kepzesSelect']['paramName'] = 'kepzesId';
    }
}

function getKerdoivSelect() {

	

	if (!is_array($GLOBALS['TOOL']['kerdoivSelect']['kerdoiv'])) $GLOBALS['TOOL']['kerdoivSelect']['kerdoiv'] = getKerdoiv();
	if ( !isset($GLOBALS['TOOL']['kerdoivSelect']['paramName']) || $GLOBALS['TOOL']['kerdoivSelect']['paramName'] == '' )
	    $GLOBALS['TOOL']['kerdoivSelect']['paramName'] = 'kerdoivId';

}

function getSzuloSelect() {

    

    $GLOBALS['TOOL']['szuloSelect']['szulo'] = getSzulok(array('result' => 'indexed'));
    if (!is_array($GLOBALS['TOOL']['szuloSelect']['szulo']) || count($GLOBALS['TOOL']['szuloSelect']['szulo']) == 0) {
	unset($GLOBALS['TOOL']['szuloSelect']);
    } else {
	if ( !isset($GLOBALS['TOOL']['szuloSelect']['paramName']) || $GLOBALS['TOOL']['szuloSelect']['paramName'] == '' )
	    $GLOBALS['TOOL']['szuloSelect']['paramName'] = 'szuloId';
    }
}


/* TANEV FÜGGŐK */

function getOrarendiHetSelect() {

	
	$GLOBALS['TOOL']['orarendiHetSelect']['hetek'] = getOrarendiHetek($GLOBALS['TOOL']['orarendiHetSelect']); // tolDt, igDt, tanev
	if ( !isset($GLOBALS['TOOL']['orarendiHetSelect']['paramName']) || $GLOBALS['TOOL']['orarendiHetSelect']['paramName']=='' )
	    $GLOBALS['TOOL']['orarendiHetSelect']['paramName'] = 'het';

}

function getTanarOraLapozo() {

	
	global $tanarId,$tolDt,$igDt,$oraId;
	$_X = $GLOBALS['TOOL']['tanarOraLapozo']['orak'] = getTanarOrak(
	    $tanarId,array('tolDt' => $tolDt, 'igDt' => $igDt, 'tipus' => array('normál','normál máskor','helyettesítés','felügyelet','összevonás'))
	);
	for ($i = 0; $i < count($_X); $i++) {
	    if ($_X[$i]['oraId'] == $oraId) {
		$GLOBALS['TOOL']['tanarOraLapozo']['oraAdat'] = $_X[$i];
		if (is_array($_X[($i-1)])) $GLOBALS['TOOL']['tanarOraLapozo']['elozo'] = $_X[$i-1];
		if (is_array($_X[($i+1)])) $GLOBALS['TOOL']['tanarOraLapozo']['kovetkezo'] = $_X[$i+1];
		break;
	    }
	}
	if ( !isset($GLOBALS['TOOL']['tanarOraLapozo']['paramName']) || $GLOBALS['TOOL']['tanarOraLapozo']['paramName']=='' )
	    $GLOBALS['TOOL']['tanarOraLapozo']['paramName'] = 'oraId';

}

function getIgazolasOsszegzo() {

    
    global $diakId;
    global $_TANEV;
    if ($diakId!='') {
	$GLOBALS['TOOL']['igazolasOsszegzo']['igazolasok'] = getIgazolasSzam($diakId);
	$_T = getDiakHianyzasOsszesites(array($diakId),$_TANEV);
	$GLOBALS['TOOL']['igazolasOsszegzo']['hianyzasok'] = $_T[$diakId];
    }

}

function getZaradekSelect() {

	

	if (!is_array($GLOBALS['TOOL']['zaradekSelect']['zaradekok'])) $GLOBALS['TOOL']['zaradekSelect']['zaradekok'] = getZaradekok();

	if (!isset($GLOBALS['TOOL']['zaradekSelect']['paramName']) || $GLOBALS['TOOL']['zaradekSelect']['paramName']=='' )
	    $GLOBALS['TOOL']['zaradekSelect']['paramName'] = 'zaradekIndex';

}

function getKerelemStat() {
	
	$GLOBALS['TOOL']['kerelemStat']['stat'] = getKerelemOsszesito();
}

//function getTelephelySelect() {
//    
//    if (!is_array($GLOBALS['TOOL']['telephelySelect']['telephelyek'])) {
//	$GLOBALS['TOOL']['telephelySelect']['telephelyek'] = getTelephely();
//    }
//
//}

function getVissza() {
    
    if ($GLOBALS['TOOL']['vissza']['icon']=='') { $GLOBALS['TOOL']['vissza']['icon'] = 'arrow-left'; } // default, egyelőre csak a 'vissza' típusnál használjuk
}

?>
