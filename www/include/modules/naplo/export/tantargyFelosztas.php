<?php

    function getTankorOraszamok() {

	$q = "SELECT tankorId, tankorNev, targyId, tankorTipusId, tanev, szemeszter, oraszam
		FROM tankor LEFT JOIN tankorSzemeszter USING (tankorId)
		WHERE tanev=".__TANEV;
	$return = db_query($q, array('fv'=>'getTankorOraszamok','modul'=>'naplo_intezmeny','result'=>'indexed'));
	for ($i=0; $i<count($return); $i++) {
	    $return[$i]['tanarIds'] = getTankorTanaraiByInterval($return[$i]['tankorId'], array('tanev' => __TANEV, 'tolDt' => '', 'igDt' => '', 'result' => 'idonly', 'datumKenyszeritessel' => false));
	    $return[$i]['osztalyIds'] = getTankorOsztalyai($return[$i]['tankorId']);
	}
	return $return;

    }

    function exportTantargyFelosztas($file, $ADAT) {

	$T = array();

	$T[0] = array('név','tantárgy');
	foreach ($ADAT['osztalyok'] as $oAdat) { 
	    $T[0][] = $oAdat['osztalyJel'];  
	}
	$T[0][] = 'rész'; $T[0][] = 'szum';

	foreach ($ADAT['tanarAdat'] as $tanarId => $tAdat) {
	    $sum = 0;
	    $utolsoTargyId = end((array_keys($ADAT['export'][$tanarId])));
	    foreach ($ADAT['export'][$tanarId] as $targyId => $targyAdat) {
		
		    $sor = array($tAdat['tanarNev'], $ADAT['targyAdat'][$targyId]['targyNev']);
		    $resz = 0;
		    foreach ($ADAT['osztalyok'] as $oAdat) { 
			if (($targyAdat[$oAdat['osztalyId']][1]+$targyAdat[$oAdat['osztalyId']][2])/2 != 0)
			    $sor[] = ($targyAdat[$oAdat['osztalyId']][1]+$targyAdat[$oAdat['osztalyId']][2])/2;
			else $sor[] = null;
			$resz += ($targyAdat[$oAdat['osztalyId']][1]+$targyAdat[$oAdat['osztalyId']][2])/2;
		    }
		    $sor[] = $resz;
		    $sum += $resz;
		    if ($targyId != $utolsoTargyId) $sor[] = null;
		    else $sor[] = $sum;
		    $T[] = $sor;
		
	    }
	}
//dump($T);
        if ($ADAT['formatum'] == 'xml') return generateXLS("$file.${ADAT['formatum']}", $T, 'tantárgyFelosztás');
        elseif ($ADAT['formatum'] == 'csv') return generateCSV("$file.${ADAT['formatum']}", $T, 'tantárgyFelosztás');
        elseif ($ADAT['formatum'] == 'ods') return generateODS("$file.${ADAT['formatum']}", $T, 'tantárgyFelosztás');
        else return false;

    }

?>