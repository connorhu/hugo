<?php
#
# MaYoR keretrendszer - figyelmeztető üzenetek
#

// Base
if (file_exists('include/alert/'.$lang.'/base.php')) {
    require('include/alert/'.$lang.'/base.php');
} elseif (file_exists('include/alert/'._DEFAULT_LANG.'/base.php')) {
    require('include/alert/'._DEFAULT_LANG.'/base.php');
}

// Policy
if (file_exists('include/alert/'.$lang.'/'.$AUTH[$policy]['backend'].'.php')) {
    require('include/alert/'.$lang.'/'.$AUTH[$policy]['backend'].'.php');
} elseif (file_exists('include/alert/'._DEFAULT_LANG.'/'.$AUTH[$policy]['backend'].'.php')) {
    require('include/alert/'._DEFAULT_LANG.'/'.$AUTH[$policy]['backend'].'.php');
}

// Module
if (file_exists('include/alert/'.$lang.'/module-'.$page.'.php')) {
    require('include/alert/'.$lang.'/module-'.$page.'.php');
} elseif (file_exists('include/alert/'._DEFAULT_LANG.'/module-'.$page.'.php')) {
    require('include/alert/'._DEFAULT_LANG.'/module-'.$page.'.php');
}

?>
