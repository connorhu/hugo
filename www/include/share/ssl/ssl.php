<?php

    function getSslKeyPair() {
	$q = "SELECT * FROM mayorSsl";
	$r = db_query($q, array('modul'=>'login','result'=>'record'));
	if ($r=='') {
		$SSLKeyPair = generateSSLKeyPair();
		$secret = sha1(mt_rand(100000000000000,999999999999999));
		$q = "INSERT INTO mayorSsl (privateKey,publicKey,secret) VALUES ('%s','%s','%s')";
		$values = array($SSLKeyPair['privateKey'],$SSLKeyPair['publicKey'],$secret);
		$r = db_query($q, array('modul'=>'login', 'values'=>$values));
		return $SSLKeyPair;
	} else {
	    return $r;
	}
    }

    function generateSSLKeyPair() {
        $SSL_KEY_PAIR=openssl_pkey_new();
        // Get private key
        openssl_pkey_export($SSL_KEY_PAIR, $privatekey);
        // Get public key
        $publickey=openssl_pkey_get_details($SSL_KEY_PAIR);                                                                                                                                                
        $publickey=$publickey["key"];                                                                                                                                                                                               
	return array('privateKey'=>$privatekey,'publicKey'=>$publickey);                        
    }

    function getSslPublicKey() {
	$SSLKeyPair = getSslKeyPair();
	return $SSLKeyPair['publicKey'];
    }

    function getSslPublicKeyByOMKod($OMKod) {
	if (is_numeric($OMKod)) {
	    $q = "SELECT publicKey FROM mayorKeychain WHERE valid=1 AND OMKod='%u'";
	    $values = array($OMKod);
	    $result = db_query($q, array('debug'=>false,'modul'=>'login', 'values'=>$values,'result'=>'value'));
	    if ($result=='') return false;
	    else return $result;
	} else {
	    return false;
	}
    }
    
    /* symmetric cryptographic module */
    class AES {
	public function __construct() { }
	public function encrypt($mit,$mivel) {
	    return base64_encode($mit);
//	    return base64_encode(db_query("SELECT aes_encrypt('%s','%s')",array('fv'=>'class AES','result'=>'value','modul'=>'login','values'=>array($mit,$mivel))));
	}
	public function decrypt($mit,$mivel) {
	    return $mit;
//	    return  db_query("SELECT aes_decrypt('%s','%s')",array('fv'=>'class AES','result'=>'value','modul'=>'login','values'=>array($mit,$mivel)));
	}
    }

    class Interconnect {

	/* A: küldő, B: fogadó használja */

	private $sessionKey;	// egy kommunikációhoz használt session kulcs
	private $KP;		// a saját kulcspárom
	private $remotePublicKey;
	private $remoteOMKod;
	private $remoteHost;
	private $request;
	private $response;

	/* Konstruktor */
	public function __construct() {
	    if (!defined('__OMKOD')) throw new Exception('OMKod is missing');
	    $this->sessionKey  = $this->_genSessionKey();
	    $this->KP = getSSLKeyPair();
	}

	/* Privát metódusok */
        private function _yconv($get) {
            $get = str_replace(' ','+',$get);   // hm. erre miért van szükség??? autokonverzió?
            $get = str_replace('\/','/',$get);  // hm. erre miért van szükség??? autokonverzió?
            $get = str_replace('\\','',$get);  // hm. erre miért van szükség??? autokonverzió?\"'
            return $get;
        }

	private function _curlGet($get = '',$psf='') { // additional get parameters
// TODO a paraméterben nem kell a host, vegyük a remoteHost-ot inkább! Ki kell vezetni a második paramétert
	    $host = $this->remoteHost;
	    if ($host=='') {
		$url = "http://www.mayor.hu/index.php?skin=rpc&page=portal&sub=rpc&f=rpc&init=".__OMKOD."&detail=".$get;
	    } elseif ($psf!='') {
		$url = $host."/index.php?skin=rpc&".$psf."&init=".__OMKOD."&detail=".$get;
	    } else {
		$url = $host."/index.php?skin=rpc&page=naplo&sub=rpc&f=rpc&init=".__OMKOD."&detail=".$get;
	    }
//dump($url);
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // a választ feldolgozzuk
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
	    curl_setopt($ch, CURLOPT_HEADER, 0); 
//	    curl_setopt($ch, CURLOPT_TIMEOUT,60);
	    curl_setopt($ch, CURLOPT_USERAGENT, "MaYoR-interconnect (php; cURL)");
	    curl_setopt($ch, CURLOPT_VERBOSE, true);
	    $response = curl_exec($ch);
	    $INFO = curl_getinfo($ch); // ha kell
	    if ($INFO['http_code'] == 200) { // all as well
//		var_dump($INFO);
//		dump($response);
	    } else {
		echo '<a href="'.$url.'">URL</a>';
		dump($response);
		throw new Exception($INFO['http_code']);
	    }
	    curl_close($ch);
	    return $response;
	}
	private function _genSessionKey() {
	    return base64_encode(pack('N6', mt_rand(), mt_rand(), mt_rand(),mt_rand(), mt_rand(), mt_rand()));
	}
	private function _sessionKeyEncode() {
	    $crypttext = '';
    	    $res = openssl_private_encrypt($this->sessionKey, $crypttext, $this->KP['privateKey']); // a saját privát kulcsunkkal
    	    return base64_encode($crypttext);
	}
	private function _sessionKeyDecode($in) {
	    $decodedtext = '';
	    $in = str_replace(' ','+',$in); // vajh ez hol romlik el, hogy kell?
	    $in = str_replace('\/','/',$in);
    	    $res = openssl_public_decrypt(base64_decode($in), $decodedtext, $this->remotePublicKey);
    	    return $decodedtext;
	}
	private function _encodeRequest($IN = array()) {
	    $IN['fromOM'] = __OMKOD; // hozzáadjuk a saját OM kódunkat (ha van...)
	    $ADAT['details'] = AES::encrypt(json_encode($IN),$this->sessionKey); // implicit base64_encode
	    $ADAT['sessionKeyEncoded'] = $this->_sessionKeyEncode(); // implicit base64_encode
	    //$SEND = (json_encode($ADAT)); // nem itt!
	    return $ADAT;
	    /*	details: 
		    AES::encrypt (sessionKey)
			json_encode
		sessionKeyEndcoded
	    */
	}
	private function _decodeRequest($response) {
	    $response = str_replace(' ','+',$response);
//	    $in = str_replace('\/','/',$in);
	    return json_decode($response); // nem csak ennyit kell csinálni!
	}
	private function setRemotePublicKey() {
	    if ($this->remoteOMKod!='0') {
		    $this->remotePublicKey = getSslPublicKeyByOMKod($this->remoteOMKod);
		    if ($this->remotePublicKey===false) throw new Exception('invalid public key for ('.$OMKod.') ');
	    } else {
		$this->remotePublicKey = $this->KP['publicKey'];
	    }	    
	}
	/* Publikus metódusok */
	public function setRemoteHost($remoteHost) {
	    $this->remoteHost = $remoteHost;
	    $this->setRemotePublicKey();
	}
	public function setRemoteOMKod($OMKod) {
	    $this->remoteOMKod = $OMKod;
	    $this->setRemotePublicKey();
	}
	public function sendRequest($ADAT = array(),$host) {
	    $get = urlencode(json_encode($this->_encodeRequest($ADAT)));
	    //$get = ($this->_encodeRequest($ADAT));
	    $response = $this->_curlGet($get,$ADAT['psf']);
	    // ide jön a viszontválasz feldolgozás
	    $decodedResponse = $this->_decodeRequest($response);
	    /* A másik oldal szintén details|sessionKeyEncoded tömb jön vissza */
	    if ($this->sessionKey === $this->_sessionKeyDecode($decodedResponse->sessionKeyEncoded)) {
		$decodedObject = json_decode(AES::decrypt(base64_decode($decodedResponse->details),$this->sessionKey));
		return ($decodedObject);
	    } else {
		return false; // hibás session
	    }
	}
	/* B oldal! */
	public function processRequest($get) { 	// a másik oldal
	    if ($get=='') $get = $_GET['detail'];
	    $get = str_replace('\"','"',$get);  // hm. erre miért van szükség??? autokonverzió?            
	    $get = str_replace(' ','+',$get); 	// hm. erre miért van szükség??? autokonverzió?
	    $get = $this->_yconv($get);
	    $GET = json_decode(urldecode($get)); 		// object
	    $_sessionKey = $this->_sessionKeyDecode($GET->sessionKeyEncoded);
						// használhatjuk az osztály változóját is, mivel egyszerre csak egy sessionKey-vel van dolgunk
	    $this->sessionKey = $_sessionKey;
	    $RESULT = json_decode(AES::decrypt(base64_decode($this->_yconv($GET->details)),$_sessionKey));

//	    $RESULT = AES::decrypt(($this->_yconv($GET->details)),$_sessionKey);
	    $this->request = $RESULT;
	    return $RESULT;
	}
	public function prepareReply($ADAT) {
	    return $this->_encodeRequest($ADAT);
	}
	public function getRequest() {
	    return $this->request;
	}
	// response
	public function getResponse() {
	    return $this->response;
	}
	public function setResponse($DATA) {
	    $this->response = json_encode($DATA);
	}
    }


?>
