<?php
/* 
    MaYoR http://www.mayor.hu./ 
    License: LICENSE.txt
*/

@error_reporting(E_ERROR | E_WARNING | E_PARSE);

require('include/base/config.php');    
require('include/base/mysql.php');
require('include/base/var.php');
require('include/base/base.php');
require('include/base/str.php');
require('include/base/log.php');
require('include/alert/base.php');
if ( defined('__TESTERRORREPORTER') && __TESTERRORREPORTER===true) require('include/base/error.php');    

$GLOBALS['AUTH'] = $AUTH;
$GLOBALS['POLICIES'] = $POLICIES;

require('include/base/rights.php');
require('include/base/login.php');
require('include/menu/base.php') ;
/* --- */
require('skin/default/base/html/base.phtml');
if (file_exists("skin/$skin/base/html/alert.phtml")) {
    require("skin/$skin/base/html/alert.phtml");
} else {
    require('skin/'._DEFAULT_SKIN.'/base/html/alert.phtml');
}

$skin = 'classic';

require("skin/$skin/base/html/base.phtml");

html_base($sessionID, $policy, $page, $sub, $f, $lang, $skin, $MENU);
