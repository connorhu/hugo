
Event.observe(window, 'load', myPSFLoader, false);

    doShowInfo = function(event) {

	var elem = $(Event.element(event));

        if (elem.hasClassName('info')) {
	    var elemInform = $('inform');
            $('informTanmenetId').value = $F(elem.previous('select'));
            if ($('informTanmenetId').value != '') {
		updateForm(elemInform, elem);
Effect.Grow('infodiv');
//		elemInform.up('div').show();
	    } else {
		alert('Válassz tanmenetet');
	    }
	    Event.stop(event);
        }

    }

    doHideInfo = function(event) {

	var elemInform = $('inform');
	Effect.Shrink('infodiv');
//    	elemInform.up('div').hide();

    }

    doOnTanmenetChange = function(event) {
	var elem = $(Event.element(event));

        if (elem.hasClassName('onChangeUpdateGomb')) {
	    var optionElem = $(elem.options[ elem.selectedIndex ]);
	    var statusz = optionElem.getAttribute('class');
/*
	    if (statusz == 'modosithato') {
		elem.next('input').show();
		elem.next('input',1).hide();
	    } else {
		elem.next('input').hide();
		elem.next('input',1).show();
	    }
*/
	    var elemInform = $('inform');
            $('informTanmenetId').value = $F(elem);
	    updateForm(elemInform, elem);

	}
    }

function myPSFLoader(evt) {

    Event.observe(document.body, 'click', doShowInfo);
    Event.observe('infodivclose', 'click', doHideInfo);

    Event.observe(document.body, 'mayor:change',  doOnTanmenetChange);
    Event.observe(document.body, 'change', doOnTanmenetChange);
var mydrag = new Draggable('infodiv', { scroll: window, handle: $('infodivheader') });

}
