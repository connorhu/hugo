
Event.observe(window, 'load', myPSFLoader, false);

initTemakorLista = function() {

	var oraszam = 0;
	var maxoraszam = parseInt($F('tanmenetOraszam'));
	// Az eddigi összóraszám meghatározása
	var osszoraszam = 0;
        $$('span.handle').each(
            function(elem, index) {
		selectElem = elem.next('select'); textElem = elem.next('textarea');
		if ($F(selectElem) != '0' && $F(textElem) != '') osszoraszam = osszoraszam+parseInt($F(selectElem));
            }
        );
	var szabadoraszam = maxoraszam-osszoraszam;
	if (szabadoraszam<0) szabadoraszam=0;

	// Az egyes témakörök sorainak beállítása
        $$('span.handle').each(
            function(elem, index) {

		selectElem = elem.next('select'); textElem = elem.next('textarea'); oraszamSpan = elem.next('span');

		if ($F(selectElem) != '0' && $F(textElem) != '') { // Már kitöltött témakör esetén
		    // Mivel nem tudjuk, hogy korábban milyen osztályba volt sorolva ezért mindkét osztályból eltávolítjuk
		    oraszamSpan.removeClassName('jo');		// jo: még belefér az óraszámba
		    oraszamSpan.removeClassName('nagy');	// nagy: már nem fér bele az óraszámba
		    // A témakör által lefoglalt órák kiírása
            	    elem.next('span').innerHTML = oraszam+1;
		    oraszam = oraszam+parseInt($F(selectElem));
            	    elem.next('span').innerHTML += '-'+oraszam;
		    // osztályokba sorolás
		    if (oraszam > maxoraszam) oraszamSpan.addClassName('nagy');	// már nem fér bele a tanmenet óraszámába
		    else oraszamSpan.addClassName('jo');				// még belefér a tanmenet óraszámába
		    // Az oraszám-select érvényes óraszámokra való leszűkítése
		    value = parseInt($F(selectElem));
		    selectElem.options.length = 0;
		    for (k=0; k<value+szabadoraszam+1; k++) {
			selectElem.options[k] = new Option(k, k, (k==value), (k==value));
		    }
		} else { // Az üres, utolsó bejegyzés beállítása
		    if (oraszam < maxoraszam) { // ha van még szabad óra
			// A hátralévő, szabad órák kiírása
			elem.next('span').innerHTML = oraszam+1;
			elem.next('span').innerHTML += '-'+maxoraszam;
			textElem.enable();		// Az esetleg korábban letiltott textarea elem engedélyezése
		    } else {
			elem.next('span').innerHTML = '-';		// nincs már szabad óra
			textElem.disable();		// textarea	 elem letiltása - nem lehet felvenni újabb témakört
		    }
		    // A fennmaradt szabad óraszámhoz igazítás
		    selectElem.options.length = 0;
		    for (k=0; k<szabadoraszam+1; k++) {
			selectElem.options[k] = new Option(k, k, (k==0), (k==0));
		    }
		}
            }
        );

}

//
// Ha változik valamelyik témakör szövege, vagy óraszáma, akkor fut le ez a függvény
//
doOnTemakorChange = function(event) {
       
        var element = $(Event.element(event));
	// Óraszzámváltozáskor újrainicializáljuk a listát...
        if (element.hasClassName('temakorOraszam')) {
            initTemakorLista();
        }
	// Új témakör felvételekor
	if (element.hasClassName('uj')) {
		spanElement = element.previous('span');
		selectElement = spanElement.next('select');
		textElement = spanElement.next('textarea');
		if ($F(selectElement) != '0' && $F(textElement) != '') { // Ha érvényes bejegyzés keletkezett
		    // Létrehozunk egy új, üres témakör-beviteli lehetőséget
		    var newLi = document.createElement("li");
		    newLi.innerHTML = element.up('li').innerHTML;
		    // Az eddigi 'uj' mostantól már nem új...
		    selectElement.removeClassName('uj');
		    textElement.removeClassName('uj');
		    // hanem egy érvényes témakör -aminek óraszámát figyelembe kell venni
		    selectElement.addClassName('temakorOraszam');
		    // és a bejegyzést mozgathatóvá, rendezhetővé kell tenni
		    element.up('li').addClassName('sortable');
		    Sortable.create("temakor",
			{scroll: window, dropOnEmpty:true, elements:$$('#temakor li.sortable'),handle:'handle',containment:["temakor"],constraint:false,onChange:initTemakorLista}
		    );
		    // Az új elemet felvesszük a lista végére
		    element.up('ul').appendChild(newLi);
		    $(newLi).down('select').focus();

		    textareaElements.push(new textareaObject($(newLi).down('textarea')));

		    // inicializáljuk a listát - hisz lett egy új érvényes óraszámunk
		    initTemakorLista();
		}
	}

}

doOnTemakorDelete = function(event) {
        var element = $(Event.element(event));
	// Témakör törlése
        if (element.hasClassName('close')) {
	    element.up('li').remove();
	    initTemakorLista();
// Ez nem műxik így... //	    element.fire('mayor:change');
        }
}



var textareaObject = Class.create();
    textareaObject.prototype = {
        initialize: function(element) {
            this.element = $(element);
            this.element.observe('focus',this.resize.bindAsEventListener(this));
            this.element.observe('blur',this.restore.bindAsEventListener(this));
        },

        resize: function(evt, extraInfo) {
		this.element.setStyle({backgroundColor: '#eed', height: '100px'});
                Event.stop(evt);
        },
        restore: function(evt, extraInfo) {
		this.element.setStyle({backgroundColor: '#fff', height: '50px'});
                Event.stop(evt);
        }
    }

var textareaElements = new Array();


function myPSFLoader(evt) {

    initTemakorLista();	// lista inicializálása
    Sortable.create(	// a lista tételeinek mozgathatóvá tétele
	"temakor",
	{dropOnEmpty:true, elements:$$('#temakor li.sortable'),handle:'handle',containment:["temakor"],constraint:false,onChange:initTemakorLista}
    );
    // Új eseménykezelők hozzáadása
    Event.observe(document.body, 'mayor:change',  doOnTemakorChange);
    Event.observe(document.body, 'click', doOnTemakorDelete);
    Event.observe(document.body, 'change', doOnTemakorChange);

    $$('#temakor textarea').each(
        function (elem, index) {
            textareaElements.push(new textareaObject(elem));
        }
    );


}
