Event.observe(window, 'load', myPSFLoader, false);

function myPSFLoader(evt) {
    Event.observe(document.body, 'click', function(event) {
	var element = $(Event.element(event));

        if (element.hasClassName('setAll')) {
	    tableElement = element.next('td').down('table');
	    tableElement.select('input[type="checkbox"]').each(
    		function (elem, index) {
		    elem.checked = !elem.checked;
    		}
	    );
	}
    });
}
