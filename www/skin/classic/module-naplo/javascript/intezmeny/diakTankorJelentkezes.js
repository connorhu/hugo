Event.observe(window, 'load', myPSFLoader, false);


function myPSFLoader(evt) {

    $$('table.tankor tr').each(
        function (elem, index) {

	    if (elem.getAttribute('class') != null) {
		Event.observe(elem, 'mouseover', function(event) {

    		    var element = $(Event.element(event));
		    var trElement = element.up('tr');
		    var classNames = $w(trElement.className.split('benne').join(' '));
		    if (classNames.length > 0) {
			var tdElements = $$('tr.'+classNames.join(' td, tr.')+' td');
			for (i=0; i<tdElements.length; i++) {
			    if (tdElements[i].up('tr') != trElement) tdElements[i].addClassName('utkozik');
			}
		    }

		});
		Event.observe(elem, 'mouseout', function(event) {

    		    var element = $(Event.element(event));
//		    var classNames = $w(element.up('tr').className);
		    var trElement = element.up('tr');
		    var classNames = $w(trElement.className.split('benne').join(' '));
		    if (classNames.length > 0) {
			var tdElements = $$('tr.'+classNames.join(' td, tr.')+' td');
			for (i=0; i<tdElements.length; i++) tdElements[i].removeClassName('utkozik');
		    }
		});

	    }

        }
    );
}
