Event.observe(window, 'load', myPSFLoader, false);

function myPSFLoader(evt) {
    $$('select.zaradek').each(
        function (elem, index) {
	    Event.observe(elem, 'change', function(event) {
    	      var element = $(Event.element(event));
	      if ($F(element) == '') {
		element.previous('input[type=submit]').hide();
		element.next('div').innerHTML='';
	      } else {
		var zArray = element.options[element.selectedIndex].innerHTML.split('%');
		var zTxt = '<p>';
		var tableTxt = '<table>';
		for (i=0;i<zArray.length;i++) {
		    if (i % 2 == 0) {
			zTxt = zTxt + zArray[i];
		    } else {
			zTxt = zTxt + '<span id="'+element.getAttribute('id')+i+'span'+'">'+zArray[i]+'</span>';
			tableTxt = tableTxt + '<tr><th><label for="'+element.getAttribute('id')+i+'">'+zArray[i]+'</label></th>';
			tableTxt = tableTxt + '<td><input type="text" name="values[]" value="" id="'+element.getAttribute('id')+i+'" class="onChangeReplaceSpan" /></td></tr>';
		    }
		}
		tableTxt = tableTxt + '</table>';
		zTxt = '<p>' + zTxt + '</p>' + tableTxt;
		element.next('div').innerHTML=zTxt;
		element.next('div').select('table input[type=text]').each(
    		    function (elem, index) {
			Event.observe(elem, 'change', function(event) {
    			    var element = $(Event.element(event));
			    $(element.getAttribute('id')+'span').innerHTML=element.value.escapeHTML(); 
			}); // Event.observe
    		    }
		);
		element.previous('input[type=submit]').show();
	      }
	    }); // Event.observe
        }
    );
}
