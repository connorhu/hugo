Event.observe(window, 'load', myPSFLoader, false);

function myPSFLoader(evt) {
    $$('table.szemeszter input[type=text]').each(
        function (elem, index) {
	    Event.observe(elem, 'change', function(event) {
    		var element = $(Event.element(event));
		$('c'+element.getAttribute('id').substring(1)).checked=(element.value != ''); 
	    }); // Event.observe
        }
    );
}
