Event.observe(window, 'load', myPSFLoader, false);

function myPSFLoader(evt) {
    $$('form.zaradek table input[type=text]').each(
        function (elem, index) {
	    Event.observe(elem, 'change', function(event) {
    		var element = $(Event.element(event));
		$(element.getAttribute('id')+'span').innerHTML=element.value.escapeHTML(); 
	    }); // Event.observe
        }
    );
}
