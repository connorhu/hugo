Event.observe(window, 'load', myPSFLoader, false);

function myPSFLoader(evt) {

    Event.observe(document.body, 'click', function(event) {
        var element = $(Event.element(event));

        if (element.hasClassName('gotoCreateAccount')) {
                // A gotoCreateAccount elemre kattintva a inUserAccount értéket a href-hez illesztjük...
		var userAccount = $F(element.previous());
		if (userAccount == '') {
		    userAccount = element.getAttribute('title');
		    element.previous().writeAttribute('value',userAccount);
		    element.up('form').request();
		}
		element.writeAttribute('href', element.getAttribute('href').gsub(/userAccount=([^&]*)/,'userAccount='+userAccount));

        }
    });

    /* Jogviszonyváltozás kezelése - záradékok és egyéb paraméterek megjelenítése */
    $$('.rejtett').each(
        function(elem, index) { elem.hide(); }
    );

    $('jogviszonyValtasSelect').observe('change', function(event) {
	var element = $(Event.element(event));
	var statusz = $F(element);
	$$('.rejtett').each(
    	    function(elem, index) { elem.hide(); }
	);
	if (statusz == 'jogviszonya lezárva') {
	    $('lezaras').show();
	    if ($F('lezarasZaradek') == 41) $('igazolatlan').show();
	    else if ($F('lezarasZaradek') == 45) $('iskola').show();
	} else if (statusz == 'jogviszonya felfüggesztve') $('felfuggesztes').show();
    });

    $('lezarasZaradek').observe('change', function(event) {
	var zaradekIndex = $F(Event.element(event));
	if (zaradekIndex == 41) $('igazolatlan').show();
	else $('igazolatlan').hide();
	if (zaradekIndex == 45) $('iskola').show();
	else $('iskola').hide();
    });

    /*
	Az oktatási azonosító
	11 jegyű, az első jegye 7, a 11. jegye ellenőrző kód: sum(i*xi) % 11, ahol a 10-es maradék nem megengedett

	    9. számú melléklet a 79/2006. (IV. 5.) Korm. rendelethez
		A hallgatói és az oktatói azonosító szám képzésének szabálya
		1. Az azonosító szám tizenegy jegyű szám.
		2. Az azonosító szám képzése:
		    a) az 1. számjegy konstans 7-es szám,
		    b) a 2-10. számjegyek összessége egy garantáltan egyedi,
			véletlenszerűen generált szám,
		    c) a 11. számjegy az 1-10. számjegyek felhasználásával, matematikai
			módszerekkel képzett ellenőrző szám.
		3. Az azonosító szám 11. számjegyét úgy kell képezni, hogy a 2. a)-b)
		    pontok szerint képzett 10 számjegy mindegyikét szorozni kell azzal a
		    sorszámmal, ahányadik helyet foglalja el az azonosító számon belül.
		    (Első számjegy szorozva eggyel, második számjegy szorozva kettővel és
		    így tovább.)
		    Az így kapott szorzatok összegét el kell osztani 11-gyel, és az osztás
		    maradéka a 11. számjeggyel lesz egyenlő.

		A 2. b) pont szerinti sorszám nem adható ki, ha a 11-gyel való osztás
		maradéka egyenlő tízzel.
    */
    $('oId').observe('change', function(event) {

	var element = $(Event.element(event));
	var oId = $F(element);

        var oIdString = String(oId);
        if (oIdString.length != 11) {
            alert('Hibás oktatási azonosító! (nem 11 jegyű)');
	    setTimeout('document.getElementById(\'oId\').activate()',100);
            return false;
        }
        if (oIdString.substring(0,1) != '7') {
            alert('Hibás oktatási azonosító! (első jegy nem 7-es)');
	    setTimeout('document.getElementById(\'oId\').activate()',100);
            return false;
        }
        var chk = 0;
        for (i = 0; i < 10; i++) {
            chk += (i+1)*oIdString.substring(i,i+1);
        }
        chk = chk % 11;
        if (chk == 10) {
            alert('Hibás oktatási azonosító! (a 11-el vett osztási maradék nem lehet 10)');
	    setTimeout('document.getElementById(\'oId\').activate()',100);
            return false;
        }
        if (chk != +oIdString.substring(10)) {
            alert('Hibás oktatási azonosító! (Az ellenőrző összeg nem egyezik az utolsó számjeggyel)');
	    setTimeout('document.getElementById(\'oId\').activate()',100);
            return false;
        }
        return true;
    });
    /*
	Adóazonosító
	10 jegyű, 
	az 1. jegy 8
	!! - a 2-6. számjegyek a személy születési időpontja és az 1867. január 1. között eltelt napok száma,
	a 10. jegye ellenőrző kód: sum(i*xi) % 11, ahol a 10-es maradék nem megengedett
    */
    if ($('adoazonosito')) $('adoazonosito').observe('change', function(event) {

	var element = $(Event.element(event));
	var aa = $F(element);

        var aaString = String(aa);
        if (aaString.length != 10) {
            alert('Hibás adóazonosító! (nem 10 jegyű)');
	    setTimeout('document.getElementById(\'adoazonosito\').activate()',100);
            return false;
        }
        if (aaString.substring(0,1) != '8') {
            alert('Hibás adóazonosító! (első jegy nem 8-as - magánszemély)');
	    setTimeout('document.getElementById(\'adoazonosito\').activate()',100);
            return false;
        }
        var chk = 0;
        for (i = 0; i < 9; i++) {
            chk += (i+1)*aaString.substring(i,i+1);
        }
        chk = chk % 11;
        if (chk == 10) {
            alert('Hibás adóazonosító! (a 11-el vett osztási maradék nem lehet 10)');
	    setTimeout('document.getElementById(\'adoazonosito\').activate()',100);
            return false;
        }
        if (chk != +aaString.substring(9)) {
            alert('Hibás adóazonosító! (Az ellenőrző összeg nem egyezik az utolsó számjeggyel)');
	    setTimeout('document.getElementById(\'adoazonosito\').activate()',100);
            return false;
        }
        return true;
    });
    /*
	A TAJ
	9 jegyű, az első 8 egy folyamatos sorszám, a 9. CDV kód: sum(1-4)(3*x(2i-1)+7*x(2i)) % 10
    */
    $('tajSzam').observe('change', function(event) {

	var element = $(Event.element(event));
	var tajSzam = $F(element);

        var tajString = String(tajSzam);
        if (tajString.length != 9) {
            alert('Hibás Társadalombiztosítási Azonosító Jel! (nem 9 jegyű)');
	    setTimeout('document.getElementById(\'tajSzam\').activate()',100);
            return false;
        }
        var chk = 0;
        for (i = 0; i < 4; i++) {
            chk += 3*tajString.substring(2*i,2*i+1)+7*tajString.substring(2*i+1,2*i+2);
        }
        chk = chk % 10;
        if (chk != +tajString.substring(8)) {
            alert('Hibás Tásradalombiztosítási Azonosító Jel! (Az ellenőrző összeg nem egyezik az utolsó számjeggyel)');
	    setTimeout('document.getElementById(\'tajSzam\').activate()',100);
            return false;
        }
        return true;
    }	);
    /*
	A Diákigazolvány szám
	10 jegyű
    */
    $('diakigazolvanySzam').observe('change', function(event) {

	var element = $(Event.element(event));
	var dSzam = $F(element);

        var dString = String(dSzam);
        if (dString.length != 10) {
            alert('Hibás Diákigazolvány szám! (nem 10 jegyű)');
	    setTimeout('document.getElementById(\'diakigazolvanySzam\').activate()',100);
            return false;
        }
        return true;
    }	);
    /*
        Születési év
        4 jegyű
    */
    if ($('szuletesiEv')) $('szuletesiEv').observe('change', function(event) {

        var element = $(Event.element(event));
        var dSzam = $F(element);

        var dString = String(dSzam);
        if (dString.length != 4) {
            alert('Hibás évszám! (nem 4 jegyű)');
            setTimeout('document.getElementById(\'szuletesiEv\').activate()',100);
            return false;
        }
        return true;
    }   );

}

