Event.observe(window, 'load', myPSFLoader, false);

function myPSFLoader(evt) {

    /*
	Születési év
	4 jegyű
    */
    $('szuletesiEv').observe('change', function(event) {

	var element = $(Event.element(event));
	var dSzam = $F(element);

        var dString = String(dSzam);
        if (dString.length != 4) {
            alert('Hibás évszám! (nem 4 jegyű)');
	    setTimeout('document.getElementById(\'szuletesiEv\').activate()',100);
            return false;
        }
        return true;
    }	);
}

