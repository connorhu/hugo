
 Event.observe(window, 'load', myPSFLoader, false);

showUpdateLayer = function(option) {
    takaroElem = $('takaro');
    if (takaroElem) takaroElem.show();
    $('updateWindow').show();
    $('updateForm').writeAttribute('tabindex',1);
    $('updateForm').focus();
}

processJSON = function(json) {

    if (json.action == 'jegyModositas') {
	var tdElement = $('td-'+json.data.diakId+'-'+json.data.dolgozatId);
	var aElement = $('jegy-'+json.data.jegyId);
	var oldDid = (aElement.up('td').id.split('-'))[2];
	if (tdElement) tdElement.insert(aElement); // áthelyzés a megfelelő dolgozat alá
	aElement.removeClassName('jegy1');
	aElement.removeClassName('jegy2');
	aElement.removeClassName('jegy3');
	aElement.removeClassName('jegy4');
	aElement.removeClassName('jegy5');
	aElement.addClassName('jegy'+json.data.tipus);
	aElement.update(json.data.jegyStr); // jegy módosítása

	if (json.data.dolgozatId != oldDid) updateDolgozatAtlag(oldDid); // eredeti dolgozat átlaga
	updateDolgozatAtlag(json.data.dolgozatId); // új dolgozat átlaga

	$('updateWindow').hide();
	$('takaro').hide();
    } else if (json.action == 'jegyTorles') {
	var aElement = $('jegy-'+json.data.jegyId);
	aElement.remove();
	$('updateWindow').hide();
	$('takaro').hide();
    } else {
	alert('processJSON: ismeretlen visszatérési érték!');
    }

}

updateDolgozatAtlag = function(did) {
	var sum=0, db=0;
	$$('td.d'+did+' a').each(function(elem, index) {
	    sum = sum + parseInt(elem.innerHTML); db++;
	    if (elem.innerHTML.include('/')) sum = sum + 0.5;
	});
	avg=(sum/db);
	avgElement = $('avg-d'+did);
	if (avgElement) avgElement.update(avg.toFixed(2));
}

function myPSFLoader(evt) {

   // Csoportos jegy beírásakor ellenőrizzük, hogy ki lett-e választva dolgozat
    var CheckDolgozatObject = Class.create();
    CheckDolgozatObject.prototype = {
        initialize: function(element) {
            this.element = $(element);
	    this.eDid = $('did');
            this.element.observe('click',this.checkDolgozat.bindAsEventListener(this));
        },

        checkDolgozat: function(evt, extraInfo) {

    	    if (this.eDid.value == '') {
        	alert(this.eDid.getAttribute('title'));
		Event.stop(evt);
    	    }

        }
    }

    var checkDolgozatElements = new Array();
    $$('.check').each(
        function (elem, index) {
            checkDolgozatElements.push(new CheckDolgozatObject(elem));
        }
    );

    Event.observe(document.body, 'keyup', function(event) {
        var element = $(Event.element(event));
        var charKeyCode = event.keyCode ? event.keyCode : event.which;
        // input elem esetén ne legyen semmi
        if(!element.match('input') && !element.match('textarea') && !element.match('select')) {
            if (charKeyCode==46 && $('updateWindow').visible()) { // Del gomb
		$('jegyTorles').click();
	    }
	}
    });

    var mydrag = new Draggable('updateWindow', { scroll: window, handle: $('updateHeader') });
}
