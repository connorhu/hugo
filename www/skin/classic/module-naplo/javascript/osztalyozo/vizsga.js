
    Event.observe(window, 'load', myPSFLoader, false);

    hideOptions = function(event) {
	var element = $(Event.element(event));
	if (element.hasClassName('jegyTipus')) {
	    var jegyek = element.next('select');
	    if (element.selectedIndex == 0) {
		jegyek.childElements().each(
		    function(element, index) { element.show(); }
		);		
	    } else {
		jegyek.selectedIndex = 0;
		jegyek.childElements().each(
		    function(element, index) { element.hide(); }
		);
		var selector='optgroup[label="'+$F(element)+'"]';
		jegyek.select(selector).each(
		    function(element, index) { element.show(); }
		);
	    }
	}
	if (element.hasClassName('jegyAdat')) {
	    var tipusok = element.previous('select');
	    var option = element.options[element.selectedIndex];
	    var jegyTipus = $(option).up('optgroup').getAttribute('label');
	    index = 0;
	    while (tipusok.options[index].value != jegyTipus && index < tipusok.options.length && index < 100) index++;
	    if (index < tipusok.options.length) tipusok.selectedIndex=index;
	    else tipusok.selecedIndex=0;
	}
    }

    function myPSFLoader(evt) {
	// Halasztás elrejtése
	$$('div.rejtett').each(
	    function(elem, index) {
		elem.hide();
	    }
	);

	Event.observe(document.body, 'click', function(event) {
    	    var element = $(Event.element(event));

	    // értékeléskor...
    	    if (element.hasClassName('ertekeles')) {
                // ... a halasztasDt üresreállítása
		var container = $(element.up('.hideShowContainer'));
		var halasztasDt = $(container.getElementsByClassName('halasztasDt'));
		$A(halasztasDt).each(
                        function(element, index) {
                                element.value = '';
                        }
                );
    	    }
	    // halasztáskor...
    	    if (element.hasClassName('halasztas')) {
		// ... a jegytípus/jegy kiválasztást megszüntetjük
		var container = $(element.up('.hideShowContainer'));
		var jegyTipusElements = container.select('.jegyTipus');
		var jegyek = jegyTipusElements[0].next('select');
		jegyTipusElements[0].selectedIndex=0;
		jegyek.selectedIndex=0;
		jegyek.childElements().each(
		    function(element, index) {
			element.show();
		    }
		);
	    }
	})

	Event.observe(document.body, 'mayor:change',  hideOptions);
	Event.observe(document.body, 'change',  hideOptions);

    }
