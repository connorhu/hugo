
Event.observe(window, 'load', myLoader);
// BUG? Event.observe(window, 'load', mayorNavResize);
Event.observe(window, "resize", function() {
    mayorNavResize();
});

function mayorNavResize() {
    var viewport = document.viewport.getDimensions();
    var wi = viewport.width;
    var navElem = $('nav2');
    var pozElem = $('poz');
    var mayorbodyElem = $('mayorbody');
    var mayorfootElem = $('mayorfoot');
//    var mayorheadElem = $('mayorhead');
    if (navElem) {
	if (wi<1024 && navElem.hasClassName('vertical')) {
	    navElem.removeClassName('vertical');
	    mayorbodyElem.removeClassName('leftpad');
	    mayorfootElem.removeClassName('leftpad');
	    navElem.addClassName('horizontal');
	    mayorbodyElem.addClassName('toppad');
	    mayorfootElem.addClassName('toppad');
	    //mayorheadElem.addClassName('small');
	    new Ajax.Request('/index.php?skin=ajax&request=true&page=session&sub=&f=set&policy=public&layout=1', {
		method: 'get',
		onSuccess: function(transport) {
		}
	    });
	} else if (wi>=1024 && navElem.hasClassName('horizontal')) {
	    navElem.removeClassName('horizontal');
	    mayorbodyElem.removeClassName('toppad');
	    mayorfootElem.removeClassName('toppad');
	    navElem.addClassName('vertical');
	    mayorbodyElem.addClassName('leftpad');
	    mayorfootElem.addClassName('leftpad');
	    // mayorheadElem.removeClassName('small');
	    new Ajax.Request('/index.php?skin=ajax&request=true&page=session&sub=&f=set&policy=public&layout=0', {
		method: 'get',
		onSuccess: function(transport) {
		}
	    });
	} else {
	    //console.log('Nothing to do');
	}
    } else {
    	//console.log('Not ready');
    }
}

function myLoader(evt) {

    if ($('help') != null) var helpdrag = new Draggable('help', { scroll: window, handle: $('helpHeader') });
    if (Prototype.Browser.IE) {
	    // input és select elemek change eseménye váltson ki egy másik eseményt - ami felkúszik (bubbling) a body-ig...
	    var changeObject = Class.create();
	    changeObject.prototype = {
		initialize: function (inputElement) {
		    this.inputElement = $(inputElement);
		    this.inputElement.observe('change', this.fireChangeEvent.bindAsEventListener(this));
		},
		fireChangeEvent: function(evt, extraInfo) {
		    this.inputElement.fire('mayor:change');
		}
	    }
	    
	    var changeElements = new Array();
	    $$('input, select, button').each(
		function (elem, index) {
		    changeElements.push(new changeObject(elem));
		}
	    );
	    // form.SUBMIT bubbling
	    $$('form').each(
		function (elem, index) {
		    Event.observe(elem, 'submit', function(evt) {
			var e = $(Event.element(evt));
			e.fire('mayor:submit');
			if (elem.hasClassName('onSubmitUpdate')) {
			    Event.stop(evt);
			    return false;
			}
		    });

		}
	    );
    } // IE hack

    Event.observe(document.body, 'keypress', function(event) {
	var element = $(Event.element(event));
	var charKeyCode = event.keyCode ? event.keyCode : event.which;
	var shiftKey = event.shiftKey ? event.shiftKey : ((charKeyCode == 16) ? true : false);
	var altKey = event.altKey ? event.altKey : ((charKeyCode == 18) ? true : false);
	var ctrlKey = event.ctrlKey ? event.ctrlKey : ((charKeyCode == 17) ? true : false);

	// Caps-lock check
	if ($('capsLockWarning')) {
	    if (
		((charKeyCode >= 65 && charKeyCode <= 90) && !shiftKey)
		|| ((charKeyCode >= 97 && charKeyCode <= 122) && shiftKey)
	    ) { // Caps-Lock ON
		$(capsLockWarning).show();
	    } else { // Caps-Lock OFF
		$(capsLockWarning).hide();
	    }
	}

	// input elem esetén ne legyen semmi
	if(!element.match('input') && !element.match('textarea') && !element.match('select')) {
	    var location = window.location.toString();
	    if (charKeyCode==63) {
		$('takaro').toggle();
		$('keyHelp').toggle();
	    } else if (charKeyCode==83) { // S: settings
		$('settings').toggle();
	    } else if (charKeyCode==68) { // D: debug
		$('debug').toggle();
	    } else if (charKeyCode==104) {
		alert('Alt-H: HELP: '+event.altKey);
	    } else if (charKeyCode==76) { // L: login/logout
		if (location.include('policy=private') || location.include('policy=parent')) {
		    window.location = window.location.toString().gsub(/page=([^&]*)/,'page=session').gsub(/sub=([^&]*)/,'sub=').gsub(/f=([^&]*)/,'f=logout').gsub(/policy=([^&]*)/,'policy=public');
		} else {
		    if (!location.include('?')) {
			window.location = window.location+'?page=auth&sub=&f=login&policy=public';
		    } else {
			window.location = window.location.toString().gsub(/page=([^&]*)/,'page=auth').gsub(/sub=([^&]*)/,'sub=').gsub(/f=([^&]*)/,'f=login').gsub(/policy=([^&]*)/,'policy=public');
		    }
		}
	    } else {
//alert('Key: '+charKeyCode+' Ctrl: '+ctrlKey);
//    		alert("Pressed: "+charKeyCode.toString()+" "+element+" Ctrl: "+ctrlKey+" Alt: "+altKey+" Shift: "+shiftKey);
	    }
	}

    });

    Event.observe(document.body, 'keydown', function(event) { // ekkor csak nagybetűs kódokat kapunk!

	var element = $(Event.element(event));
	var charKeyCode = event.keyCode ? event.keyCode : event.which;
	var shiftKey = event.shiftKey ? event.shiftKey : ((charKeyCode == 16) ? true : false);
	var altKey = event.altKey ? event.altKey : ((charKeyCode == 18) ? true : false);
	var ctrlKey = event.ctrlKey ? event.ctrlKey : ((charKeyCode == 17) ? true : false);
	var location = window.location.toString();

	// Esc - WebKit csak keyup esetén
	if (charKeyCode==27) { // Esc
	    $('takaro').hide();			// takaró elem
	    $('keyHelp').hide();		// gyorsbillentyűk
	    $('help').hide();			// súgó
	    if ($('inform') != null) $('inform').up('div').hide();	// haladási/tanmenet
	    if ($('haladform') != null) $('haladform').up('div').hide();	// haladási/tankör órái
	    if ($('jegyAdat') != null) $('jegyAdat').hide();		// osztályozó/tankor
	    $('updateWindow').hide();		// update ablak
	} else if (charKeyCode==112) { // F1 - Help // charKeyCode==121 F10 
	    params = window.location.toString().toQueryParams();
	    var page=params.page?params.page:'';
	    var sub=params.sub?params.sub:'';
	    var f=params.f?params.f:'';
	    //var url = "wiki/dok.php?id="+page+":"+sub+":"+f;
	    var url = "http://wiki.mayor.hu/doku.php?id="+page+":"+sub+":"+f;
	    var helpElement = $('help');
	    if (typeof(helpElement) == 'undefined' || helpElement == null) {
		window.open(url,'help');
	    } else {
		if (helpElement.getStyle('display') == 'none') helpElement.down('iframe').src=url;
		helpElement.toggle();
	    }
	    Event.stop(event);
	} else if (charKeyCode==86 && ctrlKey && shiftKey) { // Ctrl-Shift-V: Vkbarát nézet
		if (location.include('skin')) {
		    window.location = location.gsub(/skin=([^&]*)/,'skin=vakbarat');
		} else {
		    if (location.include('?')) window.location = location+'&skin=vakbarat';
		    else window.location = location+'?skin=vakbarat';
		}
	} else {
//    	    alert("Pressed: "+charKeyCode.toString()+" "+element+" Ctrl: "+ctrlKey+" Alt: "+altKey+" Shift: "+shiftKey);
//Event.stop(event);
	}

    });

    Event.observe(document.body, 'click', function(event) {
	var element = $(Event.element(event));

	if (element.hasClassName('confirm')) {
	    // A confirm elemre kattintva a form jóváhagyását előbb jóvá kell hagyni
	    if (!confirm(element.getAttribute('title'))) Event.stop(event);
	}
	if (element.getAttribute('id') == 'updateCloseButtonClick') {
	    if ($('updateWindow')) $('updateWindow').hide();
	    if ($('takaro')) $('takaro').hide();
	}
	if (element.hasClassName('onClickUpdateAction')) {
	    // A megnyomott submit gombtól függ az include name=action
            var inArray = element.up('form').select('input[name=action]');
            $(inArray[0]).setValue(element.getAttribute('id'));
	}
	if (element.hasClassName('onClickUpdateWindow')) {
	    // A kattintott elem href paraméteréből vett url-ből kiveszi a getParameters-ben megadott
	    // paramétereken kívüli paramétereket, ezeket post paraméterként rakja az updateForm-ba.
	    // Az url "maradéka" pedig a form action-be kerül, végül update-eli a form-ot...
	    formElement = $('updateForm');
	    formElement.update();
	    var origUrl = '', title = '';
	    if (element.hasAttribute('title')) $('updateHeader').update(element.getAttribute('title'));
	    if (element.hasAttribute('href')) origUrl = element.getAttribute('href');
	    var pos = origUrl.indexOf("?");
	    if (pos != -1) url = origUrl.substr(0,pos);
	    else url = origUrl;
	    var getParameters = ["page", "sub", "f", "lang", "sessionID", "policy"];
	    var separator = '?';
	    $H(origUrl.toQueryParams()).each(function(pair) {
		if (getParameters.indexOf(pair.key) != -1) {
		    url = url+separator+pair.key+'='+pair.value;
		    separator='&';
		} else if (pair.key == 'skin') {
		    url = url+separator+'skin=ajax';
		    separator='&';
		} else {
		    formElement.insert({ bottom: new Element('input', {type : 'hidden', name : pair.key, value : pair.value}) });
		}
	    });
	    formElement.writeAttribute('action',url);
	    updateForm(formElement, element);
	    Event.stop(event);
	}
	if (element.hasClassName('onClickHideShow')) {
	    	// Az onClickHideShow elem kattintásra becsukja/kinyitja az őt tartalmazó
	    	// hideShowContainer elemen belüli openable elemeket
	    	var container = element.up('.hideShowContainer');
	    	var openable  = container.getElementsBySelector('.openable');
		// var openable = $$('.openable');
	    	$A(openable).each(
				function(element, index) {
		    		element.toggle(); 
				}
	    	);
	}
	if (element.hasClassName('onClickShow')) {
	    // Az onClickShow elem kattintásra kinyitja a hozzárendelt elemet, miközben a többi
	    // őt tartalmazó hideShowContainer-en belüli openable elemet becsukaja. A hozzárendelés
	    // az id-ben történik: 'show'+ElemId
	    	var container = element.up('.hideShowContainer');
	    	var openable  = container.getElementsBySelector('.openable');
		var onclickshow = container.getElementsBySelector('.onClickShow');
	    	$A(openable).each(function(element, index) { element.hide(); });
		$A(onclickshow).each(function(element, index) { element.removeClassName('selected'); });
		var id = element.getAttribute('id');
		if (id && id.substring(0,4) == 'show') {
		    var openableId = id.charAt(4).toLowerCase()+id.substring(5);
		    $(openableId).show();
		}
		element.addClassName('selected');
	}
        if (element.hasClassName('onClickHide')) {
            // Az onClickHide elem kattintásra becsukja a hozzárendelt elemet. A hozzárendelés
            // az id-ben történik: 'hide'+ElemId+[':'+elemId2...]
                var id = element.getAttribute('id');
                if (id && id.substring(0,4) == 'hide') {
                    var openableId = (id.charAt(4).toLowerCase()+id.substring(5)).split(':');
                    openableId.each(function(elem, index){ $(elem).hide(); });
                }
        }
	if (element.hasClassName('onClickUpdate')) {
	    	// Az onClickUpdate class tagjaira kattintva update-eli az őket tartalmazó formot
		var formElement = element.up('form');
	    	updateForm(formElement, element);
	    	Event.stop(event);
	}
	if (element.hasClassName('onClickDisable')) {
		element.disable();
	    	var container = element.up('form');
	    	var input = container.getElementsBySelector('.onClickDisableValue');
//	    	var input = $$('.onClickDisableValue');
	    	$A(input).each(
			function(element, index) {
		    		element.value='true';
			}
	    	);
		container.submit();
		Event.stop(event);
	}

    });

    doOnChange = function(event) {

	    var element = $(Event.element(event));
	    if (element.hasClassName('onChangeSubmit') && !window.location.toString().include('skin=vakbarat')) {
	    	    element.up('form').submit();
	    }
	    if (element.up('form.onChangeRequest')) {
	    	var formElement = $(element.up('form.onChangeRequest'));

	    	if (typeof formElement.getAttribute('action') == 'string') var uriOrig = formElement.getAttribute('action');
		else var uriOrig = formElement.attributes.item('action').nodeValue;

		if (typeof formElement.getAttribute('action') == 'string') var uri = formElement.getAttribute('action').gsub(/skin=([^&]*)/,'skin=ajax')+'&request=true';
		else var uri = formElement.attributes.item('action').nodeValue.gsub(/skin=([^&]*)/,'skin=ajax')+'&request=true';

		// class="ajaxOption-ban megadott value="onChangeRequest" tudja tiltani az eseményt
		var ajaxOptions = $A(formElement.getElementsBySelector('input.ajaxOption'));
		var ajaxRequest = true;
		ajaxOptions.each(
			function (elem, index) {
			    if ($F(elem) == 'onChangeRequestStop') ajaxRequest = false;
			}
		);		
/* EXPERIMENTAL */
		var noSubmit = $A(formElement.getElementsBySelector('.DOA')); // disabled on ajax req
		noSubmit.each(
			function (elem, index) {
			    elem.disabled = true;
			    elem.addClassName('ajaxRequest');
			}
		);		
/* -- */
		if (typeof formElement.getAttribute('action') == 'string') formElement.setAttribute('action', uri);
		else formElement.attributes.item('action').nodeValue=uri;

		element.addClassName('ajaxRequest');
		if (ajaxRequest) {
		    formElement = $(formElement);

    		    requestForm(formElement, {
        		onSuccess: function() {
		    		element.addClassName('ajaxRequestDone');
		    		element.removeClassName('ajaxRequest');
		    		element.removeClassName('ajaxRequestError');
				noSubmit = $A(formElement.getElementsBySelector('.DOA')); // disabled on ajax req
				noSubmit.each(
				    function (elem, index) {
					elem.disabled = false;
					elem.removeClassName('ajaxRequest');
				    }
				);		
			},
        		onFailure: function(y) {
		    		element.addClassName('ajaxRequestError'); 
		    		element.removeClassName('ajaxRequest');
		    		element.removeClassName('ajaxRequestDone');
				noSubmit = $A(formElement.getElementsBySelector('.DOA')); // disabled on ajax req
				noSubmit.each(
				    function (elem, index) {
					elem.disabled = false;
					elem.removeClassName('ajaxRequest');
				    }
				);
				$('updateWindow').show();
				$('updateHeader').innerHTML = "Error!";
				$('updateForm').innerHTML = y.responseJSON.alertHTML;
				setTimeout( function() {$('updateWindow').hide()},10000);
			}
    		    });
		}
		if (typeof formElement.getAttribute('action') == 'string') formElement.setAttribute('action', uriOrig);
		else formElement.attributes.item('action').nodeValue=uriOrig;
    	    }
	    var formElement = $(element.up('form'));
	    if (element.hasClassName('onChangeUpdate') || formElement.hasClassName('onChangeUpdate')) {
	    	// Az onChangeUpdate class tagjaira kattintva update-eli az őket tartalmazó formot
	    	if (element.tagName.toLowerCase() == 'form') var formElement = element;
	    	else var formElement = element.up('form');
	    	updateForm(formElement, element);
	    	Event.stop(event);
	    }
    }

    doOnSubmit = function(event) {
		var element = $(Event.element(event));
		if (element.tagName.toLowerCase() == 'form') var formElement = element;
		else var formElement = element.up('form');
		if (formElement.hasClassName('onSubmitUpdate')) {
	    	// Az onSubmitUpdate class-ba sorolt formok submit esetén update-elve lesznek - !! az összes submit elmegy !!
			updateForm(formElement, element);
			Event.stop(event);
		}
    }

    Event.observe(document.body, 'mayor:change',  doOnChange);
    Event.observe(document.body, 'change', doOnChange);

    Event.observe(document.body, 'mayor:submit',  doOnSubmit);
    Event.observe(document.body, 'submit', doOnSubmit);

// --------------------------

    var OnLoadUpdateObject = Class.create();
    OnLoadUpdateObject.prototype = {
        initialize: function(element) {
	    if ($(element).tagName.toLowerCase() == 'form') this.element = $(element);
	    else this.element = $(element).up('form');

	    if (typeof this.element.getAttribute('action') == 'string') this.url = this.element.getAttribute('action').gsub(/skin=([^&]*)/,'skin=ajax');
	    else this.url = this.element.attributes.item('action').nodeValue.gsub(/skin=([^&]*)/,'skin=ajax');

            this.myAjax = new Ajax.mayorUpdater(
                this.element,
                this.url,
                {
                    method: this.element.getAttribute('method'),
                    parameters: this.element.serialize(true),
		    evalScripts: false,
                    onComplete: function() {
                            if (Prototype.Browser.IE) { // bubbling
                                var changeElements = new Array();
                                element.select('input, select, button').each(
                                    function (elem, index) {
                                        changeElements.push(new changeObject(elem));
                                    }
                                );
                            }
                        },
		    onSuccess: function(transport) {
			var json = transport.responseText.evalJSON(true);
			// json.html
		    }
                }
            )
        }
    }

    var onLoadUpdateElements = new Array();
    $$('.onLoadUpdate').each(
        function (elem, index) {
            onLoadUpdateElements.push(new OnLoadUpdateObject(elem));
        }
    );


// -----------------------------

    // A focus id-t használhatjuk automatikus fokuszálásra...
    if (e = $('focus')) e.activate();

    // A loadUrl id-t használhatjuk automatikus betöltésre... hol használunk ilyet? Például a download oldalon.
    if (e = $('loadUrl')) window.location.replace(e.getAttribute('href'));

    // Date picker
    var skin = window.location.href.toQueryParams().skin;
    var dateArray = $$('.date');
    var datetimeArray = $$('.datetime');
    if (dateArray.length > 0 || datetimeArray.length > 0) {
        includeJS(
	    'skin/classic/share/javascript/calendar_date_select/javascripts/calendar_date_select/calendar_date_select.js',
	    'skin/classic/share/javascript/calendar_date_select/javascripts/calendar_date_select/format_mayor.js',
//	    'skin/classic/share/javascript/calendar_date_select/javascripts/calendar_date_select/format_hyphen_ampm.js',
	    'skin/classic/share/javascript/calendar_date_select/javascripts/calendar_date_select/locale/hu.js'
        );
        includeCSS('skin/classic/share/javascript/calendar_date_select/stylesheets/calendar_date_select/red.css');
        includeCSS('skin/'+skin+'/share/css/calendar_date_select.css');
        var imgStr = '<img alt="Calendar" onclick="new CalendarDateSelect( $(this).previous(), {time:false, year_range:10, onchange: function() { $(this).fire(\'mayor:change\'); } } );" src="skin/classic/share/javascript/calendar_date_select/images/calendar_date_select/calendar.gif" style="margin: 0px 2px; position: relative; top: 4px; border:0px; cursor:pointer;" />';
        $$('.date').each(
            function (elem, index) {
                $(elem).insert({after: imgStr});
            }
        );
        var imgStr = '<img alt="Calendar" onclick="new CalendarDateSelect( $(this).previous(), {time:true, year_range:10, onchange: function() { $(this).fire(\'mayor:change\'); } } );" src="skin/classic/share/javascript/calendar_date_select/images/calendar_date_select/calendar.gif" style="margin: 0px 2px; position: relative; top: 4px; border:0px; cursor:pointer;" />';
        $$('.datetime').each(
            function (elem, index) {
                $(elem).insert({after: imgStr});
            }
        );
    }



    if ($('SESSION_MAX_IDLE_TIME') != undefined) {
	var sessionMaxIdleTime = $F('SESSION_MAX_IDLE_TIME');
	if (sessionMaxIdleTime>0) setTimeout('mayorSessionTimedOut()',sessionMaxIdleTime);
    }
    if ($('SESSION_ALERT_TIME')!= undefined) {
	var sessionAlertTime = $F('SESSION_ALERT_TIME');
	if (sessionAlertTime>0) setTimeout('mayorSessionTimeOutAlert()',sessionAlertTime);
    }
    mayorNavResize();
}

/**
******************************************************************************************************************
******************************************************************************************************************
******************************************************************************************************************
******************************************************************************************************************
**/


function includeJS() {
    var headElement = document.getElementsByTagName('head').item(0);
    for (i = 0; i < arguments.length; i++) {
        included = false;
        url = arguments[i];
        $$('script').each(
            function (elem, index) {
                if (elem.getAttribute('src') == url) included = true;
            }
        );
        if (!included) {
//            var js = document.createElement('script');
//            js.setAttribute('language', 'javascript');
//            js.setAttribute('type', 'text/javascript');
//            js.setAttribute('src', url);
//            headElement.appendChild(js);
	    $(headElement).insert({
		bottom: new Element('script', {language: 'javascript', type: 'text/javascript', src: url})
	    });
        }
    }
}

function includeCSS() {
    var headElement = document.getElementsByTagName('head').item(0);
    for (i = 0; i < arguments.length; i++) {
        included = false;
        url = arguments[i];
        $$('link').each(
            function (elem, index) {
                if (elem.getAttribute('href') == url) included = true;
            }
        );
        if (!included) {
	    $(headElement).insert({
		bottom: new Element('link', {rel: 'Stylesheet', media: 'screen', type: 'text/css', href: url})
	    });
	}
    }
}

// Hasznos függvények

// string funkciók
function LTrim(str) { return str.replace(/\s*((\S+\s*)*)/, "$1"); }
function RTrim(str) { return str.replace(/((\s*\S+)*)\s*/, "$1"); }
function Trim(str)  { return LTrim(RTrim(str)); }

// formátum-ellenőrzők
function IsEmpty(str) {
    return (str == null | Trim(str) == "") ? true : false;
}

function IsEmail(str) {
    var reEmail = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/;
    return (str) ? reEmail.test(Trim(str)) : false;
}

function IsInteger(str) {
    var reInteger = /^[+-]?\d+?$/;
    return (str) ? reInteger.test(Trim(str)) : false;
}

function IsNumber(str) {
    var reNumber = /^[+-]?\d+([,\.]\d+)?$/;
    return (str) ? reNumber.test(Trim(str)) : false;
}

function IsDate(str) {
    var reDate = /^(19|20)\d\d([-/.])(0[1-9]|1[0-2])\2(0[1-9]|[12][0-9]|3[01])$/;
    return (str) ? reDate.test(Trim(str)) : false;
}

function CorrectDate(str) {
    if (IsDate(str))
	return Trim(str).replace(/^(\d{4})[-/.](\d{2})[-/.](\d{2})$/, "$1.$2.$3");
    else
	return null;
}

function mayorSessionTimeOutAlert() {

    var attrs = { 
	'class'   : 'infoMsg' 
    };
    var figyelmezteto = new Element('div', attrs);
    figyelmezteto.update('Hamarosan lejár a munkamenet!');
    $('mayorerror').insert(figyelmezteto);

}

function mayorSessionTimedOut() {

    var attrs = { 'class'   : 'errorMsg' };
    var figyelmezteto = new Element('div', attrs);
    figyelmezteto.update('A munkamenet lejárt, kérjük jelentkezzen be újra!');
    $('mayorerror').insert(figyelmezteto);
    $('mayorbody').remove();

}

/**
 * Az ajax hívások vésszatérési értékét feldolgozó és a layer-t megjelenítő függvény változók inicializálása (hogy ne legyen undefined)
**/
var showUpdateLayer = function() {
}
var processJSON;

    /**
     *  A header Etag mezője (__SALTVALUE-__SALTNAME[-...]) alapján frissítjük az input.salt mezőket
    **/
    updateSalt = function(response) {
	    $$('input.salt').each(
		function(elem, index) {
		    // tömörített oldalküldés (?) esetén a web-szerver 
		    etagArray=response.getHeader('Etag').split("-");
		    if ($(elem).getAttribute('name') == etagArray[1]) $(elem).value=etagArray[0];
		}
	    );
    }

    /** 
     *  A Prototype request() metódusa kiegészítve azzal, hogy
     *	a header Etag mezője (__SALTVALUE-__SALTNAME[-...]) alapján frissítjük az input.salt mezőket
    **/
    requestForm = function(form, options) {
	form = $(form), options = Object.clone(options || { });

	var params = options.parameters, action = form.readAttribute('action') || '';
	if (action.blank()) action = window.location.href;
	options.parameters = form.serialize(true);

	if (params) {
    	    if (Object.isString(params)) params = params.toQueryParams();
    	    Object.extend(options.parameters, params);
	}

	if (form.hasAttribute('method') && !options.method)
        options.method = form.method;

	tmpComplete = options.onComplete;
	options.onComplete = function(response) {
	    if(typeof tmpComplete == 'function') { 
		tmpComplete(); 
	    }
	    updateSalt(response);
	}

	return new Ajax.Request(action, options);
    }


    updateForm = function(formElement, excludedSubmitElement) {
	    if (typeof formElement.getAttribute('action') == 'string') var url = formElement.getAttribute('action').gsub(/skin=([^&]*)/,'skin=ajax');
	    else var url = formElement.attributes.item('action').nodeValue.gsub(/skin=([^&]*)/,'skin=ajax');
	    // Csak azt a submitet küldjük el, amelyikre kattintottak - a többit letiltjuk
	    inputElements = $A(formElement.getElementsByTagName('input'))
	    inputElements.each(
		function(elem, index) {
		    if (elem.getAttribute('type') == 'submit') if ($(elem) != excludedSubmitElement) elem.disabled=true;
		}
	    );
	    var myAjax = new Ajax.mayorUpdater(
		    formElement, url, { method: formElement.getAttribute('method'), parameters: formElement.serialize(true) }
		);

		// Engedélyezzük a letiltott submit-eket
		inputElements.each(
		function(elem, index) {
		    if (elem.getAttribute('type') == 'submit') if ($(elem) != excludedSubmitElement) elem.disabled=false;
		}
	    );
    }



// A Prototype Ajax.Updater módosítása
Ajax.mayorUpdater = Class.create(Ajax.Request, {
  initialize: function($super, container, url, options) {
    this.container = {
      success: (container.success || container),
      failure: (container.failure || (container.success ? null : container))
    };
          
    options = Object.clone(options);

    var onComplete = options.onComplete;
    options.onComplete = (function(response, json) {
	if (response.getHeader('Content-type').include('application/json') != false || response.getHeader('Content-type').include('text/html') != false) {
	    if (response.getHeader('Content-type').include('text/html') != false) {
		var json = response.responseText.evalJSON();
	    } else { 
		var json = response.responseJSON;
	    }
	    json.element = this;
	    if (json.html != null) {
		this.updateContent(json.html);
		//if (typeof showUpdateLayer == 'function' && Object.isFunction(showUpdateLayer)) showUpdateLayer();
		if (Object.isFunction(showUpdateLayer)) showUpdateLayer();
	    } else if (Object.isFunction(processJSON)) {
		processJSON(json);
	    } else 
		if (true) alert(processJSON);
		else json.action.each(function(elem, index) {
		/**
		 * Ezeket nem igazán használjuk, de estleg teszteléshez jók lehetnek?
		**/
		    if (elem.func == 'alert') { alert($A(elem.param).join(', ')); }
		    else if (elem.func == 'hide') { elem.param.each(function(id, idx) { if ($(id)) $(id).hide(); }); }
		    else if (elem.func == 'show') { elem.param.each(function(id, idx) { if ($(id)) $(id).show(); }); }
		    else if (elem.func == 'clear') { elem.param.each(function(id, idx) { if ($(id)) $(id).update(); }); }
		    else if (elem.func == 'remove') { elem.param.each(function(id, idx) { if ($(id)) $(id).remove(); }); }
		    else if (elem.func == 'update') { elem.param.each(function(e, idx) {$(e.id).update(e.content); }); }
		    else if (elem.func == 'setClass') { elem.param.each(function(e, idx) { $(e.id).writeAttribute('class', e.className);}); }
		    else if (elem.func == 'addClass') { elem.param.each(function(e, idx) {$(e.id).addClassName(e.className);}); }
		    else if (elem.func == 'removeClass') { elem.param.each(function(e, idx) {$(e.id).removeClassName(e.className);}); }
		});
	} else {
    	    this.updateContent(response.responseText);
	}
	updateSalt(response);
        if (Object.isFunction(onComplete)) onComplete(response, json);
	if (Prototype.Browser.IE) { // IE bubbling
		var changeElements = new Array();
		var formElement = $(this.container['success']);
		formElement.select('input, select, button').each(function (elem, index) { changeElements.push(new changeObject(elem)); });
	}

    }).bind(this);

    $super(url, options);
  },

  updateContent: function(responseText) {

    var receiver = this.container[this.success() ? 'success' : 'failure'],
        options = this.options;

    if (!options.evalScripts) responseText = responseText.stripScripts();
    if (receiver = $(receiver)) {
      if (options.insertion) {
        if (Object.isString(options.insertion)) {
          var insertion = { }; insertion[options.insertion] = responseText;
          receiver.insert(insertion);
        }
        else options.insertion(receiver, responseText);
      } else {
	receiver.update(responseText);
      }
    }

  }
});

