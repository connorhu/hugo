// UK lang variables

tinyMCE.addToLang('devkit',{
title : 'TinyMCE Development Kit',
info_tab : 'Info',
settings_tab : 'Beállítások',
log_tab : 'Log',
content_tab : 'Tartalom',
command_states_tab : 'Commands',
undo_redo_tab : 'Undo/Redo',
misc_tab : 'Egyéb',
filter : 'Filter:',
clear_log : 'Log törlése',
refresh : 'Frissítés',
info_help : 'Az információk megtekintéséhez nyomd meg a Frissítés gombot!',
settings_help : 'Press Refresh to display the settings array for each TinyMCE_Control instance.',
content_help : 'Press Refresh to display the raw and cleaned HTML content for each TinyMCE_Control instance.',
command_states_help : 'Press Refresh to display the current command states from inst.queryCommandState. This list will also mark unsupported commands.',
undo_redo_help : 'Press Refresh to display the global and instance undo/redo levels.',
misc_help : 'Here are various tools for debugging and development purposes.',
debug_events : 'Debug events',
undo_diff : 'Diff undo levels'
});
