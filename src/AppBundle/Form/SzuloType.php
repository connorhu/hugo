<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SzuloType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nevelotag')
            ->add('csaladinev')
            ->add('utonev')
            ->add('szuleteskorinevelotag')
            ->add('szuleteskoricsaladinev')
            ->add('szuleteskoriutonev')
            ->add('email')
            ->add('nem')
            ->add('szuletesiev')
            ->add('cimorszag')
            ->add('cimhelyseg')
            ->add('cimirsz')
            ->add('cimkozteruletnev')
            ->add('cimkozteruletjelleg')
            ->add('cimhazszam')
            ->add('cimemelet')
            ->add('cimajto')
            ->add('mobil')
            ->add('telefon')
            ->add('foglalkozas')
            ->add('munkahely')
            ->add('useraccount')
            ->add('statusz')
            ->add('oid')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Szulo'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_szulo';
    }
}
