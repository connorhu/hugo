<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class OsztalyjellegType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('osztalyjellegnev')
            ->add('erettsegizo')
            ->add('kovosztalyjellegid')
            ->add('kezdoevfolyam')
            ->add('vegzoevfolyam')
            ->add('elokeszitoevfolyam')
            ->add('osztalyjellegeles')
            ->add('evfolyamjelek')
            ->add('kirosztalyjellegid')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Osztalyjelleg'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_osztalyjelleg';
    }
}
