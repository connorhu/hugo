<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DiakType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('neveloid')
            ->add('oid')
            ->add('diakigazolvanyszam')
            ->add('tajszam')
            ->add('adoazonosito')
            ->add('szemelyiigazolvanyszam')
            ->add('tartozkodasiokiratszam')
            ->add('viseltnevelotag')
            ->add('viseltcsaladinev')
            ->add('viseltutonev')
            ->add('szuleteskorinevelotag')
            ->add('szuleteskoricsaladinev')
            ->add('szuleteskoriutonev')
            ->add('szuletesihely')
            ->add('szuletesiido')
            ->add('nem')
            ->add('allampolgarsag')
            ->add('lakhelyorszag')
            ->add('lakhelyhelyseg')
            ->add('lakhelyirsz')
            ->add('lakhelykozteruletnev')
            ->add('lakhelykozteruletjelleg')
            ->add('lakhelyhazszam')
            ->add('lakhelyemelet')
            ->add('lakhelyajto')
            ->add('tartorszag')
            ->add('tarthelyseg')
            ->add('tartirsz')
            ->add('tartkozteruletnev')
            ->add('tarthazszam')
            ->add('tartemelet')
            ->add('tartajto')
            ->add('jogviszonykezdete')
            ->add('jogviszonyvege')
            ->add('telefon')
            ->add('mobil')
            ->add('email')
            ->add('statusz')
            ->add('penzugyistatusz')
            ->add('tartkozteruletjelleg')
            ->add('szocialishelyzet')
            ->add('fogyatekossag')
            ->add('gondozasiszam')
            ->add('elozoiskolaomkod')
            ->add('lakohelyijellemzo')
            ->add('torvenyeskepviselo')
            ->add('megjegyzes')
            ->add('nekazonosito')
            ->add('torzslapszam')
            ->add('apaid')
            ->add('kezdotanev')
            ->add('gondviseloid')
            ->add('anyaid')
            ->add('tanev')
            ->add('osztalyid')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Diak'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_diak';
    }
}
