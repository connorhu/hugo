<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class OsztalyType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('leiras')
            ->add('kezdotanev')
            ->add('vegzotanev')
            ->add('jel')
            ->add('kezdoevfolyam')
            ->add('kezdoevfolyamsorszam')
            ->add('osztalyjellegid')
            ->add('telephelyid')
            ->add('tankorid')
            ->add('diakid')
            ->add('kepzesid')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Osztaly'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_osztaly';
    }
}
