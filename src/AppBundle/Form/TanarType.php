<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TanarType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('oid')
            ->add('bedt')
            ->add('kidt')
            ->add('viseltnevelotag')
            ->add('viseltcsaladinev')
            ->add('viseltutonev')
            ->add('szuletesihely')
            ->add('szuletesiido')
            ->add('dn')
            ->add('szuleteskoriutonev')
            ->add('szuleteskoricsaladinev')
            ->add('szuleteskorinevelotag')
            ->add('hetimunkaora')
            ->add('nekazonosito')
            ->add('statusz')
            ->add('hetikotelezooraszam')
            ->add('megjegyzes')
            ->add('besorolas')
            ->add('kepesitesid')
            ->add('mkid')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Tanar'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_tanar';
    }
}
