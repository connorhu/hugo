<?php

namespace AppBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;

class Mayor404Redirect
{
	private $kernel;
    private $container;
    
	public function __construct($kernel)
	{
		$this->kernel = $kernel;
        $this->container = $this->kernel->getContainer();
	}

	public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $path = $event->getRequest()->getPathInfo();
        
        if ($path == '/') {
            $path = '/index.php';
        }
        
    	if (substr($path, 0, strlen('/index.php')) == '/index.php') {
            $GLOBALS['router'] = $this->container->get('router');

    		$root = $this->kernel->getRootDir() .'/../www';
    		chdir($root);
    		ini_set('include_path', ini_get('include_path') .':'. $root);
    		include_once $root . $path;
            
            $event->setResponse(new Response());
    	}
    }
}