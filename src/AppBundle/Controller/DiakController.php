<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Diak;
use AppBundle\Form\DiakType;

/**
 * Diak controller.
 *
 * @Route("/intezmeny/diak")
 */
class DiakController extends Controller
{

    /**
     * Lists all Diak entities.
     *
     * @Route("/", name="intezmeny_diak")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Diak')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Diak entity.
     *
     * @Route("/", name="intezmeny_diak_create")
     * @Method("POST")
     * @Template("AppBundle:Diak:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Diak();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('intezmeny_diak_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Diak entity.
     *
     * @param Diak $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Diak $entity)
    {
        $form = $this->createForm(new DiakType(), $entity, array(
            'action' => $this->generateUrl('intezmeny_diak_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Diak entity.
     *
     * @Route("/new", name="intezmeny_diak_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Diak();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Diak entity.
     *
     * @Route("/{id}", name="intezmeny_diak_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Diak')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Diak entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Diak entity.
     *
     * @Route("/{id}/edit", name="intezmeny_diak_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Diak')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Diak entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Diak entity.
    *
    * @param Diak $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Diak $entity)
    {
        $form = $this->createForm(new DiakType(), $entity, array(
            'action' => $this->generateUrl('intezmeny_diak_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Diak entity.
     *
     * @Route("/{id}", name="intezmeny_diak_update")
     * @Method("PUT")
     * @Template("AppBundle:Diak:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Diak')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Diak entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('intezmeny_diak_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Diak entity.
     *
     * @Route("/{id}", name="intezmeny_diak_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Diak')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Diak entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('intezmeny_diak'));
    }

    /**
     * Creates a form to delete a Diak entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('intezmeny_diak_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
