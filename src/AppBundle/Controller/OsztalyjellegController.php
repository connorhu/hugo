<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Osztalyjelleg;
use AppBundle\Form\OsztalyjellegType;

/**
 * Osztalyjelleg controller.
 *
 * @Route("/program/osztalyjelleg")
 */
class OsztalyjellegController extends Controller
{

    /**
     * Lists all Osztalyjelleg entities.
     *
     * @Route("/", name="program_osztalyjelleg")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Osztalyjelleg')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Osztalyjelleg entity.
     *
     * @Route("/", name="program_osztalyjelleg_create")
     * @Method("POST")
     * @Template("AppBundle:Osztalyjelleg:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Osztalyjelleg();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('program_osztalyjelleg_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Osztalyjelleg entity.
     *
     * @param Osztalyjelleg $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Osztalyjelleg $entity)
    {
        $form = $this->createForm(new OsztalyjellegType(), $entity, array(
            'action' => $this->generateUrl('program_osztalyjelleg_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Osztalyjelleg entity.
     *
     * @Route("/new", name="program_osztalyjelleg_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Osztalyjelleg();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Osztalyjelleg entity.
     *
     * @Route("/{id}", name="program_osztalyjelleg_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Osztalyjelleg')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Osztalyjelleg entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Osztalyjelleg entity.
     *
     * @Route("/{id}/edit", name="program_osztalyjelleg_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Osztalyjelleg')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Osztalyjelleg entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Osztalyjelleg entity.
    *
    * @param Osztalyjelleg $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Osztalyjelleg $entity)
    {
        $form = $this->createForm(new OsztalyjellegType(), $entity, array(
            'action' => $this->generateUrl('program_osztalyjelleg_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Osztalyjelleg entity.
     *
     * @Route("/{id}", name="program_osztalyjelleg_update")
     * @Method("PUT")
     * @Template("AppBundle:Osztalyjelleg:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Osztalyjelleg')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Osztalyjelleg entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('program_osztalyjelleg_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Osztalyjelleg entity.
     *
     * @Route("/{id}", name="program_osztalyjelleg_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Osztalyjelleg')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Osztalyjelleg entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('program_osztalyjelleg'));
    }

    /**
     * Creates a form to delete a Osztalyjelleg entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('program_osztalyjelleg_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
