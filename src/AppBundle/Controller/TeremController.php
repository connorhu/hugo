<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Terem;
use AppBundle\Form\TeremType;

/**
 * Terem controller.
 *
 * @Route("/intezmeny/terem")
 */
class TeremController extends Controller
{

    /**
     * Lists all Terem entities.
     *
     * @Route("/", name="intezmeny_terem")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Terem')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Terem entity.
     *
     * @Route("/", name="intezmeny_terem_create")
     * @Method("POST")
     * @Template("AppBundle:Terem:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Terem();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('intezmeny_terem_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Terem entity.
     *
     * @param Terem $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Terem $entity)
    {
        $form = $this->createForm(new TeremType(), $entity, array(
            'action' => $this->generateUrl('intezmeny_terem_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Terem entity.
     *
     * @Route("/new", name="intezmeny_terem_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Terem();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Terem entity.
     *
     * @Route("/{id}", name="intezmeny_terem_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Terem')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Terem entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Terem entity.
     *
     * @Route("/{id}/edit", name="intezmeny_terem_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Terem')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Terem entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Terem entity.
    *
    * @param Terem $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Terem $entity)
    {
        $form = $this->createForm(new TeremType(), $entity, array(
            'action' => $this->generateUrl('intezmeny_terem_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Terem entity.
     *
     * @Route("/{id}", name="intezmeny_terem_update")
     * @Method("PUT")
     * @Template("AppBundle:Terem:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Terem')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Terem entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('intezmeny_terem_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Terem entity.
     *
     * @Route("/{id}", name="intezmeny_terem_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Terem')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Terem entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('intezmeny_terem'));
    }

    /**
     * Creates a form to delete a Terem entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('intezmeny_terem_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
