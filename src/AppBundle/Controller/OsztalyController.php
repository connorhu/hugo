<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Osztaly;
use AppBundle\Form\OsztalyType;

/**
 * Osztaly controller.
 *
 * @Route("/intezmeny/osztaly")
 */
class OsztalyController extends Controller
{

    /**
     * Lists all Osztaly entities.
     *
     * @Route("/", name="intezmeny_osztaly")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Osztaly')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Osztaly entity.
     *
     * @Route("/", name="intezmeny_osztaly_create")
     * @Method("POST")
     * @Template("AppBundle:Osztaly:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Osztaly();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('intezmeny_osztaly_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Osztaly entity.
     *
     * @param Osztaly $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Osztaly $entity)
    {
        $form = $this->createForm(new OsztalyType(), $entity, array(
            'action' => $this->generateUrl('intezmeny_osztaly_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Osztaly entity.
     *
     * @Route("/new", name="intezmeny_osztaly_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Osztaly();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Osztaly entity.
     *
     * @Route("/{id}", name="intezmeny_osztaly_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Osztaly')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Osztaly entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Osztaly entity.
     *
     * @Route("/{id}/edit", name="intezmeny_osztaly_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Osztaly')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Osztaly entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Osztaly entity.
    *
    * @param Osztaly $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Osztaly $entity)
    {
        $form = $this->createForm(new OsztalyType(), $entity, array(
            'action' => $this->generateUrl('intezmeny_osztaly_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Osztaly entity.
     *
     * @Route("/{id}", name="intezmeny_osztaly_update")
     * @Method("PUT")
     * @Template("AppBundle:Osztaly:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Osztaly')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Osztaly entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('intezmeny_osztaly_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Osztaly entity.
     *
     * @Route("/{id}", name="intezmeny_osztaly_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Osztaly')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Osztaly entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('intezmeny_osztaly'));
    }

    /**
     * Creates a form to delete a Osztaly entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('intezmeny_osztaly_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
