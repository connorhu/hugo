<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Diakjogviszony
 *
 * @ORM\Table(name="diakJogviszony", indexes={@ORM\Index(name="diakJogviszony_FKIndex1", columns={"diakId"})})
 * @ORM\Entity
 */
class Diakjogviszony
{
    /**
     * @var string
     *
     * @ORM\Column(name="statusz", type="string", nullable=false)
     */
    private $statusz;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt", type="date")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $dt;

    /**
     * @var \AppBundle\Entity\Diak
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Diak")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="diakId", referencedColumnName="diakId")
     * })
     */
    private $diakid;



    /**
     * Set statusz
     *
     * @param string $statusz
     *
     * @return Diakjogviszony
     */
    public function setStatusz($statusz)
    {
        $this->statusz = $statusz;

        return $this;
    }

    /**
     * Get statusz
     *
     * @return string
     */
    public function getStatusz()
    {
        return $this->statusz;
    }

    /**
     * Set dt
     *
     * @param \DateTime $dt
     *
     * @return Diakjogviszony
     */
    public function setDt($dt)
    {
        $this->dt = $dt;

        return $this;
    }

    /**
     * Get dt
     *
     * @return \DateTime
     */
    public function getDt()
    {
        return $this->dt;
    }

    /**
     * Set diakid
     *
     * @param \AppBundle\Entity\Diak $diakid
     *
     * @return Diakjogviszony
     */
    public function setDiakid(\AppBundle\Entity\Diak $diakid)
    {
        $this->diakid = $diakid;

        return $this;
    }

    /**
     * Get diakid
     *
     * @return \AppBundle\Entity\Diak
     */
    public function getDiakid()
    {
        return $this->diakid;
    }
}
