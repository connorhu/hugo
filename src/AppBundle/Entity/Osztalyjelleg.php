<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Osztalyjelleg
 *
 * @ORM\Table(name="osztalyJelleg", indexes={@ORM\Index(name="osztalyJelleg_ibfk_1", columns={"kirOsztalyJellegId"})})
 * @ORM\Entity
 */
class Osztalyjelleg
{
    /**
     * @var string
     *
     * @ORM\Column(name="osztalyJellegNev", type="string", length=255, nullable=false)
     */
    private $osztalyjellegnev;

    /**
     * @var boolean
     *
     * @ORM\Column(name="erettsegizo", type="boolean", nullable=false)
     */
    private $erettsegizo = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="kovOsztalyJellegId", type="boolean", nullable=true)
     */
    private $kovosztalyjellegid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="_kezdoEvfolyam", type="boolean", nullable=true)
     */
    private $kezdoevfolyam;

    /**
     * @var boolean
     *
     * @ORM\Column(name="_vegzoEvfolyam", type="boolean", nullable=true)
     */
    private $vegzoevfolyam;

    /**
     * @var string
     *
     * @ORM\Column(name="elokeszitoEvfolyam", type="string", nullable=true)
     */
    private $elokeszitoevfolyam;

    /**
     * @var boolean
     *
     * @ORM\Column(name="osztalyJellegEles", type="boolean", nullable=true)
     */
    private $osztalyjellegeles = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="evfolyamJelek", type="string", length=255, nullable=true)
     */
    private $evfolyamjelek = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="osztalyJellegId", type="boolean")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $osztalyjellegid;

    /**
     * @var \AppBundle\Entity\Kirosztalyjelleg
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Kirosztalyjelleg")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="kirOsztalyJellegId", referencedColumnName="kirOsztalyJellegId")
     * })
     */
    private $kirosztalyjellegid;



    /**
     * Set osztalyjellegnev
     *
     * @param string $osztalyjellegnev
     *
     * @return Osztalyjelleg
     */
    public function setOsztalyjellegnev($osztalyjellegnev)
    {
        $this->osztalyjellegnev = $osztalyjellegnev;

        return $this;
    }

    /**
     * Get osztalyjellegnev
     *
     * @return string
     */
    public function getOsztalyjellegnev()
    {
        return $this->osztalyjellegnev;
    }

    /**
     * Set erettsegizo
     *
     * @param boolean $erettsegizo
     *
     * @return Osztalyjelleg
     */
    public function setErettsegizo($erettsegizo)
    {
        $this->erettsegizo = $erettsegizo;

        return $this;
    }

    /**
     * Get erettsegizo
     *
     * @return boolean
     */
    public function getErettsegizo()
    {
        return $this->erettsegizo;
    }

    /**
     * Set kovosztalyjellegid
     *
     * @param boolean $kovosztalyjellegid
     *
     * @return Osztalyjelleg
     */
    public function setKovosztalyjellegid($kovosztalyjellegid)
    {
        $this->kovosztalyjellegid = $kovosztalyjellegid;

        return $this;
    }

    /**
     * Get kovosztalyjellegid
     *
     * @return boolean
     */
    public function getKovosztalyjellegid()
    {
        return $this->kovosztalyjellegid;
    }

    /**
     * Set kezdoevfolyam
     *
     * @param boolean $kezdoevfolyam
     *
     * @return Osztalyjelleg
     */
    public function setKezdoevfolyam($kezdoevfolyam)
    {
        $this->kezdoevfolyam = $kezdoevfolyam;

        return $this;
    }

    /**
     * Get kezdoevfolyam
     *
     * @return boolean
     */
    public function getKezdoevfolyam()
    {
        return $this->kezdoevfolyam;
    }

    /**
     * Set vegzoevfolyam
     *
     * @param boolean $vegzoevfolyam
     *
     * @return Osztalyjelleg
     */
    public function setVegzoevfolyam($vegzoevfolyam)
    {
        $this->vegzoevfolyam = $vegzoevfolyam;

        return $this;
    }

    /**
     * Get vegzoevfolyam
     *
     * @return boolean
     */
    public function getVegzoevfolyam()
    {
        return $this->vegzoevfolyam;
    }

    /**
     * Set elokeszitoevfolyam
     *
     * @param string $elokeszitoevfolyam
     *
     * @return Osztalyjelleg
     */
    public function setElokeszitoevfolyam($elokeszitoevfolyam)
    {
        $this->elokeszitoevfolyam = $elokeszitoevfolyam;

        return $this;
    }

    /**
     * Get elokeszitoevfolyam
     *
     * @return string
     */
    public function getElokeszitoevfolyam()
    {
        return $this->elokeszitoevfolyam;
    }

    /**
     * Set osztalyjellegeles
     *
     * @param boolean $osztalyjellegeles
     *
     * @return Osztalyjelleg
     */
    public function setOsztalyjellegeles($osztalyjellegeles)
    {
        $this->osztalyjellegeles = $osztalyjellegeles;

        return $this;
    }

    /**
     * Get osztalyjellegeles
     *
     * @return boolean
     */
    public function getOsztalyjellegeles()
    {
        return $this->osztalyjellegeles;
    }

    /**
     * Set evfolyamjelek
     *
     * @param string $evfolyamjelek
     *
     * @return Osztalyjelleg
     */
    public function setEvfolyamjelek($evfolyamjelek)
    {
        $this->evfolyamjelek = $evfolyamjelek;

        return $this;
    }

    /**
     * Get evfolyamjelek
     *
     * @return string
     */
    public function getEvfolyamjelek()
    {
        return $this->evfolyamjelek;
    }

    /**
     * Get osztalyjellegid
     *
     * @return boolean
     */
    public function getOsztalyjellegid()
    {
        return $this->osztalyjellegid;
    }

    /**
     * Set kirosztalyjellegid
     *
     * @param \AppBundle\Entity\Kirosztalyjelleg $kirosztalyjellegid
     *
     * @return Osztalyjelleg
     */
    public function setKirosztalyjellegid(\AppBundle\Entity\Kirosztalyjelleg $kirosztalyjellegid = null)
    {
        $this->kirosztalyjellegid = $kirosztalyjellegid;

        return $this;
    }

    /**
     * Get kirosztalyjellegid
     *
     * @return \AppBundle\Entity\Kirosztalyjelleg
     */
    public function getKirosztalyjellegid()
    {
        return $this->kirosztalyjellegid;
    }
}
