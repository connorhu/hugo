<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Diak
 *
 * @ORM\Table(name="diak", uniqueConstraints={@ORM\UniqueConstraint(name="diakOid", columns={"oId"})}, indexes={@ORM\Index(name="kezdoTanev", columns={"kezdoTanev", "kezdoSzemeszter"}), @ORM\Index(name="anyaId", columns={"anyaId"}), @ORM\Index(name="gondviseloId", columns={"gondviseloId"}), @ORM\Index(name="apaId", columns={"apaId"})})
 * @ORM\Entity
 */
class Diak
{
    /**
     * @var integer
     *
     * @ORM\Column(name="neveloId", type="integer", nullable=true)
     */
    private $neveloid;

    /**
     * @var integer
     *
     * @ORM\Column(name="oId", type="bigint", nullable=true)
     */
    private $oid;

    /**
     * @var integer
     *
     * @ORM\Column(name="diakigazolvanySzam", type="integer", nullable=true)
     */
    private $diakigazolvanyszam;

    /**
     * @var integer
     *
     * @ORM\Column(name="tajSzam", type="integer", nullable=true)
     */
    private $tajszam;

    /**
     * @var integer
     *
     * @ORM\Column(name="adoazonosito", type="bigint", nullable=true)
     */
    private $adoazonosito;

    /**
     * @var string
     *
     * @ORM\Column(name="szemelyiIgazolvanySzam", type="string", length=16, nullable=true)
     */
    private $szemelyiigazolvanyszam;

    /**
     * @var string
     *
     * @ORM\Column(name="tartozkodasiOkiratSzam", type="string", length=16, nullable=true)
     */
    private $tartozkodasiokiratszam;

    /**
     * @var string
     *
     * @ORM\Column(name="viseltNevElotag", type="string", length=8, nullable=true)
     */
    private $viseltnevelotag = '';

    /**
     * @var string
     *
     * @ORM\Column(name="viseltCsaladinev", type="string", length=64, nullable=false)
     */
    private $viseltcsaladinev = '';

    /**
     * @var string
     *
     * @ORM\Column(name="viseltUtonev", type="string", length=64, nullable=false)
     */
    private $viseltutonev = '';

    /**
     * @var string
     *
     * @ORM\Column(name="szuleteskoriNevElotag", type="string", length=8, nullable=true)
     */
    private $szuleteskorinevelotag = '';

    /**
     * @var string
     *
     * @ORM\Column(name="szuleteskoriCsaladinev", type="string", length=64, nullable=false)
     */
    private $szuleteskoricsaladinev = '';

    /**
     * @var string
     *
     * @ORM\Column(name="szuleteskoriUtonev", type="string", length=64, nullable=false)
     */
    private $szuleteskoriutonev = '';

    /**
     * @var string
     *
     * @ORM\Column(name="szuletesiHely", type="string", length=32, nullable=true)
     */
    private $szuletesihely;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="szuletesiIdo", type="date", nullable=true)
     */
    private $szuletesiido;

    /**
     * @var string
     *
     * @ORM\Column(name="nem", type="string", nullable=true)
     */
    private $nem;

    /**
     * @var string
     *
     * @ORM\Column(name="allampolgarsag", type="string", length=16, nullable=true)
     */
    private $allampolgarsag = 'magyar';

    /**
     * @var string
     *
     * @ORM\Column(name="lakhelyOrszag", type="string", length=16, nullable=true)
     */
    private $lakhelyorszag = 'Magyarország';

    /**
     * @var string
     *
     * @ORM\Column(name="lakhelyHelyseg", type="string", length=32, nullable=true)
     */
    private $lakhelyhelyseg;

    /**
     * @var string
     *
     * @ORM\Column(name="lakhelyIrsz", type="string", length=8, nullable=true)
     */
    private $lakhelyirsz;

    /**
     * @var string
     *
     * @ORM\Column(name="lakhelyKozteruletNev", type="string", length=32, nullable=true)
     */
    private $lakhelykozteruletnev;

    /**
     * @var string
     *
     * @ORM\Column(name="lakhelyKozteruletJelleg", type="string", nullable=true)
     */
    private $lakhelykozteruletjelleg;

    /**
     * @var string
     *
     * @ORM\Column(name="lakhelyHazszam", type="string", length=20, nullable=true)
     */
    private $lakhelyhazszam;

    /**
     * @var string
     *
     * @ORM\Column(name="lakhelyEmelet", type="string", length=5, nullable=true)
     */
    private $lakhelyemelet;

    /**
     * @var string
     *
     * @ORM\Column(name="lakhelyAjto", type="string", length=5, nullable=true)
     */
    private $lakhelyajto;

    /**
     * @var string
     *
     * @ORM\Column(name="tartOrszag", type="string", length=16, nullable=true)
     */
    private $tartorszag = 'Magyarország';

    /**
     * @var string
     *
     * @ORM\Column(name="tartHelyseg", type="string", length=32, nullable=true)
     */
    private $tarthelyseg;

    /**
     * @var string
     *
     * @ORM\Column(name="tartIrsz", type="string", length=8, nullable=true)
     */
    private $tartirsz;

    /**
     * @var string
     *
     * @ORM\Column(name="tartKozteruletNev", type="string", length=32, nullable=true)
     */
    private $tartkozteruletnev;

    /**
     * @var string
     *
     * @ORM\Column(name="tartHazszam", type="string", length=20, nullable=true)
     */
    private $tarthazszam;

    /**
     * @var string
     *
     * @ORM\Column(name="tartEmelet", type="string", length=5, nullable=true)
     */
    private $tartemelet;

    /**
     * @var string
     *
     * @ORM\Column(name="tartAjto", type="string", length=5, nullable=true)
     */
    private $tartajto;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="jogviszonyKezdete", type="date", nullable=true)
     */
    private $jogviszonykezdete;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="jogviszonyVege", type="date", nullable=true)
     */
    private $jogviszonyvege;

    /**
     * @var string
     *
     * @ORM\Column(name="telefon", type="string", length=64, nullable=true)
     */
    private $telefon;

    /**
     * @var string
     *
     * @ORM\Column(name="mobil", type="string", length=64, nullable=true)
     */
    private $mobil;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=96, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="statusz", type="string", nullable=false)
     */
    private $statusz;

    /**
     * @var string
     *
     * @ORM\Column(name="penzugyiStatusz", type="string", nullable=true)
     */
    private $penzugyistatusz = 'állami finanszírozás';

    /**
     * @var string
     *
     * @ORM\Column(name="tartKozteruletJelleg", type="string", nullable=true)
     */
    private $tartkozteruletjelleg;

    /**
     * @var array
     *
     * @ORM\Column(name="szocialisHelyzet", type="simple_array", nullable=true)
     */
    private $szocialishelyzet;

    /**
     * @var array
     *
     * @ORM\Column(name="fogyatekossag", type="simple_array", nullable=true)
     */
    private $fogyatekossag;

    /**
     * @var string
     *
     * @ORM\Column(name="gondozasiSzam", type="string", length=128, nullable=true)
     */
    private $gondozasiszam;

    /**
     * @var integer
     *
     * @ORM\Column(name="elozoIskolaOMKod", type="integer", nullable=true)
     */
    private $elozoiskolaomkod;

    /**
     * @var string
     *
     * @ORM\Column(name="lakohelyiJellemzo", type="string", nullable=true)
     */
    private $lakohelyijellemzo;

    /**
     * @var array
     *
     * @ORM\Column(name="torvenyesKepviselo", type="simple_array", nullable=true)
     */
    private $torvenyeskepviselo;

    /**
     * @var string
     *
     * @ORM\Column(name="megjegyzes", type="string", length=255, nullable=true)
     */
    private $megjegyzes;

    /**
     * @var string
     *
     * @ORM\Column(name="NEKAzonosito", type="string", length=16, nullable=true)
     */
    private $nekazonosito;

    /**
     * @var string
     *
     * @ORM\Column(name="torzslapszam", type="string", length=32, nullable=true)
     */
    private $torzslapszam;

    /**
     * @var integer
     *
     * @ORM\Column(name="diakId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $diakid;

    /**
     * @var \AppBundle\Entity\Szulo
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Szulo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="apaId", referencedColumnName="szuloId")
     * })
     */
    private $apaid;

    /**
     * @var \AppBundle\Entity\Szemeszter
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Szemeszter")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="kezdoTanev", referencedColumnName="tanev"),
     *   @ORM\JoinColumn(name="kezdoSzemeszter", referencedColumnName="szemeszter")
     * })
     */
    private $kezdotanev;

    /**
     * @var \AppBundle\Entity\Szulo
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Szulo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="gondviseloId", referencedColumnName="szuloId")
     * })
     */
    private $gondviseloid;

    /**
     * @var \AppBundle\Entity\Szulo
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Szulo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="anyaId", referencedColumnName="szuloId")
     * })
     */
    private $anyaid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Szemeszter", mappedBy="diakid")
     */
    private $tanev;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Osztaly", mappedBy="diakid")
     */
    private $osztalyid;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tanev = new \Doctrine\Common\Collections\ArrayCollection();
        $this->osztalyid = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set neveloid
     *
     * @param integer $neveloid
     *
     * @return Diak
     */
    public function setNeveloid($neveloid)
    {
        $this->neveloid = $neveloid;

        return $this;
    }

    /**
     * Get neveloid
     *
     * @return integer
     */
    public function getNeveloid()
    {
        return $this->neveloid;
    }

    /**
     * Set oid
     *
     * @param integer $oid
     *
     * @return Diak
     */
    public function setOid($oid)
    {
        $this->oid = $oid;

        return $this;
    }

    /**
     * Get oid
     *
     * @return integer
     */
    public function getOid()
    {
        return $this->oid;
    }

    /**
     * Set diakigazolvanyszam
     *
     * @param integer $diakigazolvanyszam
     *
     * @return Diak
     */
    public function setDiakigazolvanyszam($diakigazolvanyszam)
    {
        $this->diakigazolvanyszam = $diakigazolvanyszam;

        return $this;
    }

    /**
     * Get diakigazolvanyszam
     *
     * @return integer
     */
    public function getDiakigazolvanyszam()
    {
        return $this->diakigazolvanyszam;
    }

    /**
     * Set tajszam
     *
     * @param integer $tajszam
     *
     * @return Diak
     */
    public function setTajszam($tajszam)
    {
        $this->tajszam = $tajszam;

        return $this;
    }

    /**
     * Get tajszam
     *
     * @return integer
     */
    public function getTajszam()
    {
        return $this->tajszam;
    }

    /**
     * Set adoazonosito
     *
     * @param integer $adoazonosito
     *
     * @return Diak
     */
    public function setAdoazonosito($adoazonosito)
    {
        $this->adoazonosito = $adoazonosito;

        return $this;
    }

    /**
     * Get adoazonosito
     *
     * @return integer
     */
    public function getAdoazonosito()
    {
        return $this->adoazonosito;
    }

    /**
     * Set szemelyiigazolvanyszam
     *
     * @param string $szemelyiigazolvanyszam
     *
     * @return Diak
     */
    public function setSzemelyiigazolvanyszam($szemelyiigazolvanyszam)
    {
        $this->szemelyiigazolvanyszam = $szemelyiigazolvanyszam;

        return $this;
    }

    /**
     * Get szemelyiigazolvanyszam
     *
     * @return string
     */
    public function getSzemelyiigazolvanyszam()
    {
        return $this->szemelyiigazolvanyszam;
    }

    /**
     * Set tartozkodasiokiratszam
     *
     * @param string $tartozkodasiokiratszam
     *
     * @return Diak
     */
    public function setTartozkodasiokiratszam($tartozkodasiokiratszam)
    {
        $this->tartozkodasiokiratszam = $tartozkodasiokiratszam;

        return $this;
    }

    /**
     * Get tartozkodasiokiratszam
     *
     * @return string
     */
    public function getTartozkodasiokiratszam()
    {
        return $this->tartozkodasiokiratszam;
    }

    /**
     * Set viseltnevelotag
     *
     * @param string $viseltnevelotag
     *
     * @return Diak
     */
    public function setViseltnevelotag($viseltnevelotag)
    {
        $this->viseltnevelotag = $viseltnevelotag;

        return $this;
    }

    /**
     * Get viseltnevelotag
     *
     * @return string
     */
    public function getViseltnevelotag()
    {
        return $this->viseltnevelotag;
    }

    /**
     * Set viseltcsaladinev
     *
     * @param string $viseltcsaladinev
     *
     * @return Diak
     */
    public function setViseltcsaladinev($viseltcsaladinev)
    {
        $this->viseltcsaladinev = $viseltcsaladinev;

        return $this;
    }

    /**
     * Get viseltcsaladinev
     *
     * @return string
     */
    public function getViseltcsaladinev()
    {
        return $this->viseltcsaladinev;
    }

    /**
     * Set viseltutonev
     *
     * @param string $viseltutonev
     *
     * @return Diak
     */
    public function setViseltutonev($viseltutonev)
    {
        $this->viseltutonev = $viseltutonev;

        return $this;
    }

    /**
     * Get viseltutonev
     *
     * @return string
     */
    public function getViseltutonev()
    {
        return $this->viseltutonev;
    }

    /**
     * Set szuleteskorinevelotag
     *
     * @param string $szuleteskorinevelotag
     *
     * @return Diak
     */
    public function setSzuleteskorinevelotag($szuleteskorinevelotag)
    {
        $this->szuleteskorinevelotag = $szuleteskorinevelotag;

        return $this;
    }

    /**
     * Get szuleteskorinevelotag
     *
     * @return string
     */
    public function getSzuleteskorinevelotag()
    {
        return $this->szuleteskorinevelotag;
    }

    /**
     * Set szuleteskoricsaladinev
     *
     * @param string $szuleteskoricsaladinev
     *
     * @return Diak
     */
    public function setSzuleteskoricsaladinev($szuleteskoricsaladinev)
    {
        $this->szuleteskoricsaladinev = $szuleteskoricsaladinev;

        return $this;
    }

    /**
     * Get szuleteskoricsaladinev
     *
     * @return string
     */
    public function getSzuleteskoricsaladinev()
    {
        return $this->szuleteskoricsaladinev;
    }

    /**
     * Set szuleteskoriutonev
     *
     * @param string $szuleteskoriutonev
     *
     * @return Diak
     */
    public function setSzuleteskoriutonev($szuleteskoriutonev)
    {
        $this->szuleteskoriutonev = $szuleteskoriutonev;

        return $this;
    }

    /**
     * Get szuleteskoriutonev
     *
     * @return string
     */
    public function getSzuleteskoriutonev()
    {
        return $this->szuleteskoriutonev;
    }

    /**
     * Set szuletesihely
     *
     * @param string $szuletesihely
     *
     * @return Diak
     */
    public function setSzuletesihely($szuletesihely)
    {
        $this->szuletesihely = $szuletesihely;

        return $this;
    }

    /**
     * Get szuletesihely
     *
     * @return string
     */
    public function getSzuletesihely()
    {
        return $this->szuletesihely;
    }

    /**
     * Set szuletesiido
     *
     * @param \DateTime $szuletesiido
     *
     * @return Diak
     */
    public function setSzuletesiido($szuletesiido)
    {
        $this->szuletesiido = $szuletesiido;

        return $this;
    }

    /**
     * Get szuletesiido
     *
     * @return \DateTime
     */
    public function getSzuletesiido()
    {
        return $this->szuletesiido;
    }

    /**
     * Set nem
     *
     * @param string $nem
     *
     * @return Diak
     */
    public function setNem($nem)
    {
        $this->nem = $nem;

        return $this;
    }

    /**
     * Get nem
     *
     * @return string
     */
    public function getNem()
    {
        return $this->nem;
    }

    /**
     * Set allampolgarsag
     *
     * @param string $allampolgarsag
     *
     * @return Diak
     */
    public function setAllampolgarsag($allampolgarsag)
    {
        $this->allampolgarsag = $allampolgarsag;

        return $this;
    }

    /**
     * Get allampolgarsag
     *
     * @return string
     */
    public function getAllampolgarsag()
    {
        return $this->allampolgarsag;
    }

    /**
     * Set lakhelyorszag
     *
     * @param string $lakhelyorszag
     *
     * @return Diak
     */
    public function setLakhelyorszag($lakhelyorszag)
    {
        $this->lakhelyorszag = $lakhelyorszag;

        return $this;
    }

    /**
     * Get lakhelyorszag
     *
     * @return string
     */
    public function getLakhelyorszag()
    {
        return $this->lakhelyorszag;
    }

    /**
     * Set lakhelyhelyseg
     *
     * @param string $lakhelyhelyseg
     *
     * @return Diak
     */
    public function setLakhelyhelyseg($lakhelyhelyseg)
    {
        $this->lakhelyhelyseg = $lakhelyhelyseg;

        return $this;
    }

    /**
     * Get lakhelyhelyseg
     *
     * @return string
     */
    public function getLakhelyhelyseg()
    {
        return $this->lakhelyhelyseg;
    }

    /**
     * Set lakhelyirsz
     *
     * @param string $lakhelyirsz
     *
     * @return Diak
     */
    public function setLakhelyirsz($lakhelyirsz)
    {
        $this->lakhelyirsz = $lakhelyirsz;

        return $this;
    }

    /**
     * Get lakhelyirsz
     *
     * @return string
     */
    public function getLakhelyirsz()
    {
        return $this->lakhelyirsz;
    }

    /**
     * Set lakhelykozteruletnev
     *
     * @param string $lakhelykozteruletnev
     *
     * @return Diak
     */
    public function setLakhelykozteruletnev($lakhelykozteruletnev)
    {
        $this->lakhelykozteruletnev = $lakhelykozteruletnev;

        return $this;
    }

    /**
     * Get lakhelykozteruletnev
     *
     * @return string
     */
    public function getLakhelykozteruletnev()
    {
        return $this->lakhelykozteruletnev;
    }

    /**
     * Set lakhelykozteruletjelleg
     *
     * @param string $lakhelykozteruletjelleg
     *
     * @return Diak
     */
    public function setLakhelykozteruletjelleg($lakhelykozteruletjelleg)
    {
        $this->lakhelykozteruletjelleg = $lakhelykozteruletjelleg;

        return $this;
    }

    /**
     * Get lakhelykozteruletjelleg
     *
     * @return string
     */
    public function getLakhelykozteruletjelleg()
    {
        return $this->lakhelykozteruletjelleg;
    }

    /**
     * Set lakhelyhazszam
     *
     * @param string $lakhelyhazszam
     *
     * @return Diak
     */
    public function setLakhelyhazszam($lakhelyhazszam)
    {
        $this->lakhelyhazszam = $lakhelyhazszam;

        return $this;
    }

    /**
     * Get lakhelyhazszam
     *
     * @return string
     */
    public function getLakhelyhazszam()
    {
        return $this->lakhelyhazszam;
    }

    /**
     * Set lakhelyemelet
     *
     * @param string $lakhelyemelet
     *
     * @return Diak
     */
    public function setLakhelyemelet($lakhelyemelet)
    {
        $this->lakhelyemelet = $lakhelyemelet;

        return $this;
    }

    /**
     * Get lakhelyemelet
     *
     * @return string
     */
    public function getLakhelyemelet()
    {
        return $this->lakhelyemelet;
    }

    /**
     * Set lakhelyajto
     *
     * @param string $lakhelyajto
     *
     * @return Diak
     */
    public function setLakhelyajto($lakhelyajto)
    {
        $this->lakhelyajto = $lakhelyajto;

        return $this;
    }

    /**
     * Get lakhelyajto
     *
     * @return string
     */
    public function getLakhelyajto()
    {
        return $this->lakhelyajto;
    }

    /**
     * Set tartorszag
     *
     * @param string $tartorszag
     *
     * @return Diak
     */
    public function setTartorszag($tartorszag)
    {
        $this->tartorszag = $tartorszag;

        return $this;
    }

    /**
     * Get tartorszag
     *
     * @return string
     */
    public function getTartorszag()
    {
        return $this->tartorszag;
    }

    /**
     * Set tarthelyseg
     *
     * @param string $tarthelyseg
     *
     * @return Diak
     */
    public function setTarthelyseg($tarthelyseg)
    {
        $this->tarthelyseg = $tarthelyseg;

        return $this;
    }

    /**
     * Get tarthelyseg
     *
     * @return string
     */
    public function getTarthelyseg()
    {
        return $this->tarthelyseg;
    }

    /**
     * Set tartirsz
     *
     * @param string $tartirsz
     *
     * @return Diak
     */
    public function setTartirsz($tartirsz)
    {
        $this->tartirsz = $tartirsz;

        return $this;
    }

    /**
     * Get tartirsz
     *
     * @return string
     */
    public function getTartirsz()
    {
        return $this->tartirsz;
    }

    /**
     * Set tartkozteruletnev
     *
     * @param string $tartkozteruletnev
     *
     * @return Diak
     */
    public function setTartkozteruletnev($tartkozteruletnev)
    {
        $this->tartkozteruletnev = $tartkozteruletnev;

        return $this;
    }

    /**
     * Get tartkozteruletnev
     *
     * @return string
     */
    public function getTartkozteruletnev()
    {
        return $this->tartkozteruletnev;
    }

    /**
     * Set tarthazszam
     *
     * @param string $tarthazszam
     *
     * @return Diak
     */
    public function setTarthazszam($tarthazszam)
    {
        $this->tarthazszam = $tarthazszam;

        return $this;
    }

    /**
     * Get tarthazszam
     *
     * @return string
     */
    public function getTarthazszam()
    {
        return $this->tarthazszam;
    }

    /**
     * Set tartemelet
     *
     * @param string $tartemelet
     *
     * @return Diak
     */
    public function setTartemelet($tartemelet)
    {
        $this->tartemelet = $tartemelet;

        return $this;
    }

    /**
     * Get tartemelet
     *
     * @return string
     */
    public function getTartemelet()
    {
        return $this->tartemelet;
    }

    /**
     * Set tartajto
     *
     * @param string $tartajto
     *
     * @return Diak
     */
    public function setTartajto($tartajto)
    {
        $this->tartajto = $tartajto;

        return $this;
    }

    /**
     * Get tartajto
     *
     * @return string
     */
    public function getTartajto()
    {
        return $this->tartajto;
    }

    /**
     * Set jogviszonykezdete
     *
     * @param \DateTime $jogviszonykezdete
     *
     * @return Diak
     */
    public function setJogviszonykezdete($jogviszonykezdete)
    {
        $this->jogviszonykezdete = $jogviszonykezdete;

        return $this;
    }

    /**
     * Get jogviszonykezdete
     *
     * @return \DateTime
     */
    public function getJogviszonykezdete()
    {
        return $this->jogviszonykezdete;
    }

    /**
     * Set jogviszonyvege
     *
     * @param \DateTime $jogviszonyvege
     *
     * @return Diak
     */
    public function setJogviszonyvege($jogviszonyvege)
    {
        $this->jogviszonyvege = $jogviszonyvege;

        return $this;
    }

    /**
     * Get jogviszonyvege
     *
     * @return \DateTime
     */
    public function getJogviszonyvege()
    {
        return $this->jogviszonyvege;
    }

    /**
     * Set telefon
     *
     * @param string $telefon
     *
     * @return Diak
     */
    public function setTelefon($telefon)
    {
        $this->telefon = $telefon;

        return $this;
    }

    /**
     * Get telefon
     *
     * @return string
     */
    public function getTelefon()
    {
        return $this->telefon;
    }

    /**
     * Set mobil
     *
     * @param string $mobil
     *
     * @return Diak
     */
    public function setMobil($mobil)
    {
        $this->mobil = $mobil;

        return $this;
    }

    /**
     * Get mobil
     *
     * @return string
     */
    public function getMobil()
    {
        return $this->mobil;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Diak
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set statusz
     *
     * @param string $statusz
     *
     * @return Diak
     */
    public function setStatusz($statusz)
    {
        $this->statusz = $statusz;

        return $this;
    }

    /**
     * Get statusz
     *
     * @return string
     */
    public function getStatusz()
    {
        return $this->statusz;
    }

    /**
     * Set penzugyistatusz
     *
     * @param string $penzugyistatusz
     *
     * @return Diak
     */
    public function setPenzugyistatusz($penzugyistatusz)
    {
        $this->penzugyistatusz = $penzugyistatusz;

        return $this;
    }

    /**
     * Get penzugyistatusz
     *
     * @return string
     */
    public function getPenzugyistatusz()
    {
        return $this->penzugyistatusz;
    }

    /**
     * Set tartkozteruletjelleg
     *
     * @param string $tartkozteruletjelleg
     *
     * @return Diak
     */
    public function setTartkozteruletjelleg($tartkozteruletjelleg)
    {
        $this->tartkozteruletjelleg = $tartkozteruletjelleg;

        return $this;
    }

    /**
     * Get tartkozteruletjelleg
     *
     * @return string
     */
    public function getTartkozteruletjelleg()
    {
        return $this->tartkozteruletjelleg;
    }

    /**
     * Set szocialishelyzet
     *
     * @param array $szocialishelyzet
     *
     * @return Diak
     */
    public function setSzocialishelyzet($szocialishelyzet)
    {
        $this->szocialishelyzet = $szocialishelyzet;

        return $this;
    }

    /**
     * Get szocialishelyzet
     *
     * @return array
     */
    public function getSzocialishelyzet()
    {
        return $this->szocialishelyzet;
    }

    /**
     * Set fogyatekossag
     *
     * @param array $fogyatekossag
     *
     * @return Diak
     */
    public function setFogyatekossag($fogyatekossag)
    {
        $this->fogyatekossag = $fogyatekossag;

        return $this;
    }

    /**
     * Get fogyatekossag
     *
     * @return array
     */
    public function getFogyatekossag()
    {
        return $this->fogyatekossag;
    }

    /**
     * Set gondozasiszam
     *
     * @param string $gondozasiszam
     *
     * @return Diak
     */
    public function setGondozasiszam($gondozasiszam)
    {
        $this->gondozasiszam = $gondozasiszam;

        return $this;
    }

    /**
     * Get gondozasiszam
     *
     * @return string
     */
    public function getGondozasiszam()
    {
        return $this->gondozasiszam;
    }

    /**
     * Set elozoiskolaomkod
     *
     * @param integer $elozoiskolaomkod
     *
     * @return Diak
     */
    public function setElozoiskolaomkod($elozoiskolaomkod)
    {
        $this->elozoiskolaomkod = $elozoiskolaomkod;

        return $this;
    }

    /**
     * Get elozoiskolaomkod
     *
     * @return integer
     */
    public function getElozoiskolaomkod()
    {
        return $this->elozoiskolaomkod;
    }

    /**
     * Set lakohelyijellemzo
     *
     * @param string $lakohelyijellemzo
     *
     * @return Diak
     */
    public function setLakohelyijellemzo($lakohelyijellemzo)
    {
        $this->lakohelyijellemzo = $lakohelyijellemzo;

        return $this;
    }

    /**
     * Get lakohelyijellemzo
     *
     * @return string
     */
    public function getLakohelyijellemzo()
    {
        return $this->lakohelyijellemzo;
    }

    /**
     * Set torvenyeskepviselo
     *
     * @param array $torvenyeskepviselo
     *
     * @return Diak
     */
    public function setTorvenyeskepviselo($torvenyeskepviselo)
    {
        $this->torvenyeskepviselo = $torvenyeskepviselo;

        return $this;
    }

    /**
     * Get torvenyeskepviselo
     *
     * @return array
     */
    public function getTorvenyeskepviselo()
    {
        return $this->torvenyeskepviselo;
    }

    /**
     * Set megjegyzes
     *
     * @param string $megjegyzes
     *
     * @return Diak
     */
    public function setMegjegyzes($megjegyzes)
    {
        $this->megjegyzes = $megjegyzes;

        return $this;
    }

    /**
     * Get megjegyzes
     *
     * @return string
     */
    public function getMegjegyzes()
    {
        return $this->megjegyzes;
    }

    /**
     * Set nekazonosito
     *
     * @param string $nekazonosito
     *
     * @return Diak
     */
    public function setNekazonosito($nekazonosito)
    {
        $this->nekazonosito = $nekazonosito;

        return $this;
    }

    /**
     * Get nekazonosito
     *
     * @return string
     */
    public function getNekazonosito()
    {
        return $this->nekazonosito;
    }

    /**
     * Set torzslapszam
     *
     * @param string $torzslapszam
     *
     * @return Diak
     */
    public function setTorzslapszam($torzslapszam)
    {
        $this->torzslapszam = $torzslapszam;

        return $this;
    }

    /**
     * Get torzslapszam
     *
     * @return string
     */
    public function getTorzslapszam()
    {
        return $this->torzslapszam;
    }

    /**
     * Get diakid
     *
     * @return integer
     */
    public function getDiakid()
    {
        return $this->diakid;
    }

    /**
     * Set apaid
     *
     * @param \AppBundle\Entity\Szulo $apaid
     *
     * @return Diak
     */
    public function setApaid(\AppBundle\Entity\Szulo $apaid = null)
    {
        $this->apaid = $apaid;

        return $this;
    }

    /**
     * Get apaid
     *
     * @return \AppBundle\Entity\Szulo
     */
    public function getApaid()
    {
        return $this->apaid;
    }

    /**
     * Set kezdotanev
     *
     * @param \AppBundle\Entity\Szemeszter $kezdotanev
     *
     * @return Diak
     */
    public function setKezdotanev(\AppBundle\Entity\Szemeszter $kezdotanev = null)
    {
        $this->kezdotanev = $kezdotanev;

        return $this;
    }

    /**
     * Get kezdotanev
     *
     * @return \AppBundle\Entity\Szemeszter
     */
    public function getKezdotanev()
    {
        return $this->kezdotanev;
    }

    /**
     * Set gondviseloid
     *
     * @param \AppBundle\Entity\Szulo $gondviseloid
     *
     * @return Diak
     */
    public function setGondviseloid(\AppBundle\Entity\Szulo $gondviseloid = null)
    {
        $this->gondviseloid = $gondviseloid;

        return $this;
    }

    /**
     * Get gondviseloid
     *
     * @return \AppBundle\Entity\Szulo
     */
    public function getGondviseloid()
    {
        return $this->gondviseloid;
    }

    /**
     * Set anyaid
     *
     * @param \AppBundle\Entity\Szulo $anyaid
     *
     * @return Diak
     */
    public function setAnyaid(\AppBundle\Entity\Szulo $anyaid = null)
    {
        $this->anyaid = $anyaid;

        return $this;
    }

    /**
     * Get anyaid
     *
     * @return \AppBundle\Entity\Szulo
     */
    public function getAnyaid()
    {
        return $this->anyaid;
    }

    /**
     * Add tanev
     *
     * @param \AppBundle\Entity\Szemeszter $tanev
     *
     * @return Diak
     */
    public function addTanev(\AppBundle\Entity\Szemeszter $tanev)
    {
        $this->tanev[] = $tanev;

        return $this;
    }

    /**
     * Remove tanev
     *
     * @param \AppBundle\Entity\Szemeszter $tanev
     */
    public function removeTanev(\AppBundle\Entity\Szemeszter $tanev)
    {
        $this->tanev->removeElement($tanev);
    }

    /**
     * Get tanev
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTanev()
    {
        return $this->tanev;
    }

    /**
     * Add osztalyid
     *
     * @param \AppBundle\Entity\Osztaly $osztalyid
     *
     * @return Diak
     */
    public function addOsztalyid(\AppBundle\Entity\Osztaly $osztalyid)
    {
        $this->osztalyid[] = $osztalyid;

        return $this;
    }

    /**
     * Remove osztalyid
     *
     * @param \AppBundle\Entity\Osztaly $osztalyid
     */
    public function removeOsztalyid(\AppBundle\Entity\Osztaly $osztalyid)
    {
        $this->osztalyid->removeElement($osztalyid);
    }

    /**
     * Get osztalyid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOsztalyid()
    {
        return $this->osztalyid;
    }
}
