<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tanar
 *
 * @ORM\Table(name="tanar", uniqueConstraints={@ORM\UniqueConstraint(name="tanarOid", columns={"oId"})})
 * @ORM\Entity
 */
class Tanar
{
    /**
     * @var integer
     *
     * @ORM\Column(name="oId", type="bigint", nullable=true)
     */
    private $oid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="beDt", type="date", nullable=true)
     */
    private $bedt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="kiDt", type="date", nullable=true)
     */
    private $kidt;

    /**
     * @var string
     *
     * @ORM\Column(name="viseltNevElotag", type="string", length=8, nullable=false)
     */
    private $viseltnevelotag = '';

    /**
     * @var string
     *
     * @ORM\Column(name="viseltCsaladinev", type="string", length=64, nullable=false)
     */
    private $viseltcsaladinev = '';

    /**
     * @var string
     *
     * @ORM\Column(name="viseltUtonev", type="string", length=64, nullable=true)
     */
    private $viseltutonev = '';

    /**
     * @var string
     *
     * @ORM\Column(name="szuletesiHely", type="string", length=16, nullable=true)
     */
    private $szuletesihely;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="szuletesiIdo", type="date", nullable=true)
     */
    private $szuletesiido;

    /**
     * @var string
     *
     * @ORM\Column(name="dn", type="string", length=128, nullable=true)
     */
    private $dn;

    /**
     * @var string
     *
     * @ORM\Column(name="szuleteskoriUtonev", type="string", length=64, nullable=true)
     */
    private $szuleteskoriutonev = '';

    /**
     * @var string
     *
     * @ORM\Column(name="szuleteskoriCsaladinev", type="string", length=64, nullable=false)
     */
    private $szuleteskoricsaladinev = '';

    /**
     * @var string
     *
     * @ORM\Column(name="szuleteskoriNevElotag", type="string", length=8, nullable=false)
     */
    private $szuleteskorinevelotag = '';

    /**
     * @var string
     *
     * @ORM\Column(name="hetiMunkaora", type="decimal", precision=3, scale=1, nullable=true)
     */
    private $hetimunkaora = '0.0';

    /**
     * @var string
     *
     * @ORM\Column(name="NEKAzonosito", type="string", length=16, nullable=true)
     */
    private $nekazonosito;

    /**
     * @var string
     *
     * @ORM\Column(name="statusz", type="string", nullable=true)
     */
    private $statusz = 'határozatlan idejű';

    /**
     * @var string
     *
     * @ORM\Column(name="hetiKotelezoOraszam", type="decimal", precision=3, scale=1, nullable=true)
     */
    private $hetikotelezooraszam = '0.0';

    /**
     * @var string
     *
     * @ORM\Column(name="megjegyzes", type="string", length=255, nullable=true)
     */
    private $megjegyzes;

    /**
     * @var string
     *
     * @ORM\Column(name="besorolas", type="string", nullable=true)
     */
    private $besorolas = 'Pedagógus I.';

    /**
     * @var integer
     *
     * @ORM\Column(name="tanarId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tanarid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Kepesites", inversedBy="tanarid")
     * @ORM\JoinTable(name="tanarkepesites",
     *   joinColumns={
     *     @ORM\JoinColumn(name="tanarId", referencedColumnName="tanarId")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="kepesitesId", referencedColumnName="kepesitesId")
     *   }
     * )
     */
    private $kepesitesid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Munkakozosseg", mappedBy="tanarid")
     */
    private $mkid;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->kepesitesid = new \Doctrine\Common\Collections\ArrayCollection();
        $this->mkid = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set oid
     *
     * @param integer $oid
     *
     * @return Tanar
     */
    public function setOid($oid)
    {
        $this->oid = $oid;

        return $this;
    }

    /**
     * Get oid
     *
     * @return integer
     */
    public function getOid()
    {
        return $this->oid;
    }

    /**
     * Set bedt
     *
     * @param \DateTime $bedt
     *
     * @return Tanar
     */
    public function setBedt($bedt)
    {
        $this->bedt = $bedt;

        return $this;
    }

    /**
     * Get bedt
     *
     * @return \DateTime
     */
    public function getBedt()
    {
        return $this->bedt;
    }

    /**
     * Set kidt
     *
     * @param \DateTime $kidt
     *
     * @return Tanar
     */
    public function setKidt($kidt)
    {
        $this->kidt = $kidt;

        return $this;
    }

    /**
     * Get kidt
     *
     * @return \DateTime
     */
    public function getKidt()
    {
        return $this->kidt;
    }

    /**
     * Set viseltnevelotag
     *
     * @param string $viseltnevelotag
     *
     * @return Tanar
     */
    public function setViseltnevelotag($viseltnevelotag)
    {
        $this->viseltnevelotag = $viseltnevelotag;

        return $this;
    }

    /**
     * Get viseltnevelotag
     *
     * @return string
     */
    public function getViseltnevelotag()
    {
        return $this->viseltnevelotag;
    }

    /**
     * Set viseltcsaladinev
     *
     * @param string $viseltcsaladinev
     *
     * @return Tanar
     */
    public function setViseltcsaladinev($viseltcsaladinev)
    {
        $this->viseltcsaladinev = $viseltcsaladinev;

        return $this;
    }

    /**
     * Get viseltcsaladinev
     *
     * @return string
     */
    public function getViseltcsaladinev()
    {
        return $this->viseltcsaladinev;
    }

    /**
     * Set viseltutonev
     *
     * @param string $viseltutonev
     *
     * @return Tanar
     */
    public function setViseltutonev($viseltutonev)
    {
        $this->viseltutonev = $viseltutonev;

        return $this;
    }

    /**
     * Get viseltutonev
     *
     * @return string
     */
    public function getViseltutonev()
    {
        return $this->viseltutonev;
    }

    /**
     * Set szuletesihely
     *
     * @param string $szuletesihely
     *
     * @return Tanar
     */
    public function setSzuletesihely($szuletesihely)
    {
        $this->szuletesihely = $szuletesihely;

        return $this;
    }

    /**
     * Get szuletesihely
     *
     * @return string
     */
    public function getSzuletesihely()
    {
        return $this->szuletesihely;
    }

    /**
     * Set szuletesiido
     *
     * @param \DateTime $szuletesiido
     *
     * @return Tanar
     */
    public function setSzuletesiido($szuletesiido)
    {
        $this->szuletesiido = $szuletesiido;

        return $this;
    }

    /**
     * Get szuletesiido
     *
     * @return \DateTime
     */
    public function getSzuletesiido()
    {
        return $this->szuletesiido;
    }

    /**
     * Set dn
     *
     * @param string $dn
     *
     * @return Tanar
     */
    public function setDn($dn)
    {
        $this->dn = $dn;

        return $this;
    }

    /**
     * Get dn
     *
     * @return string
     */
    public function getDn()
    {
        return $this->dn;
    }

    /**
     * Set szuleteskoriutonev
     *
     * @param string $szuleteskoriutonev
     *
     * @return Tanar
     */
    public function setSzuleteskoriutonev($szuleteskoriutonev)
    {
        $this->szuleteskoriutonev = $szuleteskoriutonev;

        return $this;
    }

    /**
     * Get szuleteskoriutonev
     *
     * @return string
     */
    public function getSzuleteskoriutonev()
    {
        return $this->szuleteskoriutonev;
    }

    /**
     * Set szuleteskoricsaladinev
     *
     * @param string $szuleteskoricsaladinev
     *
     * @return Tanar
     */
    public function setSzuleteskoricsaladinev($szuleteskoricsaladinev)
    {
        $this->szuleteskoricsaladinev = $szuleteskoricsaladinev;

        return $this;
    }

    /**
     * Get szuleteskoricsaladinev
     *
     * @return string
     */
    public function getSzuleteskoricsaladinev()
    {
        return $this->szuleteskoricsaladinev;
    }

    /**
     * Set szuleteskorinevelotag
     *
     * @param string $szuleteskorinevelotag
     *
     * @return Tanar
     */
    public function setSzuleteskorinevelotag($szuleteskorinevelotag)
    {
        $this->szuleteskorinevelotag = $szuleteskorinevelotag;

        return $this;
    }

    /**
     * Get szuleteskorinevelotag
     *
     * @return string
     */
    public function getSzuleteskorinevelotag()
    {
        return $this->szuleteskorinevelotag;
    }

    /**
     * Set hetimunkaora
     *
     * @param string $hetimunkaora
     *
     * @return Tanar
     */
    public function setHetimunkaora($hetimunkaora)
    {
        $this->hetimunkaora = $hetimunkaora;

        return $this;
    }

    /**
     * Get hetimunkaora
     *
     * @return string
     */
    public function getHetimunkaora()
    {
        return $this->hetimunkaora;
    }

    /**
     * Set nekazonosito
     *
     * @param string $nekazonosito
     *
     * @return Tanar
     */
    public function setNekazonosito($nekazonosito)
    {
        $this->nekazonosito = $nekazonosito;

        return $this;
    }

    /**
     * Get nekazonosito
     *
     * @return string
     */
    public function getNekazonosito()
    {
        return $this->nekazonosito;
    }

    /**
     * Set statusz
     *
     * @param string $statusz
     *
     * @return Tanar
     */
    public function setStatusz($statusz)
    {
        $this->statusz = $statusz;

        return $this;
    }

    /**
     * Get statusz
     *
     * @return string
     */
    public function getStatusz()
    {
        return $this->statusz;
    }

    /**
     * Set hetikotelezooraszam
     *
     * @param string $hetikotelezooraszam
     *
     * @return Tanar
     */
    public function setHetikotelezooraszam($hetikotelezooraszam)
    {
        $this->hetikotelezooraszam = $hetikotelezooraszam;

        return $this;
    }

    /**
     * Get hetikotelezooraszam
     *
     * @return string
     */
    public function getHetikotelezooraszam()
    {
        return $this->hetikotelezooraszam;
    }

    /**
     * Set megjegyzes
     *
     * @param string $megjegyzes
     *
     * @return Tanar
     */
    public function setMegjegyzes($megjegyzes)
    {
        $this->megjegyzes = $megjegyzes;

        return $this;
    }

    /**
     * Get megjegyzes
     *
     * @return string
     */
    public function getMegjegyzes()
    {
        return $this->megjegyzes;
    }

    /**
     * Set besorolas
     *
     * @param string $besorolas
     *
     * @return Tanar
     */
    public function setBesorolas($besorolas)
    {
        $this->besorolas = $besorolas;

        return $this;
    }

    /**
     * Get besorolas
     *
     * @return string
     */
    public function getBesorolas()
    {
        return $this->besorolas;
    }

    /**
     * Get tanarid
     *
     * @return integer
     */
    public function getTanarid()
    {
        return $this->tanarid;
    }

    /**
     * Add kepesitesid
     *
     * @param \AppBundle\Entity\Kepesites $kepesitesid
     *
     * @return Tanar
     */
    public function addKepesitesid(\AppBundle\Entity\Kepesites $kepesitesid)
    {
        $this->kepesitesid[] = $kepesitesid;

        return $this;
    }

    /**
     * Remove kepesitesid
     *
     * @param \AppBundle\Entity\Kepesites $kepesitesid
     */
    public function removeKepesitesid(\AppBundle\Entity\Kepesites $kepesitesid)
    {
        $this->kepesitesid->removeElement($kepesitesid);
    }

    /**
     * Get kepesitesid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKepesitesid()
    {
        return $this->kepesitesid;
    }

    /**
     * Add mkid
     *
     * @param \AppBundle\Entity\Munkakozosseg $mkid
     *
     * @return Tanar
     */
    public function addMkid(\AppBundle\Entity\Munkakozosseg $mkid)
    {
        $this->mkid[] = $mkid;

        return $this;
    }

    /**
     * Remove mkid
     *
     * @param \AppBundle\Entity\Munkakozosseg $mkid
     */
    public function removeMkid(\AppBundle\Entity\Munkakozosseg $mkid)
    {
        $this->mkid->removeElement($mkid);
    }

    /**
     * Get mkid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMkid()
    {
        return $this->mkid;
    }
}
