<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Osztaly
 *
 * @ORM\Table(name="osztaly", indexes={@ORM\Index(name="osztaly_telephely", columns={"telephelyId"}), @ORM\Index(name="osztaly_ibfk_1", columns={"osztalyJellegId"})})
 * @ORM\Entity
 */
class Osztaly
{
    /**
     * @var string
     *
     * @ORM\Column(name="leiras", type="string", length=64, nullable=true)
     */
    private $leiras;

    /**
     * @var integer
     *
     * @ORM\Column(name="kezdoTanev", type="smallint", nullable=true)
     */
    private $kezdotanev;

    /**
     * @var integer
     *
     * @ORM\Column(name="vegzoTanev", type="smallint", nullable=true)
     */
    private $vegzotanev;

    /**
     * @var string
     *
     * @ORM\Column(name="jel", type="string", length=20, nullable=true)
     */
    private $jel;

    /**
     * @var boolean
     *
     * @ORM\Column(name="_kezdoEvfolyam", type="boolean", nullable=true)
     */
    private $kezdoevfolyam;

    /**
     * @var boolean
     *
     * @ORM\Column(name="kezdoEvfolyamSorszam", type="boolean", nullable=true)
     */
    private $kezdoevfolyamsorszam = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="osztalyId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $osztalyid;

    /**
     * @var \AppBundle\Entity\Osztalyjelleg
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Osztalyjelleg")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="osztalyJellegId", referencedColumnName="osztalyJellegId")
     * })
     */
    private $osztalyjellegid;

    /**
     * @var \AppBundle\Entity\Telephely
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Telephely")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="telephelyId", referencedColumnName="telephelyId")
     * })
     */
    private $telephelyid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Tankor", mappedBy="osztalyid")
     */
    private $tankorid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Diak", inversedBy="osztalyid")
     * @ORM\JoinTable(name="diaktorzslapszam",
     *   joinColumns={
     *     @ORM\JoinColumn(name="osztalyId", referencedColumnName="osztalyId")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="diakId", referencedColumnName="diakId")
     *   }
     * )
     */
    private $diakid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Kepzes", mappedBy="osztalyid")
     */
    private $kepzesid;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tankorid = new \Doctrine\Common\Collections\ArrayCollection();
        $this->diakid = new \Doctrine\Common\Collections\ArrayCollection();
        $this->kepzesid = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set leiras
     *
     * @param string $leiras
     *
     * @return Osztaly
     */
    public function setLeiras($leiras)
    {
        $this->leiras = $leiras;

        return $this;
    }

    /**
     * Get leiras
     *
     * @return string
     */
    public function getLeiras()
    {
        return $this->leiras;
    }

    /**
     * Set kezdotanev
     *
     * @param integer $kezdotanev
     *
     * @return Osztaly
     */
    public function setKezdotanev($kezdotanev)
    {
        $this->kezdotanev = $kezdotanev;

        return $this;
    }

    /**
     * Get kezdotanev
     *
     * @return integer
     */
    public function getKezdotanev()
    {
        return $this->kezdotanev;
    }

    /**
     * Set vegzotanev
     *
     * @param integer $vegzotanev
     *
     * @return Osztaly
     */
    public function setVegzotanev($vegzotanev)
    {
        $this->vegzotanev = $vegzotanev;

        return $this;
    }

    /**
     * Get vegzotanev
     *
     * @return integer
     */
    public function getVegzotanev()
    {
        return $this->vegzotanev;
    }

    /**
     * Set jel
     *
     * @param string $jel
     *
     * @return Osztaly
     */
    public function setJel($jel)
    {
        $this->jel = $jel;

        return $this;
    }

    /**
     * Get jel
     *
     * @return string
     */
    public function getJel()
    {
        return $this->jel;
    }

    /**
     * Set kezdoevfolyam
     *
     * @param boolean $kezdoevfolyam
     *
     * @return Osztaly
     */
    public function setKezdoevfolyam($kezdoevfolyam)
    {
        $this->kezdoevfolyam = $kezdoevfolyam;

        return $this;
    }

    /**
     * Get kezdoevfolyam
     *
     * @return boolean
     */
    public function getKezdoevfolyam()
    {
        return $this->kezdoevfolyam;
    }

    /**
     * Set kezdoevfolyamsorszam
     *
     * @param boolean $kezdoevfolyamsorszam
     *
     * @return Osztaly
     */
    public function setKezdoevfolyamsorszam($kezdoevfolyamsorszam)
    {
        $this->kezdoevfolyamsorszam = $kezdoevfolyamsorszam;

        return $this;
    }

    /**
     * Get kezdoevfolyamsorszam
     *
     * @return boolean
     */
    public function getKezdoevfolyamsorszam()
    {
        return $this->kezdoevfolyamsorszam;
    }

    /**
     * Get osztalyid
     *
     * @return integer
     */
    public function getOsztalyid()
    {
        return $this->osztalyid;
    }

    /**
     * Set osztalyjellegid
     *
     * @param \AppBundle\Entity\Osztalyjelleg $osztalyjellegid
     *
     * @return Osztaly
     */
    public function setOsztalyjellegid(\AppBundle\Entity\Osztalyjelleg $osztalyjellegid = null)
    {
        $this->osztalyjellegid = $osztalyjellegid;

        return $this;
    }

    /**
     * Get osztalyjellegid
     *
     * @return \AppBundle\Entity\Osztalyjelleg
     */
    public function getOsztalyjellegid()
    {
        return $this->osztalyjellegid;
    }

    /**
     * Set telephelyid
     *
     * @param \AppBundle\Entity\Telephely $telephelyid
     *
     * @return Osztaly
     */
    public function setTelephelyid(\AppBundle\Entity\Telephely $telephelyid = null)
    {
        $this->telephelyid = $telephelyid;

        return $this;
    }

    /**
     * Get telephelyid
     *
     * @return \AppBundle\Entity\Telephely
     */
    public function getTelephelyid()
    {
        return $this->telephelyid;
    }

    /**
     * Add tankorid
     *
     * @param \AppBundle\Entity\Tankor $tankorid
     *
     * @return Osztaly
     */
    public function addTankorid(\AppBundle\Entity\Tankor $tankorid)
    {
        $this->tankorid[] = $tankorid;

        return $this;
    }

    /**
     * Remove tankorid
     *
     * @param \AppBundle\Entity\Tankor $tankorid
     */
    public function removeTankorid(\AppBundle\Entity\Tankor $tankorid)
    {
        $this->tankorid->removeElement($tankorid);
    }

    /**
     * Get tankorid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTankorid()
    {
        return $this->tankorid;
    }

    /**
     * Add diakid
     *
     * @param \AppBundle\Entity\Diak $diakid
     *
     * @return Osztaly
     */
    public function addDiakid(\AppBundle\Entity\Diak $diakid)
    {
        $this->diakid[] = $diakid;

        return $this;
    }

    /**
     * Remove diakid
     *
     * @param \AppBundle\Entity\Diak $diakid
     */
    public function removeDiakid(\AppBundle\Entity\Diak $diakid)
    {
        $this->diakid->removeElement($diakid);
    }

    /**
     * Get diakid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDiakid()
    {
        return $this->diakid;
    }

    /**
     * Add kepzesid
     *
     * @param \AppBundle\Entity\Kepzes $kepzesid
     *
     * @return Osztaly
     */
    public function addKepzesid(\AppBundle\Entity\Kepzes $kepzesid)
    {
        $this->kepzesid[] = $kepzesid;

        return $this;
    }

    /**
     * Remove kepzesid
     *
     * @param \AppBundle\Entity\Kepzes $kepzesid
     */
    public function removeKepzesid(\AppBundle\Entity\Kepzes $kepzesid)
    {
        $this->kepzesid->removeElement($kepzesid);
    }

    /**
     * Get kepzesid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKepzesid()
    {
        return $this->kepzesid;
    }
}
