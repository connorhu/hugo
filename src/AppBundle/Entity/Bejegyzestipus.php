<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bejegyzestipus
 *
 * @ORM\Table(name="bejegyzesTipus")
 * @ORM\Entity
 */
class Bejegyzestipus
{
    /**
     * @var string
     *
     * @ORM\Column(name="tipus", type="string", nullable=false)
     */
    private $tipus;

    /**
     * @var boolean
     *
     * @ORM\Column(name="fokozat", type="boolean", nullable=false)
     */
    private $fokozat;

    /**
     * @var string
     *
     * @ORM\Column(name="bejegyzesTipusNev", type="string", length=128, nullable=true)
     */
    private $bejegyzestipusnev;

    /**
     * @var boolean
     *
     * @ORM\Column(name="hianyzasDb", type="boolean", nullable=true)
     */
    private $hianyzasdb;

    /**
     * @var array
     *
     * @ORM\Column(name="jogosult", type="simple_array", nullable=true)
     */
    private $jogosult;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tolDt", type="date", nullable=true)
     */
    private $toldt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="igDt", type="date", nullable=true)
     */
    private $igdt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bejegyzesTipusId", type="boolean")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $bejegyzestipusid;



    /**
     * Set tipus
     *
     * @param string $tipus
     *
     * @return Bejegyzestipus
     */
    public function setTipus($tipus)
    {
        $this->tipus = $tipus;

        return $this;
    }

    /**
     * Get tipus
     *
     * @return string
     */
    public function getTipus()
    {
        return $this->tipus;
    }

    /**
     * Set fokozat
     *
     * @param boolean $fokozat
     *
     * @return Bejegyzestipus
     */
    public function setFokozat($fokozat)
    {
        $this->fokozat = $fokozat;

        return $this;
    }

    /**
     * Get fokozat
     *
     * @return boolean
     */
    public function getFokozat()
    {
        return $this->fokozat;
    }

    /**
     * Set bejegyzestipusnev
     *
     * @param string $bejegyzestipusnev
     *
     * @return Bejegyzestipus
     */
    public function setBejegyzestipusnev($bejegyzestipusnev)
    {
        $this->bejegyzestipusnev = $bejegyzestipusnev;

        return $this;
    }

    /**
     * Get bejegyzestipusnev
     *
     * @return string
     */
    public function getBejegyzestipusnev()
    {
        return $this->bejegyzestipusnev;
    }

    /**
     * Set hianyzasdb
     *
     * @param boolean $hianyzasdb
     *
     * @return Bejegyzestipus
     */
    public function setHianyzasdb($hianyzasdb)
    {
        $this->hianyzasdb = $hianyzasdb;

        return $this;
    }

    /**
     * Get hianyzasdb
     *
     * @return boolean
     */
    public function getHianyzasdb()
    {
        return $this->hianyzasdb;
    }

    /**
     * Set jogosult
     *
     * @param array $jogosult
     *
     * @return Bejegyzestipus
     */
    public function setJogosult($jogosult)
    {
        $this->jogosult = $jogosult;

        return $this;
    }

    /**
     * Get jogosult
     *
     * @return array
     */
    public function getJogosult()
    {
        return $this->jogosult;
    }

    /**
     * Set toldt
     *
     * @param \DateTime $toldt
     *
     * @return Bejegyzestipus
     */
    public function setToldt($toldt)
    {
        $this->toldt = $toldt;

        return $this;
    }

    /**
     * Get toldt
     *
     * @return \DateTime
     */
    public function getToldt()
    {
        return $this->toldt;
    }

    /**
     * Set igdt
     *
     * @param \DateTime $igdt
     *
     * @return Bejegyzestipus
     */
    public function setIgdt($igdt)
    {
        $this->igdt = $igdt;

        return $this;
    }

    /**
     * Get igdt
     *
     * @return \DateTime
     */
    public function getIgdt()
    {
        return $this->igdt;
    }

    /**
     * Get bejegyzestipusid
     *
     * @return boolean
     */
    public function getBejegyzestipusid()
    {
        return $this->bejegyzestipusid;
    }
}
