<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tanmenettemakor
 *
 * @ORM\Table(name="tanmenetTemakor", indexes={@ORM\Index(name="IDX_ADFD0F29326F86AA", columns={"tanmenetId"})})
 * @ORM\Entity
 */
class Tanmenettemakor
{
    /**
     * @var boolean
     *
     * @ORM\Column(name="oraszam", type="boolean", nullable=false)
     */
    private $oraszam;

    /**
     * @var string
     *
     * @ORM\Column(name="temakorMegnevezes", type="text", length=65535, nullable=true)
     */
    private $temakormegnevezes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="sorszam", type="boolean")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $sorszam;

    /**
     * @var \AppBundle\Entity\Tanmenet
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Tanmenet")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tanmenetId", referencedColumnName="tanmenetId")
     * })
     */
    private $tanmenetid;



    /**
     * Set oraszam
     *
     * @param boolean $oraszam
     *
     * @return Tanmenettemakor
     */
    public function setOraszam($oraszam)
    {
        $this->oraszam = $oraszam;

        return $this;
    }

    /**
     * Get oraszam
     *
     * @return boolean
     */
    public function getOraszam()
    {
        return $this->oraszam;
    }

    /**
     * Set temakormegnevezes
     *
     * @param string $temakormegnevezes
     *
     * @return Tanmenettemakor
     */
    public function setTemakormegnevezes($temakormegnevezes)
    {
        $this->temakormegnevezes = $temakormegnevezes;

        return $this;
    }

    /**
     * Get temakormegnevezes
     *
     * @return string
     */
    public function getTemakormegnevezes()
    {
        return $this->temakormegnevezes;
    }

    /**
     * Set sorszam
     *
     * @param boolean $sorszam
     *
     * @return Tanmenettemakor
     */
    public function setSorszam($sorszam)
    {
        $this->sorszam = $sorszam;

        return $this;
    }

    /**
     * Get sorszam
     *
     * @return boolean
     */
    public function getSorszam()
    {
        return $this->sorszam;
    }

    /**
     * Set tanmenetid
     *
     * @param \AppBundle\Entity\Tanmenet $tanmenetid
     *
     * @return Tanmenettemakor
     */
    public function setTanmenetid(\AppBundle\Entity\Tanmenet $tanmenetid)
    {
        $this->tanmenetid = $tanmenetid;

        return $this;
    }

    /**
     * Get tanmenetid
     *
     * @return \AppBundle\Entity\Tanmenet
     */
    public function getTanmenetid()
    {
        return $this->tanmenetid;
    }
}
