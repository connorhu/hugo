<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Koszipont
 *
 * @ORM\Table(name="kosziPont", uniqueConstraints={@ORM\UniqueConstraint(name="kosziEsemenyId", columns={"kosziEsemenyId", "kosziPontTipus", "kosziHelyezes"})}, indexes={@ORM\Index(name="IDX_F67F385E38276183", columns={"kosziEsemenyId"})})
 * @ORM\Entity
 */
class Koszipont
{
    /**
     * @var string
     *
     * @ORM\Column(name="kosziPontTipus", type="string", nullable=false)
     */
    private $kosziponttipus = 'résztvevő';

    /**
     * @var integer
     *
     * @ORM\Column(name="kosziPont", type="integer", nullable=false)
     */
    private $koszipont = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="kosziHelyezes", type="integer", nullable=true)
     */
    private $koszihelyezes;

    /**
     * @var integer
     *
     * @ORM\Column(name="kosziPontId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $koszipontid;

    /**
     * @var \AppBundle\Entity\Kosziesemeny
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Kosziesemeny")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="kosziEsemenyId", referencedColumnName="kosziEsemenyId")
     * })
     */
    private $kosziesemenyid;



    /**
     * Set kosziponttipus
     *
     * @param string $kosziponttipus
     *
     * @return Koszipont
     */
    public function setKosziponttipus($kosziponttipus)
    {
        $this->kosziponttipus = $kosziponttipus;

        return $this;
    }

    /**
     * Get kosziponttipus
     *
     * @return string
     */
    public function getKosziponttipus()
    {
        return $this->kosziponttipus;
    }

    /**
     * Set koszipont
     *
     * @param integer $koszipont
     *
     * @return Koszipont
     */
    public function setKoszipont($koszipont)
    {
        $this->koszipont = $koszipont;

        return $this;
    }

    /**
     * Get koszipont
     *
     * @return integer
     */
    public function getKoszipont()
    {
        return $this->koszipont;
    }

    /**
     * Set koszihelyezes
     *
     * @param integer $koszihelyezes
     *
     * @return Koszipont
     */
    public function setKoszihelyezes($koszihelyezes)
    {
        $this->koszihelyezes = $koszihelyezes;

        return $this;
    }

    /**
     * Get koszihelyezes
     *
     * @return integer
     */
    public function getKoszihelyezes()
    {
        return $this->koszihelyezes;
    }

    /**
     * Get koszipontid
     *
     * @return integer
     */
    public function getKoszipontid()
    {
        return $this->koszipontid;
    }

    /**
     * Set kosziesemenyid
     *
     * @param \AppBundle\Entity\Kosziesemeny $kosziesemenyid
     *
     * @return Koszipont
     */
    public function setKosziesemenyid(\AppBundle\Entity\Kosziesemeny $kosziesemenyid = null)
    {
        $this->kosziesemenyid = $kosziesemenyid;

        return $this;
    }

    /**
     * Get kosziesemenyid
     *
     * @return \AppBundle\Entity\Kosziesemeny
     */
    public function getKosziesemenyid()
    {
        return $this->kosziesemenyid;
    }
}
