<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Szrminosites
 *
 * @ORM\Table(name="szrMinosites", indexes={@ORM\Index(name="szrm_FKindex1", columns={"szempontId"})})
 * @ORM\Entity
 */
class Szrminosites
{
    /**
     * @var string
     *
     * @ORM\Column(name="minosites", type="string", length=255, nullable=true)
     */
    private $minosites;

    /**
     * @var integer
     *
     * @ORM\Column(name="minositesId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $minositesid;

    /**
     * @var \AppBundle\Entity\Szrszempont
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Szrszempont")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="szempontId", referencedColumnName="szempontId")
     * })
     */
    private $szempontid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Szovegesertekeles", mappedBy="minositesid")
     */
    private $szeid;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->szeid = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set minosites
     *
     * @param string $minosites
     *
     * @return Szrminosites
     */
    public function setMinosites($minosites)
    {
        $this->minosites = $minosites;

        return $this;
    }

    /**
     * Get minosites
     *
     * @return string
     */
    public function getMinosites()
    {
        return $this->minosites;
    }

    /**
     * Get minositesid
     *
     * @return integer
     */
    public function getMinositesid()
    {
        return $this->minositesid;
    }

    /**
     * Set szempontid
     *
     * @param \AppBundle\Entity\Szrszempont $szempontid
     *
     * @return Szrminosites
     */
    public function setSzempontid(\AppBundle\Entity\Szrszempont $szempontid = null)
    {
        $this->szempontid = $szempontid;

        return $this;
    }

    /**
     * Get szempontid
     *
     * @return \AppBundle\Entity\Szrszempont
     */
    public function getSzempontid()
    {
        return $this->szempontid;
    }

    /**
     * Add szeid
     *
     * @param \AppBundle\Entity\Szovegesertekeles $szeid
     *
     * @return Szrminosites
     */
    public function addSzeid(\AppBundle\Entity\Szovegesertekeles $szeid)
    {
        $this->szeid[] = $szeid;

        return $this;
    }

    /**
     * Remove szeid
     *
     * @param \AppBundle\Entity\Szovegesertekeles $szeid
     */
    public function removeSzeid(\AppBundle\Entity\Szovegesertekeles $szeid)
    {
        $this->szeid->removeElement($szeid);
    }

    /**
     * Get szeid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSzeid()
    {
        return $this->szeid;
    }
}
