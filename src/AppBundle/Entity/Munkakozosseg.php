<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Munkakozosseg
 *
 * @ORM\Table(name="munkakozosseg")
 * @ORM\Entity
 */
class Munkakozosseg
{
    /**
     * @var integer
     *
     * @ORM\Column(name="mkVezId", type="integer", nullable=true)
     */
    private $mkvezid;

    /**
     * @var string
     *
     * @ORM\Column(name="leiras", type="string", length=32, nullable=true)
     */
    private $leiras;

    /**
     * @var integer
     *
     * @ORM\Column(name="mkId", type="smallint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $mkid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Tanar", inversedBy="mkid")
     * @ORM\JoinTable(name="mktanar",
     *   joinColumns={
     *     @ORM\JoinColumn(name="mkId", referencedColumnName="mkId")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="tanarId", referencedColumnName="tanarId")
     *   }
     * )
     */
    private $tanarid;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tanarid = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set mkvezid
     *
     * @param integer $mkvezid
     *
     * @return Munkakozosseg
     */
    public function setMkvezid($mkvezid)
    {
        $this->mkvezid = $mkvezid;

        return $this;
    }

    /**
     * Get mkvezid
     *
     * @return integer
     */
    public function getMkvezid()
    {
        return $this->mkvezid;
    }

    /**
     * Set leiras
     *
     * @param string $leiras
     *
     * @return Munkakozosseg
     */
    public function setLeiras($leiras)
    {
        $this->leiras = $leiras;

        return $this;
    }

    /**
     * Get leiras
     *
     * @return string
     */
    public function getLeiras()
    {
        return $this->leiras;
    }

    /**
     * Get mkid
     *
     * @return integer
     */
    public function getMkid()
    {
        return $this->mkid;
    }

    /**
     * Add tanarid
     *
     * @param \AppBundle\Entity\Tanar $tanarid
     *
     * @return Munkakozosseg
     */
    public function addTanarid(\AppBundle\Entity\Tanar $tanarid)
    {
        $this->tanarid[] = $tanarid;

        return $this;
    }

    /**
     * Remove tanarid
     *
     * @param \AppBundle\Entity\Tanar $tanarid
     */
    public function removeTanarid(\AppBundle\Entity\Tanar $tanarid)
    {
        $this->tanarid->removeElement($tanarid);
    }

    /**
     * Get tanarid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTanarid()
    {
        return $this->tanarid;
    }
}
