<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Settings
 *
 * @ORM\Table(name="settings")
 * @ORM\Entity
 */
class Settings
{
    /**
     * @var string
     *
     * @ORM\Column(name="policy", type="string", nullable=false)
     */
    private $policy = 'private';

    /**
     * @var string
     *
     * @ORM\Column(name="intezmeny", type="string", length=16, nullable=true)
     */
    private $intezmeny;

    /**
     * @var boolean
     *
     * @ORM\Column(name="telephelyId", type="boolean", nullable=true)
     */
    private $telephelyid;

    /**
     * @var string
     *
     * @ORM\Column(name="userAccount", type="string", length=32)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $useraccount;



    /**
     * Set policy
     *
     * @param string $policy
     *
     * @return Settings
     */
    public function setPolicy($policy)
    {
        $this->policy = $policy;

        return $this;
    }

    /**
     * Get policy
     *
     * @return string
     */
    public function getPolicy()
    {
        return $this->policy;
    }

    /**
     * Set intezmeny
     *
     * @param string $intezmeny
     *
     * @return Settings
     */
    public function setIntezmeny($intezmeny)
    {
        $this->intezmeny = $intezmeny;

        return $this;
    }

    /**
     * Get intezmeny
     *
     * @return string
     */
    public function getIntezmeny()
    {
        return $this->intezmeny;
    }

    /**
     * Set telephelyid
     *
     * @param boolean $telephelyid
     *
     * @return Settings
     */
    public function setTelephelyid($telephelyid)
    {
        $this->telephelyid = $telephelyid;

        return $this;
    }

    /**
     * Get telephelyid
     *
     * @return boolean
     */
    public function getTelephelyid()
    {
        return $this->telephelyid;
    }

    /**
     * Get useraccount
     *
     * @return string
     */
    public function getUseraccount()
    {
        return $this->useraccount;
    }
}
