<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Kepzesoraterv
 *
 * @ORM\Table(name="kepzesOraterv", uniqueConstraints={@ORM\UniqueConstraint(name="kot_kulcs2", columns={"kepzesId", "targyId", "evfolyamJel", "szemeszter"})}, indexes={@ORM\Index(name="kepzesOraterv_FKIndex1", columns={"targyId"}), @ORM\Index(name="IDX_23082B87B846755C", columns={"kepzesId"})})
 * @ORM\Entity
 */
class Kepzesoraterv
{
    /**
     * @var string
     *
     * @ORM\Column(name="evfolyamJel", type="string", length=32, nullable=true)
     */
    private $evfolyamjel;

    /**
     * @var boolean
     *
     * @ORM\Column(name="szemeszter", type="boolean", nullable=false)
     */
    private $szemeszter;

    /**
     * @var string
     *
     * @ORM\Column(name="hetiOraszam", type="decimal", precision=4, scale=2, nullable=true)
     */
    private $hetioraszam;

    /**
     * @var string
     *
     * @ORM\Column(name="kovetelmeny", type="string", nullable=true)
     */
    private $kovetelmeny;

    /**
     * @var string
     *
     * @ORM\Column(name="tipus", type="string", nullable=true)
     */
    private $tipus = 'mintatantervi';

    /**
     * @var integer
     *
     * @ORM\Column(name="kepzesOratervId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $kepzesoratervid;

    /**
     * @var \AppBundle\Entity\Kepzes
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Kepzes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="kepzesId", referencedColumnName="kepzesId")
     * })
     */
    private $kepzesid;

    /**
     * @var \AppBundle\Entity\Targy
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Targy")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="targyId", referencedColumnName="targyId")
     * })
     */
    private $targyid;



    /**
     * Set evfolyamjel
     *
     * @param string $evfolyamjel
     *
     * @return Kepzesoraterv
     */
    public function setEvfolyamjel($evfolyamjel)
    {
        $this->evfolyamjel = $evfolyamjel;

        return $this;
    }

    /**
     * Get evfolyamjel
     *
     * @return string
     */
    public function getEvfolyamjel()
    {
        return $this->evfolyamjel;
    }

    /**
     * Set szemeszter
     *
     * @param boolean $szemeszter
     *
     * @return Kepzesoraterv
     */
    public function setSzemeszter($szemeszter)
    {
        $this->szemeszter = $szemeszter;

        return $this;
    }

    /**
     * Get szemeszter
     *
     * @return boolean
     */
    public function getSzemeszter()
    {
        return $this->szemeszter;
    }

    /**
     * Set hetioraszam
     *
     * @param string $hetioraszam
     *
     * @return Kepzesoraterv
     */
    public function setHetioraszam($hetioraszam)
    {
        $this->hetioraszam = $hetioraszam;

        return $this;
    }

    /**
     * Get hetioraszam
     *
     * @return string
     */
    public function getHetioraszam()
    {
        return $this->hetioraszam;
    }

    /**
     * Set kovetelmeny
     *
     * @param string $kovetelmeny
     *
     * @return Kepzesoraterv
     */
    public function setKovetelmeny($kovetelmeny)
    {
        $this->kovetelmeny = $kovetelmeny;

        return $this;
    }

    /**
     * Get kovetelmeny
     *
     * @return string
     */
    public function getKovetelmeny()
    {
        return $this->kovetelmeny;
    }

    /**
     * Set tipus
     *
     * @param string $tipus
     *
     * @return Kepzesoraterv
     */
    public function setTipus($tipus)
    {
        $this->tipus = $tipus;

        return $this;
    }

    /**
     * Get tipus
     *
     * @return string
     */
    public function getTipus()
    {
        return $this->tipus;
    }

    /**
     * Get kepzesoratervid
     *
     * @return integer
     */
    public function getKepzesoratervid()
    {
        return $this->kepzesoratervid;
    }

    /**
     * Set kepzesid
     *
     * @param \AppBundle\Entity\Kepzes $kepzesid
     *
     * @return Kepzesoraterv
     */
    public function setKepzesid(\AppBundle\Entity\Kepzes $kepzesid = null)
    {
        $this->kepzesid = $kepzesid;

        return $this;
    }

    /**
     * Get kepzesid
     *
     * @return \AppBundle\Entity\Kepzes
     */
    public function getKepzesid()
    {
        return $this->kepzesid;
    }

    /**
     * Set targyid
     *
     * @param \AppBundle\Entity\Targy $targyid
     *
     * @return Kepzesoraterv
     */
    public function setTargyid(\AppBundle\Entity\Targy $targyid = null)
    {
        $this->targyid = $targyid;

        return $this;
    }

    /**
     * Get targyid
     *
     * @return \AppBundle\Entity\Targy
     */
    public function getTargyid()
    {
        return $this->targyid;
    }
}
