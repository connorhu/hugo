<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Diakadatkezeles
 *
 * @ORM\Table(name="diakAdatkezeles", indexes={@ORM\Index(name="diakAdatkezeles_FKIndex1", columns={"diakId"})})
 * @ORM\Entity
 */
class Diakadatkezeles
{
    /**
     * @var string
     *
     * @ORM\Column(name="ertek", type="string", length=30, nullable=false)
     */
    private $ertek;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastModified", type="datetime", nullable=false)
     */
    private $lastmodified = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="kulcs", type="string", length=30)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $kulcs;

    /**
     * @var \AppBundle\Entity\Diak
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Diak")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="diakId", referencedColumnName="diakId")
     * })
     */
    private $diakid;



    /**
     * Set ertek
     *
     * @param string $ertek
     *
     * @return Diakadatkezeles
     */
    public function setErtek($ertek)
    {
        $this->ertek = $ertek;

        return $this;
    }

    /**
     * Get ertek
     *
     * @return string
     */
    public function getErtek()
    {
        return $this->ertek;
    }

    /**
     * Set lastmodified
     *
     * @param \DateTime $lastmodified
     *
     * @return Diakadatkezeles
     */
    public function setLastmodified($lastmodified)
    {
        $this->lastmodified = $lastmodified;

        return $this;
    }

    /**
     * Get lastmodified
     *
     * @return \DateTime
     */
    public function getLastmodified()
    {
        return $this->lastmodified;
    }

    /**
     * Set kulcs
     *
     * @param string $kulcs
     *
     * @return Diakadatkezeles
     */
    public function setKulcs($kulcs)
    {
        $this->kulcs = $kulcs;

        return $this;
    }

    /**
     * Get kulcs
     *
     * @return string
     */
    public function getKulcs()
    {
        return $this->kulcs;
    }

    /**
     * Set diakid
     *
     * @param \AppBundle\Entity\Diak $diakid
     *
     * @return Diakadatkezeles
     */
    public function setDiakid(\AppBundle\Entity\Diak $diakid)
    {
        $this->diakid = $diakid;

        return $this;
    }

    /**
     * Get diakid
     *
     * @return \AppBundle\Entity\Diak
     */
    public function getDiakid()
    {
        return $this->diakid;
    }
}
