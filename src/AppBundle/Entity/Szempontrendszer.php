<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Szempontrendszer
 *
 * @ORM\Table(name="szempontRendszer", indexes={@ORM\Index(name="szr_FKindex1", columns={"kepzesId"}), @ORM\Index(name="szr_FKindex2", columns={"targyId"}), @ORM\Index(name="szr_FKIndex3", columns={"tanev", "szemeszter"})})
 * @ORM\Entity
 */
class Szempontrendszer
{
    /**
     * @var boolean
     *
     * @ORM\Column(name="_evfolyam", type="boolean", nullable=false)
     */
    private $evfolyam;

    /**
     * @var string
     *
     * @ORM\Column(name="targyTipus", type="string", nullable=true)
     */
    private $targytipus;

    /**
     * @var string
     *
     * @ORM\Column(name="evfolyamJel", type="string", length=32, nullable=true)
     */
    private $evfolyamjel;

    /**
     * @var integer
     *
     * @ORM\Column(name="szrId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $szrid;

    /**
     * @var \AppBundle\Entity\Szemeszter
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Szemeszter")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tanev", referencedColumnName="tanev"),
     *   @ORM\JoinColumn(name="szemeszter", referencedColumnName="szemeszter")
     * })
     */
    private $tanev;

    /**
     * @var \AppBundle\Entity\Targy
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Targy")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="targyId", referencedColumnName="targyId")
     * })
     */
    private $targyid;

    /**
     * @var \AppBundle\Entity\Kepzes
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Kepzes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="kepzesId", referencedColumnName="kepzesId")
     * })
     */
    private $kepzesid;



    /**
     * Set evfolyam
     *
     * @param boolean $evfolyam
     *
     * @return Szempontrendszer
     */
    public function setEvfolyam($evfolyam)
    {
        $this->evfolyam = $evfolyam;

        return $this;
    }

    /**
     * Get evfolyam
     *
     * @return boolean
     */
    public function getEvfolyam()
    {
        return $this->evfolyam;
    }

    /**
     * Set targytipus
     *
     * @param string $targytipus
     *
     * @return Szempontrendszer
     */
    public function setTargytipus($targytipus)
    {
        $this->targytipus = $targytipus;

        return $this;
    }

    /**
     * Get targytipus
     *
     * @return string
     */
    public function getTargytipus()
    {
        return $this->targytipus;
    }

    /**
     * Set evfolyamjel
     *
     * @param string $evfolyamjel
     *
     * @return Szempontrendszer
     */
    public function setEvfolyamjel($evfolyamjel)
    {
        $this->evfolyamjel = $evfolyamjel;

        return $this;
    }

    /**
     * Get evfolyamjel
     *
     * @return string
     */
    public function getEvfolyamjel()
    {
        return $this->evfolyamjel;
    }

    /**
     * Get szrid
     *
     * @return integer
     */
    public function getSzrid()
    {
        return $this->szrid;
    }

    /**
     * Set tanev
     *
     * @param \AppBundle\Entity\Szemeszter $tanev
     *
     * @return Szempontrendszer
     */
    public function setTanev(\AppBundle\Entity\Szemeszter $tanev = null)
    {
        $this->tanev = $tanev;

        return $this;
    }

    /**
     * Get tanev
     *
     * @return \AppBundle\Entity\Szemeszter
     */
    public function getTanev()
    {
        return $this->tanev;
    }

    /**
     * Set targyid
     *
     * @param \AppBundle\Entity\Targy $targyid
     *
     * @return Szempontrendszer
     */
    public function setTargyid(\AppBundle\Entity\Targy $targyid = null)
    {
        $this->targyid = $targyid;

        return $this;
    }

    /**
     * Get targyid
     *
     * @return \AppBundle\Entity\Targy
     */
    public function getTargyid()
    {
        return $this->targyid;
    }

    /**
     * Set kepzesid
     *
     * @param \AppBundle\Entity\Kepzes $kepzesid
     *
     * @return Szempontrendszer
     */
    public function setKepzesid(\AppBundle\Entity\Kepzes $kepzesid = null)
    {
        $this->kepzesid = $kepzesid;

        return $this;
    }

    /**
     * Get kepzesid
     *
     * @return \AppBundle\Entity\Kepzes
     */
    public function getKepzesid()
    {
        return $this->kepzesid;
    }
}
