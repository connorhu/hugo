<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Csengetesirend
 *
 * @ORM\Table(name="csengetesiRend", indexes={@ORM\Index(name="csengetesiRend_telephely", columns={"telephelyId"})})
 * @ORM\Entity
 */
class Csengetesirend
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="igTime", type="time", nullable=true)
     */
    private $igtime;

    /**
     * @var boolean
     *
     * @ORM\Column(name="nap", type="boolean")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $nap;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ora", type="boolean")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $ora;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tolTime", type="time")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $toltime;

    /**
     * @var \AppBundle\Entity\Telephely
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Telephely")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="telephelyId", referencedColumnName="telephelyId")
     * })
     */
    private $telephelyid;



    /**
     * Set igtime
     *
     * @param \DateTime $igtime
     *
     * @return Csengetesirend
     */
    public function setIgtime($igtime)
    {
        $this->igtime = $igtime;

        return $this;
    }

    /**
     * Get igtime
     *
     * @return \DateTime
     */
    public function getIgtime()
    {
        return $this->igtime;
    }

    /**
     * Set nap
     *
     * @param boolean $nap
     *
     * @return Csengetesirend
     */
    public function setNap($nap)
    {
        $this->nap = $nap;

        return $this;
    }

    /**
     * Get nap
     *
     * @return boolean
     */
    public function getNap()
    {
        return $this->nap;
    }

    /**
     * Set ora
     *
     * @param boolean $ora
     *
     * @return Csengetesirend
     */
    public function setOra($ora)
    {
        $this->ora = $ora;

        return $this;
    }

    /**
     * Get ora
     *
     * @return boolean
     */
    public function getOra()
    {
        return $this->ora;
    }

    /**
     * Set toltime
     *
     * @param \DateTime $toltime
     *
     * @return Csengetesirend
     */
    public function setToltime($toltime)
    {
        $this->toltime = $toltime;

        return $this;
    }

    /**
     * Get toltime
     *
     * @return \DateTime
     */
    public function getToltime()
    {
        return $this->toltime;
    }

    /**
     * Set telephelyid
     *
     * @param \AppBundle\Entity\Telephely $telephelyid
     *
     * @return Csengetesirend
     */
    public function setTelephelyid(\AppBundle\Entity\Telephely $telephelyid = null)
    {
        $this->telephelyid = $telephelyid;

        return $this;
    }

    /**
     * Get telephelyid
     *
     * @return \AppBundle\Entity\Telephely
     */
    public function getTelephelyid()
    {
        return $this->telephelyid;
    }
}
