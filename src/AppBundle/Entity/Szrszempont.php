<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Szrszempont
 *
 * @ORM\Table(name="szrSzempont", indexes={@ORM\Index(name="szrsz_FKindex1", columns={"szrId"})})
 * @ORM\Entity
 */
class Szrszempont
{
    /**
     * @var string
     *
     * @ORM\Column(name="szempont", type="string", length=128, nullable=true)
     */
    private $szempont;

    /**
     * @var integer
     *
     * @ORM\Column(name="szempontId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $szempontid;

    /**
     * @var \AppBundle\Entity\Szempontrendszer
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Szempontrendszer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="szrId", referencedColumnName="szrId")
     * })
     */
    private $szrid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Szovegesertekeles", mappedBy="szempontid")
     */
    private $szeid;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->szeid = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set szempont
     *
     * @param string $szempont
     *
     * @return Szrszempont
     */
    public function setSzempont($szempont)
    {
        $this->szempont = $szempont;

        return $this;
    }

    /**
     * Get szempont
     *
     * @return string
     */
    public function getSzempont()
    {
        return $this->szempont;
    }

    /**
     * Get szempontid
     *
     * @return integer
     */
    public function getSzempontid()
    {
        return $this->szempontid;
    }

    /**
     * Set szrid
     *
     * @param \AppBundle\Entity\Szempontrendszer $szrid
     *
     * @return Szrszempont
     */
    public function setSzrid(\AppBundle\Entity\Szempontrendszer $szrid = null)
    {
        $this->szrid = $szrid;

        return $this;
    }

    /**
     * Get szrid
     *
     * @return \AppBundle\Entity\Szempontrendszer
     */
    public function getSzrid()
    {
        return $this->szrid;
    }

    /**
     * Add szeid
     *
     * @param \AppBundle\Entity\Szovegesertekeles $szeid
     *
     * @return Szrszempont
     */
    public function addSzeid(\AppBundle\Entity\Szovegesertekeles $szeid)
    {
        $this->szeid[] = $szeid;

        return $this;
    }

    /**
     * Remove szeid
     *
     * @param \AppBundle\Entity\Szovegesertekeles $szeid
     */
    public function removeSzeid(\AppBundle\Entity\Szovegesertekeles $szeid)
    {
        $this->szeid->removeElement($szeid);
    }

    /**
     * Get szeid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSzeid()
    {
        return $this->szeid;
    }
}
