<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tanmenet
 *
 * @ORM\Table(name="tanmenet", indexes={@ORM\Index(name="tanmenet_ibfk_1", columns={"targyId"}), @ORM\Index(name="tanmenet_ibfk_2", columns={"tanarId"})})
 * @ORM\Entity
 */
class Tanmenet
{
    /**
     * @var boolean
     *
     * @ORM\Column(name="_evfolyam", type="boolean", nullable=false)
     */
    private $evfolyam;

    /**
     * @var string
     *
     * @ORM\Column(name="tanmenetNev", type="string", length=128, nullable=true)
     */
    private $tanmenetnev;

    /**
     * @var integer
     *
     * @ORM\Column(name="oraszam", type="smallint", nullable=true)
     */
    private $oraszam;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt", type="date", nullable=false)
     */
    private $dt;

    /**
     * @var string
     *
     * @ORM\Column(name="statusz", type="string", nullable=true)
     */
    private $statusz = 'új';

    /**
     * @var string
     *
     * @ORM\Column(name="evfolyamJel", type="string", length=32, nullable=true)
     */
    private $evfolyamjel;

    /**
     * @var integer
     *
     * @ORM\Column(name="tanmenetId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tanmenetid;

    /**
     * @var \AppBundle\Entity\Tanar
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Tanar")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tanarId", referencedColumnName="tanarId")
     * })
     */
    private $tanarid;

    /**
     * @var \AppBundle\Entity\Targy
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Targy")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="targyId", referencedColumnName="targyId")
     * })
     */
    private $targyid;



    /**
     * Set evfolyam
     *
     * @param boolean $evfolyam
     *
     * @return Tanmenet
     */
    public function setEvfolyam($evfolyam)
    {
        $this->evfolyam = $evfolyam;

        return $this;
    }

    /**
     * Get evfolyam
     *
     * @return boolean
     */
    public function getEvfolyam()
    {
        return $this->evfolyam;
    }

    /**
     * Set tanmenetnev
     *
     * @param string $tanmenetnev
     *
     * @return Tanmenet
     */
    public function setTanmenetnev($tanmenetnev)
    {
        $this->tanmenetnev = $tanmenetnev;

        return $this;
    }

    /**
     * Get tanmenetnev
     *
     * @return string
     */
    public function getTanmenetnev()
    {
        return $this->tanmenetnev;
    }

    /**
     * Set oraszam
     *
     * @param integer $oraszam
     *
     * @return Tanmenet
     */
    public function setOraszam($oraszam)
    {
        $this->oraszam = $oraszam;

        return $this;
    }

    /**
     * Get oraszam
     *
     * @return integer
     */
    public function getOraszam()
    {
        return $this->oraszam;
    }

    /**
     * Set dt
     *
     * @param \DateTime $dt
     *
     * @return Tanmenet
     */
    public function setDt($dt)
    {
        $this->dt = $dt;

        return $this;
    }

    /**
     * Get dt
     *
     * @return \DateTime
     */
    public function getDt()
    {
        return $this->dt;
    }

    /**
     * Set statusz
     *
     * @param string $statusz
     *
     * @return Tanmenet
     */
    public function setStatusz($statusz)
    {
        $this->statusz = $statusz;

        return $this;
    }

    /**
     * Get statusz
     *
     * @return string
     */
    public function getStatusz()
    {
        return $this->statusz;
    }

    /**
     * Set evfolyamjel
     *
     * @param string $evfolyamjel
     *
     * @return Tanmenet
     */
    public function setEvfolyamjel($evfolyamjel)
    {
        $this->evfolyamjel = $evfolyamjel;

        return $this;
    }

    /**
     * Get evfolyamjel
     *
     * @return string
     */
    public function getEvfolyamjel()
    {
        return $this->evfolyamjel;
    }

    /**
     * Get tanmenetid
     *
     * @return integer
     */
    public function getTanmenetid()
    {
        return $this->tanmenetid;
    }

    /**
     * Set tanarid
     *
     * @param \AppBundle\Entity\Tanar $tanarid
     *
     * @return Tanmenet
     */
    public function setTanarid(\AppBundle\Entity\Tanar $tanarid = null)
    {
        $this->tanarid = $tanarid;

        return $this;
    }

    /**
     * Get tanarid
     *
     * @return \AppBundle\Entity\Tanar
     */
    public function getTanarid()
    {
        return $this->tanarid;
    }

    /**
     * Set targyid
     *
     * @param \AppBundle\Entity\Targy $targyid
     *
     * @return Tanmenet
     */
    public function setTargyid(\AppBundle\Entity\Targy $targyid = null)
    {
        $this->targyid = $targyid;

        return $this;
    }

    /**
     * Get targyid
     *
     * @return \AppBundle\Entity\Targy
     */
    public function getTargyid()
    {
        return $this->targyid;
    }
}
