<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Kirosztalyjelleg
 *
 * @ORM\Table(name="kirOsztalyJelleg")
 * @ORM\Entity
 */
class Kirosztalyjelleg
{
    /**
     * @var string
     *
     * @ORM\Column(name="kirOsztalyJellegNev", type="string", length=255, nullable=false)
     */
    private $kirosztalyjellegnev;

    /**
     * @var boolean
     *
     * @ORM\Column(name="kirOsztalyJellegId", type="boolean")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $kirosztalyjellegid;



    /**
     * Set kirosztalyjellegnev
     *
     * @param string $kirosztalyjellegnev
     *
     * @return Kirosztalyjelleg
     */
    public function setKirosztalyjellegnev($kirosztalyjellegnev)
    {
        $this->kirosztalyjellegnev = $kirosztalyjellegnev;

        return $this;
    }

    /**
     * Get kirosztalyjellegnev
     *
     * @return string
     */
    public function getKirosztalyjellegnev()
    {
        return $this->kirosztalyjellegnev;
    }

    /**
     * Get kirosztalyjellegid
     *
     * @return boolean
     */
    public function getKirosztalyjellegid()
    {
        return $this->kirosztalyjellegid;
    }
}
