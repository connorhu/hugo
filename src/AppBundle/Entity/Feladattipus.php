<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Feladattipus
 *
 * @ORM\Table(name="feladatTipus")
 * @ORM\Entity
 */
class Feladattipus
{
    /**
     * @var string
     *
     * @ORM\Column(name="feladatTipusLeiras", type="string", length=255, nullable=true)
     */
    private $feladattipusleiras;

    /**
     * @var boolean
     *
     * @ORM\Column(name="beszamithatoMaxOra", type="boolean", nullable=false)
     */
    private $beszamithatomaxora;

    /**
     * @var boolean
     *
     * @ORM\Column(name="feladatTipusId", type="boolean")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $feladattipusid;



    /**
     * Set feladattipusleiras
     *
     * @param string $feladattipusleiras
     *
     * @return Feladattipus
     */
    public function setFeladattipusleiras($feladattipusleiras)
    {
        $this->feladattipusleiras = $feladattipusleiras;

        return $this;
    }

    /**
     * Get feladattipusleiras
     *
     * @return string
     */
    public function getFeladattipusleiras()
    {
        return $this->feladattipusleiras;
    }

    /**
     * Set beszamithatomaxora
     *
     * @param boolean $beszamithatomaxora
     *
     * @return Feladattipus
     */
    public function setBeszamithatomaxora($beszamithatomaxora)
    {
        $this->beszamithatomaxora = $beszamithatomaxora;

        return $this;
    }

    /**
     * Get beszamithatomaxora
     *
     * @return boolean
     */
    public function getBeszamithatomaxora()
    {
        return $this->beszamithatomaxora;
    }

    /**
     * Get feladattipusid
     *
     * @return boolean
     */
    public function getFeladattipusid()
    {
        return $this->feladattipusid;
    }
}
