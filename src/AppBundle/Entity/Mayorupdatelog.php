<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mayorupdatelog
 *
 * @ORM\Table(name="mayorUpdateLog")
 * @ORM\Entity
 */
class Mayorupdatelog
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt", type="datetime")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $dt;

    /**
     * @var string
     *
     * @ORM\Column(name="scriptFile", type="string", length=255)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $scriptfile;



    /**
     * Set dt
     *
     * @param \DateTime $dt
     *
     * @return Mayorupdatelog
     */
    public function setDt($dt)
    {
        $this->dt = $dt;

        return $this;
    }

    /**
     * Get dt
     *
     * @return \DateTime
     */
    public function getDt()
    {
        return $this->dt;
    }

    /**
     * Set scriptfile
     *
     * @param string $scriptfile
     *
     * @return Mayorupdatelog
     */
    public function setScriptfile($scriptfile)
    {
        $this->scriptfile = $scriptfile;

        return $this;
    }

    /**
     * Get scriptfile
     *
     * @return string
     */
    public function getScriptfile()
    {
        return $this->scriptfile;
    }
}
