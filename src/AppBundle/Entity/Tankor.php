<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tankor
 *
 * @ORM\Table(name="tankor", indexes={@ORM\Index(name="tankor_FKIndex1", columns={"targyId"})})
 * @ORM\Entity
 */
class Tankor
{
    /**
     * @var string
     *
     * @ORM\Column(name="kovetelmeny", type="string", nullable=true)
     */
    private $kovetelmeny;

    /**
     * @var string
     *
     * @ORM\Column(name="_jelenlet", type="string", nullable=true)
     */
    private $jelenlet;

    /**
     * @var boolean
     *
     * @ORM\Column(name="felveheto", type="boolean", nullable=true)
     */
    private $felveheto;

    /**
     * @var string
     *
     * @ORM\Column(name="cn", type="string", length=64, nullable=true)
     */
    private $cn;

    /**
     * @var boolean
     *
     * @ORM\Column(name="min", type="boolean", nullable=false)
     */
    private $min;

    /**
     * @var boolean
     *
     * @ORM\Column(name="max", type="boolean", nullable=false)
     */
    private $max;

    /**
     * @var string
     *
     * @ORM\Column(name="_tankorTipus", type="string", nullable=true)
     */
    private $tankortipus = 'tanórai';

    /**
     * @var integer
     *
     * @ORM\Column(name="tankorTipusId", type="integer", nullable=true)
     */
    private $tankortipusid;

    /**
     * @var integer
     *
     * @ORM\Column(name="tankorId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tankorid;

    /**
     * @var \AppBundle\Entity\Targy
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Targy")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="targyId", referencedColumnName="targyId")
     * })
     */
    private $targyid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Szemeszter", mappedBy="tankorid")
     */
    private $tanev;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Osztaly", inversedBy="tankorid")
     * @ORM\JoinTable(name="tankorosztaly",
     *   joinColumns={
     *     @ORM\JoinColumn(name="tankorId", referencedColumnName="tankorId")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="osztalyId", referencedColumnName="osztalyId")
     *   }
     * )
     */
    private $osztalyid;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tanev = new \Doctrine\Common\Collections\ArrayCollection();
        $this->osztalyid = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set kovetelmeny
     *
     * @param string $kovetelmeny
     *
     * @return Tankor
     */
    public function setKovetelmeny($kovetelmeny)
    {
        $this->kovetelmeny = $kovetelmeny;

        return $this;
    }

    /**
     * Get kovetelmeny
     *
     * @return string
     */
    public function getKovetelmeny()
    {
        return $this->kovetelmeny;
    }

    /**
     * Set jelenlet
     *
     * @param string $jelenlet
     *
     * @return Tankor
     */
    public function setJelenlet($jelenlet)
    {
        $this->jelenlet = $jelenlet;

        return $this;
    }

    /**
     * Get jelenlet
     *
     * @return string
     */
    public function getJelenlet()
    {
        return $this->jelenlet;
    }

    /**
     * Set felveheto
     *
     * @param boolean $felveheto
     *
     * @return Tankor
     */
    public function setFelveheto($felveheto)
    {
        $this->felveheto = $felveheto;

        return $this;
    }

    /**
     * Get felveheto
     *
     * @return boolean
     */
    public function getFelveheto()
    {
        return $this->felveheto;
    }

    /**
     * Set cn
     *
     * @param string $cn
     *
     * @return Tankor
     */
    public function setCn($cn)
    {
        $this->cn = $cn;

        return $this;
    }

    /**
     * Get cn
     *
     * @return string
     */
    public function getCn()
    {
        return $this->cn;
    }

    /**
     * Set min
     *
     * @param boolean $min
     *
     * @return Tankor
     */
    public function setMin($min)
    {
        $this->min = $min;

        return $this;
    }

    /**
     * Get min
     *
     * @return boolean
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * Set max
     *
     * @param boolean $max
     *
     * @return Tankor
     */
    public function setMax($max)
    {
        $this->max = $max;

        return $this;
    }

    /**
     * Get max
     *
     * @return boolean
     */
    public function getMax()
    {
        return $this->max;
    }

    /**
     * Set tankortipus
     *
     * @param string $tankortipus
     *
     * @return Tankor
     */
    public function setTankortipus($tankortipus)
    {
        $this->tankortipus = $tankortipus;

        return $this;
    }

    /**
     * Get tankortipus
     *
     * @return string
     */
    public function getTankortipus()
    {
        return $this->tankortipus;
    }

    /**
     * Set tankortipusid
     *
     * @param integer $tankortipusid
     *
     * @return Tankor
     */
    public function setTankortipusid($tankortipusid)
    {
        $this->tankortipusid = $tankortipusid;

        return $this;
    }

    /**
     * Get tankortipusid
     *
     * @return integer
     */
    public function getTankortipusid()
    {
        return $this->tankortipusid;
    }

    /**
     * Get tankorid
     *
     * @return integer
     */
    public function getTankorid()
    {
        return $this->tankorid;
    }

    /**
     * Set targyid
     *
     * @param \AppBundle\Entity\Targy $targyid
     *
     * @return Tankor
     */
    public function setTargyid(\AppBundle\Entity\Targy $targyid = null)
    {
        $this->targyid = $targyid;

        return $this;
    }

    /**
     * Get targyid
     *
     * @return \AppBundle\Entity\Targy
     */
    public function getTargyid()
    {
        return $this->targyid;
    }

    /**
     * Add tanev
     *
     * @param \AppBundle\Entity\Szemeszter $tanev
     *
     * @return Tankor
     */
    public function addTanev(\AppBundle\Entity\Szemeszter $tanev)
    {
        $this->tanev[] = $tanev;

        return $this;
    }

    /**
     * Remove tanev
     *
     * @param \AppBundle\Entity\Szemeszter $tanev
     */
    public function removeTanev(\AppBundle\Entity\Szemeszter $tanev)
    {
        $this->tanev->removeElement($tanev);
    }

    /**
     * Get tanev
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTanev()
    {
        return $this->tanev;
    }

    /**
     * Add osztalyid
     *
     * @param \AppBundle\Entity\Osztaly $osztalyid
     *
     * @return Tankor
     */
    public function addOsztalyid(\AppBundle\Entity\Osztaly $osztalyid)
    {
        $this->osztalyid[] = $osztalyid;

        return $this;
    }

    /**
     * Remove osztalyid
     *
     * @param \AppBundle\Entity\Osztaly $osztalyid
     */
    public function removeOsztalyid(\AppBundle\Entity\Osztaly $osztalyid)
    {
        $this->osztalyid->removeElement($osztalyid);
    }

    /**
     * Get osztalyid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOsztalyid()
    {
        return $this->osztalyid;
    }
}
