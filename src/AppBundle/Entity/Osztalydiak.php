<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Osztalydiak
 *
 * @ORM\Table(name="osztalyDiak", indexes={@ORM\Index(name="osztalyTag_FKIndex1", columns={"osztalyId"}), @ORM\Index(name="osztalyDiak_FKIndex2", columns={"diakId"})})
 * @ORM\Entity
 */
class Osztalydiak
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="kiDt", type="date", nullable=true)
     */
    private $kidt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="beDt", type="date")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $bedt;

    /**
     * @var \AppBundle\Entity\Diak
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Diak")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="diakId", referencedColumnName="diakId")
     * })
     */
    private $diakid;

    /**
     * @var \AppBundle\Entity\Osztaly
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Osztaly")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="osztalyId", referencedColumnName="osztalyId")
     * })
     */
    private $osztalyid;



    /**
     * Set kidt
     *
     * @param \DateTime $kidt
     *
     * @return Osztalydiak
     */
    public function setKidt($kidt)
    {
        $this->kidt = $kidt;

        return $this;
    }

    /**
     * Get kidt
     *
     * @return \DateTime
     */
    public function getKidt()
    {
        return $this->kidt;
    }

    /**
     * Set bedt
     *
     * @param \DateTime $bedt
     *
     * @return Osztalydiak
     */
    public function setBedt($bedt)
    {
        $this->bedt = $bedt;

        return $this;
    }

    /**
     * Get bedt
     *
     * @return \DateTime
     */
    public function getBedt()
    {
        return $this->bedt;
    }

    /**
     * Set diakid
     *
     * @param \AppBundle\Entity\Diak $diakid
     *
     * @return Osztalydiak
     */
    public function setDiakid(\AppBundle\Entity\Diak $diakid)
    {
        $this->diakid = $diakid;

        return $this;
    }

    /**
     * Get diakid
     *
     * @return \AppBundle\Entity\Diak
     */
    public function getDiakid()
    {
        return $this->diakid;
    }

    /**
     * Set osztalyid
     *
     * @param \AppBundle\Entity\Osztaly $osztalyid
     *
     * @return Osztalydiak
     */
    public function setOsztalyid(\AppBundle\Entity\Osztaly $osztalyid)
    {
        $this->osztalyid = $osztalyid;

        return $this;
    }

    /**
     * Get osztalyid
     *
     * @return \AppBundle\Entity\Osztaly
     */
    public function getOsztalyid()
    {
        return $this->osztalyid;
    }
}
