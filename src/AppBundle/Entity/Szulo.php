<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Szulo
 *
 * @ORM\Table(name="szulo")
 * @ORM\Entity
 */
class Szulo
{
    /**
     * @var string
     *
     * @ORM\Column(name="nevElotag", type="string", length=8, nullable=true)
     */
    private $nevelotag = '';

    /**
     * @var string
     *
     * @ORM\Column(name="csaladinev", type="string", length=64, nullable=false)
     */
    private $csaladinev = '';

    /**
     * @var string
     *
     * @ORM\Column(name="utonev", type="string", length=64, nullable=false)
     */
    private $utonev = '';

    /**
     * @var string
     *
     * @ORM\Column(name="szuleteskoriNevElotag", type="string", length=8, nullable=true)
     */
    private $szuleteskorinevelotag = '';

    /**
     * @var string
     *
     * @ORM\Column(name="szuleteskoriCsaladinev", type="string", length=32, nullable=true)
     */
    private $szuleteskoricsaladinev;

    /**
     * @var string
     *
     * @ORM\Column(name="szuleteskoriUtonev", type="string", length=32, nullable=true)
     */
    private $szuleteskoriutonev;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=96, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="nem", type="string", nullable=true)
     */
    private $nem;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="szuletesiEv", type="date", nullable=true)
     */
    private $szuletesiev;

    /**
     * @var string
     *
     * @ORM\Column(name="cimOrszag", type="string", length=16, nullable=true)
     */
    private $cimorszag = 'Magyarország';

    /**
     * @var string
     *
     * @ORM\Column(name="cimHelyseg", type="string", length=32, nullable=true)
     */
    private $cimhelyseg;

    /**
     * @var string
     *
     * @ORM\Column(name="cimIrsz", type="string", length=8, nullable=true)
     */
    private $cimirsz;

    /**
     * @var string
     *
     * @ORM\Column(name="cimKozteruletNev", type="string", length=32, nullable=true)
     */
    private $cimkozteruletnev;

    /**
     * @var string
     *
     * @ORM\Column(name="cimKozteruletJelleg", type="string", nullable=true)
     */
    private $cimkozteruletjelleg;

    /**
     * @var string
     *
     * @ORM\Column(name="cimHazszam", type="string", length=20, nullable=true)
     */
    private $cimhazszam;

    /**
     * @var string
     *
     * @ORM\Column(name="cimEmelet", type="string", length=5, nullable=true)
     */
    private $cimemelet;

    /**
     * @var string
     *
     * @ORM\Column(name="cimAjto", type="string", length=5, nullable=true)
     */
    private $cimajto;

    /**
     * @var string
     *
     * @ORM\Column(name="mobil", type="string", length=64, nullable=true)
     */
    private $mobil;

    /**
     * @var string
     *
     * @ORM\Column(name="telefon", type="string", length=64, nullable=true)
     */
    private $telefon;

    /**
     * @var string
     *
     * @ORM\Column(name="foglalkozas", type="string", length=128, nullable=true)
     */
    private $foglalkozas;

    /**
     * @var string
     *
     * @ORM\Column(name="munkahely", type="string", length=128, nullable=true)
     */
    private $munkahely;

    /**
     * @var string
     *
     * @ORM\Column(name="userAccount", type="string", length=32, nullable=true)
     */
    private $useraccount;

    /**
     * @var string
     *
     * @ORM\Column(name="statusz", type="string", nullable=true)
     */
    private $statusz;

    /**
     * @var integer
     *
     * @ORM\Column(name="oId", type="bigint", nullable=true)
     */
    private $oid;

    /**
     * @var integer
     *
     * @ORM\Column(name="szuloId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $szuloid;



    /**
     * Set nevelotag
     *
     * @param string $nevelotag
     *
     * @return Szulo
     */
    public function setNevelotag($nevelotag)
    {
        $this->nevelotag = $nevelotag;

        return $this;
    }

    /**
     * Get nevelotag
     *
     * @return string
     */
    public function getNevelotag()
    {
        return $this->nevelotag;
    }

    /**
     * Set csaladinev
     *
     * @param string $csaladinev
     *
     * @return Szulo
     */
    public function setCsaladinev($csaladinev)
    {
        $this->csaladinev = $csaladinev;

        return $this;
    }

    /**
     * Get csaladinev
     *
     * @return string
     */
    public function getCsaladinev()
    {
        return $this->csaladinev;
    }

    /**
     * Set utonev
     *
     * @param string $utonev
     *
     * @return Szulo
     */
    public function setUtonev($utonev)
    {
        $this->utonev = $utonev;

        return $this;
    }

    /**
     * Get utonev
     *
     * @return string
     */
    public function getUtonev()
    {
        return $this->utonev;
    }

    /**
     * Set szuleteskorinevelotag
     *
     * @param string $szuleteskorinevelotag
     *
     * @return Szulo
     */
    public function setSzuleteskorinevelotag($szuleteskorinevelotag)
    {
        $this->szuleteskorinevelotag = $szuleteskorinevelotag;

        return $this;
    }

    /**
     * Get szuleteskorinevelotag
     *
     * @return string
     */
    public function getSzuleteskorinevelotag()
    {
        return $this->szuleteskorinevelotag;
    }

    /**
     * Set szuleteskoricsaladinev
     *
     * @param string $szuleteskoricsaladinev
     *
     * @return Szulo
     */
    public function setSzuleteskoricsaladinev($szuleteskoricsaladinev)
    {
        $this->szuleteskoricsaladinev = $szuleteskoricsaladinev;

        return $this;
    }

    /**
     * Get szuleteskoricsaladinev
     *
     * @return string
     */
    public function getSzuleteskoricsaladinev()
    {
        return $this->szuleteskoricsaladinev;
    }

    /**
     * Set szuleteskoriutonev
     *
     * @param string $szuleteskoriutonev
     *
     * @return Szulo
     */
    public function setSzuleteskoriutonev($szuleteskoriutonev)
    {
        $this->szuleteskoriutonev = $szuleteskoriutonev;

        return $this;
    }

    /**
     * Get szuleteskoriutonev
     *
     * @return string
     */
    public function getSzuleteskoriutonev()
    {
        return $this->szuleteskoriutonev;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Szulo
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set nem
     *
     * @param string $nem
     *
     * @return Szulo
     */
    public function setNem($nem)
    {
        $this->nem = $nem;

        return $this;
    }

    /**
     * Get nem
     *
     * @return string
     */
    public function getNem()
    {
        return $this->nem;
    }

    /**
     * Set szuletesiev
     *
     * @param \DateTime $szuletesiev
     *
     * @return Szulo
     */
    public function setSzuletesiev($szuletesiev)
    {
        $this->szuletesiev = $szuletesiev;

        return $this;
    }

    /**
     * Get szuletesiev
     *
     * @return \DateTime
     */
    public function getSzuletesiev()
    {
        return $this->szuletesiev;
    }

    /**
     * Set cimorszag
     *
     * @param string $cimorszag
     *
     * @return Szulo
     */
    public function setCimorszag($cimorszag)
    {
        $this->cimorszag = $cimorszag;

        return $this;
    }

    /**
     * Get cimorszag
     *
     * @return string
     */
    public function getCimorszag()
    {
        return $this->cimorszag;
    }

    /**
     * Set cimhelyseg
     *
     * @param string $cimhelyseg
     *
     * @return Szulo
     */
    public function setCimhelyseg($cimhelyseg)
    {
        $this->cimhelyseg = $cimhelyseg;

        return $this;
    }

    /**
     * Get cimhelyseg
     *
     * @return string
     */
    public function getCimhelyseg()
    {
        return $this->cimhelyseg;
    }

    /**
     * Set cimirsz
     *
     * @param string $cimirsz
     *
     * @return Szulo
     */
    public function setCimirsz($cimirsz)
    {
        $this->cimirsz = $cimirsz;

        return $this;
    }

    /**
     * Get cimirsz
     *
     * @return string
     */
    public function getCimirsz()
    {
        return $this->cimirsz;
    }

    /**
     * Set cimkozteruletnev
     *
     * @param string $cimkozteruletnev
     *
     * @return Szulo
     */
    public function setCimkozteruletnev($cimkozteruletnev)
    {
        $this->cimkozteruletnev = $cimkozteruletnev;

        return $this;
    }

    /**
     * Get cimkozteruletnev
     *
     * @return string
     */
    public function getCimkozteruletnev()
    {
        return $this->cimkozteruletnev;
    }

    /**
     * Set cimkozteruletjelleg
     *
     * @param string $cimkozteruletjelleg
     *
     * @return Szulo
     */
    public function setCimkozteruletjelleg($cimkozteruletjelleg)
    {
        $this->cimkozteruletjelleg = $cimkozteruletjelleg;

        return $this;
    }

    /**
     * Get cimkozteruletjelleg
     *
     * @return string
     */
    public function getCimkozteruletjelleg()
    {
        return $this->cimkozteruletjelleg;
    }

    /**
     * Set cimhazszam
     *
     * @param string $cimhazszam
     *
     * @return Szulo
     */
    public function setCimhazszam($cimhazszam)
    {
        $this->cimhazszam = $cimhazszam;

        return $this;
    }

    /**
     * Get cimhazszam
     *
     * @return string
     */
    public function getCimhazszam()
    {
        return $this->cimhazszam;
    }

    /**
     * Set cimemelet
     *
     * @param string $cimemelet
     *
     * @return Szulo
     */
    public function setCimemelet($cimemelet)
    {
        $this->cimemelet = $cimemelet;

        return $this;
    }

    /**
     * Get cimemelet
     *
     * @return string
     */
    public function getCimemelet()
    {
        return $this->cimemelet;
    }

    /**
     * Set cimajto
     *
     * @param string $cimajto
     *
     * @return Szulo
     */
    public function setCimajto($cimajto)
    {
        $this->cimajto = $cimajto;

        return $this;
    }

    /**
     * Get cimajto
     *
     * @return string
     */
    public function getCimajto()
    {
        return $this->cimajto;
    }

    /**
     * Set mobil
     *
     * @param string $mobil
     *
     * @return Szulo
     */
    public function setMobil($mobil)
    {
        $this->mobil = $mobil;

        return $this;
    }

    /**
     * Get mobil
     *
     * @return string
     */
    public function getMobil()
    {
        return $this->mobil;
    }

    /**
     * Set telefon
     *
     * @param string $telefon
     *
     * @return Szulo
     */
    public function setTelefon($telefon)
    {
        $this->telefon = $telefon;

        return $this;
    }

    /**
     * Get telefon
     *
     * @return string
     */
    public function getTelefon()
    {
        return $this->telefon;
    }

    /**
     * Set foglalkozas
     *
     * @param string $foglalkozas
     *
     * @return Szulo
     */
    public function setFoglalkozas($foglalkozas)
    {
        $this->foglalkozas = $foglalkozas;

        return $this;
    }

    /**
     * Get foglalkozas
     *
     * @return string
     */
    public function getFoglalkozas()
    {
        return $this->foglalkozas;
    }

    /**
     * Set munkahely
     *
     * @param string $munkahely
     *
     * @return Szulo
     */
    public function setMunkahely($munkahely)
    {
        $this->munkahely = $munkahely;

        return $this;
    }

    /**
     * Get munkahely
     *
     * @return string
     */
    public function getMunkahely()
    {
        return $this->munkahely;
    }

    /**
     * Set useraccount
     *
     * @param string $useraccount
     *
     * @return Szulo
     */
    public function setUseraccount($useraccount)
    {
        $this->useraccount = $useraccount;

        return $this;
    }

    /**
     * Get useraccount
     *
     * @return string
     */
    public function getUseraccount()
    {
        return $this->useraccount;
    }

    /**
     * Set statusz
     *
     * @param string $statusz
     *
     * @return Szulo
     */
    public function setStatusz($statusz)
    {
        $this->statusz = $statusz;

        return $this;
    }

    /**
     * Get statusz
     *
     * @return string
     */
    public function getStatusz()
    {
        return $this->statusz;
    }

    /**
     * Set oid
     *
     * @param integer $oid
     *
     * @return Szulo
     */
    public function setOid($oid)
    {
        $this->oid = $oid;

        return $this;
    }

    /**
     * Get oid
     *
     * @return integer
     */
    public function getOid()
    {
        return $this->oid;
    }

    /**
     * Get szuloid
     *
     * @return integer
     */
    public function getSzuloid()
    {
        return $this->szuloid;
    }
}
