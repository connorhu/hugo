<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Kepzesdiak
 *
 * @ORM\Table(name="kepzesDiak", indexes={@ORM\Index(name="kepzesDiak_FKIndex1", columns={"kepzesId"}), @ORM\Index(name="kepzesDiak_FKIndex2", columns={"diakId"})})
 * @ORM\Entity
 */
class Kepzesdiak
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="igDt", type="date", nullable=true)
     */
    private $igdt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tolDt", type="date")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $toldt;

    /**
     * @var \AppBundle\Entity\Diak
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Diak")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="diakId", referencedColumnName="diakId")
     * })
     */
    private $diakid;

    /**
     * @var \AppBundle\Entity\Kepzes
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Kepzes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="kepzesId", referencedColumnName="kepzesId")
     * })
     */
    private $kepzesid;



    /**
     * Set igdt
     *
     * @param \DateTime $igdt
     *
     * @return Kepzesdiak
     */
    public function setIgdt($igdt)
    {
        $this->igdt = $igdt;

        return $this;
    }

    /**
     * Get igdt
     *
     * @return \DateTime
     */
    public function getIgdt()
    {
        return $this->igdt;
    }

    /**
     * Set toldt
     *
     * @param \DateTime $toldt
     *
     * @return Kepzesdiak
     */
    public function setToldt($toldt)
    {
        $this->toldt = $toldt;

        return $this;
    }

    /**
     * Get toldt
     *
     * @return \DateTime
     */
    public function getToldt()
    {
        return $this->toldt;
    }

    /**
     * Set diakid
     *
     * @param \AppBundle\Entity\Diak $diakid
     *
     * @return Kepzesdiak
     */
    public function setDiakid(\AppBundle\Entity\Diak $diakid)
    {
        $this->diakid = $diakid;

        return $this;
    }

    /**
     * Get diakid
     *
     * @return \AppBundle\Entity\Diak
     */
    public function getDiakid()
    {
        return $this->diakid;
    }

    /**
     * Set kepzesid
     *
     * @param \AppBundle\Entity\Kepzes $kepzesid
     *
     * @return Kepzesdiak
     */
    public function setKepzesid(\AppBundle\Entity\Kepzes $kepzesid)
    {
        $this->kepzesid = $kepzesid;

        return $this;
    }

    /**
     * Get kepzesid
     *
     * @return \AppBundle\Entity\Kepzes
     */
    public function getKepzesid()
    {
        return $this->kepzesid;
    }
}
