<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tankordiak
 *
 * @ORM\Table(name="tankorDiak", indexes={@ORM\Index(name="tankorTag_FKIndex1", columns={"tankorId"}), @ORM\Index(name="diakId", columns={"diakId"})})
 * @ORM\Entity
 */
class Tankordiak
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="kiDt", type="date", nullable=true)
     */
    private $kidt;

    /**
     * @var string
     *
     * @ORM\Column(name="_jelenlet", type="string", nullable=true)
     */
    private $jelenlet = 'kötelező';

    /**
     * @var string
     *
     * @ORM\Column(name="_kovetelmeny", type="string", nullable=true)
     */
    private $kovetelmeny = 'jegy';

    /**
     * @var boolean
     *
     * @ORM\Column(name="jovahagyva", type="boolean", nullable=true)
     */
    private $jovahagyva = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="beDt", type="date")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $bedt;

    /**
     * @var \AppBundle\Entity\Diak
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Diak")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="diakId", referencedColumnName="diakId")
     * })
     */
    private $diakid;

    /**
     * @var \AppBundle\Entity\Tankor
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Tankor")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tankorId", referencedColumnName="tankorId")
     * })
     */
    private $tankorid;



    /**
     * Set kidt
     *
     * @param \DateTime $kidt
     *
     * @return Tankordiak
     */
    public function setKidt($kidt)
    {
        $this->kidt = $kidt;

        return $this;
    }

    /**
     * Get kidt
     *
     * @return \DateTime
     */
    public function getKidt()
    {
        return $this->kidt;
    }

    /**
     * Set jelenlet
     *
     * @param string $jelenlet
     *
     * @return Tankordiak
     */
    public function setJelenlet($jelenlet)
    {
        $this->jelenlet = $jelenlet;

        return $this;
    }

    /**
     * Get jelenlet
     *
     * @return string
     */
    public function getJelenlet()
    {
        return $this->jelenlet;
    }

    /**
     * Set kovetelmeny
     *
     * @param string $kovetelmeny
     *
     * @return Tankordiak
     */
    public function setKovetelmeny($kovetelmeny)
    {
        $this->kovetelmeny = $kovetelmeny;

        return $this;
    }

    /**
     * Get kovetelmeny
     *
     * @return string
     */
    public function getKovetelmeny()
    {
        return $this->kovetelmeny;
    }

    /**
     * Set jovahagyva
     *
     * @param boolean $jovahagyva
     *
     * @return Tankordiak
     */
    public function setJovahagyva($jovahagyva)
    {
        $this->jovahagyva = $jovahagyva;

        return $this;
    }

    /**
     * Get jovahagyva
     *
     * @return boolean
     */
    public function getJovahagyva()
    {
        return $this->jovahagyva;
    }

    /**
     * Set bedt
     *
     * @param \DateTime $bedt
     *
     * @return Tankordiak
     */
    public function setBedt($bedt)
    {
        $this->bedt = $bedt;

        return $this;
    }

    /**
     * Get bedt
     *
     * @return \DateTime
     */
    public function getBedt()
    {
        return $this->bedt;
    }

    /**
     * Set diakid
     *
     * @param \AppBundle\Entity\Diak $diakid
     *
     * @return Tankordiak
     */
    public function setDiakid(\AppBundle\Entity\Diak $diakid)
    {
        $this->diakid = $diakid;

        return $this;
    }

    /**
     * Get diakid
     *
     * @return \AppBundle\Entity\Diak
     */
    public function getDiakid()
    {
        return $this->diakid;
    }

    /**
     * Set tankorid
     *
     * @param \AppBundle\Entity\Tankor $tankorid
     *
     * @return Tankordiak
     */
    public function setTankorid(\AppBundle\Entity\Tankor $tankorid)
    {
        $this->tankorid = $tankorid;

        return $this;
    }

    /**
     * Get tankorid
     *
     * @return \AppBundle\Entity\Tankor
     */
    public function getTankorid()
    {
        return $this->tankorid;
    }
}
