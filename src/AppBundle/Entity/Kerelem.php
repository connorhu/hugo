<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Kerelem
 *
 * @ORM\Table(name="kerelem")
 * @ORM\Entity
 */
class Kerelem
{
    /**
     * @var string
     *
     * @ORM\Column(name="userAccount", type="string", length=32, nullable=true)
     */
    private $useraccount;

    /**
     * @var string
     *
     * @ORM\Column(name="szoveg", type="text", length=65535, nullable=true)
     */
    private $szoveg;

    /**
     * @var string
     *
     * @ORM\Column(name="valasz", type="text", length=65535, nullable=true)
     */
    private $valasz;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="rogzitesDt", type="datetime", nullable=true)
     */
    private $rogzitesdt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="jovahagyasDt", type="datetime", nullable=true)
     */
    private $jovahagyasdt;

    /**
     * @var string
     *
     * @ORM\Column(name="jovahagyasAccount", type="string", length=32, nullable=true)
     */
    private $jovahagyasaccount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lezarasDt", type="datetime", nullable=true)
     */
    private $lezarasdt;

    /**
     * @var string
     *
     * @ORM\Column(name="kategoria", type="string", length=16, nullable=true)
     */
    private $kategoria;

    /**
     * @var boolean
     *
     * @ORM\Column(name="telephelyId", type="boolean", nullable=true)
     */
    private $telephelyid;

    /**
     * @var integer
     *
     * @ORM\Column(name="kerelemId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $kerelemid;



    /**
     * Set useraccount
     *
     * @param string $useraccount
     *
     * @return Kerelem
     */
    public function setUseraccount($useraccount)
    {
        $this->useraccount = $useraccount;

        return $this;
    }

    /**
     * Get useraccount
     *
     * @return string
     */
    public function getUseraccount()
    {
        return $this->useraccount;
    }

    /**
     * Set szoveg
     *
     * @param string $szoveg
     *
     * @return Kerelem
     */
    public function setSzoveg($szoveg)
    {
        $this->szoveg = $szoveg;

        return $this;
    }

    /**
     * Get szoveg
     *
     * @return string
     */
    public function getSzoveg()
    {
        return $this->szoveg;
    }

    /**
     * Set valasz
     *
     * @param string $valasz
     *
     * @return Kerelem
     */
    public function setValasz($valasz)
    {
        $this->valasz = $valasz;

        return $this;
    }

    /**
     * Get valasz
     *
     * @return string
     */
    public function getValasz()
    {
        return $this->valasz;
    }

    /**
     * Set rogzitesdt
     *
     * @param \DateTime $rogzitesdt
     *
     * @return Kerelem
     */
    public function setRogzitesdt($rogzitesdt)
    {
        $this->rogzitesdt = $rogzitesdt;

        return $this;
    }

    /**
     * Get rogzitesdt
     *
     * @return \DateTime
     */
    public function getRogzitesdt()
    {
        return $this->rogzitesdt;
    }

    /**
     * Set jovahagyasdt
     *
     * @param \DateTime $jovahagyasdt
     *
     * @return Kerelem
     */
    public function setJovahagyasdt($jovahagyasdt)
    {
        $this->jovahagyasdt = $jovahagyasdt;

        return $this;
    }

    /**
     * Get jovahagyasdt
     *
     * @return \DateTime
     */
    public function getJovahagyasdt()
    {
        return $this->jovahagyasdt;
    }

    /**
     * Set jovahagyasaccount
     *
     * @param string $jovahagyasaccount
     *
     * @return Kerelem
     */
    public function setJovahagyasaccount($jovahagyasaccount)
    {
        $this->jovahagyasaccount = $jovahagyasaccount;

        return $this;
    }

    /**
     * Get jovahagyasaccount
     *
     * @return string
     */
    public function getJovahagyasaccount()
    {
        return $this->jovahagyasaccount;
    }

    /**
     * Set lezarasdt
     *
     * @param \DateTime $lezarasdt
     *
     * @return Kerelem
     */
    public function setLezarasdt($lezarasdt)
    {
        $this->lezarasdt = $lezarasdt;

        return $this;
    }

    /**
     * Get lezarasdt
     *
     * @return \DateTime
     */
    public function getLezarasdt()
    {
        return $this->lezarasdt;
    }

    /**
     * Set kategoria
     *
     * @param string $kategoria
     *
     * @return Kerelem
     */
    public function setKategoria($kategoria)
    {
        $this->kategoria = $kategoria;

        return $this;
    }

    /**
     * Get kategoria
     *
     * @return string
     */
    public function getKategoria()
    {
        return $this->kategoria;
    }

    /**
     * Set telephelyid
     *
     * @param boolean $telephelyid
     *
     * @return Kerelem
     */
    public function setTelephelyid($telephelyid)
    {
        $this->telephelyid = $telephelyid;

        return $this;
    }

    /**
     * Get telephelyid
     *
     * @return boolean
     */
    public function getTelephelyid()
    {
        return $this->telephelyid;
    }

    /**
     * Get kerelemid
     *
     * @return integer
     */
    public function getKerelemid()
    {
        return $this->kerelemid;
    }
}
