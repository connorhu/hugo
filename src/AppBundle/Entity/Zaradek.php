<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Zaradek
 *
 * @ORM\Table(name="zaradek", indexes={@ORM\Index(name="zaradek_FKIndex1", columns={"diakId"})})
 * @ORM\Entity
 */
class Zaradek
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt", type="date", nullable=true)
     */
    private $dt;

    /**
     * @var string
     *
     * @ORM\Column(name="sorszam", type="string", length=5, nullable=true)
     */
    private $sorszam;

    /**
     * @var array
     *
     * @ORM\Column(name="dokumentum", type="simple_array", nullable=true)
     */
    private $dokumentum;

    /**
     * @var string
     *
     * @ORM\Column(name="szoveg", type="string", length=255, nullable=true)
     */
    private $szoveg;

    /**
     * @var boolean
     *
     * @ORM\Column(name="zaradekIndex", type="boolean", nullable=true)
     */
    private $zaradekindex;

    /**
     * @var string
     *
     * @ORM\Column(name="iktatoszam", type="string", length=60, nullable=false)
     */
    private $iktatoszam = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="zaradekId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $zaradekid;

    /**
     * @var \AppBundle\Entity\Diak
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Diak")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="diakId", referencedColumnName="diakId")
     * })
     */
    private $diakid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Zarojegy", mappedBy="zaradekid")
     */
    private $zarojegyid;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->zarojegyid = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set dt
     *
     * @param \DateTime $dt
     *
     * @return Zaradek
     */
    public function setDt($dt)
    {
        $this->dt = $dt;

        return $this;
    }

    /**
     * Get dt
     *
     * @return \DateTime
     */
    public function getDt()
    {
        return $this->dt;
    }

    /**
     * Set sorszam
     *
     * @param string $sorszam
     *
     * @return Zaradek
     */
    public function setSorszam($sorszam)
    {
        $this->sorszam = $sorszam;

        return $this;
    }

    /**
     * Get sorszam
     *
     * @return string
     */
    public function getSorszam()
    {
        return $this->sorszam;
    }

    /**
     * Set dokumentum
     *
     * @param array $dokumentum
     *
     * @return Zaradek
     */
    public function setDokumentum($dokumentum)
    {
        $this->dokumentum = $dokumentum;

        return $this;
    }

    /**
     * Get dokumentum
     *
     * @return array
     */
    public function getDokumentum()
    {
        return $this->dokumentum;
    }

    /**
     * Set szoveg
     *
     * @param string $szoveg
     *
     * @return Zaradek
     */
    public function setSzoveg($szoveg)
    {
        $this->szoveg = $szoveg;

        return $this;
    }

    /**
     * Get szoveg
     *
     * @return string
     */
    public function getSzoveg()
    {
        return $this->szoveg;
    }

    /**
     * Set zaradekindex
     *
     * @param boolean $zaradekindex
     *
     * @return Zaradek
     */
    public function setZaradekindex($zaradekindex)
    {
        $this->zaradekindex = $zaradekindex;

        return $this;
    }

    /**
     * Get zaradekindex
     *
     * @return boolean
     */
    public function getZaradekindex()
    {
        return $this->zaradekindex;
    }

    /**
     * Set iktatoszam
     *
     * @param string $iktatoszam
     *
     * @return Zaradek
     */
    public function setIktatoszam($iktatoszam)
    {
        $this->iktatoszam = $iktatoszam;

        return $this;
    }

    /**
     * Get iktatoszam
     *
     * @return string
     */
    public function getIktatoszam()
    {
        return $this->iktatoszam;
    }

    /**
     * Get zaradekid
     *
     * @return integer
     */
    public function getZaradekid()
    {
        return $this->zaradekid;
    }

    /**
     * Set diakid
     *
     * @param \AppBundle\Entity\Diak $diakid
     *
     * @return Zaradek
     */
    public function setDiakid(\AppBundle\Entity\Diak $diakid = null)
    {
        $this->diakid = $diakid;

        return $this;
    }

    /**
     * Get diakid
     *
     * @return \AppBundle\Entity\Diak
     */
    public function getDiakid()
    {
        return $this->diakid;
    }

    /**
     * Add zarojegyid
     *
     * @param \AppBundle\Entity\Zarojegy $zarojegyid
     *
     * @return Zaradek
     */
    public function addZarojegyid(\AppBundle\Entity\Zarojegy $zarojegyid)
    {
        $this->zarojegyid[] = $zarojegyid;

        return $this;
    }

    /**
     * Remove zarojegyid
     *
     * @param \AppBundle\Entity\Zarojegy $zarojegyid
     */
    public function removeZarojegyid(\AppBundle\Entity\Zarojegy $zarojegyid)
    {
        $this->zarojegyid->removeElement($zarojegyid);
    }

    /**
     * Get zarojegyid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getZarojegyid()
    {
        return $this->zarojegyid;
    }
}
