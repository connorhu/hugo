<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Idoszak
 *
 * @ORM\Table(name="idoszak", indexes={@ORM\Index(name="szemeszterIdoszak_FKIndex1", columns={"tanev", "szemeszter"})})
 * @ORM\Entity
 */
class Idoszak
{
    /**
     * @var string
     *
     * @ORM\Column(name="tipus", type="string", nullable=true)
     */
    private $tipus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tolDt", type="datetime", nullable=false)
     */
    private $toldt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="igDt", type="datetime", nullable=false)
     */
    private $igdt;

    /**
     * @var integer
     *
     * @ORM\Column(name="idoszakId", type="smallint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idoszakid;

    /**
     * @var \AppBundle\Entity\Szemeszter
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Szemeszter")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tanev", referencedColumnName="tanev"),
     *   @ORM\JoinColumn(name="szemeszter", referencedColumnName="szemeszter")
     * })
     */
    private $tanev;



    /**
     * Set tipus
     *
     * @param string $tipus
     *
     * @return Idoszak
     */
    public function setTipus($tipus)
    {
        $this->tipus = $tipus;

        return $this;
    }

    /**
     * Get tipus
     *
     * @return string
     */
    public function getTipus()
    {
        return $this->tipus;
    }

    /**
     * Set toldt
     *
     * @param \DateTime $toldt
     *
     * @return Idoszak
     */
    public function setToldt($toldt)
    {
        $this->toldt = $toldt;

        return $this;
    }

    /**
     * Get toldt
     *
     * @return \DateTime
     */
    public function getToldt()
    {
        return $this->toldt;
    }

    /**
     * Set igdt
     *
     * @param \DateTime $igdt
     *
     * @return Idoszak
     */
    public function setIgdt($igdt)
    {
        $this->igdt = $igdt;

        return $this;
    }

    /**
     * Get igdt
     *
     * @return \DateTime
     */
    public function getIgdt()
    {
        return $this->igdt;
    }

    /**
     * Get idoszakid
     *
     * @return integer
     */
    public function getIdoszakid()
    {
        return $this->idoszakid;
    }

    /**
     * Set tanev
     *
     * @param \AppBundle\Entity\Szemeszter $tanev
     *
     * @return Idoszak
     */
    public function setTanev(\AppBundle\Entity\Szemeszter $tanev = null)
    {
        $this->tanev = $tanev;

        return $this;
    }

    /**
     * Get tanev
     *
     * @return \AppBundle\Entity\Szemeszter
     */
    public function getTanev()
    {
        return $this->tanev;
    }
}
