<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tanmenettankor
 *
 * @ORM\Table(name="tanmenetTankor", indexes={@ORM\Index(name="tanmenetTankor_FKIndex1", columns={"tankorId"}), @ORM\Index(name="tanmenetTankor_ibfk_2", columns={"tanmenetId"}), @ORM\Index(name="tanmenetTankor_ibfk_3", columns={"tanev"})})
 * @ORM\Entity
 */
class Tanmenettankor
{
    /**
     * @var \AppBundle\Entity\Szemeszter
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Szemeszter")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tanev", referencedColumnName="tanev")
     * })
     */
    private $tanev;

    /**
     * @var \AppBundle\Entity\Tanmenet
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Tanmenet")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tanmenetId", referencedColumnName="tanmenetId")
     * })
     */
    private $tanmenetid;

    /**
     * @var \AppBundle\Entity\Tankor
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Tankor")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tankorId", referencedColumnName="tankorId")
     * })
     */
    private $tankorid;



    /**
     * Set tanev
     *
     * @param \AppBundle\Entity\Szemeszter $tanev
     *
     * @return Tanmenettankor
     */
    public function setTanev(\AppBundle\Entity\Szemeszter $tanev)
    {
        $this->tanev = $tanev;

        return $this;
    }

    /**
     * Get tanev
     *
     * @return \AppBundle\Entity\Szemeszter
     */
    public function getTanev()
    {
        return $this->tanev;
    }

    /**
     * Set tanmenetid
     *
     * @param \AppBundle\Entity\Tanmenet $tanmenetid
     *
     * @return Tanmenettankor
     */
    public function setTanmenetid(\AppBundle\Entity\Tanmenet $tanmenetid)
    {
        $this->tanmenetid = $tanmenetid;

        return $this;
    }

    /**
     * Get tanmenetid
     *
     * @return \AppBundle\Entity\Tanmenet
     */
    public function getTanmenetid()
    {
        return $this->tanmenetid;
    }

    /**
     * Set tankorid
     *
     * @param \AppBundle\Entity\Tankor $tankorid
     *
     * @return Tanmenettankor
     */
    public function setTankorid(\AppBundle\Entity\Tankor $tankorid)
    {
        $this->tankorid = $tankorid;

        return $this;
    }

    /**
     * Get tankorid
     *
     * @return \AppBundle\Entity\Tankor
     */
    public function getTankorid()
    {
        return $this->tankorid;
    }
}
