<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Terem
 *
 * @ORM\Table(name="terem", indexes={@ORM\Index(name="terem_telephely", columns={"telephelyId"})})
 * @ORM\Entity
 */
class Terem
{
    /**
     * @var string
     *
     * @ORM\Column(name="leiras", type="string", length=64, nullable=true)
     */
    private $leiras;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ferohely", type="boolean", nullable=true)
     */
    private $ferohely;

    /**
     * @var array
     *
     * @ORM\Column(name="tipus", type="simple_array", nullable=true)
     */
    private $tipus;

    /**
     * @var integer
     *
     * @ORM\Column(name="teremId", type="smallint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $teremid;

    /**
     * @var \AppBundle\Entity\Telephely
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Telephely")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="telephelyId", referencedColumnName="telephelyId")
     * })
     */
    private $telephelyid;



    /**
     * Set leiras
     *
     * @param string $leiras
     *
     * @return Terem
     */
    public function setLeiras($leiras)
    {
        $this->leiras = $leiras;

        return $this;
    }

    /**
     * Get leiras
     *
     * @return string
     */
    public function getLeiras()
    {
        return $this->leiras;
    }

    /**
     * Set ferohely
     *
     * @param boolean $ferohely
     *
     * @return Terem
     */
    public function setFerohely($ferohely)
    {
        $this->ferohely = $ferohely;

        return $this;
    }

    /**
     * Get ferohely
     *
     * @return boolean
     */
    public function getFerohely()
    {
        return $this->ferohely;
    }

    /**
     * Set tipus
     *
     * @param array $tipus
     *
     * @return Terem
     */
    public function setTipus($tipus)
    {
        $this->tipus = $tipus;

        return $this;
    }

    /**
     * Get tipus
     *
     * @return array
     */
    public function getTipus()
    {
        return $this->tipus;
    }

    /**
     * Get teremid
     *
     * @return integer
     */
    public function getTeremid()
    {
        return $this->teremid;
    }

    /**
     * Set telephelyid
     *
     * @param \AppBundle\Entity\Telephely $telephelyid
     *
     * @return Terem
     */
    public function setTelephelyid(\AppBundle\Entity\Telephely $telephelyid = null)
    {
        $this->telephelyid = $telephelyid;

        return $this;
    }

    /**
     * Get telephelyid
     *
     * @return \AppBundle\Entity\Telephely
     */
    public function getTelephelyid()
    {
        return $this->telephelyid;
    }
}
