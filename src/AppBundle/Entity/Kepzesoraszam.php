<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Kepzesoraszam
 *
 * @ORM\Table(name="kepzesOraszam", indexes={@ORM\Index(name="kepzesOraszam_FKIndex1", columns={"kepzesId"})})
 * @ORM\Entity
 */
class Kepzesoraszam
{
    /**
     * @var string
     *
     * @ORM\Column(name="kotelezoOraszam", type="decimal", precision=4, scale=2, nullable=true)
     */
    private $kotelezooraszam;

    /**
     * @var string
     *
     * @ORM\Column(name="maximalisOraszam", type="decimal", precision=4, scale=2, nullable=true)
     */
    private $maximalisoraszam;

    /**
     * @var integer
     *
     * @ORM\Column(name="tanitasiHetekSzama", type="smallint", nullable=true)
     */
    private $tanitasihetekszama;

    /**
     * @var string
     *
     * @ORM\Column(name="evfolyamJel", type="string", length=32)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $evfolyamjel;

    /**
     * @var \AppBundle\Entity\Kepzes
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Kepzes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="kepzesId", referencedColumnName="kepzesId")
     * })
     */
    private $kepzesid;



    /**
     * Set kotelezooraszam
     *
     * @param string $kotelezooraszam
     *
     * @return Kepzesoraszam
     */
    public function setKotelezooraszam($kotelezooraszam)
    {
        $this->kotelezooraszam = $kotelezooraszam;

        return $this;
    }

    /**
     * Get kotelezooraszam
     *
     * @return string
     */
    public function getKotelezooraszam()
    {
        return $this->kotelezooraszam;
    }

    /**
     * Set maximalisoraszam
     *
     * @param string $maximalisoraszam
     *
     * @return Kepzesoraszam
     */
    public function setMaximalisoraszam($maximalisoraszam)
    {
        $this->maximalisoraszam = $maximalisoraszam;

        return $this;
    }

    /**
     * Get maximalisoraszam
     *
     * @return string
     */
    public function getMaximalisoraszam()
    {
        return $this->maximalisoraszam;
    }

    /**
     * Set tanitasihetekszama
     *
     * @param integer $tanitasihetekszama
     *
     * @return Kepzesoraszam
     */
    public function setTanitasihetekszama($tanitasihetekszama)
    {
        $this->tanitasihetekszama = $tanitasihetekszama;

        return $this;
    }

    /**
     * Get tanitasihetekszama
     *
     * @return integer
     */
    public function getTanitasihetekszama()
    {
        return $this->tanitasihetekszama;
    }

    /**
     * Set evfolyamjel
     *
     * @param string $evfolyamjel
     *
     * @return Kepzesoraszam
     */
    public function setEvfolyamjel($evfolyamjel)
    {
        $this->evfolyamjel = $evfolyamjel;

        return $this;
    }

    /**
     * Get evfolyamjel
     *
     * @return string
     */
    public function getEvfolyamjel()
    {
        return $this->evfolyamjel;
    }

    /**
     * Set kepzesid
     *
     * @param \AppBundle\Entity\Kepzes $kepzesid
     *
     * @return Kepzesoraszam
     */
    public function setKepzesid(\AppBundle\Entity\Kepzes $kepzesid)
    {
        $this->kepzesid = $kepzesid;

        return $this;
    }

    /**
     * Get kepzesid
     *
     * @return \AppBundle\Entity\Kepzes
     */
    public function getKepzesid()
    {
        return $this->kepzesid;
    }
}
