<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Kosziesemeny
 *
 * @ORM\Table(name="kosziEsemeny")
 * @ORM\Entity
 */
class Kosziesemeny
{
    /**
     * @var string
     *
     * @ORM\Column(name="kosziEsemenyNev", type="string", length=50, nullable=false)
     */
    private $kosziesemenynev;

    /**
     * @var string
     *
     * @ORM\Column(name="kosziEsemenyLeiras", type="string", length=255, nullable=false)
     */
    private $kosziesemenyleiras;

    /**
     * @var string
     *
     * @ORM\Column(name="kosziEsemenyTipus", type="string", nullable=false)
     */
    private $kosziesemenytipus;

    /**
     * @var boolean
     *
     * @ORM\Column(name="kosziEsemenyIntervallum", type="boolean", nullable=true)
     */
    private $kosziesemenyintervallum = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="kosziEsemenyId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $kosziesemenyid;



    /**
     * Set kosziesemenynev
     *
     * @param string $kosziesemenynev
     *
     * @return Kosziesemeny
     */
    public function setKosziesemenynev($kosziesemenynev)
    {
        $this->kosziesemenynev = $kosziesemenynev;

        return $this;
    }

    /**
     * Get kosziesemenynev
     *
     * @return string
     */
    public function getKosziesemenynev()
    {
        return $this->kosziesemenynev;
    }

    /**
     * Set kosziesemenyleiras
     *
     * @param string $kosziesemenyleiras
     *
     * @return Kosziesemeny
     */
    public function setKosziesemenyleiras($kosziesemenyleiras)
    {
        $this->kosziesemenyleiras = $kosziesemenyleiras;

        return $this;
    }

    /**
     * Get kosziesemenyleiras
     *
     * @return string
     */
    public function getKosziesemenyleiras()
    {
        return $this->kosziesemenyleiras;
    }

    /**
     * Set kosziesemenytipus
     *
     * @param string $kosziesemenytipus
     *
     * @return Kosziesemeny
     */
    public function setKosziesemenytipus($kosziesemenytipus)
    {
        $this->kosziesemenytipus = $kosziesemenytipus;

        return $this;
    }

    /**
     * Get kosziesemenytipus
     *
     * @return string
     */
    public function getKosziesemenytipus()
    {
        return $this->kosziesemenytipus;
    }

    /**
     * Set kosziesemenyintervallum
     *
     * @param boolean $kosziesemenyintervallum
     *
     * @return Kosziesemeny
     */
    public function setKosziesemenyintervallum($kosziesemenyintervallum)
    {
        $this->kosziesemenyintervallum = $kosziesemenyintervallum;

        return $this;
    }

    /**
     * Get kosziesemenyintervallum
     *
     * @return boolean
     */
    public function getKosziesemenyintervallum()
    {
        return $this->kosziesemenyintervallum;
    }

    /**
     * Get kosziesemenyid
     *
     * @return integer
     */
    public function getKosziesemenyid()
    {
        return $this->kosziesemenyid;
    }
}
