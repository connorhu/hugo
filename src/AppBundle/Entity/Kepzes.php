<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Kepzes
 *
 * @ORM\Table(name="kepzes", uniqueConstraints={@ORM\UniqueConstraint(name="kepzesNevTanev", columns={"kepzesNev", "tanev"})}, indexes={@ORM\Index(name="kepzes_ibfk_1", columns={"osztalyJellegId"})})
 * @ORM\Entity
 */
class Kepzes
{
    /**
     * @var string
     *
     * @ORM\Column(name="kepzesNev", type="string", length=255, nullable=false)
     */
    private $kepzesnev;

    /**
     * @var integer
     *
     * @ORM\Column(name="tanev", type="smallint", nullable=true)
     */
    private $tanev;

    /**
     * @var boolean
     *
     * @ORM\Column(name="_kezdoEvfolyam", type="boolean", nullable=true)
     */
    private $kezdoevfolyam;

    /**
     * @var boolean
     *
     * @ORM\Column(name="_zaroEvfolyam", type="boolean", nullable=true)
     */
    private $zaroevfolyam;

    /**
     * @var boolean
     *
     * @ORM\Column(name="kepzesEles", type="boolean", nullable=false)
     */
    private $kepzeseles = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="kepzesId", type="smallint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $kepzesid;

    /**
     * @var \AppBundle\Entity\Osztalyjelleg
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Osztalyjelleg")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="osztalyJellegId", referencedColumnName="osztalyJellegId")
     * })
     */
    private $osztalyjellegid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Osztaly", inversedBy="kepzesid")
     * @ORM\JoinTable(name="kepzesosztaly",
     *   joinColumns={
     *     @ORM\JoinColumn(name="kepzesId", referencedColumnName="kepzesId")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="osztalyId", referencedColumnName="osztalyId")
     *   }
     * )
     */
    private $osztalyid;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->osztalyid = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set kepzesnev
     *
     * @param string $kepzesnev
     *
     * @return Kepzes
     */
    public function setKepzesnev($kepzesnev)
    {
        $this->kepzesnev = $kepzesnev;

        return $this;
    }

    /**
     * Get kepzesnev
     *
     * @return string
     */
    public function getKepzesnev()
    {
        return $this->kepzesnev;
    }

    /**
     * Set tanev
     *
     * @param integer $tanev
     *
     * @return Kepzes
     */
    public function setTanev($tanev)
    {
        $this->tanev = $tanev;

        return $this;
    }

    /**
     * Get tanev
     *
     * @return integer
     */
    public function getTanev()
    {
        return $this->tanev;
    }

    /**
     * Set kezdoevfolyam
     *
     * @param boolean $kezdoevfolyam
     *
     * @return Kepzes
     */
    public function setKezdoevfolyam($kezdoevfolyam)
    {
        $this->kezdoevfolyam = $kezdoevfolyam;

        return $this;
    }

    /**
     * Get kezdoevfolyam
     *
     * @return boolean
     */
    public function getKezdoevfolyam()
    {
        return $this->kezdoevfolyam;
    }

    /**
     * Set zaroevfolyam
     *
     * @param boolean $zaroevfolyam
     *
     * @return Kepzes
     */
    public function setZaroevfolyam($zaroevfolyam)
    {
        $this->zaroevfolyam = $zaroevfolyam;

        return $this;
    }

    /**
     * Get zaroevfolyam
     *
     * @return boolean
     */
    public function getZaroevfolyam()
    {
        return $this->zaroevfolyam;
    }

    /**
     * Set kepzeseles
     *
     * @param boolean $kepzeseles
     *
     * @return Kepzes
     */
    public function setKepzeseles($kepzeseles)
    {
        $this->kepzeseles = $kepzeseles;

        return $this;
    }

    /**
     * Get kepzeseles
     *
     * @return boolean
     */
    public function getKepzeseles()
    {
        return $this->kepzeseles;
    }

    /**
     * Get kepzesid
     *
     * @return integer
     */
    public function getKepzesid()
    {
        return $this->kepzesid;
    }

    /**
     * Set osztalyjellegid
     *
     * @param \AppBundle\Entity\Osztalyjelleg $osztalyjellegid
     *
     * @return Kepzes
     */
    public function setOsztalyjellegid(\AppBundle\Entity\Osztalyjelleg $osztalyjellegid = null)
    {
        $this->osztalyjellegid = $osztalyjellegid;

        return $this;
    }

    /**
     * Get osztalyjellegid
     *
     * @return \AppBundle\Entity\Osztalyjelleg
     */
    public function getOsztalyjellegid()
    {
        return $this->osztalyjellegid;
    }

    /**
     * Add osztalyid
     *
     * @param \AppBundle\Entity\Osztaly $osztalyid
     *
     * @return Kepzes
     */
    public function addOsztalyid(\AppBundle\Entity\Osztaly $osztalyid)
    {
        $this->osztalyid[] = $osztalyid;

        return $this;
    }

    /**
     * Remove osztalyid
     *
     * @param \AppBundle\Entity\Osztaly $osztalyid
     */
    public function removeOsztalyid(\AppBundle\Entity\Osztaly $osztalyid)
    {
        $this->osztalyid->removeElement($osztalyid);
    }

    /**
     * Get osztalyid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOsztalyid()
    {
        return $this->osztalyid;
    }
}
