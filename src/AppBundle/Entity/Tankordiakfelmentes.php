<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tankordiakfelmentes
 *
 * @ORM\Table(name="tankorDiakFelmentes", indexes={@ORM\Index(name="tankorDiakFM_FKIndex1", columns={"tankorId"}), @ORM\Index(name="diakId", columns={"diakId"})})
 * @ORM\Entity
 */
class Tankordiakfelmentes
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="beDt", type="date", nullable=false)
     */
    private $bedt = '0000-00-00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="kiDt", type="date", nullable=true)
     */
    private $kidt;

    /**
     * @var string
     *
     * @ORM\Column(name="felmentesTipus", type="string", nullable=false)
     */
    private $felmentestipus = 'óralátogatás alól';

    /**
     * @var boolean
     *
     * @ORM\Column(name="nap", type="boolean", nullable=true)
     */
    private $nap;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ora", type="boolean", nullable=true)
     */
    private $ora;

    /**
     * @var string
     *
     * @ORM\Column(name="iktatoszam", type="string", length=60, nullable=false)
     */
    private $iktatoszam = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="tankorDiakFelmentesId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tankordiakfelmentesid;

    /**
     * @var \AppBundle\Entity\Diak
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Diak")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="diakId", referencedColumnName="diakId")
     * })
     */
    private $diakid;

    /**
     * @var \AppBundle\Entity\Tankor
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Tankor")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tankorId", referencedColumnName="tankorId")
     * })
     */
    private $tankorid;



    /**
     * Set bedt
     *
     * @param \DateTime $bedt
     *
     * @return Tankordiakfelmentes
     */
    public function setBedt($bedt)
    {
        $this->bedt = $bedt;

        return $this;
    }

    /**
     * Get bedt
     *
     * @return \DateTime
     */
    public function getBedt()
    {
        return $this->bedt;
    }

    /**
     * Set kidt
     *
     * @param \DateTime $kidt
     *
     * @return Tankordiakfelmentes
     */
    public function setKidt($kidt)
    {
        $this->kidt = $kidt;

        return $this;
    }

    /**
     * Get kidt
     *
     * @return \DateTime
     */
    public function getKidt()
    {
        return $this->kidt;
    }

    /**
     * Set felmentestipus
     *
     * @param string $felmentestipus
     *
     * @return Tankordiakfelmentes
     */
    public function setFelmentestipus($felmentestipus)
    {
        $this->felmentestipus = $felmentestipus;

        return $this;
    }

    /**
     * Get felmentestipus
     *
     * @return string
     */
    public function getFelmentestipus()
    {
        return $this->felmentestipus;
    }

    /**
     * Set nap
     *
     * @param boolean $nap
     *
     * @return Tankordiakfelmentes
     */
    public function setNap($nap)
    {
        $this->nap = $nap;

        return $this;
    }

    /**
     * Get nap
     *
     * @return boolean
     */
    public function getNap()
    {
        return $this->nap;
    }

    /**
     * Set ora
     *
     * @param boolean $ora
     *
     * @return Tankordiakfelmentes
     */
    public function setOra($ora)
    {
        $this->ora = $ora;

        return $this;
    }

    /**
     * Get ora
     *
     * @return boolean
     */
    public function getOra()
    {
        return $this->ora;
    }

    /**
     * Set iktatoszam
     *
     * @param string $iktatoszam
     *
     * @return Tankordiakfelmentes
     */
    public function setIktatoszam($iktatoszam)
    {
        $this->iktatoszam = $iktatoszam;

        return $this;
    }

    /**
     * Get iktatoszam
     *
     * @return string
     */
    public function getIktatoszam()
    {
        return $this->iktatoszam;
    }

    /**
     * Get tankordiakfelmentesid
     *
     * @return integer
     */
    public function getTankordiakfelmentesid()
    {
        return $this->tankordiakfelmentesid;
    }

    /**
     * Set diakid
     *
     * @param \AppBundle\Entity\Diak $diakid
     *
     * @return Tankordiakfelmentes
     */
    public function setDiakid(\AppBundle\Entity\Diak $diakid = null)
    {
        $this->diakid = $diakid;

        return $this;
    }

    /**
     * Get diakid
     *
     * @return \AppBundle\Entity\Diak
     */
    public function getDiakid()
    {
        return $this->diakid;
    }

    /**
     * Set tankorid
     *
     * @param \AppBundle\Entity\Tankor $tankorid
     *
     * @return Tankordiakfelmentes
     */
    public function setTankorid(\AppBundle\Entity\Tankor $tankorid = null)
    {
        $this->tankorid = $tankorid;

        return $this;
    }

    /**
     * Get tankorid
     *
     * @return \AppBundle\Entity\Tankor
     */
    public function getTankorid()
    {
        return $this->tankorid;
    }
}
