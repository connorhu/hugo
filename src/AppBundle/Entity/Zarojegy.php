<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Zarojegy
 *
 * @ORM\Table(name="zaroJegy", indexes={@ORM\Index(name="zaroJegy_FKIndex2", columns={"diakId"}), @ORM\Index(name="zaroJegy_FKIndex3", columns={"targyId"})})
 * @ORM\Entity
 */
class Zarojegy
{
    /**
     * @var boolean
     *
     * @ORM\Column(name="evfolyam", type="boolean", nullable=true)
     */
    private $evfolyam;

    /**
     * @var string
     *
     * @ORM\Column(name="evfolyamJel", type="string", length=32, nullable=true)
     */
    private $evfolyamjel;

    /**
     * @var boolean
     *
     * @ORM\Column(name="felev", type="boolean", nullable=true)
     */
    private $felev;

    /**
     * @var string
     *
     * @ORM\Column(name="jegy", type="decimal", precision=4, scale=1, nullable=false)
     */
    private $jegy;

    /**
     * @var string
     *
     * @ORM\Column(name="jegyTipus", type="string", nullable=false)
     */
    private $jegytipus = 'jegy';

    /**
     * @var string
     *
     * @ORM\Column(name="megjegyzes", type="string", nullable=true)
     */
    private $megjegyzes;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modositasDt", type="date", nullable=false)
     */
    private $modositasdt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="hivatalosDt", type="date", nullable=false)
     */
    private $hivatalosdt;

    /**
     * @var integer
     *
     * @ORM\Column(name="zaroJegyId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $zarojegyid;

    /**
     * @var \AppBundle\Entity\Diak
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Diak")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="diakId", referencedColumnName="diakId")
     * })
     */
    private $diakid;

    /**
     * @var \AppBundle\Entity\Targy
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Targy")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="targyId", referencedColumnName="targyId")
     * })
     */
    private $targyid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Zaradek", inversedBy="zarojegyid")
     * @ORM\JoinTable(name="zarojegyzaradek",
     *   joinColumns={
     *     @ORM\JoinColumn(name="zaroJegyId", referencedColumnName="zaroJegyId")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="zaradekId", referencedColumnName="zaradekId")
     *   }
     * )
     */
    private $zaradekid;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->zaradekid = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set evfolyam
     *
     * @param boolean $evfolyam
     *
     * @return Zarojegy
     */
    public function setEvfolyam($evfolyam)
    {
        $this->evfolyam = $evfolyam;

        return $this;
    }

    /**
     * Get evfolyam
     *
     * @return boolean
     */
    public function getEvfolyam()
    {
        return $this->evfolyam;
    }

    /**
     * Set evfolyamjel
     *
     * @param string $evfolyamjel
     *
     * @return Zarojegy
     */
    public function setEvfolyamjel($evfolyamjel)
    {
        $this->evfolyamjel = $evfolyamjel;

        return $this;
    }

    /**
     * Get evfolyamjel
     *
     * @return string
     */
    public function getEvfolyamjel()
    {
        return $this->evfolyamjel;
    }

    /**
     * Set felev
     *
     * @param boolean $felev
     *
     * @return Zarojegy
     */
    public function setFelev($felev)
    {
        $this->felev = $felev;

        return $this;
    }

    /**
     * Get felev
     *
     * @return boolean
     */
    public function getFelev()
    {
        return $this->felev;
    }

    /**
     * Set jegy
     *
     * @param string $jegy
     *
     * @return Zarojegy
     */
    public function setJegy($jegy)
    {
        $this->jegy = $jegy;

        return $this;
    }

    /**
     * Get jegy
     *
     * @return string
     */
    public function getJegy()
    {
        return $this->jegy;
    }

    /**
     * Set jegytipus
     *
     * @param string $jegytipus
     *
     * @return Zarojegy
     */
    public function setJegytipus($jegytipus)
    {
        $this->jegytipus = $jegytipus;

        return $this;
    }

    /**
     * Get jegytipus
     *
     * @return string
     */
    public function getJegytipus()
    {
        return $this->jegytipus;
    }

    /**
     * Set megjegyzes
     *
     * @param string $megjegyzes
     *
     * @return Zarojegy
     */
    public function setMegjegyzes($megjegyzes)
    {
        $this->megjegyzes = $megjegyzes;

        return $this;
    }

    /**
     * Get megjegyzes
     *
     * @return string
     */
    public function getMegjegyzes()
    {
        return $this->megjegyzes;
    }

    /**
     * Set modositasdt
     *
     * @param \DateTime $modositasdt
     *
     * @return Zarojegy
     */
    public function setModositasdt($modositasdt)
    {
        $this->modositasdt = $modositasdt;

        return $this;
    }

    /**
     * Get modositasdt
     *
     * @return \DateTime
     */
    public function getModositasdt()
    {
        return $this->modositasdt;
    }

    /**
     * Set hivatalosdt
     *
     * @param \DateTime $hivatalosdt
     *
     * @return Zarojegy
     */
    public function setHivatalosdt($hivatalosdt)
    {
        $this->hivatalosdt = $hivatalosdt;

        return $this;
    }

    /**
     * Get hivatalosdt
     *
     * @return \DateTime
     */
    public function getHivatalosdt()
    {
        return $this->hivatalosdt;
    }

    /**
     * Get zarojegyid
     *
     * @return integer
     */
    public function getZarojegyid()
    {
        return $this->zarojegyid;
    }

    /**
     * Set diakid
     *
     * @param \AppBundle\Entity\Diak $diakid
     *
     * @return Zarojegy
     */
    public function setDiakid(\AppBundle\Entity\Diak $diakid = null)
    {
        $this->diakid = $diakid;

        return $this;
    }

    /**
     * Get diakid
     *
     * @return \AppBundle\Entity\Diak
     */
    public function getDiakid()
    {
        return $this->diakid;
    }

    /**
     * Set targyid
     *
     * @param \AppBundle\Entity\Targy $targyid
     *
     * @return Zarojegy
     */
    public function setTargyid(\AppBundle\Entity\Targy $targyid = null)
    {
        $this->targyid = $targyid;

        return $this;
    }

    /**
     * Get targyid
     *
     * @return \AppBundle\Entity\Targy
     */
    public function getTargyid()
    {
        return $this->targyid;
    }

    /**
     * Add zaradekid
     *
     * @param \AppBundle\Entity\Zaradek $zaradekid
     *
     * @return Zarojegy
     */
    public function addZaradekid(\AppBundle\Entity\Zaradek $zaradekid)
    {
        $this->zaradekid[] = $zaradekid;

        return $this;
    }

    /**
     * Remove zaradekid
     *
     * @param \AppBundle\Entity\Zaradek $zaradekid
     */
    public function removeZaradekid(\AppBundle\Entity\Zaradek $zaradekid)
    {
        $this->zaradekid->removeElement($zaradekid);
    }

    /**
     * Get zaradekid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getZaradekid()
    {
        return $this->zaradekid;
    }
}
