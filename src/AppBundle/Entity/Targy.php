<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Targy
 *
 * @ORM\Table(name="targy", indexes={@ORM\Index(name="targy_FKIndex1", columns={"mkId"}), @ORM\Index(name="targy_ibfk_2", columns={"kirTargyId"})})
 * @ORM\Entity
 */
class Targy
{
    /**
     * @var string
     *
     * @ORM\Column(name="targyNev", type="string", length=128, nullable=true)
     */
    private $targynev;

    /**
     * @var string
     *
     * @ORM\Column(name="targyJelleg", type="string", nullable=true)
     */
    private $targyjelleg;

    /**
     * @var string
     *
     * @ORM\Column(name="evkoziKovetelmeny", type="string", nullable=true)
     */
    private $evkozikovetelmeny = 'jegy';

    /**
     * @var string
     *
     * @ORM\Column(name="zaroKovetelmeny", type="string", nullable=true)
     */
    private $zarokovetelmeny = 'jegy';

    /**
     * @var string
     *
     * @ORM\Column(name="targyRovidNev", type="string", length=64, nullable=true)
     */
    private $targyrovidnev;

    /**
     * @var integer
     *
     * @ORM\Column(name="targyId", type="smallint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $targyid;

    /**
     * @var \AppBundle\Entity\Munkakozosseg
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Munkakozosseg")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mkId", referencedColumnName="mkId")
     * })
     */
    private $mkid;

    /**
     * @var \AppBundle\Entity\Kirtargy
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Kirtargy")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="kirTargyId", referencedColumnName="kirTargyId")
     * })
     */
    private $kirtargyid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Kepesites", mappedBy="targyid")
     */
    private $kepesitesid;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->kepesitesid = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set targynev
     *
     * @param string $targynev
     *
     * @return Targy
     */
    public function setTargynev($targynev)
    {
        $this->targynev = $targynev;

        return $this;
    }

    /**
     * Get targynev
     *
     * @return string
     */
    public function getTargynev()
    {
        return $this->targynev;
    }

    /**
     * Set targyjelleg
     *
     * @param string $targyjelleg
     *
     * @return Targy
     */
    public function setTargyjelleg($targyjelleg)
    {
        $this->targyjelleg = $targyjelleg;

        return $this;
    }

    /**
     * Get targyjelleg
     *
     * @return string
     */
    public function getTargyjelleg()
    {
        return $this->targyjelleg;
    }

    /**
     * Set evkozikovetelmeny
     *
     * @param string $evkozikovetelmeny
     *
     * @return Targy
     */
    public function setEvkozikovetelmeny($evkozikovetelmeny)
    {
        $this->evkozikovetelmeny = $evkozikovetelmeny;

        return $this;
    }

    /**
     * Get evkozikovetelmeny
     *
     * @return string
     */
    public function getEvkozikovetelmeny()
    {
        return $this->evkozikovetelmeny;
    }

    /**
     * Set zarokovetelmeny
     *
     * @param string $zarokovetelmeny
     *
     * @return Targy
     */
    public function setZarokovetelmeny($zarokovetelmeny)
    {
        $this->zarokovetelmeny = $zarokovetelmeny;

        return $this;
    }

    /**
     * Get zarokovetelmeny
     *
     * @return string
     */
    public function getZarokovetelmeny()
    {
        return $this->zarokovetelmeny;
    }

    /**
     * Set targyrovidnev
     *
     * @param string $targyrovidnev
     *
     * @return Targy
     */
    public function setTargyrovidnev($targyrovidnev)
    {
        $this->targyrovidnev = $targyrovidnev;

        return $this;
    }

    /**
     * Get targyrovidnev
     *
     * @return string
     */
    public function getTargyrovidnev()
    {
        return $this->targyrovidnev;
    }

    /**
     * Get targyid
     *
     * @return integer
     */
    public function getTargyid()
    {
        return $this->targyid;
    }

    /**
     * Set mkid
     *
     * @param \AppBundle\Entity\Munkakozosseg $mkid
     *
     * @return Targy
     */
    public function setMkid(\AppBundle\Entity\Munkakozosseg $mkid = null)
    {
        $this->mkid = $mkid;

        return $this;
    }

    /**
     * Get mkid
     *
     * @return \AppBundle\Entity\Munkakozosseg
     */
    public function getMkid()
    {
        return $this->mkid;
    }

    /**
     * Set kirtargyid
     *
     * @param \AppBundle\Entity\Kirtargy $kirtargyid
     *
     * @return Targy
     */
    public function setKirtargyid(\AppBundle\Entity\Kirtargy $kirtargyid = null)
    {
        $this->kirtargyid = $kirtargyid;

        return $this;
    }

    /**
     * Get kirtargyid
     *
     * @return \AppBundle\Entity\Kirtargy
     */
    public function getKirtargyid()
    {
        return $this->kirtargyid;
    }

    /**
     * Add kepesitesid
     *
     * @param \AppBundle\Entity\Kepesites $kepesitesid
     *
     * @return Targy
     */
    public function addKepesitesid(\AppBundle\Entity\Kepesites $kepesitesid)
    {
        $this->kepesitesid[] = $kepesitesid;

        return $this;
    }

    /**
     * Remove kepesitesid
     *
     * @param \AppBundle\Entity\Kepesites $kepesitesid
     */
    public function removeKepesitesid(\AppBundle\Entity\Kepesites $kepesitesid)
    {
        $this->kepesitesid->removeElement($kepesitesid);
    }

    /**
     * Get kepesitesid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKepesitesid()
    {
        return $this->kepesitesid;
    }
}
