<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Session
 *
 * @ORM\Table(name="session")
 * @ORM\Entity
 */
class Session
{
    /**
     * @var string
     *
     * @ORM\Column(name="policy", type="string", length=10, nullable=false)
     */
    private $policy = 'private';

    /**
     * @var string
     *
     * @ORM\Column(name="intezmeny", type="string", length=16, nullable=true)
     */
    private $intezmeny;

    /**
     * @var boolean
     *
     * @ORM\Column(name="telephelyId", type="boolean", nullable=true)
     */
    private $telephelyid;

    /**
     * @var integer
     *
     * @ORM\Column(name="tanev", type="smallint", nullable=true)
     */
    private $tanev;

    /**
     * @var integer
     *
     * @ORM\Column(name="parentDiakId", type="integer", nullable=true)
     */
    private $parentdiakid;

    /**
     * @var string
     *
     * @ORM\Column(name="sessionID", type="string", length=40)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $sessionid;



    /**
     * Set policy
     *
     * @param string $policy
     *
     * @return Session
     */
    public function setPolicy($policy)
    {
        $this->policy = $policy;

        return $this;
    }

    /**
     * Get policy
     *
     * @return string
     */
    public function getPolicy()
    {
        return $this->policy;
    }

    /**
     * Set intezmeny
     *
     * @param string $intezmeny
     *
     * @return Session
     */
    public function setIntezmeny($intezmeny)
    {
        $this->intezmeny = $intezmeny;

        return $this;
    }

    /**
     * Get intezmeny
     *
     * @return string
     */
    public function getIntezmeny()
    {
        return $this->intezmeny;
    }

    /**
     * Set telephelyid
     *
     * @param boolean $telephelyid
     *
     * @return Session
     */
    public function setTelephelyid($telephelyid)
    {
        $this->telephelyid = $telephelyid;

        return $this;
    }

    /**
     * Get telephelyid
     *
     * @return boolean
     */
    public function getTelephelyid()
    {
        return $this->telephelyid;
    }

    /**
     * Set tanev
     *
     * @param integer $tanev
     *
     * @return Session
     */
    public function setTanev($tanev)
    {
        $this->tanev = $tanev;

        return $this;
    }

    /**
     * Get tanev
     *
     * @return integer
     */
    public function getTanev()
    {
        return $this->tanev;
    }

    /**
     * Set parentdiakid
     *
     * @param integer $parentdiakid
     *
     * @return Session
     */
    public function setParentdiakid($parentdiakid)
    {
        $this->parentdiakid = $parentdiakid;

        return $this;
    }

    /**
     * Get parentdiakid
     *
     * @return integer
     */
    public function getParentdiakid()
    {
        return $this->parentdiakid;
    }

    /**
     * Get sessionid
     *
     * @return string
     */
    public function getSessionid()
    {
        return $this->sessionid;
    }
}
