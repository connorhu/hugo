<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vizsga
 *
 * @ORM\Table(name="vizsga", indexes={@ORM\Index(name="vizsga_FKIndex1", columns={"diakId"}), @ORM\Index(name="vizsga_FKIndex2", columns={"targyId"}), @ORM\Index(name="zaradekId", columns={"zaradekId"}), @ORM\Index(name="zaroJegyId", columns={"zaroJegyId"})})
 * @ORM\Entity
 */
class Vizsga
{
    /**
     * @var boolean
     *
     * @ORM\Column(name="evfolyam", type="boolean", nullable=false)
     */
    private $evfolyam;

    /**
     * @var string
     *
     * @ORM\Column(name="evfolyamJel", type="string", length=32, nullable=true)
     */
    private $evfolyamjel;

    /**
     * @var boolean
     *
     * @ORM\Column(name="felev", type="boolean", nullable=true)
     */
    private $felev;

    /**
     * @var string
     *
     * @ORM\Column(name="tipus", type="string", nullable=true)
     */
    private $tipus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="jelentkezesDt", type="date", nullable=false)
     */
    private $jelentkezesdt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="vizsgaDt", type="date", nullable=true)
     */
    private $vizsgadt;

    /**
     * @var integer
     *
     * @ORM\Column(name="vizsgaId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $vizsgaid;

    /**
     * @var \AppBundle\Entity\Zarojegy
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Zarojegy")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="zaroJegyId", referencedColumnName="zaroJegyId")
     * })
     */
    private $zarojegyid;

    /**
     * @var \AppBundle\Entity\Zaradek
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Zaradek")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="zaradekId", referencedColumnName="zaradekId")
     * })
     */
    private $zaradekid;

    /**
     * @var \AppBundle\Entity\Targy
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Targy")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="targyId", referencedColumnName="targyId")
     * })
     */
    private $targyid;

    /**
     * @var \AppBundle\Entity\Diak
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Diak")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="diakId", referencedColumnName="diakId")
     * })
     */
    private $diakid;



    /**
     * Set evfolyam
     *
     * @param boolean $evfolyam
     *
     * @return Vizsga
     */
    public function setEvfolyam($evfolyam)
    {
        $this->evfolyam = $evfolyam;

        return $this;
    }

    /**
     * Get evfolyam
     *
     * @return boolean
     */
    public function getEvfolyam()
    {
        return $this->evfolyam;
    }

    /**
     * Set evfolyamjel
     *
     * @param string $evfolyamjel
     *
     * @return Vizsga
     */
    public function setEvfolyamjel($evfolyamjel)
    {
        $this->evfolyamjel = $evfolyamjel;

        return $this;
    }

    /**
     * Get evfolyamjel
     *
     * @return string
     */
    public function getEvfolyamjel()
    {
        return $this->evfolyamjel;
    }

    /**
     * Set felev
     *
     * @param boolean $felev
     *
     * @return Vizsga
     */
    public function setFelev($felev)
    {
        $this->felev = $felev;

        return $this;
    }

    /**
     * Get felev
     *
     * @return boolean
     */
    public function getFelev()
    {
        return $this->felev;
    }

    /**
     * Set tipus
     *
     * @param string $tipus
     *
     * @return Vizsga
     */
    public function setTipus($tipus)
    {
        $this->tipus = $tipus;

        return $this;
    }

    /**
     * Get tipus
     *
     * @return string
     */
    public function getTipus()
    {
        return $this->tipus;
    }

    /**
     * Set jelentkezesdt
     *
     * @param \DateTime $jelentkezesdt
     *
     * @return Vizsga
     */
    public function setJelentkezesdt($jelentkezesdt)
    {
        $this->jelentkezesdt = $jelentkezesdt;

        return $this;
    }

    /**
     * Get jelentkezesdt
     *
     * @return \DateTime
     */
    public function getJelentkezesdt()
    {
        return $this->jelentkezesdt;
    }

    /**
     * Set vizsgadt
     *
     * @param \DateTime $vizsgadt
     *
     * @return Vizsga
     */
    public function setVizsgadt($vizsgadt)
    {
        $this->vizsgadt = $vizsgadt;

        return $this;
    }

    /**
     * Get vizsgadt
     *
     * @return \DateTime
     */
    public function getVizsgadt()
    {
        return $this->vizsgadt;
    }

    /**
     * Get vizsgaid
     *
     * @return integer
     */
    public function getVizsgaid()
    {
        return $this->vizsgaid;
    }

    /**
     * Set zarojegyid
     *
     * @param \AppBundle\Entity\Zarojegy $zarojegyid
     *
     * @return Vizsga
     */
    public function setZarojegyid(\AppBundle\Entity\Zarojegy $zarojegyid = null)
    {
        $this->zarojegyid = $zarojegyid;

        return $this;
    }

    /**
     * Get zarojegyid
     *
     * @return \AppBundle\Entity\Zarojegy
     */
    public function getZarojegyid()
    {
        return $this->zarojegyid;
    }

    /**
     * Set zaradekid
     *
     * @param \AppBundle\Entity\Zaradek $zaradekid
     *
     * @return Vizsga
     */
    public function setZaradekid(\AppBundle\Entity\Zaradek $zaradekid = null)
    {
        $this->zaradekid = $zaradekid;

        return $this;
    }

    /**
     * Get zaradekid
     *
     * @return \AppBundle\Entity\Zaradek
     */
    public function getZaradekid()
    {
        return $this->zaradekid;
    }

    /**
     * Set targyid
     *
     * @param \AppBundle\Entity\Targy $targyid
     *
     * @return Vizsga
     */
    public function setTargyid(\AppBundle\Entity\Targy $targyid = null)
    {
        $this->targyid = $targyid;

        return $this;
    }

    /**
     * Get targyid
     *
     * @return \AppBundle\Entity\Targy
     */
    public function getTargyid()
    {
        return $this->targyid;
    }

    /**
     * Set diakid
     *
     * @param \AppBundle\Entity\Diak $diakid
     *
     * @return Vizsga
     */
    public function setDiakid(\AppBundle\Entity\Diak $diakid = null)
    {
        $this->diakid = $diakid;

        return $this;
    }

    /**
     * Get diakid
     *
     * @return \AppBundle\Entity\Diak
     */
    public function getDiakid()
    {
        return $this->diakid;
    }
}
