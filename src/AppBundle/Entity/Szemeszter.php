<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Szemeszter
 *
 * @ORM\Table(name="szemeszter", uniqueConstraints={@ORM\UniqueConstraint(name="szemeszter_uniq", columns={"szemeszterId"})})
 * @ORM\Entity
 */
class Szemeszter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="szemeszterId", type="smallint", nullable=false)
     */
    private $szemeszterid;

    /**
     * @var string
     *
     * @ORM\Column(name="statusz", type="string", nullable=true)
     */
    private $statusz = 'tervezett';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="kezdesDt", type="date", nullable=true)
     */
    private $kezdesdt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="zarasDt", type="date", nullable=true)
     */
    private $zarasdt;

    /**
     * @var integer
     *
     * @ORM\Column(name="tanev", type="smallint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $tanev;

    /**
     * @var boolean
     *
     * @ORM\Column(name="szemeszter", type="boolean")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $szemeszter;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Tankor", inversedBy="tanev")
     * @ORM\JoinTable(name="tankorszemeszter",
     *   joinColumns={
     *     @ORM\JoinColumn(name="tanev", referencedColumnName="tanev"),
     *     @ORM\JoinColumn(name="szemeszter", referencedColumnName="szemeszter")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="tankorId", referencedColumnName="tankorId")
     *   }
     * )
     */
    private $tankorid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Diak", inversedBy="tanev")
     * @ORM\JoinTable(name="hianyzasosszesites",
     *   joinColumns={
     *     @ORM\JoinColumn(name="tanev", referencedColumnName="tanev"),
     *     @ORM\JoinColumn(name="szemeszter", referencedColumnName="szemeszter")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="diakId", referencedColumnName="diakId")
     *   }
     * )
     */
    private $diakid;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tankorid = new \Doctrine\Common\Collections\ArrayCollection();
        $this->diakid = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set szemeszterid
     *
     * @param integer $szemeszterid
     *
     * @return Szemeszter
     */
    public function setSzemeszterid($szemeszterid)
    {
        $this->szemeszterid = $szemeszterid;

        return $this;
    }

    /**
     * Get szemeszterid
     *
     * @return integer
     */
    public function getSzemeszterid()
    {
        return $this->szemeszterid;
    }

    /**
     * Set statusz
     *
     * @param string $statusz
     *
     * @return Szemeszter
     */
    public function setStatusz($statusz)
    {
        $this->statusz = $statusz;

        return $this;
    }

    /**
     * Get statusz
     *
     * @return string
     */
    public function getStatusz()
    {
        return $this->statusz;
    }

    /**
     * Set kezdesdt
     *
     * @param \DateTime $kezdesdt
     *
     * @return Szemeszter
     */
    public function setKezdesdt($kezdesdt)
    {
        $this->kezdesdt = $kezdesdt;

        return $this;
    }

    /**
     * Get kezdesdt
     *
     * @return \DateTime
     */
    public function getKezdesdt()
    {
        return $this->kezdesdt;
    }

    /**
     * Set zarasdt
     *
     * @param \DateTime $zarasdt
     *
     * @return Szemeszter
     */
    public function setZarasdt($zarasdt)
    {
        $this->zarasdt = $zarasdt;

        return $this;
    }

    /**
     * Get zarasdt
     *
     * @return \DateTime
     */
    public function getZarasdt()
    {
        return $this->zarasdt;
    }

    /**
     * Set tanev
     *
     * @param integer $tanev
     *
     * @return Szemeszter
     */
    public function setTanev($tanev)
    {
        $this->tanev = $tanev;

        return $this;
    }

    /**
     * Get tanev
     *
     * @return integer
     */
    public function getTanev()
    {
        return $this->tanev;
    }

    /**
     * Set szemeszter
     *
     * @param boolean $szemeszter
     *
     * @return Szemeszter
     */
    public function setSzemeszter($szemeszter)
    {
        $this->szemeszter = $szemeszter;

        return $this;
    }

    /**
     * Get szemeszter
     *
     * @return boolean
     */
    public function getSzemeszter()
    {
        return $this->szemeszter;
    }

    /**
     * Add tankorid
     *
     * @param \AppBundle\Entity\Tankor $tankorid
     *
     * @return Szemeszter
     */
    public function addTankorid(\AppBundle\Entity\Tankor $tankorid)
    {
        $this->tankorid[] = $tankorid;

        return $this;
    }

    /**
     * Remove tankorid
     *
     * @param \AppBundle\Entity\Tankor $tankorid
     */
    public function removeTankorid(\AppBundle\Entity\Tankor $tankorid)
    {
        $this->tankorid->removeElement($tankorid);
    }

    /**
     * Get tankorid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTankorid()
    {
        return $this->tankorid;
    }

    /**
     * Add diakid
     *
     * @param \AppBundle\Entity\Diak $diakid
     *
     * @return Szemeszter
     */
    public function addDiakid(\AppBundle\Entity\Diak $diakid)
    {
        $this->diakid[] = $diakid;

        return $this;
    }

    /**
     * Remove diakid
     *
     * @param \AppBundle\Entity\Diak $diakid
     */
    public function removeDiakid(\AppBundle\Entity\Diak $diakid)
    {
        $this->diakid->removeElement($diakid);
    }

    /**
     * Get diakid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDiakid()
    {
        return $this->diakid;
    }
}
