<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tankortanar
 *
 * @ORM\Table(name="tankorTanar", indexes={@ORM\Index(name="tankorTanar_FKIndex1", columns={"tankorId"}), @ORM\Index(name="tankorTanar_FKIndex2", columns={"tanarId"})})
 * @ORM\Entity
 */
class Tankortanar
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="kiDt", type="date", nullable=true)
     */
    private $kidt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="beDt", type="date")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $bedt;

    /**
     * @var \AppBundle\Entity\Tanar
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Tanar")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tanarId", referencedColumnName="tanarId")
     * })
     */
    private $tanarid;

    /**
     * @var \AppBundle\Entity\Tankor
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Tankor")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tankorId", referencedColumnName="tankorId")
     * })
     */
    private $tankorid;



    /**
     * Set kidt
     *
     * @param \DateTime $kidt
     *
     * @return Tankortanar
     */
    public function setKidt($kidt)
    {
        $this->kidt = $kidt;

        return $this;
    }

    /**
     * Get kidt
     *
     * @return \DateTime
     */
    public function getKidt()
    {
        return $this->kidt;
    }

    /**
     * Set bedt
     *
     * @param \DateTime $bedt
     *
     * @return Tankortanar
     */
    public function setBedt($bedt)
    {
        $this->bedt = $bedt;

        return $this;
    }

    /**
     * Get bedt
     *
     * @return \DateTime
     */
    public function getBedt()
    {
        return $this->bedt;
    }

    /**
     * Set tanarid
     *
     * @param \AppBundle\Entity\Tanar $tanarid
     *
     * @return Tankortanar
     */
    public function setTanarid(\AppBundle\Entity\Tanar $tanarid)
    {
        $this->tanarid = $tanarid;

        return $this;
    }

    /**
     * Get tanarid
     *
     * @return \AppBundle\Entity\Tanar
     */
    public function getTanarid()
    {
        return $this->tanarid;
    }

    /**
     * Set tankorid
     *
     * @param \AppBundle\Entity\Tankor $tankorid
     *
     * @return Tankortanar
     */
    public function setTankorid(\AppBundle\Entity\Tankor $tankorid)
    {
        $this->tankorid = $tankorid;

        return $this;
    }

    /**
     * Get tankorid
     *
     * @return \AppBundle\Entity\Tankor
     */
    public function getTankorid()
    {
        return $this->tankorid;
    }
}
