<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Kirtargy
 *
 * @ORM\Table(name="kirTargy")
 * @ORM\Entity
 */
class Kirtargy
{
    /**
     * @var string
     *
     * @ORM\Column(name="kirTargyNev", type="string", length=255, nullable=true)
     */
    private $kirtargynev;

    /**
     * @var integer
     *
     * @ORM\Column(name="kirTargyId", type="smallint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $kirtargyid;



    /**
     * Set kirtargynev
     *
     * @param string $kirtargynev
     *
     * @return Kirtargy
     */
    public function setKirtargynev($kirtargynev)
    {
        $this->kirtargynev = $kirtargynev;

        return $this;
    }

    /**
     * Get kirtargynev
     *
     * @return string
     */
    public function getKirtargynev()
    {
        return $this->kirtargynev;
    }

    /**
     * Get kirtargyid
     *
     * @return integer
     */
    public function getKirtargyid()
    {
        return $this->kirtargyid;
    }
}
