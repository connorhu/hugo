<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Telephely
 *
 * @ORM\Table(name="telephely")
 * @ORM\Entity
 */
class Telephely
{
    /**
     * @var string
     *
     * @ORM\Column(name="telephelyRovidNev", type="string", length=16, nullable=false)
     */
    private $telephelyrovidnev;

    /**
     * @var string
     *
     * @ORM\Column(name="telephelyNev", type="string", length=128, nullable=false)
     */
    private $telephelynev;

    /**
     * @var boolean
     *
     * @ORM\Column(name="alapertelmezett", type="boolean", nullable=false)
     */
    private $alapertelmezett = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="cimHelyseg", type="string", length=32, nullable=true)
     */
    private $cimhelyseg;

    /**
     * @var string
     *
     * @ORM\Column(name="cimIrsz", type="string", length=8, nullable=true)
     */
    private $cimirsz;

    /**
     * @var string
     *
     * @ORM\Column(name="cimKozteruletNev", type="string", length=32, nullable=true)
     */
    private $cimkozteruletnev;

    /**
     * @var string
     *
     * @ORM\Column(name="cimKozteruletJelleg", type="string", nullable=true)
     */
    private $cimkozteruletjelleg;

    /**
     * @var string
     *
     * @ORM\Column(name="cimHazszam", type="string", length=20, nullable=true)
     */
    private $cimhazszam;

    /**
     * @var string
     *
     * @ORM\Column(name="telefon", type="string", length=64, nullable=true)
     */
    private $telefon;

    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=64, nullable=true)
     */
    private $fax;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=96, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="honlap", type="string", length=96, nullable=true)
     */
    private $honlap;

    /**
     * @var boolean
     *
     * @ORM\Column(name="telephelyId", type="boolean")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $telephelyid;



    /**
     * Set telephelyrovidnev
     *
     * @param string $telephelyrovidnev
     *
     * @return Telephely
     */
    public function setTelephelyrovidnev($telephelyrovidnev)
    {
        $this->telephelyrovidnev = $telephelyrovidnev;

        return $this;
    }

    /**
     * Get telephelyrovidnev
     *
     * @return string
     */
    public function getTelephelyrovidnev()
    {
        return $this->telephelyrovidnev;
    }

    /**
     * Set telephelynev
     *
     * @param string $telephelynev
     *
     * @return Telephely
     */
    public function setTelephelynev($telephelynev)
    {
        $this->telephelynev = $telephelynev;

        return $this;
    }

    /**
     * Get telephelynev
     *
     * @return string
     */
    public function getTelephelynev()
    {
        return $this->telephelynev;
    }

    /**
     * Set alapertelmezett
     *
     * @param boolean $alapertelmezett
     *
     * @return Telephely
     */
    public function setAlapertelmezett($alapertelmezett)
    {
        $this->alapertelmezett = $alapertelmezett;

        return $this;
    }

    /**
     * Get alapertelmezett
     *
     * @return boolean
     */
    public function getAlapertelmezett()
    {
        return $this->alapertelmezett;
    }

    /**
     * Set cimhelyseg
     *
     * @param string $cimhelyseg
     *
     * @return Telephely
     */
    public function setCimhelyseg($cimhelyseg)
    {
        $this->cimhelyseg = $cimhelyseg;

        return $this;
    }

    /**
     * Get cimhelyseg
     *
     * @return string
     */
    public function getCimhelyseg()
    {
        return $this->cimhelyseg;
    }

    /**
     * Set cimirsz
     *
     * @param string $cimirsz
     *
     * @return Telephely
     */
    public function setCimirsz($cimirsz)
    {
        $this->cimirsz = $cimirsz;

        return $this;
    }

    /**
     * Get cimirsz
     *
     * @return string
     */
    public function getCimirsz()
    {
        return $this->cimirsz;
    }

    /**
     * Set cimkozteruletnev
     *
     * @param string $cimkozteruletnev
     *
     * @return Telephely
     */
    public function setCimkozteruletnev($cimkozteruletnev)
    {
        $this->cimkozteruletnev = $cimkozteruletnev;

        return $this;
    }

    /**
     * Get cimkozteruletnev
     *
     * @return string
     */
    public function getCimkozteruletnev()
    {
        return $this->cimkozteruletnev;
    }

    /**
     * Set cimkozteruletjelleg
     *
     * @param string $cimkozteruletjelleg
     *
     * @return Telephely
     */
    public function setCimkozteruletjelleg($cimkozteruletjelleg)
    {
        $this->cimkozteruletjelleg = $cimkozteruletjelleg;

        return $this;
    }

    /**
     * Get cimkozteruletjelleg
     *
     * @return string
     */
    public function getCimkozteruletjelleg()
    {
        return $this->cimkozteruletjelleg;
    }

    /**
     * Set cimhazszam
     *
     * @param string $cimhazszam
     *
     * @return Telephely
     */
    public function setCimhazszam($cimhazszam)
    {
        $this->cimhazszam = $cimhazszam;

        return $this;
    }

    /**
     * Get cimhazszam
     *
     * @return string
     */
    public function getCimhazszam()
    {
        return $this->cimhazszam;
    }

    /**
     * Set telefon
     *
     * @param string $telefon
     *
     * @return Telephely
     */
    public function setTelefon($telefon)
    {
        $this->telefon = $telefon;

        return $this;
    }

    /**
     * Get telefon
     *
     * @return string
     */
    public function getTelefon()
    {
        return $this->telefon;
    }

    /**
     * Set fax
     *
     * @param string $fax
     *
     * @return Telephely
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Telephely
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set honlap
     *
     * @param string $honlap
     *
     * @return Telephely
     */
    public function setHonlap($honlap)
    {
        $this->honlap = $honlap;

        return $this;
    }

    /**
     * Get honlap
     *
     * @return string
     */
    public function getHonlap()
    {
        return $this->honlap;
    }

    /**
     * Get telephelyid
     *
     * @return boolean
     */
    public function getTelephelyid()
    {
        return $this->telephelyid;
    }
}
