<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tankortipus
 *
 * @ORM\Table(name="tankorTipus", indexes={@ORM\Index(name="rovidNev", columns={"rovidNev"})})
 * @ORM\Entity
 */
class Tankortipus
{
    /**
     * @var string
     *
     * @ORM\Column(name="oratervi", type="string", nullable=true)
     */
    private $oratervi = 'óratervi';

    /**
     * @var string
     *
     * @ORM\Column(name="rovidNev", type="string", length=30, nullable=false)
     */
    private $rovidnev;

    /**
     * @var string
     *
     * @ORM\Column(name="leiras", type="string", length=255, nullable=false)
     */
    private $leiras;

    /**
     * @var string
     *
     * @ORM\Column(name="jelenlet", type="string", nullable=false)
     */
    private $jelenlet;

    /**
     * @var string
     *
     * @ORM\Column(name="regisztralando", type="string", nullable=true)
     */
    private $regisztralando;

    /**
     * @var string
     *
     * @ORM\Column(name="hianyzasBeleszamit", type="string", nullable=true)
     */
    private $hianyzasbeleszamit;

    /**
     * @var string
     *
     * @ORM\Column(name="jelleg", type="string", nullable=true)
     */
    private $jelleg = 'elmélet';

    /**
     * @var string
     *
     * @ORM\Column(name="tankorJel", type="string", length=3, nullable=true)
     */
    private $tankorjel;

    /**
     * @var integer
     *
     * @ORM\Column(name="tankorTipusId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tankortipusid;



    /**
     * Set oratervi
     *
     * @param string $oratervi
     *
     * @return Tankortipus
     */
    public function setOratervi($oratervi)
    {
        $this->oratervi = $oratervi;

        return $this;
    }

    /**
     * Get oratervi
     *
     * @return string
     */
    public function getOratervi()
    {
        return $this->oratervi;
    }

    /**
     * Set rovidnev
     *
     * @param string $rovidnev
     *
     * @return Tankortipus
     */
    public function setRovidnev($rovidnev)
    {
        $this->rovidnev = $rovidnev;

        return $this;
    }

    /**
     * Get rovidnev
     *
     * @return string
     */
    public function getRovidnev()
    {
        return $this->rovidnev;
    }

    /**
     * Set leiras
     *
     * @param string $leiras
     *
     * @return Tankortipus
     */
    public function setLeiras($leiras)
    {
        $this->leiras = $leiras;

        return $this;
    }

    /**
     * Get leiras
     *
     * @return string
     */
    public function getLeiras()
    {
        return $this->leiras;
    }

    /**
     * Set jelenlet
     *
     * @param string $jelenlet
     *
     * @return Tankortipus
     */
    public function setJelenlet($jelenlet)
    {
        $this->jelenlet = $jelenlet;

        return $this;
    }

    /**
     * Get jelenlet
     *
     * @return string
     */
    public function getJelenlet()
    {
        return $this->jelenlet;
    }

    /**
     * Set regisztralando
     *
     * @param string $regisztralando
     *
     * @return Tankortipus
     */
    public function setRegisztralando($regisztralando)
    {
        $this->regisztralando = $regisztralando;

        return $this;
    }

    /**
     * Get regisztralando
     *
     * @return string
     */
    public function getRegisztralando()
    {
        return $this->regisztralando;
    }

    /**
     * Set hianyzasbeleszamit
     *
     * @param string $hianyzasbeleszamit
     *
     * @return Tankortipus
     */
    public function setHianyzasbeleszamit($hianyzasbeleszamit)
    {
        $this->hianyzasbeleszamit = $hianyzasbeleszamit;

        return $this;
    }

    /**
     * Get hianyzasbeleszamit
     *
     * @return string
     */
    public function getHianyzasbeleszamit()
    {
        return $this->hianyzasbeleszamit;
    }

    /**
     * Set jelleg
     *
     * @param string $jelleg
     *
     * @return Tankortipus
     */
    public function setJelleg($jelleg)
    {
        $this->jelleg = $jelleg;

        return $this;
    }

    /**
     * Get jelleg
     *
     * @return string
     */
    public function getJelleg()
    {
        return $this->jelleg;
    }

    /**
     * Set tankorjel
     *
     * @param string $tankorjel
     *
     * @return Tankortipus
     */
    public function setTankorjel($tankorjel)
    {
        $this->tankorjel = $tankorjel;

        return $this;
    }

    /**
     * Get tankorjel
     *
     * @return string
     */
    public function getTankorjel()
    {
        return $this->tankorjel;
    }

    /**
     * Get tankortipusid
     *
     * @return integer
     */
    public function getTankortipusid()
    {
        return $this->tankortipusid;
    }
}
