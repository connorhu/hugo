<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Osztalytanar
 *
 * @ORM\Table(name="osztalyTanar", indexes={@ORM\Index(name="osztalyTanar_FKIndex1", columns={"osztalyId"}), @ORM\Index(name="osztalyTanar_FKIndex2", columns={"tanarId"})})
 * @ORM\Entity
 */
class Osztalytanar
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="kiDt", type="date", nullable=true)
     */
    private $kidt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="beDt", type="date")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $bedt;

    /**
     * @var \AppBundle\Entity\Tanar
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Tanar")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tanarId", referencedColumnName="tanarId")
     * })
     */
    private $tanarid;

    /**
     * @var \AppBundle\Entity\Osztaly
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Osztaly")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="osztalyId", referencedColumnName="osztalyId")
     * })
     */
    private $osztalyid;



    /**
     * Set kidt
     *
     * @param \DateTime $kidt
     *
     * @return Osztalytanar
     */
    public function setKidt($kidt)
    {
        $this->kidt = $kidt;

        return $this;
    }

    /**
     * Get kidt
     *
     * @return \DateTime
     */
    public function getKidt()
    {
        return $this->kidt;
    }

    /**
     * Set bedt
     *
     * @param \DateTime $bedt
     *
     * @return Osztalytanar
     */
    public function setBedt($bedt)
    {
        $this->bedt = $bedt;

        return $this;
    }

    /**
     * Get bedt
     *
     * @return \DateTime
     */
    public function getBedt()
    {
        return $this->bedt;
    }

    /**
     * Set tanarid
     *
     * @param \AppBundle\Entity\Tanar $tanarid
     *
     * @return Osztalytanar
     */
    public function setTanarid(\AppBundle\Entity\Tanar $tanarid)
    {
        $this->tanarid = $tanarid;

        return $this;
    }

    /**
     * Get tanarid
     *
     * @return \AppBundle\Entity\Tanar
     */
    public function getTanarid()
    {
        return $this->tanarid;
    }

    /**
     * Set osztalyid
     *
     * @param \AppBundle\Entity\Osztaly $osztalyid
     *
     * @return Osztalytanar
     */
    public function setOsztalyid(\AppBundle\Entity\Osztaly $osztalyid)
    {
        $this->osztalyid = $osztalyid;

        return $this;
    }

    /**
     * Get osztalyid
     *
     * @return \AppBundle\Entity\Osztaly
     */
    public function getOsztalyid()
    {
        return $this->osztalyid;
    }
}
