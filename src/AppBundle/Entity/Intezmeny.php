<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Intezmeny
 *
 * @ORM\Table(name="intezmeny")
 * @ORM\Entity
 */
class Intezmeny
{
    /**
     * @var string
     *
     * @ORM\Column(name="rovidNev", type="string", length=16, nullable=false)
     */
    private $rovidnev;

    /**
     * @var string
     *
     * @ORM\Column(name="nev", type="string", length=128, nullable=false)
     */
    private $nev;

    /**
     * @var boolean
     *
     * @ORM\Column(name="alapertelmezett", type="boolean", nullable=false)
     */
    private $alapertelmezett = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="telefon", type="string", length=64, nullable=true)
     */
    private $telefon;

    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=64, nullable=true)
     */
    private $fax;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=96, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="honlap", type="string", length=96, nullable=true)
     */
    private $honlap;

    /**
     * @var string
     *
     * @ORM\Column(name="fenntarto", type="string", nullable=true)
     */
    private $fenntarto = 'állami';

    /**
     * @var integer
     *
     * @ORM\Column(name="OMKod", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $omkod;



    /**
     * Set rovidnev
     *
     * @param string $rovidnev
     *
     * @return Intezmeny
     */
    public function setRovidnev($rovidnev)
    {
        $this->rovidnev = $rovidnev;

        return $this;
    }

    /**
     * Get rovidnev
     *
     * @return string
     */
    public function getRovidnev()
    {
        return $this->rovidnev;
    }

    /**
     * Set nev
     *
     * @param string $nev
     *
     * @return Intezmeny
     */
    public function setNev($nev)
    {
        $this->nev = $nev;

        return $this;
    }

    /**
     * Get nev
     *
     * @return string
     */
    public function getNev()
    {
        return $this->nev;
    }

    /**
     * Set alapertelmezett
     *
     * @param boolean $alapertelmezett
     *
     * @return Intezmeny
     */
    public function setAlapertelmezett($alapertelmezett)
    {
        $this->alapertelmezett = $alapertelmezett;

        return $this;
    }

    /**
     * Get alapertelmezett
     *
     * @return boolean
     */
    public function getAlapertelmezett()
    {
        return $this->alapertelmezett;
    }

    /**
     * Set telefon
     *
     * @param string $telefon
     *
     * @return Intezmeny
     */
    public function setTelefon($telefon)
    {
        $this->telefon = $telefon;

        return $this;
    }

    /**
     * Get telefon
     *
     * @return string
     */
    public function getTelefon()
    {
        return $this->telefon;
    }

    /**
     * Set fax
     *
     * @param string $fax
     *
     * @return Intezmeny
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Intezmeny
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set honlap
     *
     * @param string $honlap
     *
     * @return Intezmeny
     */
    public function setHonlap($honlap)
    {
        $this->honlap = $honlap;

        return $this;
    }

    /**
     * Get honlap
     *
     * @return string
     */
    public function getHonlap()
    {
        return $this->honlap;
    }

    /**
     * Set fenntarto
     *
     * @param string $fenntarto
     *
     * @return Intezmeny
     */
    public function setFenntarto($fenntarto)
    {
        $this->fenntarto = $fenntarto;

        return $this;
    }

    /**
     * Get fenntarto
     *
     * @return string
     */
    public function getFenntarto()
    {
        return $this->fenntarto;
    }

    /**
     * Get omkod
     *
     * @return integer
     */
    public function getOmkod()
    {
        return $this->omkod;
    }
}
