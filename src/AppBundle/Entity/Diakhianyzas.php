<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Diakhianyzas
 *
 * @ORM\Table(name="diakHianyzas", indexes={@ORM\Index(name="diakHianyzas_FKIndex1", columns={"diakId"})})
 * @ORM\Entity
 */
class Diakhianyzas
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt", type="date", nullable=false)
     */
    private $dt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="igazolt", type="boolean", nullable=true)
     */
    private $igazolt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="igazolatlan", type="boolean", nullable=true)
     */
    private $igazolatlan;

    /**
     * @var boolean
     *
     * @ORM\Column(name="beszamit", type="boolean", nullable=true)
     */
    private $beszamit;

    /**
     * @var string
     *
     * @ORM\Column(name="megjegyzes", type="text", length=255, nullable=true)
     */
    private $megjegyzes;

    /**
     * @var integer
     *
     * @ORM\Column(name="diakHianyzasId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $diakhianyzasid;

    /**
     * @var \AppBundle\Entity\Diak
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Diak")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="diakId", referencedColumnName="diakId")
     * })
     */
    private $diakid;



    /**
     * Set dt
     *
     * @param \DateTime $dt
     *
     * @return Diakhianyzas
     */
    public function setDt($dt)
    {
        $this->dt = $dt;

        return $this;
    }

    /**
     * Get dt
     *
     * @return \DateTime
     */
    public function getDt()
    {
        return $this->dt;
    }

    /**
     * Set igazolt
     *
     * @param boolean $igazolt
     *
     * @return Diakhianyzas
     */
    public function setIgazolt($igazolt)
    {
        $this->igazolt = $igazolt;

        return $this;
    }

    /**
     * Get igazolt
     *
     * @return boolean
     */
    public function getIgazolt()
    {
        return $this->igazolt;
    }

    /**
     * Set igazolatlan
     *
     * @param boolean $igazolatlan
     *
     * @return Diakhianyzas
     */
    public function setIgazolatlan($igazolatlan)
    {
        $this->igazolatlan = $igazolatlan;

        return $this;
    }

    /**
     * Get igazolatlan
     *
     * @return boolean
     */
    public function getIgazolatlan()
    {
        return $this->igazolatlan;
    }

    /**
     * Set beszamit
     *
     * @param boolean $beszamit
     *
     * @return Diakhianyzas
     */
    public function setBeszamit($beszamit)
    {
        $this->beszamit = $beszamit;

        return $this;
    }

    /**
     * Get beszamit
     *
     * @return boolean
     */
    public function getBeszamit()
    {
        return $this->beszamit;
    }

    /**
     * Set megjegyzes
     *
     * @param string $megjegyzes
     *
     * @return Diakhianyzas
     */
    public function setMegjegyzes($megjegyzes)
    {
        $this->megjegyzes = $megjegyzes;

        return $this;
    }

    /**
     * Get megjegyzes
     *
     * @return string
     */
    public function getMegjegyzes()
    {
        return $this->megjegyzes;
    }

    /**
     * Get diakhianyzasid
     *
     * @return integer
     */
    public function getDiakhianyzasid()
    {
        return $this->diakhianyzasid;
    }

    /**
     * Set diakid
     *
     * @param \AppBundle\Entity\Diak $diakid
     *
     * @return Diakhianyzas
     */
    public function setDiakid(\AppBundle\Entity\Diak $diakid = null)
    {
        $this->diakid = $diakid;

        return $this;
    }

    /**
     * Get diakid
     *
     * @return \AppBundle\Entity\Diak
     */
    public function getDiakid()
    {
        return $this->diakid;
    }
}
