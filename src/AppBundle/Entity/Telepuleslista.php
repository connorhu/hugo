<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Telepuleslista
 *
 * @ORM\Table(name="telepulesLista", indexes={@ORM\Index(name="idx_telepulesirsz", columns={"irsz"})})
 * @ORM\Entity
 */
class Telepuleslista
{
    /**
     * @var string
     *
     * @ORM\Column(name="telepules", type="string", length=60, nullable=false)
     */
    private $telepules;

    /**
     * @var string
     *
     * @ORM\Column(name="megye", type="string", length=60, nullable=false)
     */
    private $megye;

    /**
     * @var string
     *
     * @ORM\Column(name="irsz", type="string", length=4)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $irsz;



    /**
     * Set telepules
     *
     * @param string $telepules
     *
     * @return Telepuleslista
     */
    public function setTelepules($telepules)
    {
        $this->telepules = $telepules;

        return $this;
    }

    /**
     * Get telepules
     *
     * @return string
     */
    public function getTelepules()
    {
        return $this->telepules;
    }

    /**
     * Set megye
     *
     * @param string $megye
     *
     * @return Telepuleslista
     */
    public function setMegye($megye)
    {
        $this->megye = $megye;

        return $this;
    }

    /**
     * Get megye
     *
     * @return string
     */
    public function getMegye()
    {
        return $this->megye;
    }

    /**
     * Get irsz
     *
     * @return string
     */
    public function getIrsz()
    {
        return $this->irsz;
    }
}
