<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Kepesites
 *
 * @ORM\Table(name="kepesites")
 * @ORM\Entity
 */
class Kepesites
{
    /**
     * @var string
     *
     * @ORM\Column(name="vegzettseg", type="string", nullable=true)
     */
    private $vegzettseg;

    /**
     * @var string
     *
     * @ORM\Column(name="fokozat", type="string", nullable=true)
     */
    private $fokozat;

    /**
     * @var string
     *
     * @ORM\Column(name="specializacio", type="string", nullable=true)
     */
    private $specializacio;

    /**
     * @var string
     *
     * @ORM\Column(name="kepesitesNev", type="string", length=255, nullable=true)
     */
    private $kepesitesnev;

    /**
     * @var integer
     *
     * @ORM\Column(name="kepesitesId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $kepesitesid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Tanar", mappedBy="kepesitesid")
     */
    private $tanarid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Targy", inversedBy="kepesitesid")
     * @ORM\JoinTable(name="kepesitestargy",
     *   joinColumns={
     *     @ORM\JoinColumn(name="kepesitesId", referencedColumnName="kepesitesId")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="targyId", referencedColumnName="targyId")
     *   }
     * )
     */
    private $targyid;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tanarid = new \Doctrine\Common\Collections\ArrayCollection();
        $this->targyid = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set vegzettseg
     *
     * @param string $vegzettseg
     *
     * @return Kepesites
     */
    public function setVegzettseg($vegzettseg)
    {
        $this->vegzettseg = $vegzettseg;

        return $this;
    }

    /**
     * Get vegzettseg
     *
     * @return string
     */
    public function getVegzettseg()
    {
        return $this->vegzettseg;
    }

    /**
     * Set fokozat
     *
     * @param string $fokozat
     *
     * @return Kepesites
     */
    public function setFokozat($fokozat)
    {
        $this->fokozat = $fokozat;

        return $this;
    }

    /**
     * Get fokozat
     *
     * @return string
     */
    public function getFokozat()
    {
        return $this->fokozat;
    }

    /**
     * Set specializacio
     *
     * @param string $specializacio
     *
     * @return Kepesites
     */
    public function setSpecializacio($specializacio)
    {
        $this->specializacio = $specializacio;

        return $this;
    }

    /**
     * Get specializacio
     *
     * @return string
     */
    public function getSpecializacio()
    {
        return $this->specializacio;
    }

    /**
     * Set kepesitesnev
     *
     * @param string $kepesitesnev
     *
     * @return Kepesites
     */
    public function setKepesitesnev($kepesitesnev)
    {
        $this->kepesitesnev = $kepesitesnev;

        return $this;
    }

    /**
     * Get kepesitesnev
     *
     * @return string
     */
    public function getKepesitesnev()
    {
        return $this->kepesitesnev;
    }

    /**
     * Get kepesitesid
     *
     * @return integer
     */
    public function getKepesitesid()
    {
        return $this->kepesitesid;
    }

    /**
     * Add tanarid
     *
     * @param \AppBundle\Entity\Tanar $tanarid
     *
     * @return Kepesites
     */
    public function addTanarid(\AppBundle\Entity\Tanar $tanarid)
    {
        $this->tanarid[] = $tanarid;

        return $this;
    }

    /**
     * Remove tanarid
     *
     * @param \AppBundle\Entity\Tanar $tanarid
     */
    public function removeTanarid(\AppBundle\Entity\Tanar $tanarid)
    {
        $this->tanarid->removeElement($tanarid);
    }

    /**
     * Get tanarid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTanarid()
    {
        return $this->tanarid;
    }

    /**
     * Add targyid
     *
     * @param \AppBundle\Entity\Targy $targyid
     *
     * @return Kepesites
     */
    public function addTargyid(\AppBundle\Entity\Targy $targyid)
    {
        $this->targyid[] = $targyid;

        return $this;
    }

    /**
     * Remove targyid
     *
     * @param \AppBundle\Entity\Targy $targyid
     */
    public function removeTargyid(\AppBundle\Entity\Targy $targyid)
    {
        $this->targyid->removeElement($targyid);
    }

    /**
     * Get targyid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTargyid()
    {
        return $this->targyid;
    }
}
