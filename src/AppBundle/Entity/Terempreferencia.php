<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Terempreferencia
 *
 * @ORM\Table(name="teremPreferencia", indexes={@ORM\Index(name="teremPref_FKIndex1", columns={"tanarId"}), @ORM\Index(name="teremPref_FKIndex2", columns={"targyId"})})
 * @ORM\Entity
 */
class Terempreferencia
{
    /**
     * @var string
     *
     * @ORM\Column(name="teremStr", type="string", length=255, nullable=false)
     */
    private $teremstr = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="teremPreferenciaId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $terempreferenciaid;

    /**
     * @var \AppBundle\Entity\Targy
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Targy")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="targyId", referencedColumnName="targyId")
     * })
     */
    private $targyid;

    /**
     * @var \AppBundle\Entity\Tanar
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Tanar")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tanarId", referencedColumnName="tanarId")
     * })
     */
    private $tanarid;



    /**
     * Set teremstr
     *
     * @param string $teremstr
     *
     * @return Terempreferencia
     */
    public function setTeremstr($teremstr)
    {
        $this->teremstr = $teremstr;

        return $this;
    }

    /**
     * Get teremstr
     *
     * @return string
     */
    public function getTeremstr()
    {
        return $this->teremstr;
    }

    /**
     * Get terempreferenciaid
     *
     * @return integer
     */
    public function getTerempreferenciaid()
    {
        return $this->terempreferenciaid;
    }

    /**
     * Set targyid
     *
     * @param \AppBundle\Entity\Targy $targyid
     *
     * @return Terempreferencia
     */
    public function setTargyid(\AppBundle\Entity\Targy $targyid = null)
    {
        $this->targyid = $targyid;

        return $this;
    }

    /**
     * Get targyid
     *
     * @return \AppBundle\Entity\Targy
     */
    public function getTargyid()
    {
        return $this->targyid;
    }

    /**
     * Set tanarid
     *
     * @param \AppBundle\Entity\Tanar $tanarid
     *
     * @return Terempreferencia
     */
    public function setTanarid(\AppBundle\Entity\Tanar $tanarid = null)
    {
        $this->tanarid = $tanarid;

        return $this;
    }

    /**
     * Get tanarid
     *
     * @return \AppBundle\Entity\Tanar
     */
    public function getTanarid()
    {
        return $this->tanarid;
    }
}
