<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Szovegesertekeles
 *
 * @ORM\Table(name="szovegesErtekeles", uniqueConstraints={@ORM\UniqueConstraint(name="sze_UKindex1", columns={"diakId", "targyId", "tanev", "szemeszter"})}, indexes={@ORM\Index(name="sze_FKindex1", columns={"diakId"}), @ORM\Index(name="sze_FKindex2", columns={"szrId"}), @ORM\Index(name="sze_FKindex3", columns={"targyId"}), @ORM\Index(name="sze_FKIndex4", columns={"tanev", "szemeszter"})})
 * @ORM\Entity
 */
class Szovegesertekeles
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt", type="date", nullable=false)
     */
    private $dt;

    /**
     * @var integer
     *
     * @ORM\Column(name="szeId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $szeid;

    /**
     * @var \AppBundle\Entity\Szemeszter
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Szemeszter")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tanev", referencedColumnName="tanev"),
     *   @ORM\JoinColumn(name="szemeszter", referencedColumnName="szemeszter")
     * })
     */
    private $tanev;

    /**
     * @var \AppBundle\Entity\Diak
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Diak")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="diakId", referencedColumnName="diakId")
     * })
     */
    private $diakid;

    /**
     * @var \AppBundle\Entity\Szempontrendszer
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Szempontrendszer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="szrId", referencedColumnName="szrId")
     * })
     */
    private $szrid;

    /**
     * @var \AppBundle\Entity\Targy
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Targy")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="targyId", referencedColumnName="targyId")
     * })
     */
    private $targyid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Szrminosites", inversedBy="szeid")
     * @ORM\JoinTable(name="szeminosites",
     *   joinColumns={
     *     @ORM\JoinColumn(name="szeId", referencedColumnName="szeId")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="minositesId", referencedColumnName="minositesId")
     *   }
     * )
     */
    private $minositesid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Szrszempont", inversedBy="szeid")
     * @ORM\JoinTable(name="szeegyediminosites",
     *   joinColumns={
     *     @ORM\JoinColumn(name="szeId", referencedColumnName="szeId")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="szempontId", referencedColumnName="szempontId")
     *   }
     * )
     */
    private $szempontid;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->minositesid = new \Doctrine\Common\Collections\ArrayCollection();
        $this->szempontid = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set dt
     *
     * @param \DateTime $dt
     *
     * @return Szovegesertekeles
     */
    public function setDt($dt)
    {
        $this->dt = $dt;

        return $this;
    }

    /**
     * Get dt
     *
     * @return \DateTime
     */
    public function getDt()
    {
        return $this->dt;
    }

    /**
     * Get szeid
     *
     * @return integer
     */
    public function getSzeid()
    {
        return $this->szeid;
    }

    /**
     * Set tanev
     *
     * @param \AppBundle\Entity\Szemeszter $tanev
     *
     * @return Szovegesertekeles
     */
    public function setTanev(\AppBundle\Entity\Szemeszter $tanev = null)
    {
        $this->tanev = $tanev;

        return $this;
    }

    /**
     * Get tanev
     *
     * @return \AppBundle\Entity\Szemeszter
     */
    public function getTanev()
    {
        return $this->tanev;
    }

    /**
     * Set diakid
     *
     * @param \AppBundle\Entity\Diak $diakid
     *
     * @return Szovegesertekeles
     */
    public function setDiakid(\AppBundle\Entity\Diak $diakid = null)
    {
        $this->diakid = $diakid;

        return $this;
    }

    /**
     * Get diakid
     *
     * @return \AppBundle\Entity\Diak
     */
    public function getDiakid()
    {
        return $this->diakid;
    }

    /**
     * Set szrid
     *
     * @param \AppBundle\Entity\Szempontrendszer $szrid
     *
     * @return Szovegesertekeles
     */
    public function setSzrid(\AppBundle\Entity\Szempontrendszer $szrid = null)
    {
        $this->szrid = $szrid;

        return $this;
    }

    /**
     * Get szrid
     *
     * @return \AppBundle\Entity\Szempontrendszer
     */
    public function getSzrid()
    {
        return $this->szrid;
    }

    /**
     * Set targyid
     *
     * @param \AppBundle\Entity\Targy $targyid
     *
     * @return Szovegesertekeles
     */
    public function setTargyid(\AppBundle\Entity\Targy $targyid = null)
    {
        $this->targyid = $targyid;

        return $this;
    }

    /**
     * Get targyid
     *
     * @return \AppBundle\Entity\Targy
     */
    public function getTargyid()
    {
        return $this->targyid;
    }

    /**
     * Add minositesid
     *
     * @param \AppBundle\Entity\Szrminosites $minositesid
     *
     * @return Szovegesertekeles
     */
    public function addMinositesid(\AppBundle\Entity\Szrminosites $minositesid)
    {
        $this->minositesid[] = $minositesid;

        return $this;
    }

    /**
     * Remove minositesid
     *
     * @param \AppBundle\Entity\Szrminosites $minositesid
     */
    public function removeMinositesid(\AppBundle\Entity\Szrminosites $minositesid)
    {
        $this->minositesid->removeElement($minositesid);
    }

    /**
     * Get minositesid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMinositesid()
    {
        return $this->minositesid;
    }

    /**
     * Add szempontid
     *
     * @param \AppBundle\Entity\Szrszempont $szempontid
     *
     * @return Szovegesertekeles
     */
    public function addSzempontid(\AppBundle\Entity\Szrszempont $szempontid)
    {
        $this->szempontid[] = $szempontid;

        return $this;
    }

    /**
     * Remove szempontid
     *
     * @param \AppBundle\Entity\Szrszempont $szempontid
     */
    public function removeSzempontid(\AppBundle\Entity\Szrszempont $szempontid)
    {
        $this->szempontid->removeElement($szempontid);
    }

    /**
     * Get szempontid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSzempontid()
    {
        return $this->szempontid;
    }
}
